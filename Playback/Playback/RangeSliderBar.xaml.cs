﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for RangeSliderBar.xaml
    /// </summary>
    public partial class RangeSliderBar :Canvas
    {
        /// <summary>
        /// When the value of the slider is changed
        /// </summary>
        public event EventHandler OnSliderValueChanged;
        /// <summary>
        /// When the start thumb is dragged
        /// </summary>
        public event EventHandler OnDragStartThumb;
        /// <summary>
        /// When end thumb is dragged
        /// </summary>
        public event EventHandler OnDragEndThumb;


        private long startPosition;
        private long endPosition;

        public long  StartPosition
        {
            get { return startPosition; }
            set { startPosition = value; }
        }
        
        public long EndPosition
        {
            get { return endPosition; }
            set { endPosition = value; }
        }

        public double Maximum
        {
            get { return slider1.Maximum; }
            set { slider1.Maximum = value; ComputeThumbPositions(); }
        }

        public double Minimum
        {
            get { return slider1.Minimum; }
            set { slider1.Minimum = value; ComputeThumbPositions(); }
        }

        /// <summary>
        /// Current Value  of the slider
        /// </summary>
        public double Value
        {
            get { return slider1.Value; }
            set { slider1.Value = value; }
        }


        public bool isEnabled
        {
            get { return slider1.IsEnabled; }
            set { slider1.IsEnabled = value; thumbLeft.IsEnabled = value; thumbRight.IsEnabled = value; }
        }

         
        private double leftThumpPositionDefault = 0;
        private double rightThumpPositionDefault = 2;
        
        public RangeSliderBar()
        {
           InitializeComponent();
           ComputeThumbPositions();
           leftThumpPositionDefault = Canvas.GetLeft(thumbLeft);
           rightThumpPositionDefault = Canvas.GetLeft(thumbRight);

        }

        public void ResetPositions()
        {
            Canvas.SetLeft(thumbLeft, leftThumpPositionDefault);
            Canvas.SetLeft(thumbRight, rightThumpPositionDefault);
            slider1.Value = 0;

        }

        /// <summary>
        /// Compute the position of thumbs relative to the slider1.Minium and slider1.Maximum
        /// </summary>
        private void ComputeThumbPositions()
        {
            double left = Canvas.GetLeft(thumbLeft);
            double right = Canvas.GetLeft(thumbRight);
            startPosition = (long)(left * (slider1.Maximum - slider1.Minimum) / (this.Width - 0));
            endPosition = (long)(right * (slider1.Maximum - slider1.Minimum) / (this.Width - 0));
        }


      

        private void slider1_ValueChanged(object sender,  EventArgs  e)
        {
               if (OnSliderValueChanged != null)
                   OnSliderValueChanged(this, e);
      
            
        }


        private void thumbLeft_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            double left = Canvas.GetLeft(thumbLeft);
            double right = Canvas.GetLeft(thumbRight);
            double rawPositionLeftThumb = left + e.HorizontalChange;

            if (rawPositionLeftThumb < right - 4 && left + rawPositionLeftThumb > 0)
            {   
                Canvas.SetLeft(thumbLeft, left + e.HorizontalChange);
                startPosition = (long)(rawPositionLeftThumb * (slider1.Maximum - slider1.Minimum) / (this.Width - 0));
                  
                                
                if (OnDragStartThumb != null)
                    OnDragStartThumb(this, e);
            }

        }

        /// <summary>
        /// This function is called when right  thumb is dragged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void thumbRight_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            double left = Canvas.GetLeft(thumbLeft);
            double right = Canvas.GetLeft(thumbRight);
            double rawPositionRightThumb = right + e.HorizontalChange;

            if (rawPositionRightThumb > left + 4 && rawPositionRightThumb  < this.Width) 
            {
                
                Canvas.SetLeft(thumbRight, right + e.HorizontalChange);
                endPosition = (long)(rawPositionRightThumb * (slider1.Maximum - slider1.Minimum) / (this.Width - 0));

                if (OnDragEndThumb != null)
                    OnDragEndThumb(this, e);  
            }
        }

        private List<int> frameLabelIndices = new List<int>();
         
        public void drawLabelsForFrames(List<int> frames, Color aColor,bool clear)
        {

            if (frameLabelIndices.Count > 0 && clear)
            {
                this.Children.RemoveRange(frameLabelIndices.ElementAt(0), frameLabelIndices.Count);
                frameLabelIndices.Clear();
            }
            
            foreach (int f in frames)
            {
                Ellipse el = new Ellipse();
                el.Width = 3;
                el.Height = 10;
                el.Fill = new SolidColorBrush(aColor);
                el.Tag = "" + f;
                

                double rawPosition=(this.Width *f)/(slider1.Maximum - slider1.Minimum) +4; // offset value of 6 is determined  from observation
                Canvas.SetLeft(el, rawPosition);
                Canvas.SetTop(el, 0);

               int aIndex= this.Children.Add(el);
               this.frameLabelIndices.Add(aIndex);
            }
        }

    }
}
