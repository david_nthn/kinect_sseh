﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WpfApplication1
{
    /// <summary>
    /// The angle data for one joint 
    /// Used to display angles in a table
    /// </summary>
    public class AngleData
    {
        public string JointName { get; set; }
        public string AngleValueX { get; set; }
        public string MaxX { get; set; }
        public string MinX { get; set; }

        public string AngleValueY { get; set; }
        public string MaxY { get; set; }
        public string MinY { get; set; }

        public string AngleValueZ { get; set; }
        public string MaxZ { get; set; }
        public string MinZ { get; set; }


    }

}
