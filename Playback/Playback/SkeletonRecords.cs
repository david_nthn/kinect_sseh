﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Shapes;
using System.Windows.Media;


namespace WpfApplication1
{
    class SkeletonRecords
    {
        List<SkeletonRecord> records;//=new List<SkeletonRecord>();

        /// <summary>
        /// Number of bytes taken by one skeleton record
        /// (20 *3 *8) for skeletons,
        /// (20 *3 *8) for orientatons,
        /// 8 for timestamp 
        /// 20 for validity
        /// Total=508 +480
        /// </summary>
        int recsize = 508 + 480;

        Polyline aline=new Polyline();


        public SkeletonRecords()
        {
            records = new List<SkeletonRecord>();
        }


        public void AddRecord(SkeletonRecord aRecord)
        {
            this.records.Add(aRecord);
        }

        public void Clear()
        {
            this.records.Clear();
        }

        public SkeletonRecord GetRecord(int index)
        {
            return this.records[index];
        }
        /// <summary>
        /// Clears existing records and loads  new records from the given file
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="version"></param>
        public void LoadFromFile(string fileName,int version)
        {
            this.Clear();
            double  tim;
            this.ApplyVersion(version);

            double[] orientation_holder = new double[60];
            double[] joints = new double[60];
            char[] validity=new char[20];

            using (BinaryReader b = new BinaryReader(File.Open(fileName, FileMode.Open)))
            {
                long maxlim = b.BaseStream.Length / recsize;
                for (long frame = 0; frame < maxlim; frame++)
                {
                    b.BaseStream.Seek(frame * (long)recsize, SeekOrigin.Begin);
                    for (int j = 0; j < 20; j++)
                    {
                        joints[j * 3] = b.ReadDouble();
                        joints[j * 3 + 1] = b.ReadDouble();
                        joints[j * 3 + 2] = b.ReadDouble();
                        validity[j] = b.ReadChar();
                    }
                    //read orietations chunk
                    if (version > 0)
                        for (int j = 0; j < 60; j++)
                        {
                            orientation_holder[j] = b.ReadDouble();
                        }


                    tim = b.ReadDouble();

                    AddRecord(new SkeletonRecord(orientation_holder, joints, validity,tim));
                }
            }
        }


        /// <summary>
        /// Saves records to  the data file
        /// </summary>
        /// <param name="fullFilePath"> The path of the file to be saved to</param>
        public void SaveToFile(string fullFilePath)
        {


            double[] joints;
            double[] orientations;
            char[] validity;
            double timeStamp;
            using (BinaryWriter bWriter = new BinaryWriter(File.Open(fullFilePath, FileMode.Create)))
            {
                for (int frame = 0; frame < this.records.Count; frame++)
                {
                    joints = this.records[frame].GetJointLocations();
                    orientations = this.records[frame].GetOrientations();
                    validity = this.records[frame].GetValidity();
                    timeStamp = this.records[frame].GetTimeStamp();
                    //write joint locations
                    for (int i = 0; i < 20; i++)
                    {
                        bWriter.Write(joints[i * 3]);
                        bWriter.Write(joints[i * 3 + 1]);
                        bWriter.Write(joints[i * 3 + 2]);
                        bWriter.Write(validity[i]);
                    }
                    //write orientations
                    for (int i = 0; i < 60; i++)
                    {
                        bWriter.Write(orientations[i]);

                    }

                    //write time stamps
                    bWriter.Write(timeStamp);

                }
            }

        }
         

        public long GetCount()
        {
            return this.records.Count;
        }

        /// <summary>
        //Changes record size in according to version
        /// </summary>
        private void ApplyVersion(int version)
        {
            
            if (version < 1)
            {
                this.recsize = 508;
            }
            else
            {
                this.recsize = 508 + 480;
            }
        }

        public void   MaxMinOrientationIK(int index,out double maxV, out double minV)
        {
         maxV  =  this.records.Max(t => t.GetJointAnglesIK().GetAngle(index));
         minV=this.records.Min(t => t.GetJointAnglesIK().GetAngle(index));
            
          
        //to calculate the  
        //records.GetRange()
//            records.GetRange

         //CarList.Sort((x, y) => DateTime.Compare(x.CreationDate, y.CreationDate));
        }

        public void MaxMinOrientationIK(int index, out double maxV, out double minV,int start,int end)
        {
            List<SkeletonRecord> shallowCopy = this.records.GetRange(start, end - start);

            maxV = shallowCopy.Max(t => t.GetJointAnglesIK().GetAngle(index));
            minV = shallowCopy.Min(t => t.GetJointAnglesIK().GetAngle(index));

        }

        
        /// <summary>
        /// Returns the maximum and minimum values of the angles computed using inverse kinematics for the given range in aData.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public void  MaxMinOrientationIKNew(int index,   int start, int end,AngleData aData)
        {
            if (end > this.records.Count())
                return;

            List<SkeletonRecord> shallowCopy = this.records.GetRange(start, end - start);

             
            
 
           double maXX=  shallowCopy.Max(t => t.GetJointAnglesIK().GetAngleNew(index,0));
           double minX = shallowCopy.Min(t => t.GetJointAnglesIK().GetAngleNew(index,0));

           double maXY = shallowCopy.Max(t => t.GetJointAnglesIK().GetAngleNew(index, 1));
           double minY = shallowCopy.Min(t => t.GetJointAnglesIK().GetAngleNew(index, 1));

           aData.MaxX = string.Format("{0:f2}",maXX);
           aData.MinX = string.Format("{0:f2}",minX);
           aData.MaxY = string.Format("{0:f2}",maXY);
           aData.MinY = string.Format("{0:f2}",minY);

          
            
        }



        public void saveASCII_JointAnglesKinect(string fullFilePath)
        {
            using (StreamWriter sWriter = new StreamWriter(File.Open(fullFilePath, FileMode.Create)))
            {
                for (int i = 0; i < this.records.Count(); i++)
                {
                    double[] a = records[i].GetOrientations();
                    for (int j = 0; j < 20; j++)
                        sWriter.Write(a[j] + " ");
                    sWriter.Write(Environment.NewLine);

                }
            }
        }

        /// <summary>
        /// Saves joint angles computed from inverse kinematics
        /// </summary>
        /// <param name="fullFilePath"></param>

        public void saveASCII_JointAnglesIK(string fullFilePath)
        {
            using (StreamWriter sWriter = new StreamWriter(File.Open(fullFilePath, FileMode.Create)))
            {
                for (int i = 0; i < this.records.Count(); i++)
                {
                    JointAnglesIK a = records[i].GetJointAnglesIK();
                    for (int j = 0; j < a.NumberOfAngles; j++)
                        sWriter.Write(a.GetAngle(j) + " ");
                    sWriter.Write(Environment.NewLine);

                }
            }
        }

        /// <summary>
        /// Saves joint angles computed from new inverse kinematics
        /// </summary>
        /// <param name="fullFilePath"></param>

        public void saveASCII_JointAnglesIKNew(string fullFilePath)
        {
            using (StreamWriter sWriter = new StreamWriter(File.Open(fullFilePath, FileMode.Create)))
            {
                for (int i = 0; i < this.records.Count(); i++)
                {
                    JointAnglesIK a = records[i].GetJointAnglesIK();
                    for (int j = 0; j < 8; j++)
                    {
                        for (int col = 0; col < 2; col++)
                        {
                            sWriter.Write(a.GetAngleNew(j,col) + " ");
                        }
                    }
                    sWriter.Write(Environment.NewLine);

                }
            }
        }

    }
    /// <summary>
    /// A single record of skeleton saved in a file
    /// </summary>
    class SkeletonRecord
    {
        /// <summary>
        /// Orientations extracted from Kinect API
        /// </summary>
        double[] orientations = new double[60]; //from kinect1
        /// <summary>
        /// Joint locations from kinect API
        /// </summary>
        double[] joint_locations=new double[60];
        /// <summary>
        /// Validity indicator of each joint data
        /// </summary>
        private char[] Validity_ = new char[20];

        /// <summary>
        /// Orientations computed using inverse kinematics from skeleton joint locations
        /// </summary>
        private JointAnglesIK jointAnglesIK;
         

        double timestamp_;
        public SkeletonRecord(double[] orient ,double[] joints, char[] validity,double timestamp)
        {
            if (orient != null)
            {
                for (int i = 0; i < 60; i++)
                {
                    this.orientations [i] = orient[i];
                }
            }

            if (joints != null)
            {
                for (int i = 0; i < 60; i++)
                {
                    this.joint_locations[i] = joints[i];
                }
            }

             
                for (int i = 0; i < 20; i++)
                {
                    this.Validity_[i] = validity[i];
                }
             
            this.timestamp_ = timestamp;

            //compute angle using inverse kinematic
            if (this.joint_locations != null)
            {
                this.jointAnglesIK = new JointAnglesIK(this.GetJointLocationsMatrix());
            }



        }




        public double[] GetJointLocations()
        {
            return this.joint_locations;
        }

        /// <summary>
        /// Returns joint locations as 20 x 3 matrix
        /// </summary>
        /// <returns></returns>
        public double[,] GetJointLocationsMatrix()
        {
            double[,] ret = new double[20,3];
            for (int i = 0; i < 20; i++)
            {
                ret[i,0]=joint_locations[i*3];
                ret[i, 1] = joint_locations[i * 3+1];
                ret[i, 2] = joint_locations[i * 3 + 2];

            }
            return ret;
        }

        public double[] GetOrientations()
        {
            return this.orientations;
        }

      

        public double GetOrientationValue(int index)
        {
            return this.orientations[index];
        }

        public double GetTimeStamp()
        {
            return this.timestamp_;
        }

        public char[] GetValidity()
        {
            return this.Validity_;
        }


        public JointAnglesIK GetJointAnglesIK()
        {
            return this.jointAnglesIK;
        }

  

         

    }
}

