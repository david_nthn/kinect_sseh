﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.IO;

namespace WpfApplication1
{
    class ActivityDetector
    {
        string mainstring;
        string projectDir;
        int recsize;
        long[] ans = new long[100];
        int answers = 0;

       
        public int[] mcnt;
        int beginning = 0;
        int epoch = 5; // 5 second epochs 
        BinaryReader bFile;
        
        public string res = "";

        MainWindow mainWin;




        public ActivityDetector( string project_dir,string mainString, int recSize,MainWindow main_window)
        {
            this.projectDir = project_dir;
            this.mainstring = mainString;
            this.recsize = recSize;
            this.mainWin = main_window;
        }


        public string    DetectActivities(int epoch )
        {   /* evaluate chosen recording */
            int rule = 0;
            res = "";
            bFile = new BinaryReader(File.Open(mainstring, FileMode.Open));
           long  maxlim = bFile.BaseStream.Length / recsize;
           string header = "Date,Time";

           

            double d = GetTime(0);
            DateTime dt = new DateTime((long)d);
            int time = rawtime(dt.ToLongTimeString());
            time = (int)Math.Floor((decimal)time / epoch) * epoch;
            beginning = time;
            d = GetTime(maxlim - 1);
            DateTime dt2 = new DateTime((long)d);
            int time2 = rawtime(dt2.ToLongTimeString());
            time2 = (int)Math.Floor((decimal)time2 / epoch) * epoch;

            bFile.Close();

            long nume = (time2 - time) / epoch;
            string[] opd = new string[++nume];
            mcnt = new int[nume];
            for (int ji = 0; ji < nume; ji++)
            {
                opd[ji] = dt.ToShortDateString() + "," + stringtime(time);
                time += epoch;
            }
             

            //rules are same for all sessions
            using (StreamReader b = new StreamReader(@"C:\\kinectdata\rules\rules.txt"))
            {
                string s;
                while ((s = b.ReadLine()) != null)
                {
                    res += "Looking for " + s + "..." + Environment.NewLine;
                    for (int ji = 0; ji < nume; ji++) mcnt[ji] = 0;
                    LookFor(rule++);
                                         
                    header += "," + s;
                    for (int ji = 0; ji < nume; ji++) opd[ji] += "," + mcnt[ji].ToString();
                }
            }


            res += "---------------------------------------------------------" + Environment.NewLine;
            string res2 = header + Environment.NewLine;
            for (int ji = 0; ji < nume; ji++)
                res2 += opd[ji] + Environment.NewLine;
            

            StreamWriter t = new StreamWriter(File.Open(GetFullFile("output.csv"), FileMode.Create));
            t.Write(res2);
            t.Close();
            //textBox4.Text = res + res2;
            return res + res2;

        }


        public void LookFor(int rule)
        {
            string rulestring = "";

            //rules are same for all sessions
            using (StreamReader b = new StreamReader(@"C:\\kinectdata\rules\rule" + rule.ToString() + ".txt"))
            {
                string s;
                while ((s = b.ReadLine()) != null)
                {
                    if (rulestring.Length == 0) rulestring = s;
                    else
                    {
                        string[] prt = s.Split('~');
                        if (prt[0] == "0") rulestring += '&' + s;
                        else if (prt[0] == "1") rulestring += '|' + s;
                        else rulestring += '>' + s;
                    }
                }
            }

            bFile = new BinaryReader(File.Open(mainstring, FileMode.Open));
            long maxlim = bFile.BaseStream.Length / recsize;
            for (long current = 0; current < maxlim; current++)
            {
                answers = 0;
                if (ParseRule(rulestring, current, maxlim) == true)
                {
                    res += "Found : from " + current.ToString() + " to " + ans[answers - 1].ToString() + Environment.NewLine;
                    DateTime found = new DateTime((long)GetTime(current));
                    int i = rawtime(found.ToLongTimeString());
                    i = (int)Math.Floor((decimal)(i - beginning) / epoch);
                    mcnt[i]++;
                    current = ans[answers - 1];

                }
            }

            bFile.Close();
        }


        private bool ParseRule(string r, long start, long finish)
        {
           

            int a = r.IndexOf('>');
            //            res += "[start:" + start.ToString() + "][fin:" + finish.ToString() + "][" + r + "]" + Environment.NewLine;
            if (a > 0)
            {   // process THEN operator
                if (ParseRule(r.Substring(0, a), start, finish))
                {
                    long l = ans[answers - 1];
                    for (long ll = l; (ll < l + 2) && (ll < finish); ll++)
                        if (ParseRule(r.Substring(a + 1), ll, finish)) return true;
                }
            }
            else
            {  // process AND operator
                a = r.IndexOf('&');
                if (a > 0)
                {
                    if (ParseRule(r.Substring(0, a), start, finish))
                    {
                        long asave = ans[answers - 1];
                        bool b = ParseRule(r.Substring(a + 1), start, (long)Math.Min(finish, ans[answers - 1] + 3));
                        if (b == true)
                            ans[answers - 1] = Math.Max(ans[answers - 1], asave);
                        return b;
                    }
                    else
                        return false;
                }
                else
                    return this.mainWin.Possible(r, (int)start, (int)finish);
            } 
            return false;
        }//end ParseRule



        private int rawtime(string s)
        {
            string[] pt = s.Split(':');
            int ampm = 0;
            int h = Int32.Parse(pt[0]);
            if (h == 12) h = 0;
            if (pt[2].IndexOf("PM") > 0) ampm = 43200;
            pt[2] = pt[2].Substring(0, 2);
            return ampm + Int32.Parse(pt[2]) + 60 * Int32.Parse(pt[1]) + 3600 * h;
        }

        private double GetTime(long frame)
        {
            //bFile.BaseStream.Seek(frame * recsize + 500, SeekOrigin.Begin);

            //by this way we done need to chage this function whe recSize is changed provided that timestamp is saved at the end of the record
            bFile.BaseStream.Seek(frame * recsize + recsize - 8, SeekOrigin.Begin);
            return bFile.ReadDouble();
        }


        private string stringtime(int i)
        {
            string t = "";
            if (i > 43200)
            {
                t = " PM";
                i -= 43200;
            }
            else
                t = " AM";
            int h = (int)Math.Floor((decimal)i / 3600);
            i -= h * 3600;
            if (h == 0) h = 12;
            int m = (int)Math.Floor((decimal)i / 60);
            i -= m * 60;
            t = h.ToString() + ":" + m.ToString().PadLeft(2, '0') + ':' + i.ToString().PadLeft(2, '0') + t;
            return t;

        }


        private string GetFullFile(string aFile)
        {

            return System.IO.Path.Combine(this.projectDir, aFile);

        }

         




    }
}
