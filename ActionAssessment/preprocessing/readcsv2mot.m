function [move_type] = readcsv2mot(dirname)
%UNTITLED Summary of this function goes here
%  Reads in the processed data and creates jump struct with same format
%  Jumps= readcsv2mot('.\data\OctoberHolidays2013\Jump\Jump_csv\');

contents = dir(dirname);
move_type = struct;

% First two values are subdirs
for i=3:length(contents)
    A = csvread(strcat(dirname,contents(i,1).name));
    name = contents(i,1).name;
    name = name(1:end-4);
    move_type.(name) = [];
    move_type.(name) = A;
end

