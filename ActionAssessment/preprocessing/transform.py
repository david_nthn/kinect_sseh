import os
import csv
import sys
import math
import shutil

#==============
#  CONSTANTS
#==============
DATA_FOLDER = sys.argv[1]
PROCESSED_FOLDER = sys.argv[2]

JOINT_ORDER = {"HipCenter":0,"Spine":1,"ShoulderCenter":2,"Head":3,"ShoulderLeft":4,"ElbowLeft":5,"WristLeft":6,"HandLeft":7,"ShoulderRight":8,"ElbowRight":9,"WristRight":10,"HandRight":11,"HipLeft":12,"KneeLeft":13,"AnkleLeft":14,"FootLeft":15,"HipRight":16,"KneeRight":17,"AnkleRight":18,"FootRight":19}
SPACER = '=================================='

DATA_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '\\' + DATA_FOLDER + '\\'
PROCESSED_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '\\' + PROCESSED_FOLDER + '\\'

#====================
#  GENERAL FUNCTIONS
#====================

def getPoint(timeStep, jointName, axis):
	jointIndex = JOINT_ORDER[jointName]
	axisNo = 0
	if axis == 'x':
		axisNo = 0
	elif axis == 'y':
		axisNo = 1
	elif axis == 'z':
		axisNo = 2
	else:
		print('ERROR: Axis given as ' + axis)
		sys.exit()
	return float(timeStep[3*jointIndex + axisNo])
	
def setPoint(timeStep, jointName, axis, val):
	jointIndex = JOINT_ORDER[jointName]
	axisNo = 0
	if axis == 'x':
		axisNo = 0
	elif axis == 'y':
		axisNo = 1
	elif axis == 'z':
		axisNo = 2
	else:
		print('ERROR: Axis given as ' + axis)
		sys.exit()
	timeStep[3*jointIndex + axisNo] = val

def distanceBetween(timeStep, jointName1, jointName2):
	x1 = getPoint(timeStep, jointName1, 'x')
	y1 = getPoint(timeStep, jointName1, 'y')
	z1 = getPoint(timeStep, jointName1, 'z')
	
	x2 = getPoint(timeStep, jointName2, 'x')
	y2 = getPoint(timeStep, jointName2, 'y')
	z2 = getPoint(timeStep, jointName2, 'z')
	
	return math.sqrt( (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) + (z1-z2)*(z1-z2) )
	
#========================
# MAIN CODE
#========================

# prepare export folder
if os.path.exists(PROCESSED_PATH): 
	shutil.rmtree(PROCESSED_PATH)
os.mkdir(PROCESSED_PATH)
os.mkdir(PROCESSED_PATH + 'raw')
os.mkdir(PROCESSED_PATH + 'transformed')
os.mkdir(PROCESSED_PATH + 'transformed_scaled')

# main sequence
print('Loading CSV files from ' + DATA_PATH)
print(SPACER)

# load file by file
for file in os.listdir(DATA_PATH):
	print('Processing Jump file: ' + file)
	with open(DATA_PATH + file,'r') as csvfile:
		# calculate scaling factor
		print('\tCalculating scaling factor...')
		csvReader = csv.reader(csvfile)
		distList = []
		for rowNo,row in enumerate(csvReader):
			dist = 0
			dist += distanceBetween(row, 'HandLeft', 'WristLeft')
			dist += distanceBetween(row, 'WristLeft', 'ElbowLeft')
			dist += distanceBetween(row, 'ElbowLeft', 'ShoulderLeft')
			dist += distanceBetween(row, 'ShoulderLeft', 'ShoulderCenter')
			dist += distanceBetween(row, 'ShoulderCenter', 'Spine')
			dist += distanceBetween(row, 'Spine', 'HipCenter')
			distList.append(dist)
		scalingFactor = sum(distList)/len(distList)
		print('\t\tScaling Factor is: ' + str(scalingFactor))
	
	with open(DATA_PATH + file,'r') as csvfile:
		# Change of coordinates to make hip center the origin
		print('\tChanging origin to HipCenter...')
		csvReader = csv.reader(csvfile)
		data = []
		for rowNo,row in enumerate(csvReader):
			newRow = list(row)
			xHc = getPoint(row, 'HipCenter', 'x')
			yHc = getPoint(row, 'HipCenter', 'y')
			zHc = getPoint(row, 'HipCenter', 'z')
			for jointName in JOINT_ORDER:
				x = getPoint(row, jointName, 'x')
				y = getPoint(row, jointName, 'y')
				z = getPoint(row, jointName, 'z')
				setPoint(newRow, jointName, 'x', x-xHc)
				setPoint(newRow, jointName, 'y', y-yHc)
				setPoint(newRow, jointName, 'z', z-zHc)
			data.append(newRow)
		print('\t\tDone.')
	
	# Rotate vectors so that Rhip->Lhip // x-axis
	print('\tRotating to make x-axis parallel to HipRight->HipLeft vector...')
	for i in range(len(data)):
		# calculate the rotation angle
		xLeft = getPoint(data[i], 'HipLeft', 'x')
		zLeft = getPoint(data[i], 'HipLeft', 'z')
		xRight = getPoint(data[i], 'HipRight', 'x')
		zRight = getPoint(data[i], 'HipRight', 'z')
		rot = math.atan((zLeft - zRight) / (xRight - xLeft))
		
		# check that rotation leaves HipLeft in +ve x direction
		xLeftCheck = xLeft * math.cos(rot) - zLeft * math.sin(rot);
		xRightCheck =  xRight * math.cos(rot) - zRight * math.sin(rot);
		if xLeftCheck < xRightCheck:
			rot += math.pi
		
		# rotate x and z by rot
		for jointName in JOINT_ORDER:
			x = getPoint(data[i], jointName, 'x')
			y = getPoint(data[i], jointName, 'y')
			z = getPoint(data[i], jointName, 'z')
			
			xRotated = x * math.cos(rot) - z * math.sin(rot)
			yRotated = y
			zRotated = x * math.sin(rot) + z * math.cos(rot)
			
			setPoint(data[i], jointName, 'x', xRotated)
			setPoint(data[i], jointName, 'y', yRotated)
			setPoint(data[i], jointName, 'z', zRotated)
	
	# check rot
	for row in data:
		xLeft = getPoint(data[i], 'HipLeft', 'x')
		zLeft = getPoint(data[i], 'HipLeft', 'z')
		xRight = getPoint(data[i], 'HipRight', 'x')
		zRight = getPoint(data[i], 'HipRight', 'z')
		xRightToLeft = xLeft - xRight
		zRightToLeft = zLeft - zRight
		if zRightToLeft > 0.00000000001:
			print('Did not rotate correctly. Vector z component: ' + zRightToLeft)
			sys.exit()
		if xRightToLeft < 0:
			print('X-axis orientation incorrect')
			sys.exit()
	print('\t\tDone.')
	
	# export transformed data
	print('Exporting to ' + PROCESSED_PATH + 'transformed\\' + file)	
	with open(PROCESSED_PATH + 'transformed\\' + file,'w',newline='') as newCsvFile:
		csvWriter = csv.writer(newCsvFile)	
		for i in range(len(data)):
			csvWriter.writerow(data[i])
	print('\t\tDone.')	
	
	# Scale with scaling factor
	print('\tScaling x,y,z by the scaling factor...')
	for i in range(len(data)):
		for j in range(len(data[i])):
			data[i][j] = data[i][j] / scalingFactor
	print('\t\tDone.')
	
	# export transformed and scaled data
	print('Exporting to ' + PROCESSED_PATH + 'transformed_scaled\\' + file)	
	with open(PROCESSED_PATH + 'transformed_scaled\\' + file,'w',newline='') as newCsvFile:
		csvWriter = csv.writer(newCsvFile)	
		for i in range(len(data)):
			csvWriter.writerow(data[i])
	print('\t\tDone.')

        # copy over raw data too
	shutil.copy(DATA_PATH + file, PROCESSED_PATH + 'raw')
	
	print(SPACER)
	
