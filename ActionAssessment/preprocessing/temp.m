clear all;

dirname = 'OctoberHolidays2013';
move_type = 'Jump';

rootpath = strcat('../data/',dirname,'/',move_type,'/');
featurepath = strcat(rootpath,move_type,'_extracted_features/');
filename = strcat(featurepath,'/','features_alr.csv');
csvpath = strcat(rootpath,move_type,'_csv/');

csvstruct = readcsv2mot(csvpath);
datapoints = fieldnames(csvstruct);
classlabels= importdata(strcat(rootpath,'labels.csv'));

for dp=1:length(datapoints)
    clear D V A a_r a_t
    D = csvstruct.(datapoints{dp});
    
    for k=1:3:60
        V(:,k:k+2) = diff(D(:,k:k+2));
        A(:,k:k+2) = diff(V(:,k:k+2));
    end
    
    D(1,:) = []; D(1,:) = [];
    V(1,:) = [];
    
    for i=1:size(A,1)
        for j=1:20
            a_r(i,j) = norm(A(i,3*(j-1)+1:3*(j-1)+2));
            a_t(i,j) = (V(i,3*(j-1)+1:3*(j-1)+2)*A(i,3*(j-1)+1:3*(j-1)+2)')./norm(V(i,3*(j-1)+1:3*(j-1)+2));
        end
    end
    
    ALR(dp,1:20) = sum(a_r.^2)./sum(a_t.^2);
    dp_info=cell2mat(cellfun(@(x) [str2num(x(end-2:end)) str2num(x(end-4))], datapoints(dp,1), 'UniformOutput', false));
    ALR(dp,21:22) = dp_info;
    ALR(dp,23) =  classlabels(find(classlabels(:,1) == dp_info(1)),dp_info(2)+1);
end

fid = fopen(filename, 'w');
fprintf(fid,'HipCenter,Spine,ShoulderCenter,Head,ShoulderLeft,ElbowLeft,WristLeft,HandLeft,ShoulderRight,ElbowRight,WristRight,HandRight,HipLeft,KneeLeft,AnkleLeft,FootLeft,HipRight,KneeRight,AnkleRight,FootRight,Person ID,Attempt ID,Rating\n');

nrows = size(ALR,1);
for row=1:nrows
    fprintf(fid, '%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%d,%d,%d\n',ALR(row,:));
end

fclose(fid);