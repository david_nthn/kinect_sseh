function [ A ] = read_frameseg(fname, start, stop)
%read_frameseg Alternate to readRecording that only reads specifed frame
%segment
% Will return joint trajectory matrix that is of the form for csvwrite
% i.e. segmentlength-by-60 dimension matrix
N = stop-start+1;
M = 60;
A = zeros(N,M);

framesize = 988; %size of a single frame in bytes
nonjointtraj_size = 488; %size of frame that is not jointTrajectories

fid = fopen(fname,'rb','n','UTF-8');

fseek(fid,start*framesize,'bof'); %move file pointer to beginning of segment
 
for i=1:stop-start+1
    % Reads 20 triplets of doubles with an offset of 1 bytes between those
    % reads.
   disp(['File: ' fname 'Frame: ' num2str(i)]);        
   A(i,:) = fread(fid,60,'3*double',1); 
   fseek(fid,nonjointtraj_size,'cof');
end

fclose(fid);
end

