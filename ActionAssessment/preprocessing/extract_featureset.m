function extract_featureset(dirname,move_type)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

rootpath = strcat('../data/',dirname,'/',move_type,'/');
featurepath = strcat(rootpath,move_type,'_extracted_features/');
subset = [];

features = importdata(strcat(featurepath,'features.csv'));
X = features.data(:,1:749);
Y = features.data(:,752);

[ranked,weights] = relieff(X,Y,10);

filename = strcat(featurepath,'/','features_subset.csv');
fid = fopen(filename, 'w');
str = features.textdata(1,ranked(1,1:20));
str(1,21:23) = features.textdata(1,750:752);
str = strjoin(str,',');
str = strcat(str,'\n');
fprintf(fid,str);

subset = [X(:,ranked(1,1:20)) features.data(:,750:752)];
[nrows,ncols]= size(subset);
str_num = arrayfun(@(x) num2str(x), subset, 'UniformOutput', false);

for row=1:nrows
    fprintf(fid, '%s\n', strjoin(str_num(row,:),','));
end

fclose(fid);

end

