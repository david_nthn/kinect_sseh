import sys
import os
import shutil
import csv
import re
import collections

#==============
#  CONSTANTS
#==============
ROOT_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
IN_PATH = 'C:\\kinectdata'
OUT_PATH = ROOT_PATH + '\\data\\' + sys.argv[1] # argv[1] = session ID = subfolder name

FMS_FILENAME = IN_PATH + '\\' + sys.argv[1] + '\\fms.csv'
DATA_FILENAME = 'dataSegments.csv'
LABELS_FILENAME = 'labels.csv'

DATA_PATH = OUT_PATH + '\\' + DATA_FILENAME
SPACER = '=================================='

#=================
#  MAIN FUNCTIONS
#=================

# read in FMS.csv and output 
def run():
    # save all data by movement for later separation by movement type. This map
    # will map from a movement type to a list of rows of data which can then be
    # separately written to labels.csv files for each type.
    fmsTypes = {}
    with open(FMS_FILENAME,'r') as input, \
            open(DATA_PATH,'w',newline='') as data:
            print("Reading in metadata from: " + FMS_FILENAME)
            csvReader = csv.reader(input)
            dataWriter = csv.writer(data) # for use with readRecording
            
            print("Writing in metadata to: " + OUT_PATH)
            # assuming 3 trials in headers for the moment
            dataWriter.writerow(['ID','Type','Path','#Trials','1s','1e','2s','2e','3s','3e','Row in ']) # prepend this once #trials is known?
            # labelWriter.writerow(['ID','Trial1','Trial2','Trial3']) # prepend this once #trials is known?
            
            id = ''
            path = ''
            for row in csvReader:
                # need this first as csvReader.next() call will get Description row
                if 'FMS' in row[0] and id is not '':
                    # extract from data rows, ignoring header
                    row = next(csvReader)
                    fms = row[0].upper() # movement type (e.g. 'Jump')
                    dataRow = [id, fms, path]
                    labelRow = [id]
                    tempData = []
                    numTrials = 0
                    while ('Description:' not in row):
                        numTrials += 1
                        tempData.extend([row[1],row[2]])
                        labelRow.extend([row[3]])
                        try:
                            row = next(csvReader)
                        except StopIteration:
                            break # end of file
                    dataRow.extend([numTrials])
                    dataRow.extend(tempData)
                    # write new row
                    dataRow.extend([int(path[-12:-4])])
                    dataWriter.writerow(dataRow)
                    #labelWriter.writerow(labelRow) -> shifted to separate labels.csv files

                    # append new labelRow to the list for this movement type
                    if fmsTypes.get(fms) is None:
                        fmsTypes[fms] = []
                    # get the subdir for this session - where FMS subfolders go
                    fmsTypes[fms].append(labelRow)
                
                # prepare next id, path
                if 'Description:' in row[0]:
                    regex = re.match(r"(\D)*0*(\d+)", row[1])
                    if regex is not None:
                        id = regex.group(2)
                        path = row[3]
                    
    # we have written to dataSegments.csv, but need separate labels.csv for each FMS
    #       => parse labels.csv into separate files
    print("Writing parsed data to FMS subfolders.")
    
    for fms in fmsTypes:
        print("Writing labels.csv for movement type: " + fms)
        fmsDir = OUT_PATH + '\\' + fms
        # prepare export folder
        if not os.path.exists(fmsDir):
            os.mkdir(fmsDir)
        fmsDir = fmsDir + '\\' + LABELS_FILENAME
        with open(fmsDir,'w',newline='') as labels:
            labelWriter = csv.writer(labels) # for use in extract_features
            for row in fmsTypes.get(fms):
                labelWriter.writerow(row)
    
    print('Done.')

#========================
# KICK-OFF MAIN FUNCTION
#========================

# prepare export folders
if not os.path.exists(OUT_PATH):
    os.makedirs(OUT_PATH)
    
#run
run()