function plot_generate(dirpath, model)
%plot_generate Generates Confusion Matrix and Heat Map
%   Detailed explanation goes here

% Add to list models which generate continuous prediction values
continuous_pred_list = {'svm','knn'};

A = importdata(strcat(dirpath,'\', model,'_results.csv'));

% First string in experiments will be 'Experiment Name'
experiments = unique(A.textdata(:,1),'stable');

for i=1:length(experiments)-1
    
    rows=find(strcmp(A.textdata(:,1),experiments{i+1,1}));
    rows = rows-1;
    disp(experiments);
    y = A.data(rows,3);
    y_star = A.data(rows,4);
    
    % If prediction model is continuous regression then round estimates.
    if(isempty(find(strcmp(continuous_pred_list,model)))==0)
        y_star = round(y_star);
    end
    
    res = y_star - y;
    rmse = sqrt(mean(res.^2));
    
    
    
    [C, order] = confusionmat(y,y_star);
    
    disp('Confusion Matrix');
    disp(C);
    
    C = C./ repmat(sum(C,2),1,length(C));
    C = C.*100;
    
    disp(C);
    imagesc(C); 
    h = colorbar;
    set(get(h,'ylabel'),'string','% of rating class in sample','Fontsize',14); 
    
    figname = strjoin({model,experiments{i+1,1},'RMSE=',num2str(rmse)});
    title(figname,'FontSize',16);
    str = strtrim(cellstr(num2str(order)));
    set(gca,'XTickLabel',str);
    set(gca,'YTickLabel',str)
    xlabel('Predicted','FontSize',14);
    ylabel('Actual','FontSize',14);
    figure(i);
    print('-djpeg',[dirpath,'/',model,experiments{i+1,1},'_fig','.jpg']);
    
    
    rocAUC = [];
    rating_class = unique(y);
    for c=1:length(rating_class)
        [X,Y,T,rocAUC(c)] = perfcurve(y,y_star,rating_class(c));
    end
    
    bar(rating_class,rocAUC);
    figname = strjoin({model,experiments{i+1,1},'ROC AUC per Class'});
    title(figname,'FontSize',16);
    xlabel('Rating','FontSize',14);
    ylabel('Area Under ROC Curve','FontSize',14);
    figure(i);
    print('-djpeg',[dirpath,'/',model,experiments{i+1,1},'_rocauc_fig','.jpg']);
        
end

end

