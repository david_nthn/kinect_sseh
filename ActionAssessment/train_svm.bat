@echo off

:: param passed in is the name of this fms, need to create folders accordingly
set fms=%1
:: directory this fms is in
set fmsdir=data\%2\%fms%
set procdir=%~dp0preprocessing
set expdir=%~dp0svm_experiment
set csv="%fmsdir%\%fms%_csv"
set processed="%fmsdir%\%fms%_csv_processed"
set extracted="%fmsdir%\%fms%_extracted_features"

::GOTO analysis

echo %fms% %csv% %processed% %extracted%
::set /p wait=Press Enter to transform the results and generate the features

echo Transforming %fms% results...
python "%procdir%\transform.py" %csv% %processed% >> %~dp0%fmsdir%\transform_out.txt

:: simplify labels before extracting features if this is desirable
::echo Simplifying labels to a limited scale...
::python misc_functions\simplify_labels.py "%fmsdir%\labels.csv"
:: COMMENT THE ABOVE TWO LINES OUT IF THE SCALE OF 1-10 IS TO BE USED RATHER THAN 1-3

echo Extracting features...
python "%expdir%\extract_features.py" %processed% "%fmsdir%\labels.csv" %extracted% ::>> %~dp0%fmsdir%\extract_features_out.txt

set extracted=%~dp0%fmsdir%\%fms%_extracted_features

:: Extract Feature Subset. 
cd /d "%~dp0\preprocessing\"
::echo Extracting feature subset
::matlab -nosplash -noFigureWindows -wait -r "extract_featureset('%2%','%fms%'); exit"

:: now train SVM using transformed_experiment.bat
cd /d "%~dp0\%fmsdir%"
echo Experiment Name,Person ID,Attempt ID,Mark,Estimated Mark > svm_results.csv

::set /p wait=Press Enter to train the SVM

echo Running experiment Untuned Raw %fms%...
java -Xms4096m -Xmx4096m -jar "%expdir%\experiment.jar" "Untuned Raw %fms%" "%extracted%\features.csv" "none" >> svm_results.csv
::java -Xms4096m -Xmx4096m -jar "%expdir%\experiment_pca.jar" "Untuned Raw %fms%" "%extracted%\features.csv" "none" >> svm_results_pca.csv

:: Uncomment the following 4 lines for tuned experiment
:: echo Running experiment RMSE-Tuned Raw %fms%...
:: java -Xms4096m -Xmx4096m -jar "%expdir%\experiment.jar" "RMSE-Tuned Raw %fms%" "%extracted%\features.csv" "rms" >> svm_results.csv

:: echo Running experiment Weighted RMSE-Tuned Raw %fms%...
:: java -Xms4096m -Xmx4096m -jar "%expdir%\experiment.jar" "Weighted RMSE-Tuned Raw %fms%" "%extracted%\features.csv" "wrms" >> svm_results.csv

:: this is where any output could be displayed based on the results, which are
:: stored in svm_results.csv

:: change back to analysis directory to generate plots.
cd /d "%~dp0\analysis"
matlab -nosplash -noFigureWindows -wait -r "plot_generate('..\%fmsdir%','svm'); exit"

set /p wait= Experiment Finished. Press Enter to exit.
exit
