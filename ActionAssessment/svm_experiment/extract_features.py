import sys
import os
import shutil
import csv
from math import pow
from math import sqrt
from collections import OrderedDict

#==============
#  CONSTANTS
#==============

PROCESSED_FOLDER = sys.argv[1]
LABELS_FILENAME = sys.argv[2]
FEATURE_FOLDER = sys.argv[3]

JOINT_ORDER = {"HipCenter":0,"Spine":1,"ShoulderCenter":2,"Head":3,"ShoulderLeft":4,"ElbowLeft":5,"WristLeft":6,"HandLeft":7,"ShoulderRight":8,"ElbowRight":9,"WristRight":10,"HandRight":11,"HipLeft":12,"KneeLeft":13,"AnkleLeft":14,"FootLeft":15,"HipRight":16,"KneeRight":17,"AnkleRight":18,"FootRight":19}
JOINT_LIST = sorted(list(JOINT_ORDER.keys()))

ROOT_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PROCESSED_PATH = ROOT_PATH + '\\' + PROCESSED_FOLDER + '\\'
LABELS_PATH = ROOT_PATH + '\\' + LABELS_FILENAME
FEATURE_PATH = ROOT_PATH + '\\' + FEATURE_FOLDER + '\\'
SPACER = '=================================='

#====================
#  GENERAL FUNCTIONS
#====================

def getPoint(timeStep, jointName, axis):
	jointIndex = JOINT_ORDER[jointName]
	axisNo = 0
	if axis == 'x':
		axisNo = 0
	elif axis == 'y':
		axisNo = 1
	elif axis == 'z':
		axisNo = 2
	else:
		print('ERROR: Axis given as ' + axis)
		sys.exit()
	return float(timeStep[3*jointIndex + axisNo])

def setPoint(timeStep, jointName, axis, val):
	jointIndex = JOINT_ORDER[jointName]
	axisNo = 0
	if axis == 'x':
		axisNo = 0
	elif axis == 'y':
		axisNo = 1
	elif axis == 'z':
		axisNo = 2
	else:
		print('ERROR: Axis given as ' + axis)
		sys.exit()
	timeStep[3*jointIndex + axisNo] = val

def getID(filename):
        und = filename.find('_')
        dot = filename.find('.')
        return int(filename[und+1:dot])

def getAttemptNumber(filename):
        und = filename.find('_')
        for i in range(len(filename)):
                if filename[i].isdigit():
                        break
        num = filename.find('.')
        return int(filename[i:und])

def addConstant(list, constant):
	for i in range(len(list)):
		list[i] = list[i] + constant

#====================
#  FEATURE FUNCTIONS
#====================

def jointAsymmetryFeature(baseJointName, data):
	jointNameLeft = baseJointName + 'Left'
	jointNameRight = baseJointName + 'Right'
	# error checking
	if not (jointNameLeft in JOINT_LIST and jointNameRight in JOINT_LIST):
		print('Bad joint name for symmetry feature: ' + baseJointName)
		sys.exit()
	# check symmetry between trajectories using euclidean distance
	distList = []

	xLeftList = []
	yLeftList = []
	zLeftList = []
	xRightList = []
	yRightList = []
	zRightList = []
	rightList = []
	for row in data:
		xLeftList.append(getPoint(row, jointNameLeft, 'x'))
		yLeftList.append(getPoint(row, jointNameLeft, 'y'))
		zLeftList.append(getPoint(row, jointNameLeft, 'z'))
		xRightList.append(getPoint(row, jointNameRight, 'x'))
		yRightList.append(getPoint(row, jointNameRight, 'y'))
		zRightList.append(getPoint(row, jointNameRight, 'z'))

	# centre to give zero average
	addConstant(xLeftList, - sum(xLeftList) / len(xLeftList))
	addConstant(yLeftList, - sum(yLeftList) / len(yLeftList))
	addConstant(zLeftList, - sum(zLeftList) / len(zLeftList))
	addConstant(xRightList, - sum(xRightList) / len(xRightList))
	addConstant(yRightList, - sum(yRightList) / len(yRightList))
	addConstant(zRightList, - sum(zRightList) / len(zRightList))

	# now do distance
	for i in range(len(data)):
		# mirror about x-axis (corresponds to left and right)
		xRightList[i] = -1 * xRightList[i]
		distList.append(sqrt(pow(xLeftList[i] - xRightList[i],2) + pow(yLeftList[i] - yRightList[i],2) + pow(zLeftList[i] - zRightList[i],2)))

	return sum(distList) / len(distList)

def maxPositionFeature(jointName, data, axis):
	max = float('-inf')
	for row in data:
		pt = getPoint(row, jointName, axis)
		if pt > max:
			max = pt
	return max

def minPositionFeature(jointName, data, axis):
	min = float('inf')
	for row in data:
		pt = getPoint(row, jointName, axis)
		if pt < min:
			min = pt
	return min

def averagePositionFeature(jointName, data, axis):
	posList = []
	for row in data:
		posList.append(getPoint(row, jointName, axis))
	return sum(posList) / len(posList)

def stdevPositionFeature(jointName, data, axis):
	n = 0
	s = 0
	ss = 0
	for row in data:
		n += 1
		s += getPoint(row, jointName, axis)
		ss += pow(getPoint(row, jointName, axis),2)
	return sqrt(n * ss  - pow(s,2)) / n

def powerFeature(jointName, velocity):
	energyList = []
	for row in velocity:
		vx = getPoint(row, jointName, 'x')
		vy = getPoint(row, jointName, 'y')
		vz = getPoint(row, jointName, 'z')
		energyList.append(sqrt(pow(vx,2) + pow(vy,2) + pow(vz,2)))
	return sum(energyList) / len(energyList)

def totalPowerFeature(velocity):
	totalPower = 0
	for jointName in JOINT_LIST:
		totalPower += powerFeature(jointName, velocity)
	return totalPower

#=================
#  MAIN FUNCTIONS
#=================

def run(loadPath, outputPath):

	jumpFeatures = OrderedDict()
	ratings = dict()

	print('Loading CSV files from ' + loadPath)
	print(SPACER)

	# create rating dictionary
	print(LABELS_PATH)
	with open(LABELS_PATH, 'r') as csvfile:
		csvReader = csv.reader(csvfile)
		for rowNo, row in enumerate(csvReader):
			ratings[row[0]] = row[1:4]

	# process by jump
	for file in os.listdir(loadPath):
		features = OrderedDict()
		# load in all data and store as float
		print('Loading Jump file: ' + file + '...', end=' ')
		with open(loadPath + file,'r') as csvfile:
			csvReader = csv.reader(csvfile)
			data = []
			for rowNo,row in enumerate(csvReader):
				newRow = list(row)
				for jointName in JOINT_LIST:
					x = getPoint(row, jointName, 'x')
					y = getPoint(row, jointName, 'y')
					z = getPoint(row, jointName, 'z')
					setPoint(newRow, jointName, 'x', x)
					setPoint(newRow, jointName, 'y', y)
					setPoint(newRow, jointName, 'z', z)
				data.append(newRow)
		print('Loaded.')

		# generate velocity curves
		print('Generating velocity curves...', end=' ')
		velocity = []
		for i in range(len(data)-1):
			timeSequence = []
			for j in range(len(data[i])):
				timeSequence.append(data[i+1][j] - data[i][j])
			velocity.append(timeSequence)
		print('Done.')
		# generate acceleration curves
		print('Generating acceleration curves...', end=' ')
		acceleration = []
		for i in range(len(velocity)-1):
			timeSequence = []
			for j in range(len(velocity[i])):
				timeSequence.append(velocity[i+1][j] - velocity[i][j])
			acceleration.append(timeSequence)
		print('Done.')

		# symmetry-based features
		print('Calculating symmetry-based features...', end=' ')
		symJointList = ['Shoulder', 'Elbow', 'Wrist', 'Hand', 'Hip', 'Knee', 'Ankle', 'Foot']
		for baseJointName in symJointList:
			features['Joint Asymmetry: ' + baseJointName] = jointAsymmetryFeature(baseJointName, data)
		print('Done.')

		# distribution-based features
		print('Calculating distribution-based features...', end=' ')
		features['Total Power'] = totalPowerFeature(velocity)
		for jointName in JOINT_LIST:
			features['Power: ' + jointName] = powerFeature(jointName, velocity)
			for axis in ['x','y','z']:
				for curves in [['Position',data], ['Velocity',velocity], ['Acceleration', acceleration]]:
					features['Average '+curves[0]+' ('+axis+'): ' + jointName] = averagePositionFeature(jointName, curves[1], axis)
					features['Stdev '+curves[0]+' ('+axis+'): ' + jointName] = stdevPositionFeature(jointName, curves[1], axis)
					features['Max '+curves[0]+' ('+axis+'): ' + jointName] = maxPositionFeature(jointName, curves[1], axis)
					features['Min '+curves[0]+' ('+axis+'): ' + jointName] = minPositionFeature(jointName, curves[1], axis)
		print('Done.')

		# record for later
		jumpFeatures[file] = features
		print(SPACER)

	# save features
	toExport = []
	# get headers from any featureset (all used ordered dict)
	randomFilename = os.listdir(loadPath)[0]
	featureNames = list(jumpFeatures[randomFilename].keys())
	headers = list(featureNames)
	headers.extend(['Person ID','Attempt ID','Rating'])

	# now save features for each jump
	for filename in jumpFeatures:
		id = getID(filename)
		if str(id) not in ratings:
			print('Not recording ' + filename + ', missing ratings.')
			continue
		attempt = getAttemptNumber(filename)
		rating = ratings[str(id)][attempt - 1]

		featureDict = jumpFeatures[filename]
		featureList = []
		for name in featureNames:
			featureList.append(featureDict[name])
		# add extra heading stuff
		featureList.extend([id, attempt, rating])
		toExport.append(featureList)

	# export to csv
	print('Exporting to ' + outputPath, end=' ')
	with open(outputPath,'w',newline='') as newCsvFile:
		csvWriter = csv.writer(newCsvFile)
		csvWriter.writerow(headers)
		toExport = sorted(toExport, key=lambda x: int(x[-1])) #sort by class label
		for i in range(len(toExport)):
			csvWriter.writerow(toExport[i])
	print('Done.')

#========================
# KICK-OFF MAIN FUNCTION
#========================

# prepare export folder
if os.path.exists(FEATURE_PATH):
	shutil.rmtree(FEATURE_PATH)
os.mkdir(FEATURE_PATH)

#run
run(PROCESSED_PATH + 'transformed_scaled\\', FEATURE_PATH + 'transformed_scaled_features.csv')
run(PROCESSED_PATH + 'transformed\\', FEATURE_PATH + 'transformed_features.csv')
run(PROCESSED_PATH + 'raw\\', FEATURE_PATH + 'features.csv')
