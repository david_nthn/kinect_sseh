@ECHO OFF
CD /d "%~dp0"
rem ECHO Experiment Name,Person ID,Attempt ID,Mark,Estimated Mark > transformed_results.csv

ECHO Running experiment Untuned Transformed Jump...
rem java -Xms4096m -Xmx4096m -jar experiment.jar "Untuned Transformed Jump" jumps_extracted_features\\transformed_features.csv none >> transformed_results.csv

ECHO Running experiment Untuned Transformed Kick...
rem java -Xms4096m -Xmx4096m -jar experiment.jar "Untuned Transformed Kick" kt_extracted_features\\transformed_features.csv none >> transformed_results.csv

ECHO Running experiment RMSE-Tuned Transformed Jump...
rem java -Xms4096m -Xmx4096m -jar experiment.jar "RMSE-Tuned Transformed Jump" jumps_extracted_features\\transformed_features.csv rms >> transformed_results.csv

ECHO Running experiment RMSE-Tuned Transformed Kick...
rem java -Xms4096m -Xmx4096m -jar experiment.jar "RMSE-Tuned Transformed Kick" kt_extracted_features\\transformed_features.csv rms >> transformed_results.csv

ECHO Running experiment Weighted RMSE-Tuned Transformed Jump...
java -Xms4096m -Xmx4096m -jar experiment.jar "Weighted RMSE-Tuned Transformed Jump" jumps_extracted_features\\transformed_features.csv wrms >> transformed_results.csv