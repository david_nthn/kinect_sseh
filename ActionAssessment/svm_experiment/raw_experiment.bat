@ECHO OFF
CD /d "%~dp0"
ECHO Experiment Name,Person ID,Attempt ID,Mark,Estimated Mark > raw_results.csv

ECHO Running experiment Untuned Raw Jump...
java -Xms4096m -Xmx4096m -jar experiment.jar "Untuned Raw Jump" jumps_extracted_features\\features.csv none >> raw_results.csv

ECHO Running experiment Untuned Raw Kick...
java -Xms4096m -Xmx4096m -jar experiment.jar "Untuned Raw Kick" kt_extracted_features\\features.csv none >> raw_results.csv

ECHO Running experiment RMSE-Tuned Raw Jump...
java -Xms4096m -Xmx4096m -jar experiment.jar "RMSE-Tuned Raw Jump" jumps_extracted_features\\features.csv rms >> raw_results.csv

ECHO Running experiment RMSE-Tuned Raw Kick...
java -Xms4096m -Xmx4096m -jar experiment.jar "RMSE-Tuned Raw Kick" kt_extracted_features\\features.csv rms >> raw_results.csv

ECHO Running experiment Weighted RMSE-Tuned Raw Jump...
java -Xms4096m -Xmx4096m -jar experiment.jar "Weighted RMSE-Tuned Raw Jump" jumps_extracted_features\\features.csv wrms >> raw_results.csv