import java.util.concurrent.Callable;

import weka.classifiers.Classifier;
import weka.core.Instance;
import weka.core.Instances;

/**
 * Trains and evaluates a classifer, returning it with the results in a
 * CrossValidateResults object.
 * 
 * @author Tom Horrocks
 * @version 23/08/2013
 * 
 */
public class CrossValidate implements Callable<CrossValidateResults> {
	/**
	 * Classifier, untrained but with hyperparameters set.
	 */
	private Classifier classifier;
	/**
	 * Folds to be used for training, indexed by foldNum.
	 */
	private Instances[] trainFolds;
	/**
	 * Folds to be used for testing, indexed by foldNum.
	 */
	private Instances[] testFolds;
	/**
	 * Class weights to be used for calculated weighted RMSE, indexed by
	 * foldNum.
	 */
	private double[][] trainLabelWeights;

	/**
	 * Simple constructor, stores all variables internally without duplication.
	 * 
	 * @param classifier
	 *            Classifier, untrained but with hyperparameters set
	 * @param trainFolds
	 *            Folds to be used for training, indexed by foldNum
	 * @param testFolds
	 *            Folds to be used for testing, indexed by foldNum
	 * @param trainLabelWeights
	 *            Class weights to be used for calculated weighted RMSE, indexed
	 *            by foldNum
	 */
	public CrossValidate(Classifier classifier, Instances[] trainFolds, Instances[] testFolds, double[][] trainLabelWeights) {
		this.classifier = classifier;
		this.trainFolds = trainFolds;
		this.testFolds = testFolds;
		this.trainLabelWeights = trainLabelWeights;
	}

	@Override
	public CrossValidateResults call() throws Exception {

		double sumSquares = 0;
		double weightedSumSquares = 0;
		int nInstances = 0;

		// perform 5-fold cross-validation training
		int numFolds = trainFolds.length;
		for (int foldNum = 0; foldNum < numFolds; foldNum++) {
			Instances trainSet = trainFolds[foldNum];
			Instances testSet = testFolds[foldNum];
			double labelWeights[] = trainLabelWeights[foldNum];

			classifier.buildClassifier(trainSet);
			// evaluate classifier on test instances
			for (Instance testInstance : testSet) {
				double estLabel = classifier.classifyInstance(testInstance);
				double trueLabel = testInstance.classValue();
				double err = trueLabel - estLabel;
				sumSquares += Math.pow(err, 2);
				double weightedErr = err * labelWeights[(int) trueLabel];
				weightedSumSquares += Math.pow(weightedErr, 2);
				nInstances++;
			}

		}

		double weightedRms = Math.sqrt(weightedSumSquares / nInstances);
		double rms = Math.sqrt(sumSquares / nInstances);

		CrossValidateResults results = new CrossValidateResults(classifier, rms, weightedRms);
		// help garbage collector
		classifier = null;
		trainFolds = null;
		testFolds = null;
		trainLabelWeights = null;

		return results;
	}

}