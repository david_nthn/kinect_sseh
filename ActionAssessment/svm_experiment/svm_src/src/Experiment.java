import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import weka.classifiers.Classifier;
import weka.classifiers.functions.SMOreg;
import weka.classifiers.functions.supportVector.RBFKernel;
import weka.classifiers.functions.supportVector.RegSMOImproved;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.RemoveByName;
import weka.filters.unsupervised.attribute.RemoveUseless;

/**
 * Runs an epsilon Support Vector Machine (e-SVM) one the features extracted
 * from kinect joint trajectories.
 * 
 * The features file is expected to be of the form:
 * <feature1> <feature2> .... <featureN> <Person ID> <Attempt ID> <Rating>
 * With the first row being headers.
 * 
 * @author Tom Horrocks
 * @version 23/08/2013
 * 
 */
public class Experiment {

	private static final int NUM_CLASS_VALUES = 11;
	private static final String CLASS_NAME = "Rating";

	/**
	 * Enum to store tuning metric preference.
	 * 
	 * @author Tom Horrocks
	 * @version 23/08/2013
	 * 
	 */
	private enum TuningMetric {
		/**
		 * Tune acording to Root Mean Square Error (RMSE) of mark prediction.
		 * **/
		RMS,
		/**
		 * Tune according to the RMSE, but data points have been weighted
		 * according to their relative proportion in the dataset.
		 */
		WRMS,
		/**
		 * No tuning; Weka defaults used for all parameters. These are:
		 * Complexity (C) of SVM: 1 Gamma (G) of RBF Kernel: 0.01 Epsilon (e) of
		 * RegOptimiser learning algorithm: 1e-3
		 */
		NONE
	};

	/**
	 * Number of concurrent threads to run, by default set to the number of
	 * available processors (or hyperthreads if CPU supports it).
	 */
	private static final int N_CONCURRENT_THREADS = Runtime.getRuntime().availableProcessors();
	/**
	 * Number of folds to use when tuning with gridsearch. If 0 is chosen, the
	 * maximum number of folds will be used.
	 */
	private static final int NUM_GRIDSEARCH_FOLDS = 0;

	/**
	 * To be used when running the experiment.
	 * 
	 * @param args
	 *            1) Experiment Name, 
	 *            2) Filepath of features.csv, 
	 *            3) Tuning method ("rms", "wrms", or "none")
	 */
	public static void main(final String[] args) {

		if (args.length != 3) {
			System.err.println("Expecting 3 arguments:\n"
					+ "\t1) Experiment Name\n" 
					+ "\t2) Features Filepath\n" 
					+ "\t3) Tuning metric (rms or wrms or none)");
			System.exit(0);
		}

		String inExperimentName = args[0];
		String inFilePath = args[1];
		TuningMetric tuning = null;

		if (args[2].equals("wrms")) {
			tuning = TuningMetric.WRMS;
		} else if (args[2].equals("rms")) {
			tuning = TuningMetric.RMS;
		} else if (args[2].equals("none")) {
			tuning = TuningMetric.NONE;
		} else {
			System.err.println("Argument 3 must be \"wrms\" or \"rms\" or \"none\". It was: " + args[2]);
			System.exit(0);
		}

		Experiment experiment = new Experiment();
		experiment.runExperiment(inExperimentName, inFilePath, tuning);
	}

	/**
	 * Tasks a list of Instance objects and returns an Instances object.
	 * 
	 * @param parentInstancesForContainer Used for the headers
	 * @param wantedInstanceList List of Instance objects to be compiled
	 * @return The compiled Instances object
	 */
	private static Instances compileInstances(
			final Instances parentInstancesForContainer, 
			final List<Instance> wantedInstanceList) {
		Instances constructedInstances = new Instances(parentInstancesForContainer);
		while (constructedInstances.numInstances() > 0) {
			constructedInstances.remove(0);
		}
		for (Instance inst : wantedInstanceList) {
			constructedInstances.add(inst);
		}
		return constructedInstances;
	}

	/**
	 * Gets the set of Person IDs referred to in the data, with duplicates
	 * deleted.
	 * 
	 * @param allData
	 *            Set of all data.
	 * @return Set of person IDs.
	 */
	private static Set<Integer> getUniquePersonIDs(final Instances allData) {
		Set<Integer> idSet = new HashSet<Integer>();
		Attribute idAttribute = allData.attribute("Person ID");
		for (Instance inst : allData) {
			int personID = (int) inst.value(idAttribute);
			idSet.add(personID);
		}
		return idSet;
	}

	/**
	 * Load the features from a given filepath.
	 * 
	 * @param inFilePath
	 *            Path to the csv.
	 * @return The data as instances.
	 * @throws Exception
	 *             If the file cannot be found.
	 */
	private static Instances loadData(final String inFilePath) throws Exception {
		// load data from the CSV
		CSVLoader loader = new CSVLoader();
		loader.setSource(new File(inFilePath));
		Instances data = loader.getDataSet();
		// set the class (Rating)
		data.setClass(data.attribute(CLASS_NAME));
		// remove features that have the same value across all instances.
		RemoveUseless removeUselessFilter = new RemoveUseless();
		removeUselessFilter.setMaximumVariancePercentageAllowed(100);
		removeUselessFilter.setInputFormat(data);
		Instances filteredData = Filter.useFilter(data, removeUselessFilter);
		return filteredData;
	}

	/**
	 * Calculates the weight to multiply with the error for each true class.
	 * 
	 * @param trainingSet
	 *            The training set upon which class populations are calculated.
	 * @return The label weights for each class, indexed by the mark [0,10]
	 */
	private double[] getLabelWeights(final Instances trainingSet) {
		// get population of each label
		Attribute classAttribute = trainingSet.classAttribute();
		int numInstances = 0;
		// known marks are from 0 to 10, so use array
		int[] markCounts = new int[NUM_CLASS_VALUES];
		for (Instance instance : trainingSet) {
			int classValue = (int) instance.value(classAttribute);
			markCounts[classValue]++;
			numInstances++;
		}
		// use inverse of percent population as weight
		double[] markWeights = new double[NUM_CLASS_VALUES];
		for (int i = 0; i < markCounts.length; i++) {
			if (markCounts[i] == 0) {
				markWeights[i] = Double.NaN;
			} else {
				double proportion = ((double) markCounts[i]) / numInstances;
				markWeights[i] = 1 / proportion;
			}
		}
		return markWeights;
	}

	/**
	 * Responsible for tuning an e-SVM using grid-search. Search is diatic in
	 * all variables, with ranges:
	 * C: 2^-1, 2^0, ...2^6 
	 * G: 2^-8, 2^-7, ... 2^0
	 * e: 2^-8, 2^-7, ... 2^-1
	 * 
	 * @param fc
	 *            The filtered classifier containing an SVM, and a filter to
	 *            remove ID features (Person ID, Attempt ID). SVM will be
	 *            replaced with a tuned and trained SVM.
	 * @param tuneData
	 *            The data to be used when tuning.
	 * @param useRmsWeight
	 *            True iff the weighting system is to be used, otherwise default
	 *            RMSE used.
	 * @throws Exception If there is an error with the filter in 
	 * 			  FilteredClassifier.
	 */
	private void tuneAndTrain(
			final FilteredClassifier fc, 
			final Instances tuneData, 
			final boolean useRmsWeight) throws Exception {
		// Get list of unique Person IDs in order to create ID-based folds
		List<Integer> uniquePersonIDs = 
				new LinkedList<Integer>(getUniquePersonIDs(tuneData));
		Collections.sort(uniquePersonIDs);
		// distribute IDs amongst each of the NUM_GRIDSEARCH_FOLDSs
		int numFolds;
		if (NUM_GRIDSEARCH_FOLDS > 0) {
			numFolds = NUM_GRIDSEARCH_FOLDS;
		} else {
			numFolds = uniquePersonIDs.size();
		}
		ArrayList<List<Integer>> folds = new ArrayList<List<Integer>>(numFolds);
		for (int i = 0; i < numFolds; i++) {
			folds.add(i, new LinkedList<Integer>());
		}
		for (int i = 0; i < uniquePersonIDs.size(); i++) {
			int id = uniquePersonIDs.get(i);
			int foldNum = i % numFolds;
			folds.get(foldNum).add(id);
		}
		// use IDs to build a test instance and train instance for each fold
		Instances[] testFolds = new Instances[numFolds];
		Instances[] trainFolds = new Instances[numFolds];
		double[][] trainLabelWeights = new double[numFolds][];
		for (int foldNum = 0; foldNum < numFolds; foldNum++) {
			// lists to hold test instances temporarily
			List<Instance> testInstanceList = new LinkedList<Instance>();
			List<Instance> trainInstanceList = new LinkedList<Instance>();
			// populate lists using helper method
			splitInstancesByPersonIDs(tuneData, folds.get(foldNum), testInstanceList, trainInstanceList);
			// compile list of Instance objects into Instances object, and store in array.
			testFolds[foldNum] = compileInstances(tuneData, testInstanceList);
			trainFolds[foldNum] = compileInstances(tuneData, trainInstanceList);
			trainLabelWeights[foldNum] = getLabelWeights(trainFolds[foldNum]);
		}

		/*
		 * Now folds are created, start the gridsearch.
		 */
		double minError = Double.POSITIVE_INFINITY;
		Classifier bestTunedClassifier = null;
		for (int c = -1; c <= 6; c++) {
			for (int g = -8; g <= 0; g++) {
				/*
				 * evaluate the grids in a threaded fashion for every (c,g)
				 * combination in order to save memory.
				 */
				List<CrossValidate> tasks = new LinkedList<CrossValidate>();
				for (int e = -8; e <= -1; e++) {
					// make new filtered classifier with appropriate parameters
					SMOreg gridSVM = new SMOreg();
					gridSVM.setC(Math.pow(2, c));

					RBFKernel gridKernel = new RBFKernel();
					gridKernel.setGamma(Math.pow(2, g));
					// speed up that biyatch! Yeow.
					gridKernel.setChecksTurnedOff(true);
					gridSVM.setKernel(gridKernel);

					RegSMOImproved reg = new RegSMOImproved();
					reg.setEpsilonParameter(Math.pow(2, e));
					gridSVM.setRegOptimizer(reg);

					FilteredClassifier gridFc = new FilteredClassifier();
					gridFc.setFilter(Filter.makeCopy(fc.getFilter()));
					gridFc.setClassifier(gridSVM);

					// create task and save for threaded execution later.
					CrossValidate task = new CrossValidate(gridFc, trainFolds, testFolds, trainLabelWeights);
					tasks.add(task);
				}
				// now have all tasks for a given (c,g): Execute them!
				ExecutorService service = Executors.newFixedThreadPool(N_CONCURRENT_THREADS);
				List<Future<CrossValidateResults>> futures = null;
				try {
					// THIS LINE BLOCKS THE MAIN THREAD UNTIL COMPLETION
					futures = service.invokeAll(tasks);
				} catch (InterruptedException e) {
					System.err.println("One thread was interrputed! Programming error.");
					System.err.println("Gridsearch not performed for (c,g)=("+c+","+g+")");
					e.printStackTrace();
				}
				if (futures == null) {
					continue; // nothing to evaluate
				}
				for (int i = 0; i < futures.size(); i++) {
					CrossValidateResults results = futures.get(i).get();
					double error;
					if (useRmsWeight) {
						error = results.getWeightedRms();
					} else {
						error = results.getRms();
					}
					if (error < minError) {
						minError = error;
						bestTunedClassifier = results.getClassifier();
					}
					results.freeMemory();
					futures.set(i, null);
				}
				futures = null;
				tasks = null;
				service = null;
			}

		}
		// update fc with tuning
		fc.setClassifier(bestTunedClassifier);
		fc.buildClassifier(tuneData);
	}

	/**
	 * Sees the experiment through from start to finish. Outputs results by
	 * printing to stdout in the form of:
	 *  <ExperimentName>,<PersonID>,<AttemptID>,<RealMark>,<EstimatedMark>
	 * 
	 * @param experimentName Name of the experiment for printing to STDOUT.
	 * @param filePath Filepath to the features.csv
	 * @param tuningMetric Tuning metric to be used when gridsearching.
	 */
	public final void runExperiment(
			final String experimentName, 
			final String filePath, 
			final TuningMetric tuningMetric) {
		// load CSV
		Instances allData = null;
		try {
			allData = loadData(filePath);
		} catch (Exception e) {
			System.err.println("Unable to load CSV: " + filePath);
			e.printStackTrace();
			System.exit(0);
		}

		// get set of Person IDs
		Set<Integer> uniquePersonIDs = getUniquePersonIDs(allData);

		// main outer loop (cross-validation)
		for (int testPersonID : uniquePersonIDs) {
			List<Instance> testInstanceList = new LinkedList<Instance>();
			List<Instance> trainInstanceList = new LinkedList<Instance>();

			// split into test and train set
			splitInstancesByPersonID(allData, testPersonID, testInstanceList, trainInstanceList);

			// set up ID stripping filter
			RemoveByName removeFilter = new RemoveByName();
			removeFilter.setExpression(".* ID");
			try {
				removeFilter.setInputFormat(allData);
			} catch (Exception e) {
				System.err.println("Problem setting an ID filter!");
				e.printStackTrace();
				System.exit(0);
			}
			// set up classifier
			SMOreg svm = new SMOreg();
			svm.setKernel(new RBFKernel());

			// join into filteredclassifier
			FilteredClassifier fc = new FilteredClassifier();
			fc.setFilter(removeFilter);
			fc.setClassifier(svm);

			// assemble training set
			Instances trainingSet = compileInstances(allData, trainInstanceList);
			// tune and train classifier
			try {
				switch (tuningMetric) {
				case NONE:
					fc.buildClassifier(trainingSet);
					break;
				case RMS:
					tuneAndTrain(fc, trainingSet, false);
					break;
				case WRMS:
					tuneAndTrain(fc, trainingSet, true);
					break;
				default:
					System.err.println("TuningMetric enum has been changed.");
					System.exit(0);
					break;
				}

				// evaluate classifier on test instances
				for (Instance testInstance : testInstanceList) {
					double estLabel = fc.classifyInstance(testInstance);
					double trueLabel = testInstance.classValue();
					int attemptID = (int) testInstance.value(testInstance.numAttributes() - 2);

					System.out.println(experimentName + "," + testPersonID + "," + attemptID + "," + trueLabel + "," + estLabel);
				}
			} catch (Exception e) {
				System.err.println("Cannot train, tune, or evaluate SVM due to an error in WEKA");
				System.err.println("Results for id " + testPersonID + " will not be included.");
				e.printStackTrace();
				continue;
			}
		}
	}

	/**
	 * Splits a data set into a training set and a test set, placing all
	 * instances related to the testPersonID into the test set and the rest into
	 * the training set.
	 * 
	 * No copying is done on each instance.
	 * 
	 * @param allData
	 *            The data to be split.
	 * @param testPersonID
	 *            The ID for the person whose data forms the test set
	 * @param testInstanceList
	 *            Instances that form the test set
	 * @param trainInstanceList
	 *            Instances that form the training set
	 */
	private void splitInstancesByPersonID(final Instances allData, final int testPersonID, final List<Instance> testInstanceList, final List<Instance> trainInstanceList) {
		Attribute attributePersonID = allData.attribute("Person ID");
		for (Instance inst : allData) {
			int personID = (int) inst.value(attributePersonID);
			if (personID == testPersonID) {
				testInstanceList.add(inst);
			} else {
				trainInstanceList.add(inst);
			}
		}
	}

	/**
	 * Splits a data set into a training set and a test set, placing all
	 * instances related to the testPersonID into the test set and the rest into
	 * the training set.
	 * 
	 * No copying is done on each instance.
	 * 
	 * @param allData
	 *            The data to be split.
	 * @param personIDs
	 *            The ID on which to split
	 * @param selectedInstances
	 *            Instances that form the test set
	 * @param restOfInstances
	 *            Instances that form the training set
	 */
	private void splitInstancesByPersonIDs(final Instances allData, final List<Integer> personIDs, final List<Instance> selectedInstances, final List<Instance> restOfInstances) {
		Attribute attributePersonID = allData.attribute("Person ID");
		for (Instance inst : allData) {
			int instanceID = (int) inst.value(attributePersonID);
			for (int splitID : personIDs) {
				if (splitID == instanceID) {
					selectedInstances.add(inst);
				} else {
					restOfInstances.add(inst);
				}
			}

		}
	}

}
