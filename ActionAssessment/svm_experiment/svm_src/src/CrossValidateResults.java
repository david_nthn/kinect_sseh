import weka.classifiers.Classifier;

/**
 * Simple class for holding cross-validation results from the gridsearch.
 * 
 * @author Tom Horrocks
 * @version 23/08/2013
 * 
 */
class CrossValidateResults {
	/**
	 * Trained classifier which produced the results stored in the class.
	 */
	private Classifier classifier;
	/**
	 * Root Mean Square error.
	 */
	private double rms;
	/**
	 * Weighted Root Mean Square error.
	 */
	private double weightedRms;

	/**
	 * Create a new results-holding object.
	 * 
	 * @param classifier
	 *            Trained classifier which produced the results stored in the
	 *            class
	 * @param rms
	 *            Root Mean Square error
	 * @param weightedRms
	 *            Weighted Root Mean Square error
	 */
	public CrossValidateResults(Classifier classifier, double rms, double weightedRms) {
		this.classifier = classifier;
		this.rms = rms;
		this.weightedRms = weightedRms;
	}

	/**
	 * Call to help garbage collector if this result is no longer required.
	 */
	public void freeMemory() {
		classifier = null;
	}

	/**
	 * Get the trained classifier that produced these results.
	 * @return the classifier.
	 */
	public Classifier getClassifier() {
		return classifier;
	}

	/**
	 * Get the RMSE.
	 * 
	 * @return rmse
	 */
	public double getRms() {
		return rms;
	}

	/**
	 * Get the weighted RMSE.
	 * 
	 * @return weighted rmse.
	 */
	public double getWeightedRms() {
		return weightedRms;
	}

}