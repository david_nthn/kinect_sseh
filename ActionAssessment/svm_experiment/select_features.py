import sys
import os
import shutil
import csv
import numpy as np
import scipy.stats as st
import math as m

#====================================
#  CONSTANTS
#====================================
# verbose and debug toggles
v = True
# v = True
d = True
# d = True

# file structure related
IN_FILE = sys.argv[2]
SUBSET = sys.argv[3]
INPUT_PATH = sys.argv[1]  + '//' + IN_FILE # dir and file
OUTPUT_PATH = sys.argv[1] + '//' + IN_FILE[:-4] + '_' + SUBSET + '.csv' # append subset to filename

if v: print('SUBSET is ' + SUBSET)

# Joint related
JOINT_ORDER = {"HipCenter":0,"Spine":1,"ShoulderCenter":2,"Head":3,"ShoulderLeft":4,"ElbowLeft":5,"WristLeft":6,"HandLeft":7,"ShoulderRight":8,"ElbowRight":9,"WristRight":10,"HandRight":11,"HipLeft":12,"KneeLeft":13,"AnkleLeft":14,"FootLeft":15,"HipRight":16,"KneeRight":17,"AnkleRight":18,"FootRight":19}
JOINT_MAP = {v:k for k, v in JOINT_ORDER.items()} # reverse map so that names can be accessed by index
COORD_MAP = {0:"x", 1:"y", 2:"z"}

# read in headers
with open(INPUT_PATH, 'r') as input:
    csvReader = csv.reader(input, delimiter=',')
    header = next(csvReader) # read headers as strings, then abandon
headerIndex = {header[index]:index for index in range(len(header))}

# features to ALWAYS keep: Person ID, Attempt ID, Rating
keepList = [751, 750, 749]

#====================================
#  FEATURES TO REMOVE
#====================================
# full set to help with set differences/complements
ALL = set(range(748))

# JUMP
JUMP_JOINTS = {"Head":3,"ElbowLeft":5,"WristLeft":6,"ElbowRight":9,"WristRight":10,"KneeLeft":13,"AnkleLeft":14,"KneeRight":17,"AnkleRight":18}
JUMP_SET = set([])

for joint in JOINT_ORDER.keys():
    if not joint in JUMP_JOINTS.keys():
        # find columns associated with this joint and add their indexes to be removed
        JUMP_SET.update( [headerIndex[col] for col in header if joint in col] )

STD_DEV_SET = set([headerIndex[col] for col in header if 'Std' not in col])
# STD_DEV_SET.update( [headerIndex[col] for col in header if "Hand" in col] ) 
STD_DEV_SET.update( [headerIndex[col] for col in header if "Foot" in col] ) 
STD_DEV_SET.update( [headerIndex[col] for col in header if "(z)" in col] ) 

STD_DEV_JUMP_SET = set([])
for joint in JOINT_ORDER.keys():
    if not joint in JUMP_JOINTS.keys():
        # find columns associated with this joint and add their indexes to be removed
        STD_DEV_JUMP_SET.update( [headerIndex[col] for col in header if joint in col or 'Std' not in col] )

# LOWER
LOWER_JOINTS = {"HipCenter":0,"KneeLeft":13,"AnkleLeft":14,"KneeRight":17,"AnkleRight":18,"HipLeft":12,"HipRight":16}
LOWER_SET = set([])

for joint in JOINT_ORDER.keys():
    if not joint in LOWER_JOINTS.keys():
        # find columns associated with this joint and add their indexes to be removed
        LOWER_SET.update( [headerIndex[col] for col in header if joint in col] )

# UPPER
UPPER_JOINTS = {"HipCenter":0,"Spine":1,"ShoulderCenter":2,"Head":3,"ShoulderLeft":4,"ElbowLeft":5,"WristLeft":6,"HandLeft":7,"ShoulderRight":8,"ElbowRight":9,"WristRight":10,"HandRight":11}
#UPPER_JOINTS = {"HipCenter":0,"Spine":1,"ShoulderCenter":2,"ShoulderLeft":4,"ElbowLeft":5,"WristLeft":6,"ShoulderRight":8,"ElbowRight":9,"WristRight":10}
UPPER_SET = set([])

for joint in JOINT_ORDER.keys():
    if not joint in UPPER_JOINTS.keys():
        # find columns associated with this joint and add their indexes to be removed
        UPPER_SET.update( [headerIndex[col] for col in header if joint in col and not "Std" in col] )
    # remove asym features
    #UPPER_SET.update( [headerIndex[col] for col in header if "Power" in col] )


#====================================
#  MAIN FUNCTION
#====================================

def run(loadPath, outputPath, removeList):
    # make sure we don't remove Person ID, Attempt ID, or Rating
    for col in keepList:
        if col in removeList: removeList.remove(col)

    print('Reading in data and headers...')
    # read in features csv, including the column names
    data = np.genfromtxt(loadPath, dtype=float, delimiter=',', skip_header=1)

    # build a list of cols to delete in descending order - if ascending order then later indexes will be wrong
    removeList.sort(reverse=True)

    print('Done. Starting with ' + str(len(header)) + ' features.')

    print('Removing features...')
    for index in removeList:
        # delete column rather than row (i.e. dim=1)
        data = np.delete(data, index, 1)
        # need to separately remove from header (in-place)
        del header[index]

    with open(outputPath, 'w', newline='') as output:
        writer = csv.writer(output, delimiter=',', )
        writer.writerow(header)
        for row in data:
            writer.writerow(row)
    print('Done. There are now ' + str(len(header)) + ' features.')

if SUBSET == 'jump':
    run(INPUT_PATH, OUTPUT_PATH, list(JUMP_SET)) # RMS = 1.41
elif SUBSET == 'std':
    run(INPUT_PATH, OUTPUT_PATH, list(STD_DEV_SET)) # RMS = 1.34 
elif SUBSET == 'jump_std':
    run(INPUT_PATH, OUTPUT_PATH, list(STD_DEV_JUMP_SET)) # RMS = 1.41
elif SUBSET == 'lower':
    run(INPUT_PATH, OUTPUT_PATH, list(LOWER_SET)) # RMS = 1.63 for sidestep
elif SUBSET == 'upper':
    run(INPUT_PATH, OUTPUT_PATH, list(UPPER_SET)) # RMS = 1.49 UArmThrow
