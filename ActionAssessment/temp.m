clear all

addpath('dtw_experiment\');
addpath('analysis\');
addpath('dtw_experiment\HelperFunctions\');


labels = importdata('./data/Test/Jump/labels.csv');
M = readcsv2mot('./data/Test/Jump/Jump_extracted_features/transformed_hoj3d/');

C= unique(labels(:,2:4));
results = cell(length(unique(labels(:,1))+1),5);
hoj3dbyclass = struct;
% Generate Struct of HOJ3D that is sorted by Class
for i=1:length(C)
    [row,col] = find(C(i)==labels(:,2:4));
    hoj3dbyclass.(strcat('C',num2str(C(i)))) = struct;
    for j=1:length(row)
        nme = strcat('Jump',num2str(col(j)),'_',num2str(labels(row(j),1)));
        hoj3dbyclass.(strcat('C',num2str(C(i)))).(nme) = M.(nme);
    end
end

mfnames = fieldnames(M);
nrow = 0;
for mf=1:length(mfnames)
    nrow = nrow +1;
    % Get Test instance
    x_te = M.(mfnames{mf}); %Rating is 7
    str = mfnames{mf};
    pid = str2num(str(end-2:end));
    n4 = str2num(str(end-4));
    fstr = strcat('C',num2str(labels(find(labels==pid),n4+1)));
    hoj3dbyclass.(fstr) = rmfield(hoj3dbyclass.(fstr),mfnames{mf});
    
    [n,m] = size(x_te);
    clear m
    c_result = zeros(1,n);
    result = [];
        
    % Just for Class 5
    for i3=1:length(C)
        cfname = fieldnames(hoj3dbyclass.(strcat('C',num2str(C(i3)))));
        c5le = length(cfname);
        for i2=1:n
            c_result(1,i2) = realmax('double');
            for j2 = 1:c5le
                x_tr = hoj3dbyclass.(strcat('C',num2str(C(i3)))).(cfname{j2,1});
                dist = pdist2( x_te(i2,:), x_tr(1,:) , 'emd');
                [n2,m2] = size(x_tr);
                for j3 =2:n2
                    d = pdist2( x_te(i2,:), x_tr(j3,:) , 'emd');
                    if d < dist
                        dist =d;
                    end
                end
                
                if dist < c_result(1,i2)
                    c_result(1,i2) = dist;
                    ref = j2;
                end
                
            end
           
        end
        result(i3) = mean(c_result);
    end
    
    [k,I] = min(result);
    str_pred = C(I);
    pred = str_pred(end);

    results{nrow,1} = strcat('HOJ3D dtw ', 'Jump');
    results{nrow,2} = pid;
    results{nrow,3} = n4;
    results{nrow,4} = labels(find(labels==pid),n4+1);
    results{nrow,5} = pred;
    disp(nrow);
end
   
    write_results(pwd,'dtw',results);
    plot_generate(pwd,'dtw');
    
    
    
    
    
    
    
    
    
    
    
