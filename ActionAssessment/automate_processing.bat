@ECHO off

:: need to find the FMS.csv file to extract the start/finish frames and score from it
set /p session=Enter the session name (e.g. July2013): 
:: assuming fms.csv in current dir
python preprocessing\extract_fms.py %session%
:: add second statement here with different out files if multiple FMS files
GOTO experiment

:: read raw data into individual movements based on segments defined by extract_fms
echo Parsing data from raw input files with Matlab...
cd preprocessing
matlab -nosplash -noFigureWindows -wait -r "extract_dta('%session%'); exit"
cd ..

: experiment
:: not sure if this is how the final batch script should work, but this is an easy
:: toggle for testing purposes
:: set fms=Jump
set /p fms=Which FMS would you like to analyse? (Jump/Kick/OArmThrow/SideStep/UArmThrow)

set /p exp=Which experiment do you want to run? (dtw/svm)

echo exp is %exp%

if "%exp%"== "dtw" GOTO dtw

if "%exp%"== "svm" GOTO svm

echo No experiment selected
GOTO end

:dtw
start cmd /k call "%~dp0\train_dtw.bat" %fms% %session%
set /p wait= Refer to other CMD to run %exp% experiment.


GOTO end

:svm
start cmd /k call "%~dp0\train_svm.bat" %fms% %session%
set /p wait= Refer to other CMD to run %exp% experiment.


:: loop through each subdir in data/%session%/ and calculate features
:: cd data\%session%
:: for	/d %%f IN (*) DO start cmd /k call "%~dp0\train_svm.bat" %%f %session%

:: set /p hey= Waiting for SVM results... Press enter when all other windows have closed.

:: this is where any output could be displayed based on the results, which are
:: stored in transformed_results.csv and raw_results.csv
:end
