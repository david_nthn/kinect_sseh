import sys
import os
import shutil
import csv
import numpy as np
import scipy.stats as st
import math as m
import re
from sklearn.neighbors import NearestNeighbors
from scipy.spatial import cKDTree

#====================================
#  CONSTANTS
#====================================
# file structure related
INPUT_PATH = sys.argv[1] + '\\transformed_eigenjoints\\'
OUTPUT_PATH = os.path.dirname(sys.argv[1]) + '\\nnclass_results.csv' 
LABELS_PATH = os.path.dirname(sys.argv[1]) + '\\labels.csv'
FMS = sys.argv[2]

# name
EXP_NAME = 'Eigenjoints'

# verbose and debug toggles
v = False
# v = True
d = False
# d = True

# read in labels and store them for easy reference by pID
ratings = {}
if d: print(LABELS_PATH)
with open(LABELS_PATH, 'r') as csvfile:
    csvReader = csv.reader(csvfile)
    for rowNo, row in enumerate(csvReader):
        ratings[row[0]] = row[1:4]
        if d: print('Ratings[' + row[0] + '] = ' + str(row[1:4]))

#====================================
#  MAIN FUNCTION
#====================================

""" Get a list of the three trials for this pID, and a set of trees for quick
    NN distance querying of all other frames within each rating.
"""
def splitByPID(thispID, data):
    # store so that we can iterate only over used ratings
    distinctRatings = set([])

    # store distinct frames from each class in a dict
    ratingFrames = {}

    # store three trials for this this pID, maintaining structure/order of frames
    pIDTrials = []

    for (rating,pID,trial), frameList in data.items():
        if pID == thispID:
            pIDTrials.append(frameList)
        else:
            distinctRatings.add(rating)
            if not rating in ratingFrames:
                ratingFrames[rating] = []
            ratingFrames[rating].extend(frameList)

    # build a dict of trees, one for each rating
    trees = {}
    for rating in distinctRatings:
        tree = cKDTree(ratingFrames[rating], leafsize=50)
        trees[rating] = tree

    return pIDTrials, trees



def run(loadPath, outputPath):
    print("Reading in CSVs...")
    
    ratingPIDTrialMap = {}
    pIDList = []

    # read in ALL trials and store in map by rating, pID, trial for easy access
    i = 1
    for file in os.listdir(loadPath):
        subset = re.match(r".*subset.*", file)
        if subset is None: continue # ignore unfiltered csvs, only want feature subset


        # initialise params to store
        pID = 0
        trial = 0

        regex = re.match(r".*[A-Za-z]+([0-9]+)_(\d+).*", file)
        if regex is not None:
            pID = regex.group(2)
            trial = int(regex.group(1))
        
        if pID == 0 or trial == 0: print ('Failed to parse file: ' + file); exit(1)

        #pID = str(file[-14:-11])
        pIDList.append(pID)
        if v:
            print('Reading in file: ' + file)
        #trial = int(file[-16])
        rating = ratings[pID][trial-1]

        # get frames for this movement and store them in the map
        frames = np.genfromtxt(loadPath + file, dtype=float, delimiter=',', skip_header=1)
        ratingPIDTrialMap[(rating,pID,trial)] = frames

        i = i+1

    # converting to/from set removes duplicates, back to list for easy sorting
    pIDSet = list(set(pIDList))
    pIDSet.sort()

    """ Psuedocode for the next block is as follows:
    for each pID:
        kdTreeDict[rating] = all frames from trials with that rating, excluding this pID
        test = the three trials for this pID
        for trial in this pID:
            ratingDistances = [(rating,distance)]     # store all distances in case we want to check confidence later
            for ratingTree in kdTreeDict:
                thisDistance = 0
                for frame in trial:
                    frameDistance = ratingTree.getNNDistance(frame)
                    thisDistance = thisDistance + frameDistance
                ratingDistances[rating] = thisDistance
    """

    if v:
        print('pIDSet:\n' + str(pIDSet))

    # store a list of [pID, trial, actualRating, predictedRating, %difference to next rating]
    scores = [['Experiment Name', 'Person ID', 'Attempt ID', 'Mark', 'Estimated Mark']]

    for pID in pIDSet:
        # get the list of three trials to be tested, and the set of trees to test against
        test, trees = splitByPID(pID, ratingPIDTrialMap)

        # here we need a list of ALL frames for each class, and a list of 

        trialNo = 0
        for trial in test:
            score = [EXP_NAME, pID, (trialNo+1), ratings[pID][trialNo]]

            # store all distances in case we want to check confidence later
            ratingDistances = []
            
            for rating, tree in trees.items():
                if v:
                    print('Tree params (rating,n,m): (' + str(rating) + ',' + str(tree.n) + ',' + str(tree.m) + ')')
                # sum distance across all frames in this trial
                thisDistance = 0
                for frame in trial:
                    # discard index of nearest frame - meangingless, only need distance
                    frameDistance, _ = tree.query(frame,k=1)
                    thisDistance = thisDistance + frameDistance

                # add NN Video-to-Class distance for this rating
                ratingDistances.append((thisDistance, rating))

            # sort to get smallest distance first, i.e. closest class label first
            ratingDistances.sort()

            if v: print('Distances for pID ' + pID + ', trial ' + str(trialNo) + ': ' + str(ratingDistances))

            # add predicted rating and %distance difference to next closest rating 
            score.append(ratingDistances[0][1]) # predicted rating and distance
            #score.append(float(ratingDistances[1][0])/float(ratingDistances[0][0])*100) # % difference to next rating
            #score.append(ratingDistances[1][1]) # next best predicted rating

            if v: print('Score vector for ' + pID + ' is ' + str(score))
            scores.append(score)

            trialNo = trialNo + 1

    with open(OUTPUT_PATH,'w',newline='') as output:
        results = csv.writer(output)
        for row in scores:
            results.writerow(row)

    print("Done")            

run(INPUT_PATH, OUTPUT_PATH)