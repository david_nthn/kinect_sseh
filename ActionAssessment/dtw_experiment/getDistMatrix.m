function [ D ] = getDistMatrix(move_type)
%getDistMatrix This is for the DTW joint experiment
%   Returns struct which contains a njumps by njoints matrix for dtw
%   distance for each csv. 

D = struct;
names = fieldnames(move_type);

for i=1:length(names)
    D.(names{i,1}) = [];
    for row=1:length(names)
        for col=1:20           
            D.(names{i,1})(row,col) = dtw(move_type.(names{i,1}){col,1},move_type.(names{row,1}){col,1});
        end
    end
end

end

