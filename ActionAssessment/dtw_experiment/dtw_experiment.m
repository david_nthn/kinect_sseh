function dtw_experiment(dirname, move_type)
%dtw_experiment Uses HOJ3D to determine nearest-neighbour rating. 
%   Detailed explanation goes here

rootpath = strcat('../data/',dirname,'/',move_type,'/');
featurepath = strcat(rootpath,move_type,'_extracted_features/transformed_hoj3d/');

labels = importdata(strcat(rootpath,'labels.csv'));
hoj3d = readcsv2mot(featurepath);

% TODO: Extract each participant's 3 HOJ3D sequence 
% and use DTW to find the nearest HOJ3D sequence whose label is used 
% as the predicted rating. 

datapoints = fieldnames(hoj3d);
personids = cellfun(@(x) str2num(x(end-2:end)), datapoints, 'UniformOutput', false);
personids = cell2mat(personids);
persons = unique(personids);

results = cell(length(persons)+1,5);
nrow = 1;
for pid=persons'
    % Split into training and testing sets
    tr_rows = find(pid~=personids);
    te_rows = find(pid==personids);

    for n=1:length(te_rows)
        % The Design Matrix
        D = [];
        for i=1:length(tr_rows)
            te_fieldname = datapoints(te_rows(n));
            tr_fieldname = datapoints(tr_rows(i));
            
            H1 = hoj3d.(te_fieldname{1,1});
            H2 = hoj3d.(tr_fieldname{1,1});

            D(i) = dtw(H1,H2,'hist','emd');
        end
        
        %Setup Results file with Actual labels       
        results{nrow,1} = strcat('HOJ3D dtw ', move_type);
        results{nrow,2} = pid;
        results{nrow,3} = n;
        results{nrow,4} = labels(find(labels==pid),n+1);
        %Make prediction from DTW distances
        
        % 1-Nearest Neighbour by DTW distance
        [C, I] = min(D);
        pred_point = datapoints(tr_rows(I));
        str = pred_point{1,1};
        results{nrow,5} = labels(find(labels==str2num(str(end-2:end))),str2num(str(end-4))+1);
        
        %locally-weighted by DTW distance 
        %{
        S = sort(D);
        [C, ia, ib] = intersect(D,S(1:5));
        pred_point = datapoints(tr_rows(ia));
        pred_labels = [];
        for ws = 1:5
            str = pred_point{ws,1};
            pred_labels(ws) = labels(find(labels==str2num(str(end-2:end))),str2num(str(end-4))+1);
        end
        weight = 1./C;
        
        results{nrow,5} = sum(weight.*pred_labels) / sum(weight);
        %}
        
        % Debug column
        %results{nrow,6} = strjoin({str,num2str(min(D))});
        nrow = nrow+1;  
    end        
end
write_results(rootpath,'dtw',results);
end

