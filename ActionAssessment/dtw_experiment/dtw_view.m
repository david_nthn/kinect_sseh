function dtw_view(D,w)
%dtw_view Summary of this function goes here
%   D - accumulated distance matrix
%   w - optimal warping path
%

colormap(1-gray);
imagesc(D);
hold on; plot(w(:,2),w(:,1),'r'); hold off
ylabel('Template (t)');
xlabel('Test Series (r)');

end

