import sys
import os
import shutil
import csv
import numpy as np
from numpy.linalg import norm
import scipy.stats as st
import math as m

#====================================
#  CONSTANTS
#====================================
# file structure related
INPUT_PATH = sys.argv[1]  + '\\transformed\\'
OUTPUT_PATH = sys.argv[2] + '\\transformed_eigenjoints\\'

# orders unused apart from establishing other data structures
LOWER_JOINT_ORDER = {"HipCenter":0,"Spine":1,"ShoulderCenter":2,"Head":3,"ShoulderLeft":4,"ElbowLeft":5,"WristLeft":6,"HandLeft":7,"ShoulderRight":8,"ElbowRight":9,"WristRight":10,"HandRight":11}
UPPER_JOINT_ORDER = {"HipLeft":12,"KneeLeft":13,"AnkleLeft":14,"FootLeft":15,"HipRight":16,"KneeRight":17,"AnkleRight":18,"FootRight":19}

# Joint related
LOWER_JOINT_MAP = {v:k for k, v in LOWER_JOINT_ORDER.items()} # reverse map so that names can be accessed by index for headers
UPPER_JOINT_MAP = {v:k for k, v in UPPER_JOINT_ORDER.items()} # reverse map so that names can be accessed by index for headers
LOWER_JOINT_INDEXES = list(LOWER_JOINT_ORDER.values())
LOWER_JOINT_INDEXES.sort()
UPPER_JOINT_INDEXES = list(UPPER_JOINT_ORDER.values())
UPPER_JOINT_INDEXES.sort()
LOWER_SIZE = len(LOWER_JOINT_ORDER)
UPPER_SIZE = len(UPPER_JOINT_ORDER)
COORD_MAP = {0:"x", 1:"y", 2:"z"}

# verbose and debug toggles
v = False
# v = True
d = True
# d = True

#====================================
#  HELPER FUNCTIONS
#====================================

def getDifference(joint1, joint2):
    # j1 = np.array(joint1)
    # j2 = np.array(joint2)
    # return norm(j1-j2)
    return [joint1[0] - joint2[0], joint1[1] - joint2[1], joint1[2] - joint2[2]]
            
def getPoint(frame, jointIndex):
    x = 3*jointIndex
    return float(frame[x]) , float(frame[x+1]), float(frame[x+2])# (x,y,z) for jointIndex joint

#====================================
#  MAIN FUNCTION
#====================================

def run(loadPath, outputPath):
    print("Determining eigenjoints from spatial data...")

    # generate header
    header = []
    for i in LOWER_JOINT_INDEXES:
        for j in range(i,LOWER_SIZE):
            if not i == j:
                for k in range(3):
                    header.append(str(LOWER_JOINT_MAP[i] + "-" + LOWER_JOINT_MAP[j] + "-" + COORD_MAP[k]))

        for j in LOWER_JOINT_INDEXES:
            for k in range(3):
                header.append(str("prevFrame" + "-" + LOWER_JOINT_MAP[i] + "-" + LOWER_JOINT_MAP[j] + "-" + COORD_MAP[k]))
            for k in range(3):
                header.append(str("firstFrame" + "-" + LOWER_JOINT_MAP[i] + "-" + LOWER_JOINT_MAP[j] + "-" + COORD_MAP[k]))

    for i in UPPER_JOINT_INDEXES:
        for j in range(i,LOWER_SIZE + UPPER_SIZE):
            if not i == j:
                for k in range(3):
                    header.append(str(UPPER_JOINT_MAP[i] + "-" + UPPER_JOINT_MAP[j] + "-" + COORD_MAP[k]))

        for j in UPPER_JOINT_INDEXES:
            for k in range(3):
                header.append(str("prevFrame" + "-" + UPPER_JOINT_MAP[i] + "-" + UPPER_JOINT_MAP[j] + "-" + COORD_MAP[k]))
            for k in range(3):
                header.append(str("firstFrame" + "-" + UPPER_JOINT_MAP[i] + "-" + UPPER_JOINT_MAP[j] + "-" + COORD_MAP[k]))


    # process by jump
    for file in os.listdir(loadPath):
        data = np.genfromtxt(loadPath + file, dtype=float, delimiter=',')
        if v: print(data[1])

        # store results in an array as they are calculated
        outData = [header]
        
        # need  frames for each iteration
        prevFrame = []
        firstFrame = []

        for i in range(len(data)):
            # data for this frame
            eigenjoints = []
            # store joint values in a temp list to update prevFrame at the end
            frame = []
           
            # get LOWER joint differences within this frame
            for jointIndex in LOWER_JOINT_INDEXES:
                joint = getPoint(data[i],jointIndex)
                frame.append(joint)

                if not i == 0:
                    # pair-wise differences within frame - 66 values (number of distinct pairs)
                    for jointIndex2 in range(jointIndex, LOWER_SIZE):
                        if not jointIndex == jointIndex2:
                            joint2 = getPoint(data[i],jointIndex2)
                            eigenjoints.extend(getDifference(joint,joint2))

                    # get previous frame pair-wise differences - 144 pairs * 2 = 288
                    for prevJointIndex in LOWER_JOINT_INDEXES:
                        prevJoint = prevFrame[prevJointIndex]
                        firstJoint = firstFrame[prevJointIndex]
                        eigenjoints.extend(getDifference(joint, prevJoint))
                        eigenjoints.extend(getDifference(joint, firstJoint))

                else:
                    # on first iteration don't append to outData, just save firstFrame
                    firstFrame.append(joint)

            # get UPPER joint differences within this frame
            for jointIndex in UPPER_JOINT_INDEXES:
                joint = getPoint(data[i], jointIndex)
                frame.append(joint)

                if not i == 0:
                    # pair-wise differences within frame - 28 values (number of distinct pairs)
                    for jointIndex2 in range(jointIndex, LOWER_SIZE + UPPER_SIZE):
                        if not jointIndex == jointIndex2:
                            joint2 = getPoint(data[i], jointIndex2)
                            eigenjoints.extend(getDifference(joint,joint2))

                    # get previous frame pair-wise differences - 64 pairs * 2 = 128
                    # we are indexing into the second half of the prev frames - need offset of LOWER_SIZE
                    for prevJointIndex in UPPER_JOINT_INDEXES:
                        prevJoint = prevFrame[prevJointIndex]
                        firstJoint = firstFrame[prevJointIndex]
                        eigenjoints.extend(getDifference(joint, prevJoint))
                        eigenjoints.extend(getDifference(joint, firstJoint))

                else:
                    # on first iteration don't append to outData, just save firstFrame
                    firstFrame.append(joint)

            prevFrame = frame
            if not i == 0: outData.append(eigenjoints)


        fileName = os.path.split(file)[1]
        ''' Given the transformed (not scaled) data, write an output file
        with one row for each frame, where each value in a row is an eigenjoint
        value.
        '''

        with open(outputPath + fileName,'w',newline='') as output:
            csvWriter = csv.writer(output) # for use in extract_features
            # after all joint votes added, write row
            for row in outData:
                csvWriter.writerow(row)

    print("Done")            

def runSeparate(loadPath, outputPath, JOINT_MAP, JOINT_INDEXES, dataset):
    print("Determining eigenjoints from spatial data for dataset: " + dataset)
    SIZE = len(JOINT_MAP)

    # generate header
    header = []
    for i in JOINT_INDEXES:
        for j in range(i, JOINT_INDEXES[0] + SIZE):
            if not i == j:
                for k in range(3):
                    header.append(str(JOINT_MAP[i] + "-" + JOINT_MAP[j] + "-" + COORD_MAP[k]))

        for j in JOINT_INDEXES:
            for k in range(3):
                header.append(str("prevFrame" + "-" + JOINT_MAP[i] + "-" + JOINT_MAP[j] + "-" + COORD_MAP[k]))
            for k in range(3):
                header.append(str("firstFrame" + "-" + JOINT_MAP[i] + "-" + JOINT_MAP[j] + "-" + COORD_MAP[k]))

    # process by jump
    for file in os.listdir(loadPath):
        data = np.genfromtxt(loadPath + file, dtype=float, delimiter=',')
        if v: print(data[1])

        # store results in an array as they are calculated
        outData = [header]
        
        # need  frames for each iteration
        prevFrame = []
        firstFrame = []

        for i in range(len(data)):
            # data for this frame
            eigenjoints = []
            # store joint values in a temp list to update prevFrame at the end
            frame = []
           
            # get LOWER joint differences within this frame
            for jointIndex in JOINT_INDEXES:
                joint = getPoint(data[i],jointIndex)
                frame.append(joint)

                if not i == 0:
                    # pair-wise differences within frame
                    for jointIndex2 in range(jointIndex, JOINT_INDEXES[0] + SIZE):
                        if not jointIndex == jointIndex2:
                            joint2 = getPoint(data[i],jointIndex2)
                            eigenjoints.extend(getDifference(joint,joint2))

                    # get previous frame pair-wise differences
                    for prevJointIndex in range(SIZE):
                        prevJoint = prevFrame[prevJointIndex]
                        firstJoint = firstFrame[prevJointIndex]
                        eigenjoints.extend(getDifference(joint, prevJoint))
                        eigenjoints.extend(getDifference(joint, firstJoint))

                else:
                    # on first iteration don't append to outData, just save firstFrame
                    firstFrame.append(joint)

            prevFrame = frame
            if not i == 0: outData.append(eigenjoints)


        fileName = os.path.split(file)[1]
        ''' Given the transformed (not scaled) data, write an output file
        with one row for each frame, where each value in a row is an eigenjoint
        value.
        '''

        with open(outputPath + dataset + '_' + fileName,'w',newline='') as output:
            csvWriter = csv.writer(output) # for use in extract_features
            # after all joint votes added, write row
            for row in outData:
                csvWriter.writerow(row)

    print("Done")            

if os.path.exists(OUTPUT_PATH):
    shutil.rmtree(OUTPUT_PATH)
os.mkdir(OUTPUT_PATH)

runSeparate(INPUT_PATH, OUTPUT_PATH, LOWER_JOINT_MAP, LOWER_JOINT_INDEXES, 'lower')
runSeparate(INPUT_PATH, OUTPUT_PATH, UPPER_JOINT_MAP, UPPER_JOINT_INDEXES, 'upper')