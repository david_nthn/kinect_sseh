function write_results(rootpath,experiment,cellarray)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

[nrows,ncols]= size(cellarray);

filename = strcat(rootpath,'/',experiment,'_results.csv');
fid = fopen(filename, 'w');
fprintf(fid,'Experiment Name, Person ID, Attempt ID, Mark, Estimated Mark\n');

for row=1:nrows
    fprintf(fid, '%s, %d, %d, %d, %d\n', cellarray{row,:});
end

fclose(fid);

end

