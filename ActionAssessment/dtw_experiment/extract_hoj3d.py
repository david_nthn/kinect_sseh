import sys
import os
import shutil
import csv
import numpy as np
import scipy.stats as st
import math as m

#====================================
#  HOJ3D CONSTANTS
#====================================


# number of bins to use
INCLIN_NUM = 7
AZIMUTH_NUM = 12
NUM_BINS = INCLIN_NUM * AZIMUTH_NUM
# slightly more complicated than azimuth due to half size bin at top and bottom
INCLIN_BINS = np.concatenate([[(0,180/(INCLIN_NUM-1)/2)],
              [(x,x+30) for x in range(int (180/(INCLIN_NUM-1)/2),int (180-180/(INCLIN_NUM-1)/2),int (180/(INCLIN_NUM-1)))],
              [(180 - 180/(INCLIN_NUM-1)/2,180)]])
AZIMUTH_BINS = [(x,x+360/AZIMUTH_NUM) for x in range(0,int (360-360/AZIMUTH_NUM+1),int (360/AZIMUTH_NUM))]
# 3 std dev to one bin width
STD_DEV = 360/AZIMUTH_NUM/3

# the 9 joints required for the HOJ3D histogram representation
JOINTS = {"Head":3,"ElbowLeft":5,"WristLeft":6,"ElbowRight":9,"WristRight":10,"KneeLeft":13,"AnkleLeft":14,"KneeRight":17,"AnkleRight":18}
JOINT_LIST = sorted(list(JOINTS.values()))

#====================================
#  OTHER CONSTANTS
#====================================
# file structure related
INPUT_PATH = sys.argv[1]  + '\\transformed\\'
OUTPUT_PATH = sys.argv[2] + '\\transformed_hoj3d\\'

# verbose and debug toggles
v = False
# v = True
d = True
# d = True

#====================================
#  HELPER FUNCTIONS
#====================================

def cartToSph(x,y,z):
    '''convert cartesian coordinates to spherical coordinates'''
    x2z2 = m.pow(x,2)+m.pow(z,2)
    r = m.sqrt(m.pow(y,2) + x2z2)
    

    y = np.nextafter(y,1)

    # inclination angle is between the vector and the y-axis, and is confined to [0,180]
    inclin = abs(np.arctan2(m.sqrt(x2z2),y)) * 180/np.pi
    
    # z axis is depth rel to kinect, x is horizontal -> azimuthal in this plane
    azimuth = np.arctan2(z,x) * 180/np.pi
    # shift to [0,360] rather than [-180,180]
    azimuth = azimuth if azimuth > 0 else 360 + azimuth
    
    return r, inclin, azimuth
    
def getBins(alpha,theta):
    ''' Find the respective indexes of the bins in AZIMUTH_BINS and 
    INCLIN_BINS in which the value (alpha,theta) lies.
    '''
    return [binHelper(alpha,AZIMUTH_BINS),binHelper(theta,INCLIN_BINS)]

def getBinsH(histogramBin):
    ''' Find the respective bins in AZIMUTH_BINS and 
    INCLIN_BINS by reverse engineering them from the histogram bin number.
    '''
    aziBin = AZIMUTH_BINS[histogramBin % AZIMUTH_NUM]
    inclinBin = INCLIN_BINS[int (histogramBin / AZIMUTH_NUM)]
    return (aziBin, inclinBin)
    
def getBin(alpha,theta):
    ''' Returns the overall histogram bin number for a given azimuthal angle
    (alpha) and inclination angle (theta), which is in the range:
    [0, INCL_NUM * AZIMUTH_NUM - 1]
    '''
    theta = theta % 180
    alpha = alpha % 360
    angleBins = getBins(alpha,theta)
    return angleBins[1]*AZIMUTH_NUM + angleBins[0]
    
def binHelper(value, binList):
    ''' returns the index into binList that corresponds to the bin for this value
    e.g. binHelper(10, [(0,5),(5,10),(10,15)]) = 1 '''
    i = 0;
    for (start,end) in binList:
        if abs(value) >= start and abs(value) <= end:
            return i
        i=i+1
    raise Exception("ERROR")

def getVote(gaussians,histogramBin):
    ''' Returns the vote for this value and bin, calculated using probabilistic
    voting with a Gaussian centred on the value.
    '''
    (azim, inclin) = getBinsH(histogramBin)
    # take difference between boundaries of the bin in the CDF
    azimVote = abs(gaussians[0].cdf(azim[1]) - gaussians[0].cdf(azim[0]))
    inclinVote = abs(gaussians[1].cdf(inclin[1]) - gaussians[1].cdf(inclin[0]))
    #print ("calculated vote of " + str (azimVote * inclinVote))
    # joint vote is the product - assume independence of error for theta,alpha
    return azimVote * inclinVote
    
def getVotes(x,y,z):
    ''' Gets histogram votes for each of the nine bins that this value
    contributes a vote towards. They are returned as dict of the form:
        {histogramBinNumber: vote}
    '''
    (r, theta, alpha) = cartToSph(x,y,z)
    if theta >= 180:
        raise Exception("Theta was outside the expected range: " + theta)
    
    if theta >= 135 and d:
        print ("Theta greater than 135: " + str(theta))

    gaussians = (st.norm(alpha, STD_DEV), st.norm(theta, STD_DEV))
    (azim,inclin) = getBins(alpha,theta)

    if v: print ("Bins were " + str(azim) + " " + str (inclin))

    # choose range to account for edge cases where bin has same neighbour on both sides
    inclinRange = []
    if inclin == INCLIN_NUM -1:
        inclinRange = [inclin-1,inclin,inclin-1]
    elif inclin == 0:
        inclinRange = [inclin+1,inclin,inclin+1]
    else:
        inclinRange = [inclin-1,inclin,inclin+1]

    voteMap = {}
    for i in range(azim-1,azim+2):
        for j in inclinRange:
            i = i%12
            bin = j*AZIMUTH_NUM + i
            voteMap[bin] = getVote(gaussians, bin)

    if v: print ("VoteMap is " + str (voteMap))

    return voteMap
            
def getPoint(frame, jointIndex):
    x = 3*jointIndex
    return float(frame[x]) , float(frame[x+1]), float(frame[x+2])# (x,y,z) for jointIndex joint

############# DEBUG #############
def printBin(inclin,azim):
    str1 = "Bin for ({0}, {1}) is {2}"
    str2 = "Bins for ({0}, {1}) are {2}"
    str3 = "BinsH for {0} are {1}"
    binsStr = str(getBins(azim,inclin))
    print (str1.format(inclin, azim, getBin(azim,inclin)))
    print (str2.format(inclin, azim, binsStr))
    print (str3.format(binsStr, str(getBinsH(getBin(azim,inclin)))))

def printCartToSph(x,y,z):
    str1 = "(theta,alpha) for {0} is {1}, and the histogram bin is {2}"
    (r,inclin,az) = cartToSph(x,y,z)
    bin = getBin(az,inclin)
    print (str1.format(str((x,y,z)), str((inclin,az)), bin))

if d: 
    print (INCLIN_BINS)
    print (AZIMUTH_BINS)
    # print (getBins(20,30))
    # print (getBins(140,90))
    # print (getBin(90,10))
    # print (getBin(350,10))
    # print (getBin(10,90))
    printBin(10,175)
    printBin(140,175)
    printBin(150,175)
    printBin(165,175)
    printBin(180,175)
    printBin(180,300)
    printBin(180,340)
    printCartToSph(0,0,0)
    printCartToSph(0,0,1)
    printCartToSph(0,1,0)
    printCartToSph(0,1,1)
    printCartToSph(1,1,1)
    printCartToSph(-1,-1,-1)
    printCartToSph(0,-1,-1)
    printCartToSph(0.5,-2,-0.5)
    printCartToSph(-0.06492184610874638,-0.8354699999999999,0.019927667270211412)
    printCartToSph(-0.0710550989243361,-0.75854,-0.021407594116407077)


############# DEBUG #############

def run(loadPath, outputPath):
    # process by jump
    for file in os.listdir(loadPath):
        data = np.genfromtxt(loadPath + file, dtype=float, delimiter=',')
        print(data[1])

        # store results in an array as they are calculated
        histogramArray = np.zeros((len(data),NUM_BINS),float)
        if v: print ("histogramArray has dimensions: " + str(len(histogramArray)) + " " + str(len(histogramArray[0])))
        for i in range(len(data)):
            for jointIndex in JOINT_LIST:
                p = getPoint(data[i],jointIndex)
                votes = getVotes(p[0],p[1],p[2])
                for bin, vote in votes.items():
                    histogramArray[i][bin] += vote
                    if v: print("Added vote of " + str(vote) + " to bin " + str(bin) + ", which is now " + str(histogramArray[i][bin]))

        # with open(loadPath + file,'r') as csvfile:
        #         csvReader = csv.reader(csvfile)
        #         data = []
        #         for rowNo,row in enumerate(csvReader):
        #          data.append(list(row))
        #          #print(data[1])


        fileName = os.path.split(file)[1]
        ''' Given the transformed (not scaled) data, write an output file
        with one row for each frame, where each value in a row is the vote for the
        corresponding histogramArray bin. Output is a CSV with (AZIMUTH_NUM*INCLIN_NUM)
        values per line (i.e. that many histogramArray bins).
        '''

        with open(outputPath + fileName,'w',newline='') as output:
            histogramWriter = csv.writer(output) # for use in extract_features
            # after all joint votes added, write row
            for row in histogramArray:
                histogramWriter.writerow(row.tolist())

    print("Done")            

if os.path.exists(OUTPUT_PATH):
    shutil.rmtree(OUTPUT_PATH)
os.mkdir(OUTPUT_PATH)

run(INPUT_PATH, OUTPUT_PATH)