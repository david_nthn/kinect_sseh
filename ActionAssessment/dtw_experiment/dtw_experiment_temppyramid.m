function dtw_experiment_temppyramid(dirname, move_type)
%dtw_experiment Uses HOJ3D to determine nearest-neighbour rating and
%Temporal Pyramid to mitigate misalignment.
%   Detailed explanation goes here

rootpath = strcat('../data/',dirname,'/',move_type,'/');
featurepath = strcat(rootpath,move_type,'_extracted_features/transformed_hoj3d/');

labels = importdata(strcat(rootpath,'labels.csv'));
hoj3d = readcsv2mot(featurepath);

% TODO: Extract each participant's 3 HOJ3D sequence 
% and use DTW to find the nearest HOJ3D sequence whose label is used 
% as the predicted rating. 

datapoints = fieldnames(hoj3d);
personids = cellfun(@(x) str2num(x(end-2:end)), datapoints, 'UniformOutput', false);
personids = cell2mat(personids);
persons = unique(personids);

results = cell(length(persons)+1,5);
nrow = 1;
for pid=persons'
    % Split into training and testing sets
    tr_rows = find(pid~=personids);
    te_rows = find(pid==personids);

    for n=1:length(te_rows)
        % The Design Matrix
        D = [];
        for i=1:length(tr_rows)
            te_fieldname = datapoints(te_rows(n));
            tr_fieldname = datapoints(tr_rows(i));
            
            H1 = hoj3d.(te_fieldname{1,1});
            H2 = hoj3d.(tr_fieldname{1,1});
            
            % DTW Temporal Pyramid
            % Level 1
            featurevector(1) = dtw(H1,H2,'hist','emd');
            % Level 2 
            [nrows1,ncols1] = size(H1); [nrows2,ncols2]=size(H2);
            midpnt1 = nrows1/2; midpnt2 = nrows2/2;
            featurevector(2) = dtw(H1(1:midpnt1,:),H2(1:midpnt2,:),'hist','emd');
            featurevector(3) = dtw(H1(midpnt1:nrows1,:),H2(midpnt2:nrows2,:),'hist','emd');
            % Level 3
            l3_midpnt1 = midpnt1/2; l3_midpnt2 = midpnt2/2;
            l3_upper_midpnt1 = l3_midpnt1 + midpnt1; l3_upper_midpnt2 = l3_midpnt2 + midpnt2;
            featurevector(4) = dtw(H1(1:l3_midpnt1,:),H2(1:l3_midpnt2,:),'hist','emd');
            featurevector(5) = dtw(H1(l3_midpnt1:midpnt1,:),H2(l3_midpnt2:midpnt2,:),'hist','emd');
            featurevector(6) = dtw(H1(midpnt1:l3_upper_midpnt1,:),H2(midpnt2:l3_upper_midpnt2,:),'hist','emd');
            featurevector(7) = dtw(H1(l3_upper_midpnt1:nrows1,:),H2(l3_upper_midpnt2:nrows2,:),'hist','emd');
            
            %Take mean of DTW Temporal Pyramid
            D(i) = mean(featurevector);
            
        end
        [C,I] = min(D);
        pred_point = datapoints(tr_rows(I));
        results{nrow,1} = strcat('HOJ3D dtw ', move_type);
        results{nrow,2} = pid;
        results{nrow,3} = n;
        results{nrow,4} = labels(find(labels==pid),n+1);
        str = pred_point{1,1};
        results{nrow,5} = labels(find(labels==str2num(str(end-2:end))),str2num(str(end-4))+1);
        %results{nrow,6} = strjoin({str,num2str(min(D))});
        nrow = nrow+1;  
    end        
end
write_results(rootpath,'dtw',results);
end

