import sys
import os
import shutil
import csv
import numpy as np
import scipy.stats as st
import math as m

#====================================
#  CONSTANTS
#====================================
# file structure related
INPUT_PATH = sys.argv[1]  + '\\transformed_eigenjoints\\'
OUTPUT_PATH = sys.argv[1] + '\\'

# verbose and debug toggles
v = False
# v = True
d = True
# d = True

#====================================
#  MAIN FUNCTION
#====================================

def run(loadPath, outputPath):
    print("Merging all csvs into one MEGACSV...")
    # process by jump
    with open(outputPath + 'megafile.csv','w',newline='') as output:
        csvWriter = csv.writer(output) # for use in extract_features

        i = 0
        for file in os.listdir(loadPath):
            # write header
            if i == 0:
                with open(INPUT_PATH + file, 'r') as csvfile:
                    csvReader = csv.reader(csvfile)
                    header = next(csvReader)
                    csvWriter.writerow(header)

            print('Copying file #' + str(i))
            data = np.genfromtxt(loadPath + file, dtype=float, delimiter=',')
            first=True # skip header
            # after all joint votes added, write row
            for row in data:
                if first: first=False; continue
                csvWriter.writerow(row)
            i = i+1
        
    print("Done")            

run(INPUT_PATH, OUTPUT_PATH)