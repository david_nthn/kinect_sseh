import sys
import os
import shutil
import csv
import numpy as np
import scipy.stats as st
import math as m

#====================================
#  CONSTANTS
#====================================
# file structure related
INPUT_PATH = sys.argv[1] + '\\'
OUTPUT_PATH = sys.argv[1]  + '\\transformed_eigenjoints\\'

# verbose and debug toggles
v = False
# v = True
d = True
# d = True

#====================================
#  MAIN FUNCTION
#====================================

def run(loadPath, outputPath):
    print("Separating MEGACSV_subset into its constituents...")
    # process by jump
    with open(loadPath + 'megafile_subset.csv','r') as input:
        reader = csv.reader(input) # for use in extract_features
        header = next(reader)

        i = 0
        for file in os.listdir(outputPath):
            newFile = OUTPUT_PATH + file[:-4] + '_subset.csv'
            print('Copying file #' + str(i))
            # count number of rows in this file
            with open(OUTPUT_PATH + file,'r') as original, \
                open(newFile,'w',newline='') as subset:
                row_count = sum(1 for row in original) - 1

                # write the next row_count rows from the compiled csv to file
                writer = csv.writer(subset)
                writer.writerow(header)
                for i in range(row_count):
                    writer.writerow(next(reader))

            i = i+1
        
    print("Done")            

run(INPUT_PATH, OUTPUT_PATH)