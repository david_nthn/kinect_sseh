function [a,b] = avg_trajectory(u,v)
%avg_trajectory Takes in two trajectories as vectors and returns the
% average of the two
%   If two vectors do not have the same dimension, then DTW is used to find
%   optimal warping path.
%b=0;
if length(u) ~= length(v)
    [Dist,D,k,w] = dtw(u,v);
    k = int64(k);
    % Placeholders for warped vectors
    dtwu= zeros([1,k]); dtwv=zeros([1,k]);
    for i=1:k
        dtwu(k+1-i) = u(w(i,1));
        dtwv(k+1-i) = v(w(i,2));
    end
    u= dtwu;
    v= dtwv;    
end

% Initialize to zero
a = zeros([length(u),1]);

for i=1:length(u)
    a(i) = (u(i)+(v(i)))/2;
end

end

