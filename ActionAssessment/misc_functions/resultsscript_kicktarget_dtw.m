clear all;

ktrating = struct;
filename = strcat(pwd,'\ratings_labels\kt_labels.csv');
A = importdata(filename);

for i=1:length(A.data)
    id = num2str(A.data(i,1));
    
    for j=2:4
        name = strcat(A.textdata{1,j},'_',id);
        ktrating.(name) = A.data(i,j);
    end
end

% calculated DTW data
load('MTHW_KT_DTW.mat');

% Alternative
%foldername = strcat(pwd, '\kt_csv\');
%foldername = strcat(pwd, '\kt_csv_processed\transformed\');
%kicktarget= readcsv2mot(foldername);
%M = getDistMatrix(kicktarget);

[RP, Rating] = jointRatingPredict(M,ktrating);

fnames = fieldnames(Rating);
prating = [];
arating = [];
Z = zeros(10);
se = [];
mse =0;

for i=1:length(fnames)
    prating(i) = mode(Rating.(fnames{i,1})(1,:));
    arating(i) = ktrating.(upper(fnames{i,1}));
end

res = prating(1,:) - arating(1,:);
rmse = sqrt(mean(res.^2));

disp(['For DTW rating prediction RMSE= ',num2str(rmse)]);

for i=1:length(fnames)
    Z(prating(1,i),arating(1,i)) = Z(prating(1,i),arating(1,i)) +1;
end

for i=1:length(Z)
    s=sum(Z(:,i));
    if s ~= 0
        Z(:,i) = Z(:,i)./s;
        Z(:,i) = Z(:,i).*100;
    end
end

contourf(Z);
colorbar;
title('Kick for Target DTW Accuracy');
xlabel('Actual Rating');
ylabel('Predicted Rating');