import sys
import os
import shutil
import csv
import numpy as np
import scipy.stats as st
import math as m

#====================================
#  CONSTANTS
#====================================
# file structure related
INPUT_PATH = sys.argv[1] # root FMS folder\labels.csv

ALL_LABELS = range(10)
# map of old labels to new labels
LABEL_MAP = {
    '1' : str('1')  , '2' : '1', '3' : '1', '4' : '1',
    '5' : '1', '6' : '2', '7' : '3',
    '8' : '4', '9' : '5', '10' : '5'
}
# REVERSE_MAP = {
#     1 = range(1-4),
#     2 = range(5-7),
#     3 = range(8-10)
# }

# verbose and debug toggles
v = False
# v = True
d = True
# d = True

#====================================
#  HELPER FUNCTIONS
#====================================


############# DEBUG #############



############# DEBUG #############

def run(loadPath):
    print("Reading in labels")

    with open(loadPath,'r') as csvfile:
        csvReader = csv.reader(csvfile)
        data = []
        for rowNo,row in enumerate(csvReader):
            if v: print (str(row))
            newRow = []
            # replace label with new label, leaving person IDs unchanged
            first = True
            for label in row:
                if first: first = False; newRow.append(label); continue # skip person ID
                # try:
                newRow.extend(LABEL_MAP[label])
                pass
                # except Exception as e:
                    # print ("Given an unexpected label value: " + e)
            data.append(newRow)

    print("Writing new labels to file")

    with open(loadPath,'w',newline='') as output:
        csvWriter = csv.writer(output)
        for row in data:
            csvWriter.writerow(row)

    print("Done")            

run(INPUT_PATH)