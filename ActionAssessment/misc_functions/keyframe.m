% Script to calculate distribution of joints in 3D space throughout motion
% The quadratic sum of the eigenvalues is used as the space distribution

% Assumes that motion is already stored in nx60 matrix A where n is the
% number of frames.
P = mat2cell(A, ones([1 size(A,1)]), [13*3 7*3]); % Joint 1-13 are Upper, 14-20 are Lower
P = cellfun(@(x) reshape(x,size(x,2)/3,3), P, 'UniformOutput',false);

p_u = cell2mat(P(:,1));
p_l = cell2mat(P(:,2));

[coeffs, p_u_star] = princomp(p_u);
[coeffs, p_l_star] = princomp(p_l);
clear coeffs

D_u = sqrt(trace(p_u_star*p_u_star'));
D_l = sqrt(trace(p_l_star*p_l_star'));

if D_l >= D_u % Lower Joints if larger spatial distribution over entire motion and vice versa
    S_M  = cell2mat(cellfun(@(x) sqrt(trace(x*x')), P(:,2), 'UniformOutput',false));
elseif D_l < D_u
    S_M  = cell2mat(cellfun(@(x) sqrt(trace(x*x')), P(:,1), 'UniformOutput',false));
end

plot(S_M,'b*');