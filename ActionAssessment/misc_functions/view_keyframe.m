% Script for Viewing the KeyFrames of all Jumps for a particular session
% Place in same directory as folder containing CSV of Jumps

contents = dir('./Jump_csv/');
figure;
var = cell(1,1);

% First two values are subdirs
for i=3:length(contents)
    A = importdata(strcat('./Jump_csv/',contents(i,1).name));
    clf;
    hold all
    keyframe;
    x = 1:length(S_M);
    S_M = smooth(x,S_M,0.1','loess');
    plot(S_M,'g--');
    var{i,1} = contents(i,1).name; var{i,2} = {S_M};
    [pks,locs] = findpeaks(S_M, 'Threshold',0.0001);
    plot(locs,pks+0.01,'k^','markerfacecolor',[1 0 0]);
    numlmax = size(locs,1);
    [pks,locs] = findpeaks(-S_M, 'Threshold',0.0001);
    plot(locs,-pks+0.01,'k^','markerfacecolor',[0 1 0]);
    numlmin = size(locs,1);
    disp({contents(i,1).name D_l D_u numlmax numlmin});
    pause;
end
