function [ a,b,c ] = extract3(mot,a1,a2,b1,b2,c1,c2)
%extract3 Extracts the 3 segments of joint trajectories from mot specified 
% by the frame numbers that start and end each segment

a = cell(1,1);
b = cell(1,1);
c = cell(1,1);
 
 for i=1:20
     a{i,1} = mot.jointTrajectories{i,1}(:,a1:a2);
     b{i,1} = mot.jointTrajectories{i,1}(:,b1:b2);
     c{i,1} = mot.jointTrajectories{i,1}(:,c1:c2);
 end

end

