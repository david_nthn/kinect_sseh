clear all;

% Use features (raw or transformed) of kick for target
filename = strcat(pwd,'\experiment_features\kt_extracted_features\transformed_features.csv');
A = importdata(filename);

% Request the number of nearest values to use
k = input('please enter the value of k: ');

% Separate dataset into inputs X and target y
X = A.data(:,1:749);
y = A.data(:,752);

% Predicted y target values
y_star =[];

% Find unique person ids
personids = A.data(:,750);
persons = unique(personids);

% Test on each person id
for pid=persons'
    % Split into training and testing sets
    tr_rows = find(pid~=personids);
    te_rows = find(pid==personids);
    
    X_tr = X(tr_rows,:);
    y_tr = y(tr_rows,:);
    
    X_te = X(te_rows,:);
    y_te = y(te_rows,:);
    
    % Make prediction for each test point
    for i=1:length(te_rows)
        [id,dist] = knnsearch(X_tr, X_te(i,:), 'k', k, 'Distance', 'euclidean');
        weight = 1./dist';
        y_star(te_rows(i),1) = sum(weight.*y_tr(id)) / sum(weight);
    end    
end

res = y_star-y;
rmse = sqrt(mean(res.^2));
disp(['For ',num2str(k),'-NN weighted regression RMSE= ',num2str(rmse)]);

% Round predicted ratings to nearest integer
y_star = round(y_star);
Z= zeros(10);

for j=1:length(y)
    Z(y_star(j),y(j)) = Z(y_star(j),y(j)) + 1;
end

for i=1:length(Z)
    s=sum(Z(:,i));
    if s ~= 0
        Z(:,i) = Z(:,i)./s;
        Z(:,i) = Z(:,i).*100;
    end
end

contourf(Z);

colorbar;
title('Kicktarget KNN Accuracy');
xlabel('Actual Rating');
ylabel('Predicted Rating');
