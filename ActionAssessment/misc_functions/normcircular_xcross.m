function [c_uv] = normcircular_xcross(u,v)
%normcircular_xcross Calculates the normalized circular cross-correlation 
% of two vectors. If they do not have the same dimension, then DTW is used
% to calculate optimal warping path.

if length(u) ~= length(v)
    [Dist,d,k,w] = dtw(u,v);
    k = int8(k);
    % Placeholders for warped vectors
    dtwu= zeros([1,k]); dtwv=zeros([1,k]);
    for i=1:k
        dtwu(k+1-i) = u(w(i,1));
        dtwv(k+1-i) = v(w(i,2));
    end
    u = dtwu;
    v = dtwv;
end

% Product of the lengths of the mean difference is normalizing factor.
nu = norm(u-mean(u),2);
nv = norm(v-mean(v),2);
factor = 1/(nu*nv);

% Circular Cross-Correlation can be computed as 
% F^(-1)[F[u].F[R(v)]] where F is the discrete Fourier transform and R is
% the reflection of the time-series vector.
c_uv = ifft(fft(u-mean(u))./fft(fliplr(v)-mean(v)))*factor;

end

