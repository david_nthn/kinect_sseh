clear all;

jrating = struct;
filename = './data/OctoberHolidays2013/Jump/labels.csv';
A = importdata(filename);

for i=1:length(A)
    id = num2str(A(i,1));
    
    for j=2:4
        name = strcat('Jump',num2str(j-1),'_',id);
        jrating.(name) = A(i,j);
    end
end

% calculated DTW data
load('DistM.mat');

% Alternative
%foldername = './data/OctoberHolidays2013/Jump/Jump_csv/';
%foldername = strcat(pwd, '\jumps_csv_processed\transformed\');
%jumps= readcsv2mot(foldername);
%M = getDistMatrix(jumps);


[RP, Rating] = jointRatingPredict(M,jrating);

fnames = fieldnames(Rating);
prating = [];
arating = [];
Z = zeros(10);
se = [];
mse =0;

for i=1:length(fnames)
    prating(i) = mode(Rating.(fnames{i,1})(1,:));
    arating(i) = jrating.(fnames{i,1});
end

for i=1:length(fnames)
    se(i) = (prating(1,i) - arating(1,i))^2;
    mse = mse + se(i);
end

res = prating(1,:) - arating(1,:);
rmse = sqrt(mean(res.^2));

disp(['For DTW rating prediction RMSE= ',num2str(rmse)]);

for i=1:length(fnames)
    Z(prating(1,i),arating(1,i)) = Z(prating(1,i),arating(1,i)) +1;
end


for i=1:length(Z)
    s=sum(Z(:,i));
    if s ~= 0
        Z(:,i) = Z(:,i)./s;
        Z(:,i) = Z(:,i).*100;
    end
end

contourf(Z);

colorbar;
title('Jump DTW Accuracy');
xlabel('Actual Rating');
ylabel('Predicted Rating');