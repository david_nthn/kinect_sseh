@echo off

:: param passed in is the name of this fms, need to create folders accordingly
set fms=%1
:: directory this fms is in
set fmsdir=data\%2\%fms%
set procdir=%~dp0preprocessing
set expdir=%~dp0dtw_experiment
set csv="%fmsdir%\%fms%_csv"
set processed="%fmsdir%\%fms%_csv_processed"
set extracted=%fmsdir%\%fms%_extracted_features


:: simplify labels before extracting features if this is desirable
::echo Simplifying labels to a limited scale...
::python misc_functions\simplify_labels.py "%fmsdir%\labels.csv"
:: COMMENT THE ABOVE TWO LINES OUT IF THE SCALE OF 1-10 IS TO BE USED RATHER THAN 1-3

set /p data=Would you like to use HOJ3D distance, eigenjoints, or raw spatial data? (hoj/eig/raw)

echo Data format is %data%

if "%data%"== "hoj" GOTO hoj
if "%data%"== "eig" GOTO eig
if "%data%"== "raw" GOTO raw

set /p wait=Press Enter to generate features and train DTW

:hoj
echo Finished transform and beginning HOJ3D...
::del "%~dp0%fmsdir%\extract_hoj3d_out.txt"
::type NUL > "%~dp0%fmsdir%\extract_hoj3d_out.txt"
::python "%expdir%\extract_hoj3d.py" %processed% %extracted% >> "%~dp0%fmsdir%\extract_hoj3d_out.txt"
GOTO raw

:eig
::echo Skipping eigenjoint extraction and CSV merging.
GOTO fs
echo Finished transform and beginning eigenjoints...
type NUL > "%~dp0%fmsdir%\extract_eigenjoints_out.txt"
python "%expdir%\extract_eigenjoints.py" %processed% %extracted%

echo Completed. Beginning merging/PCA process.

echo Merging all eigenjoints csvs into one file for feature reduction
python "%expdir%\merge_all.py" %extracted%

set /p alg=How would you like to perform attribute selection? (pca/relf/cfs)

echo Performing attribute selection with algorith: %alg%
set csv=%extracted%\megafile.csv
echo %csv%
echo %alg%
java -Xms8192m -Xmx8192m -jar "%expdir%\select_attributes.jar" "Progname" "%csv%" "%alg%" "128"


echo Separating reduced data into separate files...
python "%expdir%\separate_processed.py" %extracted%

set /p wait=Waiting.
:fs
echo Attribute selection completed, performing Nearest Neighbour classification.
python "%expdir%\NN_video_to_class.py" %extracted% %fms%
GOTO analysis


:raw
set extracted=%~dp0%fmsdir%\%fms%_extracted_features

:: now train DTW using dtw_experiment. 
cd /d "%~dp0\dtw_experiment\"
matlab -nosplash -noFigureWindows -wait -r "dtw_experiment('%2%','%fms%'); exit"

:: this is where any output could be displayed based on the results, which are
:: stored in dtw_results.csv
:analysis
echo Performing analysis in Matlab.
:: change to analysis directory to generate plot.
cd /d "%~dp0\analysis\"
matlab -nosplash -noFigureWindows -wait -r "plot_generate('..\%fmsdir%','nnclass'); exit"

set /p wait= Experiment Finished. Press Enter to exit.
exit
