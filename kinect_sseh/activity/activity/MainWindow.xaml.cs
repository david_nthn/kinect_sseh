﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Windows.Threading;


namespace activity
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public delegate void AutoTimerUp(object sender, EventArgs e);
    public partial class MainWindow : Window
    {
        DTimer test = new DTimer();

        int recsize = 508;
        long maxlim = 10000L;
        bool timerEnabled = false;
        string secondstring = @"c:\\kinectdata\c00000001.dta";
        double[, ,] dta = new double[2, 20, 3];
        char[,] tst = new char[2, 20];
        double[] obs = new double[] { 0.0, 0.0, 0.0 }; /* observation point */
        const double conv = 57.29577951308232; /* radians to degrees */
        const double pi = 3.141592653589793;
        double depth = 4.25;
        const double maxx = 400.0;
        const double maxy = 400.0;
        const int points = 20;
        double[] fcs = new double[] { 0.0, 0.0, 4.25 }; /* focus point - center of screen */
        double[,] mp = new double[points, 2];
        int[] alv = null;
        double[] ap = new double[16];
        const int grandtot = 13;
        const int postot = 14;
        const int movtot = 15;
        const int smooth = 2;

        string projectDir=@"C:\kinectdata\";
        /// <summary>
        /// to track project version so that old files could be opened by new program
        /// </summary>
        int version; 
        
        public MainWindow()
        {
            InitializeComponent();
            this.populateSessionComboBox();
           // UpdateList();
           // OpenFile();
            test.DoMe += new AutoTimerUp(test_DoMe);
        }

        void test_DoMe(object sender, EventArgs e)
        {
            if (timerEnabled)
            {
                var i = Int32.Parse(textBox1.Text);
                if (++i >= maxlim) i--;
                textBox1.Text = i.ToString();
                DrawIt();

            }
        }

        private void UpdateList()
        {
            string masterstring =GetFullFile("recordings.txt");
            listBox1.Items.Clear();
            if (File.Exists(masterstring))
            {
                using (StreamReader b = new StreamReader(masterstring))
                {
                    string s;
                    int index = 0;//line number  of a record in recordings.txt
                    while ((s = b.ReadLine()) != null)
                    {

                        if (!s.Contains("Calibrat") && s.Length > 0)
                        {
                            ListBoxItem li = new ListBoxItem();
                            li.Content = s;
                            li.Tag = "" + index;
                            listBox1.Items.Add(li);
                        }

                        index++;
                    }
                }
            }
        }

        private double Jat(double v1, double v2)
        {
            if (v2 == 0.0)
            {
                if (v1 == 0.0) return 0.0;
                if (v1 >= 0.0) return pi / 2.0;
                else return pi * 1.5;
            }
            double ang = Math.Atan(v1 / v2);
            if (v2 < 0.0) ang += pi;
            return ang;
        }

        private void DrawIt()
        {
            double l, tim;
            string nw = "";


            depth = Math.Sqrt(Math.Pow(fcs[0] - obs[0], 2) + Math.Pow(fcs[1] - obs[1], 2) + Math.Pow(fcs[2] - obs[2], 2));

            double zx = Math.Pow(obs[0] - fcs[0], 2) + Math.Pow(obs[2] - fcs[2], 2);
            double th1 = Jat(fcs[1] - obs[1], Math.Sqrt(zx));
            double th2 = Jat(fcs[0] - obs[0], fcs[2] - obs[2]);
            double[] tmp = new double[3];
            using (BinaryReader b = new BinaryReader(File.Open(secondstring, FileMode.Open)))
            {
                b.BaseStream.Seek((long)Int32.Parse(textBox1.Text) * (long)recsize, SeekOrigin.Begin);
                for (int j = 0; j < 20; j++)
                {
                    dta[0, j, 0] = b.ReadDouble();
                    dta[0, j, 1] = b.ReadDouble();
                    dta[0, j, 2] = b.ReadDouble();
                    tst[0, j] = b.ReadChar();
                }
                tim = b.ReadDouble();
            }
            for (int i = 0; i < points; i++)
            {   /* translate point by observer position */
                tmp[0] = dta[0, i, 0] - obs[0];
                tmp[1] = dta[0, i, 1] - obs[1];
                tmp[2] = dta[0, i, 2] - obs[2];
                /* rotate by negative of observer z-x orientation */
                l = tmp[0] * Math.Cos(th2) - tmp[2] * Math.Sin(th2);
                tmp[2] = tmp[0] * Math.Sin(th2) + tmp[2] * Math.Cos(th2);
                tmp[0] = l;
                /* rotate by negative of observer y-x orientation */
                l = tmp[1] * Math.Cos(th1) - tmp[2] * Math.Sin(th1);
                tmp[2] = tmp[1] * Math.Sin(th1) + tmp[2] * Math.Cos(th1);
                tmp[1] = l;
                if (tmp[2] > 0.0)
                {
                    mp[i, 0] = tmp[0] * depth / tmp[2];
                    mp[i, 1] = tmp[1] * depth / tmp[2];
                }
                else
                {
                    mp[i, 0] = 0;
                    mp[i, 1] = 0;
                }
            }

            drawSkel(0);
            drawActivity();
           DateTime dt = new DateTime((long)tim);
            nw += "Time Frame: " + dt.ToShortDateString() + " : " + dt.ToLongTimeString() + Environment.NewLine;

            textBox3.Text = nw;
        }

        private string JName(int s)
        {
            switch (s)
            {
                case 0: return "hip centre";
                case 1: return "spine";
                case 2: return "shoulder centre";
                case 3: return "head";
                case 4: return "left shoulder";
                case 5: return "left elbow";
                case 6: return "left wrist";
                case 7: return "left hand";
                case 8: return "right shoulder";
                case 9: return "right elbow";
                case 10: return "right wrist";
                case 11: return "right hand";
                case 12: return "left hip";
                case 13: return "left knee";
                case 14: return "left ankle";
                case 15: return "left foot";
                case 16: return "right hip";
                case 17: return "right knee";
                case 18: return "right ankle";
                case 19: return "right foot";
                case 20: return "straight up";
                case 21: return "straight down";

            }
            return "ERROR!";
        }
        private double JKAngle(double[] j1, double[] j2, double[] j3)
        // p1x, double p1y, double p1z, double p2x, double p2y, double p2z, double p3x, double p3y, double p3z)
        {
            double[] v1 = new double[3];
            double[] v2 = new double[3];
            v1[0] = j2[0] - j1[0];
            v1[1] = j2[1] - j1[1];
            v1[2] = j2[2] - j1[2];
            v2[0] = j3[0] - j1[0];
            v2[1] = j3[1] - j1[1];
            v2[2] = j3[2] - j1[2];
            double dp = v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
            double av1 = v1[0] * v1[0] + v1[1] * v1[1] + v1[2] * v1[2];
            double av2 = v2[0] * v2[0] + v2[1] * v2[1] + v2[2] * v2[2];
            double cth = Math.Acos(dp / (Math.Sqrt(av1) * Math.Sqrt(av2)));
            cth *= conv;
            return cth;
        }

        private void drawActivity()
        {
            Canvas c = canvas2;
            c.Children.Clear();
            Rectangle rect1 = new Rectangle { Stroke = Brushes.IndianRed, StrokeThickness = 1 };
            SolidColorBrush myCol1 = new SolidColorBrush();
            myCol1.Color = Color.FromRgb(245, 142, 142);
            rect1.Fill = myCol1;
            Canvas.SetLeft(rect1, 0);
            Canvas.SetTop(rect1, 0);
            rect1.Width = 402;
            rect1.Height = 100;
            c.Children.Add(rect1);
            Rectangle rect2 = new Rectangle { Stroke = Brushes.Lavender, StrokeThickness = 1};
            SolidColorBrush myCol2 = new SolidColorBrush();
            myCol2.Color = Color.FromRgb(230, 230, 250);
            rect2.Fill = myCol2;
            Canvas.SetLeft(rect2, 0);
            Canvas.SetTop(rect2, 100);
            rect2.Width = 402;
            rect2.Height = 100;
            c.Children.Add(rect2); 
            Rectangle rect3 = new Rectangle { Stroke = Brushes.PaleGreen, StrokeThickness = 1 };
            SolidColorBrush myCol3 = new SolidColorBrush();
            myCol3.Color = Color.FromRgb(152, 251, 152);
            rect3.Fill = myCol3;
            Canvas.SetLeft(rect3, 0);
            Canvas.SetTop(rect3, 200);
            rect3.Width = 402;
            rect3.Height = 100;
            c.Children.Add(rect3);
            Rectangle rect4 = new Rectangle { Stroke = Brushes.LightCyan, StrokeThickness = 1 };
            SolidColorBrush myCol4 = new SolidColorBrush();
            myCol4.Color = Color.FromRgb(224, 255, 255);
            rect4.Fill = myCol4;
            Canvas.SetLeft(rect4, 0);
            Canvas.SetTop(rect4, 300);
            rect4.Width = 402;
            rect4.Height = 100;
            c.Children.Add(rect4);
            int frame = Int32.Parse(textBox1.Text)-5;
            for (int i = 0; i < 10; i++)
            {
                if ((frame >= 0) && (frame < maxlim - 2))
                {
                    drawAct(i, alv[frame], alv[frame + 1]);
                }
                frame++;
            }
            Line l = new Line { StrokeThickness = 1 };
            l.Stroke = Brushes.Gainsboro;
            l.X1 = 200;
            l.X2 = 200;
            l.Y1 = 0;
            l.Y2 = 399;
            canvas2.Children.Add(l);
        }

        private void drawAct(int i, int a, int b)
        {
            Line l = new Line { StrokeThickness = 2 };
            l.Stroke = Brushes.Black;
            l.X1 = 40 * i;
            l.X2 = l.X1 + 40;
            l.Y1 = 400 - a;
            l.Y2 = 400 - b;
            canvas2.Children.Add(l);
        }

        private void drawSkel(int i)
        {
            Canvas c;
            if (i == 0) c = canvas1;
            else c = canvas2;

            c.Children.Clear();
            Rectangle rect = new Rectangle { Stroke = Brushes.Black, StrokeThickness = 1 };
            Canvas.SetLeft(rect, 0);
            Canvas.SetTop(rect, 0);
            rect.Width = 400;
            rect.Height = 400;
            c.Children.Add(rect);

            drawSeg(i, 4, 5);
            drawSeg(i, 5, 6);
            drawSeg(i, 8, 9);
            drawSeg(i, 9, 10);
            drawSeg(i, 4, 8);
            drawSeg(i, 0, 2);
            drawSeg(i, 12, 13);
            drawSeg(i, 13, 14);
            drawSeg(i, 16, 17);
            drawSeg(i, 17, 18);
            drawSeg(i, 0, 12);
            drawSeg(i, 0, 16);
            drawHand(i, 7);
            drawHand(i, 11);
            drawHand(i, 15);
            drawHand(i, 19);
            drawHand(i, 3);
        }

        private void drawSeg(int i, int a, int b)
        {
            Line l = new Line { StrokeThickness = 2 };
            char ch = tst[0, a];
            if (tst[0, b] < tst[0, a]) ch = tst[0, b];
            if (ch == '0')
                l.Stroke = Brushes.Red;
            if (ch == '1')
                l.Stroke = Brushes.Blue;
            if (ch == '2')
                l.Stroke = Brushes.Green;
            if (ch == '3')
                l.Stroke = Brushes.DodgerBlue;
            l.X1 = ScreenX(mp[a, 0]);
            l.X2 = ScreenX(mp[b, 0]);
            l.Y1 = ScreenY(mp[a, 1]);
            l.Y2 = ScreenY(mp[b, 1]);
            if (i == 0)
                canvas1.Children.Add(l);
            else
                canvas2.Children.Add(l);
        }

        private void drawHand(int i, int a)
        {
            Line l1 = new Line { StrokeThickness = 2 };
            Line l2 = new Line { StrokeThickness = 2 };

            if (tst[0, a] == '0')
                l1.Stroke = l2.Stroke = Brushes.Red;
            if (tst[0, a] == '1')
                l1.Stroke = l2.Stroke = Brushes.Blue;
            if (tst[0, a] == '2')
                l1.Stroke = l2.Stroke = Brushes.Green;
            if (tst[0, a] == '3')
                l1.Stroke = l2.Stroke = Brushes.DodgerBlue;

            l1.X1 = ScreenX(mp[a, 0]) - 2;
            l1.X2 = l1.X1 + 4;
            l1.Y1 = ScreenY(mp[a, 1]) - 2;
            l1.Y2 = l1.Y1 + 4;
            l2.X1 = l1.X1;
            l2.X2 = l1.X2;
            l2.Y1 = l1.Y2;
            l2.Y2 = l1.Y1;

            if (i == 0)
            {
                canvas1.Children.Add(l1);
                canvas1.Children.Add(l2);
            }
            else
            {
                canvas2.Children.Add(l1);
                canvas2.Children.Add(l2);

            }

        }

        private int ScreenX(double x)
        {
            return 200 - (int)(x * 400.0 / depth);
        }

        private int ScreenY(double y)
        {
            return 200 - (int)(y * 400.0 / depth);
        }

        private string JShow(double d)
        {
            double d2 = Math.Round(d, 4);
            return d2.ToString();
        }

        public double ToVert(int j1, int j2)
        {
         double basel = Math.Sqrt(Math.Pow(dta[0,j1,0]-dta[0,j2,0],2.0) + Math.Pow(dta[0,j1,2] - dta[0,j2,2],2.0));
            double htl = dta[0, j1, 1] - dta[0, j2, 1];
            double d = Math.Atan2(basel,htl) * conv;
            if (d < 0) d += 360;
            return d;
        }

        public double JDist(int joint)
        {  return Math.Sqrt(Math.Pow((dta[0,joint,0] - dta[1,joint,0]),2) + Math.Pow((dta[0,joint,1]-dta[1,joint,1]),2) + Math.Pow((dta[0,joint,2]-dta[1,joint,2]),2));
        }

        private void GetParams()
        {
            string rulefile = @"c://kinectdata/actrules.dta";
            if (!File.Exists(rulefile))
            {
                  MessageBox.Show("Rule file not found: "+rulefile);
                  return;
            }
            using (BinaryReader b = new BinaryReader(File.Open(rulefile, FileMode.Open)))
            {
                for (int i = 0; i < 13; i++)
                  ap[i] = Double.Parse(b.ReadInt32().ToString());
            }
            ap[grandtot] = ap[0] + ap[1] + ap[2]; // grand total 
            ap[postot] = ap[3] * 2.0 + ap[4] + ap[5] + ap[6] + ap[7]; // total for position
            ap[movtot] = ap[8] + ap[9] + ap[10] + ap[11] + ap[12]; // total for movement
        }

        private void OpenFile()
        {
            if (!File.Exists(secondstring)) { return; }
            textBox1.Text = "0";
            double d, d2, d3, d4, ybase;
            int stot = 0;
            String op = "";
            GetParams();
            using (BinaryReader b = new BinaryReader(File.Open(secondstring, FileMode.Open)))
            {
                maxlim = b.BaseStream.Length / recsize;
                label1.Content = "/ " + maxlim.ToString();
 
                // get center of skeleton for focussing playback image
                b.BaseStream.Seek((long)25, SeekOrigin.Begin);
                for (int j = 0; j < 3; j++)
                {
                    fcs[j] = b.ReadDouble(); 
                    obs[j] = 0.0;
                } 
                b.BaseStream.Seek((long)358, SeekOrigin.Begin);  // left ankle start
                d = b.ReadDouble();
                b.BaseStream.Seek((long)458, SeekOrigin.Begin);  // right ankle start
                d2 = b.ReadDouble();
                ybase = (d + d2) / 2.0 + 0.05;   // average of ankles plus 5cm = minimum height of jump
                for (int j=0; j<20; j++) 
                   dta[0,j,0]=dta[0,j,1]=dta[0,j,2]=0.0;
                alv = new Int32[maxlim];
                for (int frame = 0; frame < maxlim; frame++)
                {
                    b.BaseStream.Seek((long)frame * (long) recsize, SeekOrigin.Begin);
                    for (int j = 0; j < 20; j++)
                    {
                        dta[1, j, 0] = dta[0, j, 0];
                        dta[1, j, 1] = dta[0, j, 1];
                        dta[1, j, 2] = dta[0, j, 2];
                        dta[0, j, 0] = b.ReadDouble();
                        dta[0, j, 1] = b.ReadDouble();
                        dta[0, j, 2] = b.ReadDouble();
                        b.ReadChar();
                    }
                    d = ToVert(2,0) * ap[3] * 2.0 + (ToVert(4,5) + ToVert(8,9)) * ap[4] + (ToVert(5,6) + ToVert(9,10)) * ap[5] + (ToVert(12,13) + ToVert(16,17)) * ap[6] + (ToVert(13,14) + ToVert(17,18)) * ap[7];
                    d *= 2.0 / ap[postot];
                    if (d > 200.0) d = 200.0;
                    if (frame > 0)
                    {
                        d2 = (JDist(0) + JDist(2)) * ap[8] + (JDist(5) + JDist(9)) * ap[9] + (JDist(10) + JDist(6)) * ap[10];
                        d2 += (JDist(12) + JDist(16)) * ap[11] + (JDist(13) + JDist(17)) * ap[12];
                        d2 *= 400.0 / ap[movtot];
                        if (d2 > 200) d2 = 200.0;
                    }
                    else d2 = 0.0;
                    d3 = 0.0;
                    if (dta[0, 14, 1] > ybase) d3 += 100.0;  // left ankle up off the ground
                    if (dta[0, 18, 1] > ybase) d3 += 100.0;  // right ankle up off the ground
                    d4 = ((d * ap[0]) + (d2 * ap[1]) + (d3 * ap[2])) / ap[grandtot];
                    d4 = Math.Round(2.0 * d4, 0);

                    Int32.TryParse(d4.ToString(), out  alv[frame]);
                       // alv[frame] = Int32.Parse(d4.ToString());
                    

                   
                    // alv[frame] = frame;
                   //  op += "Frame "+frame.ToString() + " = " + d4.ToString() + Environment.NewLine;

                }
            }
            int bsmooth = smooth * 2 + 1;
            for (int i = 0; i < bsmooth; i++) stot += alv[i];
            for (int i = smooth; i < maxlim-smooth; i++) //fixed bug arrayindexoutof bound suman
            {
                stot += alv[i+smooth] - alv[i - smooth];
                alv[i] = (int) (stot / bsmooth);
           }
            textBox4.Text = op;
            DrawIt();
        }

        private void JScale(double depth2)
        {
            depth2 /= depth;
            obs[0] = fcs[0] + depth2 * (obs[0] - fcs[0]);
            obs[1] = fcs[1] + depth2 * (obs[1] - fcs[1]);
            obs[2] = fcs[2] + depth2 * (obs[2] - fcs[2]);

            DrawIt();

        }

        private void JMove(double ang1, double ang2)
        {
            double zx = Math.Pow(obs[0] - fcs[0], 2) + Math.Pow(obs[2] - fcs[2], 2);
            double theta1 = Jat(obs[1] - fcs[1], Math.Sqrt(zx));
            double theta2 = Jat(obs[2] - fcs[2], obs[0] - fcs[0]);

            theta1 += ang1 / conv;
            theta2 += ang2 / conv;
            obs[0] = depth * Math.Cos(theta1) * Math.Cos(theta2) + fcs[0];
            obs[1] = depth * Math.Sin(theta1) + fcs[1];
            obs[2] = depth * Math.Sin(theta2) * Math.Cos(theta1) + fcs[2];
            DrawIt();
        }

        private void button8_Click(object sender, RoutedEventArgs e)
        {
            /* Zoom In */
            if (depth > 0.5)
                JScale(depth - 0.5);

        }

        private void button9_Click(object sender, RoutedEventArgs e)
        {
            /* Zoom Out */
            JScale(depth + 0.5);

        }

        private string stringtime(int i)
        {
            string t = "";
            if (i > 43200)
            {
                t = " PM";
                i -= 43200;
            }
            else
                t = " AM";
            int h = (int)Math.Floor((decimal)i / 3600);
            i -= h * 3600;
            if (h == 0) h = 12;
            int m = (int)Math.Floor((decimal)i / 60);
            i -= m * 60;
            t = h.ToString() + ":" + m.ToString().PadLeft(2, '0') + ':' + i.ToString().PadLeft(2, '0') + t;
            return t;

        }

        private int rawtime(string s)
        {
            string[] pt = s.Split(':');
            int ampm = 0;
            int h = Int32.Parse(pt[0]);
            if (h == 12) h = 0;
            if (pt[2].IndexOf("PM") > 0) ampm = 43200;
            pt[2] = pt[2].Substring(0, 2);
            return ampm + Int32.Parse(pt[2]) + 60 * Int32.Parse(pt[1]) + 3600 * h;
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            var i = Int32.Parse(this.textBox1.Text);
            if (i > 0) i--;
            timerEnabled = false;
            textBox1.Text = i.ToString();
            this.DrawIt();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var i = Int32.Parse(textBox1.Text);
            if (++i >= maxlim) i--;
            timerEnabled = false;
            textBox1.Text = i.ToString();
            DrawIt();

        }

        private void listBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            timerEnabled = false;
           // try
            //{
                ListBoxItem li = (ListBoxItem)listBox1.SelectedItem;
                string selectedIndex = li.Tag + "";
                String nd = @"000" + selectedIndex.PadLeft(5, '0') + ".dta";
                secondstring = GetFullFile("c" + nd);
                OpenFile();
                
           // } 
            //catch
            //{
            //}
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            timerEnabled = true;
            test.Start();
        }

        private void button5_Click_1(object sender, RoutedEventArgs e)
        {
            JMove(0.0, -5.0);

        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {
            JMove(0.0, 5.0);
        }

        private void button6_Click(object sender, RoutedEventArgs e)
        {
            JMove(5.0, 0.0);
        }

        private void button7_Click(object sender, RoutedEventArgs e)
        {
            JMove(-5.0, 0.0);
        }

        private void sessions_combo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxItem ci = (ComboBoxItem)this.sessions_combo.SelectedValue;
            this.projectDir = (string)ci.Tag;
            this.ApplyVersion();
            this.UpdateList();
        }




        /// <summary>
        /// Populates the session combobox with the  recorded sessions saved in directories under c:\kinectdata\
        /// </summary>
        /// <returns> String array of list of data directories</returns>
        private string[] populateSessionComboBox()
        {
            string[] dirlist = Directory.GetDirectories(@"C:\kinectdata");
            List<string> aList = dirlist.ToList();
            aList.Remove(@"C:\kinectdata\rules");
            string[] finallist = aList.ToArray();
            ComboBoxItem cbox_message = new ComboBoxItem();
            cbox_message.Content = "Select a session directory";
            cbox_message.Tag = "";

            sessions_combo.Items.Add(cbox_message);

            for (int i = 0; i < finallist.Length; i++)
            {
                ComboBoxItem cboxitem = new ComboBoxItem();

                cboxitem.Content = finallist[i];
                cboxitem.Tag = finallist[i];
                this.sessions_combo.Items.Add(cboxitem);

            }

            return finallist;

        }


        private string GetFullFile(string aFile)
        {

            return System.IO.Path.Combine(this.projectDir, aFile);

        }

        /// <summary>
        /// Loads the version number from version.txt file in the project data directory and sets the record size accordingly
        /// Currently  the version is tracked   so that old files could be opened by the  new program
        /// </summary>
        private void ApplyVersion()
        {
            string vfile = GetFullFile("version.txt");


            if (File.Exists(vfile))
            {
                String vstring = new StreamReader(vfile).ReadLine();
                if (!int.TryParse(vstring, out this.version))
                {
                    this.version = 0;
                }
            }
            else
            {
                this.version = 0;
            }

            if (this.version < 1)
            {
                this.recsize = 508;
            }
            else
            {
                this.recsize = 508 + 480;
            }
        }



 
   

    }

    public class DTimer
    {
        private DispatcherTimer timer;
        public event AutoTimerUp DoMe;


        public DTimer()
        {
            timer = new DispatcherTimer();
        }
        public void Start()
        {
            timer.Interval = TimeSpan.FromMilliseconds(120);
            timer.Tick += timer_Task;
            timer.Start();
        }

        public void Stop()
        {
            timer.Stop();
        }
        private void timer_Task(object sender, EventArgs e)
        {
       
            DoMe(null, EventArgs.Empty);
        }


    }

}
