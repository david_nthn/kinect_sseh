﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;

namespace WpfApplication1
{ 
    /// <summary>
    /// This class computes the joint angles from joint location using inverse kinematics and stores them.
    /// </summary>
      class JointAnglesIK
    {
       //
       static  int[,] keyj = new int[,] { { 4, 5, 6 }, { 8, 9, 10 }, { 12, 13, 14 }, { 16, 17, 18 }, { 1, 0, 13 }, { 1, 0, 17 }, { 1, 2, 5 }, { 1, 2, 9 }, { 12, 4, 5 }, { 2, 0, 20 }, { 0, 2, 10 } };

       private  static    double conv = 57.29577951308232; /* radians to degrees */

       private double[]   angles_ik = new double[11];
       private double[,] angles_ik_new = new double[8,2];


       private  int NumOfAngles = 11;

       public int NumberOfAngles
       {
           get { return NumOfAngles; }
       }

       enum JointID
       {
           HipCenter, Spine, ShoulderCenter, Head, ShoulderLeft, ElbowLeft, WristLeft, HandLeft, ShoulderRight, ElbowRight,
           WristRight, HandRight, HipLeft, KneeLeft, AnkleLeft, FootLeft, HipRight, KneeRight, AnkleRight, FootRight
       };

     public    enum JointIndex{
            ElbowLeft=0,ShoulderLeft,ElbowRight, ShoulderRight, KneeLeft,HipLeft,KneeRight,HipRight

       };

        
//Tree - HipLeft,KneeLeft,AnkleLeft,FootLeft
       // -HipRight,KneeRight,AnkleRight,FootRight
      //Spine,shoulderCenter,
          
          
          /// <summary>
          /// Creates JointAnglesIK instance from 3D joint locatioins
          /// </summary>
          /// <param name="dtaj">20 x 3 matrix of 3D joint locations </param>
       public JointAnglesIK(double[,] joints)
       {
           this.SkeletonJointsToAnglesIK(joints);
           SkeletonJointsToAnglesIKNew(joints);
       }

       public static string JName(int s)
       {
           switch (s)
           {
               case 0: return "hip centre";
               case 1: return "spine";
               case 2: return "shoulder centre";
               case 3: return "head";
               case 4: return "left shoulder";
               case 5: return "left elbow";
               case 6: return "left wrist";
               case 7: return "left hand";
               case 8: return "right shoulder";
               case 9: return "right elbow";
               case 10: return "right wrist";
               case 11: return "right hand";
               case 12: return "left hip";
               case 13: return "left knee";
               case 14: return "left ankle";
               case 15: return "left foot";
               case 16: return "right hip";
               case 17: return "right knee";
               case 18: return "right ankle";
               case 19: return "right foot";
               case 20: return "straight up";
               case 21: return "straight down";

           }
           return "ERROR!";
       }
          /// <summary>
          /// Returns the joint name specified by ith row and jth column of keyj matrix
          /// </summary>
          /// <param name="i"></param>
          /// <param name="j"></param>
          /// <returns></returns>
       public static  string JointNameForKey(int i, int j)
       {
           return JName(keyj[i, j]);
       }

        /// <summary>
        /// Computes an angle  between lines j1-j2 and j2-j3
        /// </summary>
        /// <param name="j1"></param>
        /// <param name="j2"></param>
        /// <param name="j3"></param>
        /// <returns></returns>
        /// <remarks> Jonathons method</remarks>
       private static double JKAngle(double[] j1, double[] j2, double[] j3)
       // p1x, double p1y, double p1z, double p2x, double p2y, double p2z, double p3x, double p3y, double p3z)
       {
           double[] v1 = new double[3];
           double[] v2 = new double[3];
           v1[0] = j2[0] - j1[0];
           v1[1] = j2[1] - j1[1];
           v1[2] = j2[2] - j1[2];
           v2[0] = j3[0] - j1[0];
           v2[1] = j3[1] - j1[1];
           v2[2] = j3[2] - j1[2];
           double dp = v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
           double av1 = v1[0] * v1[0] + v1[1] * v1[1] + v1[2] * v1[2];
           double av2 = v2[0] * v2[0] + v2[1] * v2[1] + v2[2] * v2[2];
           double cth = Math.Acos(dp / (Math.Sqrt(av1) * Math.Sqrt(av2)));
           cth *= conv;
           return cth;
       }


        /// <summary>
        /// Computes angles using inverse kinematics (jonathens code restructred here)
        /// </summary>
        /// <param name="dtaj"></param>
        /// <param name="angles_ik"> 11 dimensional array to store angles</param>
       private    void  SkeletonJointsToAnglesIK(double[,] dtaj)
       {



          
           //number of rows of keyj matrix is 11
           for (int i = 0; i < 11; i++)
           {
               double[] ang1 = new double[] { dtaj[keyj[i, 0], 0], dtaj[keyj[i, 0], 1], dtaj[keyj[i, 0], 2] };
               double[] ang2 = new double[] { dtaj[keyj[i, 1], 0], dtaj[keyj[i, 1], 1], dtaj[keyj[i, 1], 2] };
               double[] ang3 = new double[] { 0.0000, 0.00000, 0.00000 };
               int kj = keyj[i, 2];
               if (kj < 20)
               {
                   ang3[0] = dtaj[kj, 0];
                   ang3[1] = dtaj[kj, 1];
                   ang3[2] = dtaj[kj, 2];
               }
               if (kj == 20)
               {
                   ang3[0] = dtaj[keyj[i, 0], 0];
                   ang3[1] = 10000;
                   ang3[2] = dtaj[keyj[i, 0], 2];
               }
               if (kj == 21)
               {
                   ang3[0] = dtaj[keyj[i, 0], 0];
                   ang3[1] = -100000;
                   ang3[2] = dtaj[keyj[i, 0], 2];
               }
               double ang_value = JKAngle(ang2, ang1, ang3);
               angles_ik[i] = ang_value;
               //nw += "Angle of " + JName(keyj[i, 1]) + " (" + JName(keyj[i, 0]) + " to " + JName(keyj[i, 2]) + ") : " + ang_value + Environment.NewLine;
           }

           

           

       }
           
       public double GetAngle(int index)
       {
           return  this.angles_ik[index];
       }

       public double GetAngleNew(int index,int i)
       {
           return this.angles_ik_new[index,i];
       }


       private void SkeletonJointsToAnglesIKNew(double[,] skeleton)
       {
           // Retrieve the position vectors we care about.
           Vector3D leftShoulder = ExtractVectorFromJoint(skeleton,JointID.ShoulderLeft);
           Vector3D leftElbow = ExtractVectorFromJoint(skeleton,JointID.ElbowLeft);
           Vector3D leftWrist = ExtractVectorFromJoint(skeleton, JointID.WristLeft);
           //Vector3D head = ExtractVectorFromJoint(skeleton.Joints[JointID.Head]);
           Vector3D centerShoulder = ExtractVectorFromJoint(skeleton,JointID.ShoulderCenter);
           Vector3D centerHip = ExtractVectorFromJoint(skeleton,JointID.HipCenter);
           Vector3D spine=ExtractVectorFromJoint(skeleton,JointID.Spine);
           Vector3D rightShoulder = ExtractVectorFromJoint(skeleton,JointID.ShoulderRight);
           Vector3D rightElbow = ExtractVectorFromJoint(skeleton,JointID.ElbowRight);
           Vector3D rightWrist = ExtractVectorFromJoint(skeleton,JointID.WristRight);
           
           Vector3D rightHip=ExtractVectorFromJoint(skeleton,JointID.HipRight);
           Vector3D rightKnee=ExtractVectorFromJoint(skeleton,JointID.KneeRight);
           Vector3D rightAnkle=ExtractVectorFromJoint(skeleton,JointID.AnkleRight);

           Vector3D leftHip=ExtractVectorFromJoint(skeleton,JointID.HipLeft);
           Vector3D leftKnee=ExtractVectorFromJoint(skeleton,JointID.KneeLeft);
           Vector3D leftAnkle=ExtractVectorFromJoint(skeleton,JointID.AnkleLeft);


           // Directional vectors
           Vector3D upSpine = centerShoulder - spine;
           Vector3D upHip=spine-centerHip;

           Vector3D leftToRightShoulder = rightShoulder - leftShoulder;
           Vector3D rightToLeftShoulder = leftShoulder - rightShoulder;
           Vector3D rightToLeftHip=leftHip-rightHip;
           Vector3D leftToRightHip= rightHip-leftHip;

           Vector3D upSpinePitch = Vector3D.CrossProduct(upSpine, rightToLeftShoulder);
           Vector3D upHipPitch = Vector3D.CrossProduct(upHip, rightToLeftHip);

           // Right-side vectors
           Vector3D rightElbowToShoulder = rightShoulder - rightElbow;
           Vector3D rightElbowToWrist = rightWrist - rightElbow;
           Vector3D rightShoulderPitch = Vector3D.CrossProduct(rightElbowToShoulder, rightToLeftShoulder);
           
           Vector3D rightKneeToHip=rightHip-rightKnee;
           Vector3D rightKneeToAnkle=rightAnkle-rightKnee;
           Vector3D rightHipPitch=Vector3D.CrossProduct(rightKneeToHip,rightToLeftHip);
           
           

           // Left-side vectors
           Vector3D leftElbowToShoulder = leftShoulder - leftElbow;
           Vector3D leftElbowToWrist = leftWrist - leftElbow;
           Vector3D leftShoulderPitch = Vector3D.CrossProduct(leftElbowToShoulder, rightToLeftShoulder);

           Vector3D leftKneeToHip= leftHip-leftKnee;
           Vector3D leftKneeToAnkle=leftAnkle-leftKnee;
           Vector3D leftHipPitch=Vector3D.CrossProduct(leftKneeToHip,rightToLeftHip);


           // Find angles of right-side  joints
           float rightElbowRollAngle = (float)(180-Vector3D.AngleBetween(rightElbowToShoulder, rightElbowToWrist) );
           float rightShoulderRollAngle = (float)(90 - Vector3D.AngleBetween(rightToLeftShoulder, rightElbowToShoulder));
           float rightShoulderPitchAngle = (float)( Vector3D.AngleBetween(rightShoulderPitch, upSpinePitch));


           float rightKneeRollAngle = (float)(180 - Vector3D.AngleBetween(rightKneeToHip, rightKneeToAnkle));
           float rightHipRollAngle = (float)(90 - Vector3D.AngleBetween(leftToRightHip, rightKneeToHip));
           float rightHipPitchAngle = (float)( Vector3D.AngleBetween(rightHipPitch, upHipPitch));


           // Find angles of left-side   joints
           float leftElbowRollAngle = (float)(180-Vector3D.AngleBetween(leftElbowToShoulder, leftElbowToWrist));
           float leftShoulderRollAngle = (float)(90 - Vector3D.AngleBetween(rightToLeftShoulder, leftElbowToShoulder));
           float leftShoulderPitchAngle = (float)(Vector3D.AngleBetween(leftShoulderPitch, upSpinePitch));
           



           float leftKneeRollAngle=(float) (180-Vector3D.AngleBetween(leftKneeToHip,leftKneeToAnkle));
           float leftHipRollAngle = (float)(90 -Vector3D.AngleBetween(leftToRightHip,leftKneeToHip));
           float leftHipPitchAngle= (float)(Vector3D.AngleBetween(leftHipPitch, upHipPitch));



           this.angles_ik_new[(int)JointIndex.ElbowLeft, 0] = leftElbowRollAngle;
           this.angles_ik_new[(int)JointIndex.ElbowLeft, 1] = 0;//pitch is zero for elbow
           this.angles_ik_new[(int)JointIndex.ShoulderLeft,0]=leftShoulderRollAngle;
           this.angles_ik_new[(int)JointIndex.ShoulderLeft, 1] = leftShoulderPitchAngle;
           
           this.angles_ik_new[(int)JointIndex.ElbowRight, 0] = rightElbowRollAngle;
           this.angles_ik_new[(int)JointIndex.ElbowRight, 1] = 0;//pitch is zero for elbow
           this.angles_ik_new[(int)JointIndex.ShoulderRight, 0] = rightShoulderRollAngle;
           this.angles_ik_new[(int)JointIndex.ShoulderRight, 1] = rightShoulderPitchAngle;

           this.angles_ik_new[(int)JointIndex.KneeLeft, 0] = leftKneeRollAngle;
           this.angles_ik_new[(int)JointIndex.KneeLeft, 1] = 0; //pitch is zero for knee
           this.angles_ik_new[(int)JointIndex.HipLeft, 0] = leftHipRollAngle;
           this.angles_ik_new[(int)JointIndex.HipLeft, 1] = leftHipPitchAngle;

           this.angles_ik_new[(int)JointIndex.KneeRight, 0] = rightKneeRollAngle;
           this.angles_ik_new[(int)JointIndex.KneeRight, 1] = 0; //pitch is zero for knee
           this.angles_ik_new[(int)JointIndex.HipRight, 0] = rightHipRollAngle;
           this.angles_ik_new[(int)JointIndex.HipRight, 1] = rightHipPitchAngle;


           //todo: fix the pitch angle of hip joint using similar technique of shoulder joint
           //instead of using righttoleft use hipcentertolefthip and hipcenter to right hip to compute pitch vector

           //example1 if you add 180 to x component of elbow orientation (stored in wrist) then it equals to elbowRollAngle

            


           //look at how the  coordinate frames are attached in 
           //http://msdn.microsoft.com/en-us/library/hh973073.aspx
           //In the above code, only roll and pitch of left shoulder is computed i.e rotation around x and y axis  not z axis.
           //If we look at the diagram, the shoulder can rotate on two ways only, around y and z axis (rotation around x axis could not be reconstructed from 
           //skeleton points. So the roll angle of shoulder is rotation about z-axis and pitch is rotation around y-axis.  
           //Can we make this notion independent of coordinate system, for example, for example in shoulder case, roll is rot around z, pitch is rotation around
           //y and yaw is rotation around x (because x axis of . For elbow, yaw is actually rotation around y. (that means 
           
           //Looking at the vicon coordinate system, the angles should be associated accordingly
       }

       /// <summary>
       /// Extracts a Vector3D of the position of the given joint.
       /// </summary>
       /// <param name="j">joint of interest</param>
       /// <returns>3D vector of joint position</returns>
       private Vector3D ExtractVectorFromJoint(double[,] skel,JointID jid)
       {
           int i = (int)jid;
           return new Vector3D(skel[i,0], skel[i,1], skel[i,2]);
       }



    }



}
