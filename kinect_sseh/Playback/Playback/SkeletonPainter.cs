﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Shapes;
using System.Windows.Controls;
using System.Windows.Media;

namespace WpfApplication1
{
    /// <summary>
    /// This class displays the skeleton on a Canvas
    /// </summary>
    class SkeletonPainter
    {
        private Canvas canvas;
        Rectangle rect = new Rectangle { Stroke = Brushes.Black, StrokeThickness = 1 };

        double pi=Math.PI;
        double[] fcs=new double[3];
        double[] obs=new double[3];

        char [] tst;
        double[,] mp=new double[20,2];    
        double depth;
            public SkeletonPainter(Canvas c)
            {
            this.canvas=c;
            
         
            rect.Width = 400;
            rect.Height = 400;
            Canvas.SetLeft(rect, 0);
            Canvas.SetTop(rect, 0);
   

            }
        public  void drawSkel(SkeletonRecord skel,double[] fcs_,double[] obs_)
        {
            for(int i=0;i<3;i++)
            {
                this.fcs[i]=fcs_[i];
                this.obs[i]=obs_[i];
            }

             depth = Math.Sqrt(Math.Pow(fcs[0] - obs[0], 2) + Math.Pow(fcs[1] - obs[1], 2) + Math.Pow(fcs[2] - obs[2], 2));
             this.tst=skel.GetValidity();
             PointsToScreen(skel.GetJointLocationsMatrix(),out mp);

             Canvas.SetLeft(rect, 0);
             Canvas.SetTop(rect, 0);
            canvas.Children.Clear();
            canvas.Children.Add(rect);

            drawSeg( 4, 5);
            drawSeg(5, 6);
            drawSeg( 8, 9);
            drawSeg( 9, 10);
            drawSeg( 4, 8);
            drawSeg( 0, 2);
            drawSeg( 12, 13);
            drawSeg( 13, 14);
            drawSeg( 16, 17);
            drawSeg( 17, 18);
            drawSeg( 0, 12);
            drawSeg( 0, 16);
            drawHand( 7);
            drawHand( 11);
            drawHand( 15);
            drawHand( 19);
            drawHand( 3);
        }

        private void drawSeg( int a, int b)
        {
            Line l = new Line { StrokeThickness = 2 };
            char ch = tst[ a];
            if (tst[ b] < tst[a]) ch = tst[ b];
            if (ch == '0')
                l.Stroke = Brushes.Red;
            if (ch == '1')
                l.Stroke = Brushes.Blue;
            if (ch == '2')
                l.Stroke = Brushes.Green;
            if (ch == '3')
                l.Stroke = Brushes.DodgerBlue;
            l.X1 = ScreenX(mp[a, 0]);
            l.X2 = ScreenX(mp[b, 0]);
            l.Y1 = ScreenY(mp[a, 1]);
            l.Y2 = ScreenY(mp[b, 1]);
            canvas.Children.Add(l);
             
        }

        private void drawHand( int a)
        {
            Line l1 = new Line { StrokeThickness = 2 };
            Line l2 = new Line { StrokeThickness = 2 };

            if (tst[ a] == '0')
                l1.Stroke = l2.Stroke = Brushes.Red;
            if (tst[ a] == '1')
                l1.Stroke = l2.Stroke = Brushes.Blue;
            if (tst[ a] == '2')
                l1.Stroke = l2.Stroke = Brushes.Green;
            if (tst[ a] == '3')
                l1.Stroke = l2.Stroke = Brushes.DodgerBlue;

            l1.X1 = ScreenX(mp[a, 0]) - 2;
            l1.X2 = l1.X1 + 4;
            l1.Y1 = ScreenY(mp[a, 1]) - 2;
            l1.Y2 = l1.Y1 + 4;
            l2.X1 = l1.X1;
            l2.X2 = l1.X2;
            l2.Y1 = l1.Y2;
            l2.Y2 = l1.Y1;

             canvas.Children.Add(l1);
             canvas.Children.Add(l2);
         

        }

        private int ScreenX(double x)
        {
            return 200 - (int)(x * 400.0 / depth);
        }

        private int ScreenY(double y)
        {
            return 200 - (int)(y * 400.0 / depth);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="points3d"> A 20 x 3 matrix of 3D joint locations</param>
        public void PointsToScreen(double[,] points3d,out double[,] points2d_screen)
        {
            int points=20;
            double[,] points2d=new double[20,2];
           

            double zx = Math.Pow(obs[0] - fcs[0], 2) + Math.Pow(obs[2] - fcs[2], 2);
            double th1 = Jat(fcs[1] - obs[1], Math.Sqrt(zx));
            double th2 = Jat(fcs[0] - obs[0], fcs[2] - obs[2]);
            double[] tmp = new double[3];

            for (int i = 0; i < points; i++)
            {   /* translate point by observer position */
                double tmp0 = points3d[i, 0] - obs[0];
                double tmp1 = points3d[ i, 1] - obs[1];
                double tmp2 = points3d[i, 2] - obs[2];
                /* rotate by negative of observer z-x orientation */
               double  l = tmp0 * Math.Cos(th2) - tmp2 * Math.Sin(th2);
                tmp2 = tmp0 * Math.Sin(th2) + tmp2 * Math.Cos(th2);
                tmp0 = l;
                /* rotate by negative of observer y-x orientation */
                l = tmp1 * Math.Cos(th1) - tmp2 * Math.Sin(th1);
                tmp2 = tmp1 * Math.Sin(th1) + tmp2 * Math.Cos(th1);
                tmp1 = l;
                if (tmp2 > 0.0)
                {
                    points2d[i, 0] = tmp0 * depth / tmp2;
                    points2d[i, 1] = tmp1 * depth / tmp2;
                }
                else
                {
                    points2d[i, 0] = 0;
                    points2d[i, 1] = 0;
                }
            }

            points2d_screen= points2d;
        }

        
        private double Jat(double v1, double v2)
        {
            if (v2 == 0.0)
            {
                if (v1 == 0.0) return 0.0;
                if (v1 >= 0.0) return pi / 2.0;
                else return pi * 1.5;
            }
            double ang = Math.Atan(v1 / v2);
            if (v2 < 0.0) ang += pi;
            return ang;
        }


    }
}
