﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
//using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Timers;
using System.Collections.ObjectModel;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int epoch = 5; // 5 second epochs //
        int nume; // number of epochs in data - calculated at run-time //

        /// <summary>
        /// Number of bytes taken by one skeleton record
        /// (20 *3 *8) for skeletons,
        /// (20 *3 *8) for orientatons,
        /// 8 for timestamp 
        /// 20 for validity
        /// Total=508 +480
        /// </summary>
        int recsize = 508+480;
        long maxlim = 10000L;
        string mainstring = "";
        string secondstring = "";
        double[, ,] dta = new double[2, 20, 3];
        char[,] tst = new char[2, 20];
        bool autoon = false;
        double[] obs = new double[] { 0.0, 0.0, 0.0 }; /* observation point */
        const double conv = 57.29577951308232; /* radians to degrees */
        const double pi = 3.141592653589793;
        double depth = 4.25;
        const double maxx = 400.0;
        const double maxy = 400.0;
        const int points = 20;
        double[] fcs = new double[] { 0.0, 0.0, 4.25 }; /* focus point - center of screen */
        double[,] mp = new double[points, 2];
        int[,] keyj = new int[,] { { 4, 5, 6 }, { 8, 9, 10 }, { 12, 13, 14 }, { 16, 17, 18 }, { 1, 0, 13 }, { 1, 0, 17 }, { 1, 2, 5 }, { 1, 2, 9 }, { 12, 4, 5 }, { 2, 0, 20 }, { 0, 2, 10 } };
        BinaryReader bFile;
        int answers = 0;
        string res = "";
        string rulestring = "";
        long[] ans = new long[100];
        string[] cr = new string[15];
        bool debug = false;
        string[] opd;
        string header;
        int[] mcnt;
        int beginning = 0;

        string projectDir;
         

        SkeletonRecords records;
        SkeletonRecords records_clean;
        SkeletonPainter painter1;
        SkeletonPainter painter2;

        System.Collections.Hashtable activityFrames = new System.Collections.Hashtable();

       
        /// <summary>
        /// to track project version so that old files could be opened by new program
        /// </summary>
        int version;

        /// <summary>
        /// AngleCollection is binded to the ListView control 
        /// </summary>
          public ObservableCollection<AngleData> _AngleCollection = new ObservableCollection<AngleData>();


        public MainWindow()
        {
       

            InitializeComponent();
            
            populateSessionComboBox();
            this.records=new SkeletonRecords();
            this.records_clean = new SkeletonRecords();

            Rectangle rect = new Rectangle { Stroke = Brushes.Black, StrokeThickness = 1 };
             
         
            this.painter1 = new SkeletonPainter(this.canvas1);
            this.painter2 = new SkeletonPainter(this.canvas2);
           // UpdateList();
            //OpenFile();

        }

        public ObservableCollection<AngleData> AnglesCollection
        {
             get { return _AngleCollection; } 
        }

        /// <summary>
        /// Loads the version number from version.txt file in the project data directory
        /// Currently  the version is tracked   so that old files could be opened by the  new program
        /// </summary>
        private void ApplyVersion()
        {
            string vfile = GetFullFile("version.txt");
              

            if (File.Exists(vfile))
            {
                String vstring=new StreamReader(vfile).ReadLine();
                if (!int.TryParse(vstring,out this.version))
                {
                    this.version = 0;
                }
            }
            else
            {
                this.version = 0;
            }

            if (this.version < 1)
            {
                this.recsize = 508;
            }else
            {
                this.recsize = 508 + 480;
            }
        }


        private void button2_Click(object sender, EventArgs e)
        {
            var i = Int32.Parse(this.textBox1.Text);
            if (i > 0) i--;
            textBox1.Text = i.ToString();
            this.DrawIt();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var i = Int32.Parse(this.textBox1.Text);
            if (++i >= maxlim) i--;
            textBox1.Text = i.ToString();
            this.DrawIt();

        }
        /// <summary>
        /// Updates the ListBox with the available recordings
        /// </summary>
        private void UpdateList()
        {
            string masterstring =  GetFullFile("recordings.txt");
            listBox1.Items.Clear();
            if (File.Exists(masterstring))
            {
                using (StreamReader b = new StreamReader(masterstring))
                {
                    string s;
                    int index = 0;
                     
                    
                     
                    while ((s = b.ReadLine()) != null)
                    {
                        if (!s.Contains("Calibrat") && s.Length>0 )
                        {
                            ListBoxItem li = new ListBoxItem();
                            li.Content = s;
                            li.Tag = ""+index;
                            listBox1.Items.Add(li);
                        }

                        index++;
                    }
                }
            }
        }

        private double Jat(double v1, double v2)
        {
            if (v2 == 0.0)
            {
                if (v1 == 0.0) return 0.0;
                if (v1 >= 0.0) return pi / 2.0;
                else return pi * 1.5;
            }
            double ang = Math.Atan(v1 / v2);
            if (v2 < 0.0) ang += pi;
            return ang;
        }

        private void DrawIt()
        {
            depth = Math.Sqrt(Math.Pow(fcs[0] - obs[0], 2) + Math.Pow(fcs[1] - obs[1], 2) + Math.Pow(fcs[2] - obs[2], 2));
            

            int frame = Int32.Parse(textBox1.Text);
                
            //Display the first skeleton
          SkeletonRecord firstSkeleton=this.records.GetRecord(frame);
          this.painter1.drawSkel(firstSkeleton, fcs, obs);

          //Display the synchronized skeleton
          SkeletonRecord synchronizedSkel=this.records_clean.GetRecord(frame);
          this.painter2.drawSkel(synchronizedSkel, fcs, obs);


        
         int startPos=(int)this.rangeSlider1.StartPosition;
         int endPos = (int)this.rangeSlider1.EndPosition;

         JointAnglesIK angles_ik = synchronizedSkel.GetJointAnglesIK();
                     
         String[] names = Enum.GetNames(typeof(JointAnglesIK.JointIndex));
         this._AngleCollection.Clear();
            for (int i = 0; i < names.Length; i++)
            {

                AngleData aData = new AngleData() 
                {
                    JointName=names[i],
                    AngleValueX=string.Format("{0:f2}",angles_ik.GetAngleNew(i,0)),
                    AngleValueY = string.Format("{0:f2}", angles_ik.GetAngleNew(i, 1)),

                };
                if(endPos>startPos)
                    this.records_clean.MaxMinOrientationIKNew(i, startPos, endPos, aData);
                this._AngleCollection.Add(aData);

                 
            }
                        
            DateTime dt = new DateTime((long)synchronizedSkel.GetTimeStamp());
            this.timeStamp_label.Content = "TimeStamp: " + dt.ToShortDateString() + " : " + dt.ToLongTimeString();
            this.rangeLabel.Content="Selected Range: ("+startPos.ToString() +" : "+ endPos.ToString()+")";

            if (this.version > 0)
                DisplayOrientations(synchronizedSkel,firstSkeleton);

           
            
           
        }

 
        /// <summary>
        /// Displays 20 x 3 bone orientations in the textbox
        /// </summary>
        private void DisplayOrientations(SkeletonRecord synchronizedSkeleton, SkeletonRecord firstSkeleton)
        {
            double[] orientation_holder = firstSkeleton.GetOrientations();
            double[] orientation_holder3 = synchronizedSkeleton.GetOrientations();
            string orient_string = "";
            for (int j = 0; j < 20; j++)
            {
                orient_string += JointAnglesIK.JName(j) + ": " + string.Format("{0:f2}", orientation_holder3[j*3] * 180 / pi) +
                                             ",  " + string.Format("{0:f2}", orientation_holder3[j*3 + 1] * 180 / pi) +
                                             ",  " + string.Format("{0:f2}", orientation_holder3[j*3 + 2] * 180 / pi) + " ";
                 
                orient_string += "first " + string.Format("{0:f2}", orientation_holder[j*3] * 180 / pi) +
                                           ",  " + string.Format("{0:f2}", orientation_holder[j*3 + 1] * 180 / pi) +
                                           ",  " + string.Format("{0:f2}", orientation_holder[j*3 + 2] * 180 / pi) + "\n";

            }
            this.textBox4.Text = orient_string;

        }


        
       /* private string JName(int s)
        {
            switch (s)
            {
                case 0: return "hip centre";
                case 1: return "spine";
                case 2: return "shoulder centre";
                case 3: return "head";
                case 4: return "left shoulder";
                case 5: return "left elbow";
                case 6: return "left wrist";
                case 7: return "left hand";
                case 8: return "right shoulder";
                case 9: return "right elbow";
                case 10: return "right wrist";
                case 11: return "right hand";
                case 12: return "left hip";
                case 13: return "left knee";
                case 14: return "left ankle";
                case 15: return "left foot";
                case 16: return "right hip";
                case 17: return "right knee";
                case 18: return "right ankle";
                case 19: return "right foot";
                case 20: return "straight up";
                case 21: return "straight down";

                    //key {12 4 5} is not possible left hip, left shoulder , left elbow,  
                    //there should be spine inplace of left hip


            }
            return "ERROR!";
        }
        */


        private double JKAngle(double[] j1, double[] j2, double[] j3)
        // p1x, double p1y, double p1z, double p2x, double p2y, double p2z, double p3x, double p3y, double p3z)
        {
            double[] v1 = new double[3];
            double[] v2 = new double[3];
            v1[0] = j2[0] - j1[0];
            v1[1] = j2[1] - j1[1];
            v1[2] = j2[2] - j1[2];
            v2[0] = j3[0] - j1[0];
            v2[1] = j3[1] - j1[1];
            v2[2] = j3[2] - j1[2];
            double dp = v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
            double av1 = v1[0] * v1[0] + v1[1] * v1[1] + v1[2] * v1[2];
            double av2 = v2[0] * v2[0] + v2[1] * v2[1] + v2[2] * v2[2];
            double cth = Math.Acos(dp / (Math.Sqrt(av1) * Math.Sqrt(av2)));
            cth *= conv;
            return cth;
        }
      

        private void listBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.records.Clear();
           // try
            //{
                 
                ListBoxItem li = (ListBoxItem)listBox1.SelectedItem; 
                if (li != null)
                {
                    this.ApplyVersion();
                    string selectedIndex = li.Tag + "";
                    String nd = "000" + selectedIndex.PadLeft(5, '0') + ".dta";
                    mainstring = GetFullFile("a" + nd);
                    secondstring = GetFullFile("c" + nd);
                    if (File.Exists(mainstring) && File.Exists(secondstring))
                    {
                        this.records.LoadFromFile(mainstring, this.version);
                        this.records_clean.LoadFromFile(secondstring, this.version);

                        this.button3.IsEnabled = true;
                        this.button2.IsEnabled = true;
                        this.rangeSlider1.isEnabled = true;
                       
                        textBox4.Text = "";
                        this.label1.Content = "/" + this.records_clean.GetCount();
                        // this.slider1.Maximum = this.records_clean.GetCount()-1;
                        //this.slider1.Minimum = 0;
                        //this.slider1.Value = 0;

                        this.rangeSlider1.Maximum = this.records_clean.GetCount() - 1;
                        this.rangeSlider1.Minimum = 0;
                        this.rangeSlider1.Value = 0;
                        this.rangeSlider1.ResetPositions();
                         
                        OpenFile();
                      //  records.saveASCII_JointAnglesIK(GetFullFile("jaik.txt"));
                       // records.saveASCII_JointAnglesKinect(GetFullFile("ja_kinect.txt"));

                       
                        
                        
                        
                        

                    }
                    else
                    {
                        
                        rangeSlider1.isEnabled = false;
                        this.canvas1.Children.Clear();
                        this.canvas2.Children.Clear();
                        this.button3.IsEnabled = false;
                        this.button2.IsEnabled = false;
                        textBox4.Text = "Data not found for the selection";

                    }
                }
                
                 
          //  }
          //  catch(Exception exx)
          //  {
           //     MessageBox.Show(exx.Message);
            //}
        }
 
        
        private string JShow(double d)
        {
            double d2 = Math.Round(d, 4);
            return d2.ToString();
        }

        private void OpenFile()
        {

            if (this.records_clean.GetCount() > 0)
            {
                double[] joints = this.records_clean.GetRecord(0).GetJointLocations();
                fcs[0] = joints[3];
                fcs[1] = joints[4];
                fcs[2] = joints[5];

                DrawIt();
            }
        }

        private void JScale(double depth2)
        {
            depth2 /= depth;
            obs[0] = fcs[0] + depth2 * (obs[0] - fcs[0]);
            obs[1] = fcs[1] + depth2 * (obs[1] - fcs[1]);
            obs[2] = fcs[2] + depth2 * (obs[2] - fcs[2]);

            DrawIt();

        }

        private void JMove(double ang1, double ang2)
        {
            double zx = Math.Pow(obs[0] - fcs[0], 2) + Math.Pow(obs[2] - fcs[2], 2);
            double theta1 = Jat(obs[1] - fcs[1], Math.Sqrt(zx));
            double theta2 = Jat(obs[2] - fcs[2], obs[0] - fcs[0]);

            theta1 += ang1 / conv;
            theta2 += ang2 / conv;
            obs[0] = depth * Math.Cos(theta1) * Math.Cos(theta2) + fcs[0];
            obs[1] = depth * Math.Sin(theta1) + fcs[1];
            obs[2] = depth * Math.Sin(theta2) * Math.Cos(theta1) + fcs[2];
            DrawIt();
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {
            /* Move Left */
            JMove(0.0, -5.0);

        }

        private void button5_Click(object sender, RoutedEventArgs e)
        {
            /* Move Right */
            JMove(0.0, 5.0);

        }

        private void button6_Click(object sender, RoutedEventArgs e)
        {
            /* Move Up */
            JMove(-5.0, 0.0);

        }

        private void button7_Click(object sender, RoutedEventArgs e)
        {
            /* Move Down */
            JMove(5.0, 0.0);

        }

        private void button8_Click(object sender, RoutedEventArgs e)
        {
            /* Zoom In */
            if (depth > 0.5)
                JScale(depth - 0.5);

        }

        private void button9_Click(object sender, RoutedEventArgs e)
        {
            /* Zoom Out */
            JScale(depth + 0.5);

        }

        private void button10_Click(object sender, RoutedEventArgs e)
        {   /* evaluate chosen recording */
            int rule = 0;
            res = "";
            bFile = new BinaryReader(File.Open(mainstring, FileMode.Open));
            maxlim = bFile.BaseStream.Length / recsize;

            header = "Date,Time";

            epoch = Int32.Parse(textBox5.Text);

            double d = GetTime(0);
            DateTime dt = new DateTime((long)d);
            int time = rawtime(dt.ToLongTimeString());
            time = (int)Math.Floor((decimal)time / epoch) * epoch;
            beginning = time;
            d = GetTime(maxlim - 1);
            DateTime dt2 = new DateTime((long)d);
            int time2 = rawtime(dt2.ToLongTimeString());
            time2 = (int)Math.Floor((decimal)time2 / epoch) * epoch;

            bFile.Close();

            nume = (time2 - time) / epoch;
            opd = new string[++nume];
            mcnt = new int[nume];
            for (int ji = 0; ji < nume; ji++)
            {
                opd[ji] = dt.ToShortDateString() + "," + stringtime(time);
                time += epoch;
            }

      


            //rules are same for all sessions
            this.activityFrames.Clear();
            using (StreamReader b = new StreamReader(@"C:\\kinectdata\rules\rules.txt"))
            {
                string s;
                while ((s = b.ReadLine()) != null)
                {
                    res += "Looking for " + s + "..." + Environment.NewLine;
                    for (int ji = 0; ji < nume; ji++) mcnt[ji] = 0;
                    List<long> frames=LookFor(rule++);
                    this.activityFrames.Add(s, frames);
                                                          
                    header += "," + s;
                    for (int ji = 0; ji < nume; ji++) opd[ji] += "," + mcnt[ji].ToString();
                }
            }


            res += "---------------------------------------------------------" + Environment.NewLine;
            string res2 = header + Environment.NewLine;
            for (int ji = 0; ji < nume; ji++)
                res2 += opd[ji] + Environment.NewLine;
            textBox4.Text = res + res2;

            string op_csvfile=(string)((ListBoxItem)listBox1.SelectedItem).Content+".csv"; 
            StreamWriter t = new StreamWriter(File.Open( GetFullFile(op_csvfile), FileMode.Create));
            t.Write(res2);
            t.Close();

             

            Color[] colors = {Colors.Red, Colors.Green,Colors.Blue,Colors.Yellow,Colors.Black, Colors.Aqua,Colors.Black,Colors.Beige,Colors.Azure,Colors.BlanchedAlmond };
            int count = 0;
            this.activityFrames_combo.Items.Clear();
            foreach( System.Collections.DictionaryEntry  aF in this.activityFrames)
            {
                List<long> frames = (List<long>)aF.Value;
                string anAction = (string)aF.Key;
                ComboBoxItem cbox_act = new ComboBoxItem();
                cbox_act.Content= anAction+"("+ frames.Count +")";
                cbox_act.Tag=anAction;
                
                this.activityFrames_combo.Items.Add(cbox_act);
                bool clearPrevLabels;
                if (count > 0)
                    clearPrevLabels = false;
                else
                    clearPrevLabels = true;

                this.rangeSlider1.drawLabelsForFrames(frames, colors[count++%colors.Length], clearPrevLabels);
            }     
             
             
        }


        /**
         * 
         */

        private void activityFrames_combo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxItem ci = (ComboBoxItem)this.activityFrames_combo.SelectedValue;
            if (ci != null)
            {
                string aKey = (string)ci.Tag;
                if (this.activityFrames.ContainsKey(aKey))
                {
                    List<long> frames = (List<long>)this.activityFrames[aKey];
                    this.rangeSlider1.drawLabelsForFrames(frames, Colors.Red, true);

                }
            }
            

        }


        private void button10_Click_new_not_used(object sender, RoutedEventArgs e)
        {
            ActivityDetector aD = new ActivityDetector(this.projectDir,this.mainstring, this.recsize,this);
            epoch = Int32.Parse(textBox5.Text);
            textBox4.Text = aD.DetectActivities(epoch); 

        }


        /*
         * 
         * @returns  a list of  frame numbers where the activities are detected. 
         */

        private List<long> LookFor(int rule)
        {
            rulestring = "";
            List<long> frames = new List<long>();
            
            

            //rules are same for all sessions
            using (StreamReader b = new StreamReader(@"C:\\kinectdata\rules\rule" + rule.ToString() + ".txt"))
            {
                string s;
                while ((s = b.ReadLine()) != null)
                {
                    if (rulestring.Length == 0) rulestring = s;
                    else
                    {
                        string[] prt = s.Split('~');
                        if (prt[0] == "0") rulestring += '&' + s;
                        else if (prt[0] == "1") rulestring += '|' + s;
                        else rulestring += '>' + s;
                    }
                }
            }

            bFile = new BinaryReader(File.Open(mainstring, FileMode.Open));
            maxlim = bFile.BaseStream.Length / recsize;
            for (long current = 0; current < maxlim; current++)
            {
                answers = 0;
                if (ParseRule(rulestring, current, maxlim) == true)
                {
                    frames.Add(current);
                    res += "Found : from " + current.ToString() + " to " + ans[answers - 1].ToString() + Environment.NewLine;
                    DateTime found = new DateTime((long)GetTime(current));
                    int i = rawtime(found.ToLongTimeString());
                    i = (int)Math.Floor((decimal)(i - beginning) / epoch);
                    mcnt[i]++;
                    current = ans[answers - 1];

                }
            }

            bFile.Close();

            return frames;
        }

        private bool ParseRule(string r, long start, long finish)
        {

            int a = r.IndexOf('>');
            //            res += "[start:" + start.ToString() + "][fin:" + finish.ToString() + "][" + r + "]" + Environment.NewLine;
            if (a > 0)
            {   // process THEN operator
                if (ParseRule(r.Substring(0, a), start, finish))
                {
                    long l = ans[answers - 1];
                    for (long ll = l; (ll < l + 2) && (ll < finish); ll++)
                        if (ParseRule(r.Substring(a + 1), ll, finish)) return true;
                }
            }
            else
            {  // process AND operator
                a = r.IndexOf('&');
                if (a > 0)
                {
                    if (ParseRule(r.Substring(0, a), start, finish))
                    {
                        long asave = ans[answers - 1];
                        bool b = ParseRule(r.Substring(a + 1), start, (long)Math.Min(finish, ans[answers - 1] + 3));
                        if (b == true)
                            ans[answers - 1] = Math.Max(ans[answers - 1], asave);
                        return b;
                    }
                    else
                        return false;
                }
                else
                    return Possible(r, start, finish);
            }
            return false;
        }



        private double[] GetPoint(long frame, int joint)
        {
            double[] point = new double[3];
            bFile.BaseStream.Seek(frame * recsize + (long)joint * 25, SeekOrigin.Begin);
            point[0] = bFile.ReadDouble();
            point[1] = bFile.ReadDouble();
            point[2] = bFile.ReadDouble();
            return point;
        }

        private double[] GetExtraPoint(long frame, int joint, double[] t1, double[] t2)
        {
            double[] point = new double[3];
            if (joint < 20) return GetPoint(frame, joint);
            if (joint == 20)
            {
                point[0] = t1[0];
                point[1] = 10000;
                point[2] = t1[2];
                return point;
            }
            if (joint == 21)
            {
                point[0] = t1[0];
                point[1] = -100000;
                point[2] = -t1[2];
                return point;
            }
            if (joint == 22)
            {
                point[0] = t2[0];
                point[1] = t2[1];
                point[2] = t1[2];
                return point;
            }
            return null;
        }


        private double GetTime(long frame)
        {
            //bFile.BaseStream.Seek(frame * recsize + 500, SeekOrigin.Begin);
            
            //by this way we done need to chage this function whe recSize is changed provided that timestamp is saved at the end of the record
            bFile.BaseStream.Seek(frame * recsize + recsize-8, SeekOrigin.Begin);
            return bFile.ReadDouble();
        }

        public  bool Possible(string r, long begin, long finish)
        {
            double[] tmp = new double[3];
            double[] tmp1 = new double[3];
            double[] tmp2 = new double[3];
            double[] tmp3 = new double[3];
            double stt = 0, fin = 0, chng = 0;
            cr = r.Split('~');
            int j = Int32.Parse(cr[2]), typ = Int32.Parse(cr[1]);
            long rec = begin;
            double start = GetTime(begin), tnow;
            tmp = GetPoint(rec, j);
            if (typ == 1)
            {
                tmp2 = GetPoint(rec, Int32.Parse(cr[3]));
                tmp3 = GetExtraPoint(rec, Int32.Parse(cr[4]), tmp, tmp2);
                stt = JKAngle(tmp, tmp2, tmp3);
            }
            else stt = 0;
            if (debug == true)
                res += "Posible [" + r + "][stt=" + stt.ToString() + "][begin=" + begin.ToString() + "][finish=" + finish.ToString() + "]" + Environment.NewLine;
            if (cr[7].Length > 0)
                if (stt < Double.Parse(cr[7])) return false;
            if (cr[8].Length > 0)
                if (stt > Double.Parse(cr[8])) return false;

            double temp_test;
            while (++rec < finish)
            {
                bool ok = true;
                tnow = GetTime(rec);
                if (cr[12].Length > 0)
                    if (Double.TryParse(cr[12], out temp_test) && (tnow - start) / 10000000.0 > Double.Parse(cr[12])) return false;
                tmp1 = GetPoint(rec, j);
                if (typ == 1)
                {
                    tmp2 = GetPoint(rec, Int32.Parse(cr[3]));
                    tmp3 = GetExtraPoint(rec, Int32.Parse(cr[4]), tmp1, tmp2);
                    fin = JKAngle(tmp1, tmp2, tmp3);
                }
                if (typ == 2)
                    fin = 100 * (tmp1[1] - tmp[1]);
                if (typ == 3)
                    fin = 100 * Math.Sqrt(Math.Pow(tmp1[0] - tmp[0], 2) + Math.Pow(tmp1[1] - tmp[1], 2) + Math.Pow(tmp1[2] - tmp[2], 2));
                chng = fin - stt;
                if (debug == true)
                    res += "    rec=" + rec.ToString() + " fin=" + fin.ToString() + " chng=" + chng.ToString() + Environment.NewLine;
                if (cr[9].Length > 0)
                    if (fin < Double.Parse(cr[9])) ok = false;
                if (cr[10].Length > 0)
                    if (fin > Double.Parse(cr[10])) ok = false;
                if (cr[5].Length > 0)
                    if (chng < Double.Parse(cr[5])) ok = false;
                if (cr[6].Length > 0)
                    if (chng > Double.Parse(cr[6])) ok = false;
                if (cr[11].Length > 0)
                    if ((tnow - start) / 10000000.0 < Double.Parse(cr[11])) ok = false;
                if (ok == true)
                {
                    ans[answers++] = rec;
                    if (debug == true)
                        res += "Found answer upto frame " + rec.ToString() + Environment.NewLine;
                    return true;
                }
            }
            return false;
        }

        private string stringtime(int i)
        {
            string t = "";
            if (i > 43200)
            {
                t = " PM";
                i -= 43200;
            }
            else
                t = " AM";
            int h = (int)Math.Floor((decimal)i / 3600);
            i -= h * 3600;
            if (h == 0) h = 12;
            int m = (int)Math.Floor((decimal)i / 60);
            i -= m * 60;
            t = h.ToString() + ":" + m.ToString().PadLeft(2, '0') + ':' + i.ToString().PadLeft(2, '0') + t;
            return t;

        }

        private int rawtime(string s)
        {
            string[] pt = s.Split(':');
            int ampm = 0;
            int h = Int32.Parse(pt[0]);
            if (h == 12) h = 0;
            if (pt[2].IndexOf("PM") > 0) ampm = 43200;
            pt[2] = pt[2].Substring(0, 2);
            return ampm + Int32.Parse(pt[2]) + 60 * Int32.Parse(pt[1]) + 3600 * h;
        }

         
        /// <summary>
        /// Populates the session combobox with the  recorded sessions saved in directories under c:\kinectdata\
        /// </summary>
        /// <returns> String array of list of data directories</returns>
        private string[] populateSessionComboBox()
        {
            string[] dirlist = Directory.GetDirectories(@"C:\kinectdata");
            List<string> aList = dirlist.ToList();
            aList.Remove(@"C:\kinectdata\rules");
            string[] finallist = aList.ToArray();
            ComboBoxItem cbox_message = new ComboBoxItem();
            cbox_message.Content = "Select a session directory";
            cbox_message.Tag = "";
            
            this.sessions_combo.Items.Add(cbox_message);

            for (int i = 0; i < finallist.Length; i++)
            {
                ComboBoxItem cboxitem = new ComboBoxItem();

                 cboxitem.Content = finallist[i];
                 cboxitem.Tag = finallist[i];
                 this.sessions_combo.Items.Add(cboxitem);
                
            }

            return finallist;

        }

        private void sessions_combo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxItem ci=(ComboBoxItem)this.sessions_combo.SelectedValue;
            this.projectDir = (string)ci.Tag;
            UpdateList();
            this.rangeSlider1.isEnabled = false; 
            //OpenFile();
        }

        private string GetFullFile(string aFile)
        {

            return System.IO.Path.Combine(this.projectDir, aFile);
            
        }

       /* private void slider1_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            
            textBox1.Text = Math.Round(this.slider1.Value) + "";
            this.DrawIt();
        }
        */

        private void rangeSlider1_ValueChanged(object sender, EventArgs  e)
        {
           // this.textBox2.Text = this.rangeSlider1.slider1.Value.ToString();
            textBox1.Text = Math.Round(this.rangeSlider1.Value) + "";
            this.DrawIt(); 
            
        }

        private void rangeSlider1_ThumbChanged(object sender, EventArgs e)
        {
            this.textBox2.Text =  (int) this.rangeSlider1.StartPosition+", "+this.rangeSlider1.EndPosition.ToString();
           // updateStatistics();
            DrawIt();

        }


 
    }


    
}
