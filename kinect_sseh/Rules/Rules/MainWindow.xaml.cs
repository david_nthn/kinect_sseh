﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace WpfApplication1
{ 
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        String mainstring = @"c:\\kinectdata\rules\rules.txt";
        int action = -1;
        byte editmode = 0;

        public MainWindow()
        {
            InitializeComponent();
            UpdateList();
            SetUpBoxes();
        }

        private void SetUpBoxes()
        {
            listBox2.Items.Clear();
            joint1.Items.Clear();
            joint2.Items.Clear();
            joint3.Items.Clear();
            listBox2.Items.Add("AND");
            listBox2.Items.Add("OR");
            listBox2.Items.Add("THEN");
            for (int e = 0; e < 20; e++)
            {
                joint1.Items.Add(JName(e));
                joint2.Items.Add(JName(e));
                joint3.Items.Add(JName(e));

            }
            for (int e = 20; e < 23; e++)
                joint3.Items.Add(JName(e));
        }

        private void UpdateList()
        {

            listBox1.Items.Clear();
            if (File.Exists(mainstring))
            {
                String s;
                using (StreamReader b = new StreamReader(mainstring))
                {
                    while ((s = b.ReadLine()) != null)
                            listBox1.Items.Add(s);
                }
            }
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            if (textBox1.Text.Length > 0)
            {
                using (StreamWriter b = new StreamWriter(mainstring, true))
                    b.WriteLine(textBox1.Text);
                textBox1.Text = "";
                textBox1.Focus();
                UpdateList();

            }
        }

        private void listBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            goAddMode();
            ShowThisRule();
        }

        private string JName(int s)
        {
            switch (s)
            {
                case 0: return "hip centre";
                case 1: return "spine";
                case 2: return "shoulder centre";
                case 3: return "head";
                case 4: return "left shoulder";
                case 5: return "left elbow";
                case 6: return "left wrist";
                case 7: return "left hand";
                case 8: return "right shoulder";
                case 9: return "right elbow";
                case 10: return "right wrist";
                case 11: return "right hand";
                case 12: return "left hip";
                case 13: return "left knee";
                case 14: return "left ankle";
                case 15: return "left foot";
                case 16: return "right hip";
                case 17: return "right knee";
                case 18: return "right ankle";
                case 19: return "right foot";
                case 20: return "straight up";
                case 21: return "straight down";
                case 22: return "horizontal";

            }
            return "ERROR!";
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            if (editmode == 1)
            {
                saveEdit();
                return;
            }
            if (action < 0)
            {   MessageBox.Show("Please Choose an Action!");
                return;
            }

            using (StreamWriter b = new StreamWriter(@"C:\\kinectdata\rules\rule" + action.ToString() + ".txt", true))
            {
                String s = listBox2.SelectedIndex.ToString() + '~';
                if (radioButton1.IsChecked == true) s = s + '1';
                if (radioButton2.IsChecked == true) s = s + '2';
                if (radioButton3.IsChecked == true) s = s + '3';
                s = s + '~' + joint1.SelectedIndex.ToString() + '~' + joint2.SelectedIndex.ToString() + '~' + joint3.SelectedIndex.ToString() + '~' + textBox2.Text + '~' + textBox3.Text + '~' + textBox4.Text + '~' + textBox5.Text + '~' + textBox6.Text + '~' + textBox7.Text + '~' + textBox8.Text + '~' + textBox9.Text;
                b.WriteLine(s);
            }
            ShowThisRule();
        }

        private void ShowThisRule()
        {
            action = listBox1.SelectedIndex;
            String Fname = @"c:\\kinectdata\\rules\rule" + action.ToString() + ".txt";
            ruleBox.Items.Clear();
            if (File.Exists(Fname))
            {
                String s;
                using (StreamReader b = new StreamReader(Fname))
                {
                    while ((s = b.ReadLine()) != null)
                        ruleBox.Items.Add(ParseRule(s));
                    
                }
            }
        }

      

        private String ParseRule(String s)
        {   bool vrg = true;
            string rv = "", units="";
            string[] v = s.Split('~');
            int j1 = Int32.Parse(v[2]), j2 = Int32.Parse(v[3]), j3 = Int32.Parse(v[4]);
            if (v[0] == "0") rv = "AND";
            else if (v[0] == "1") rv = "OR";
            else
            {
                if (vrg == false) rv = "THEN";
            }
            vrg = false;
            if (v[1] == "2") rv += " Vertical Movement of the " + JName(j1);
            else if (v[1] == "3") rv += " Total Movement  of the " + JName(j1);
            else rv += " Joint Angle at " + JName(j1) + " from the " + JName(j2) + " to the " + JName(j3);

            if ((v[1] == "0") || (v[1] == "1")) units = " degrees";
            else units = " centimetres";
            if ((v[5].Length > 0) || (v[6].Length > 0))
            {
                if (v[5].Length > 0)
                {
                    if (v[6].Length > 0)
                        rv = rv + " changes between " + v[5] + " to " + v[6];
                    else
                        rv = rv + " changes at least " + v[5];
                }
                else
                    rv = rv + " changes less than  " + v[6];
                rv = rv + units;
            }
            if ((v[7].Length > 0) || (v[8].Length > 0))
            {
                if (v[7].Length > 0)
                {
                    if (v[8].Length > 0)
                        rv = rv + " starts from between " + v[7] + " and " + v[8];
                    else
                        rv = rv + " starts from at least " + v[7];
                }
                else
                    rv = rv + " starts from less than  " + v[8];
                rv = rv + units;
            }

            if ((v[9].Length > 0) || (v[10].Length > 0))
            {
                if (v[9].Length > 0)
                {
                    if (v[10].Length > 0)
                        rv = rv + " ends between " + v[9] + " and " + v[10];
                    else
                        rv = rv + " ends at least " + v[9];
                }
                else
                    rv = rv + " ends less than  " + v[10];
                rv = rv + units;
            }


            if ((v[11].Length > 0) || (v[12].Length > 0))
            {
                if (v[11].Length > 0)
                {
                    if (v[12].Length > 0)
                        rv = rv + " within " + v[11] + " to " + v[12] + " seconds";
                    else
                        rv = rv + " in at least " + v[11] + " seconds";
                }
                else
                    rv = rv + " in under " + v[12] + " seconds";
            }
            return rv;
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            if (editmode > 0)
            {
                goAddMode();
            }
            else
            {
                action = listBox1.SelectedIndex;
                String Fname = @"c:\\kinectdata\\rules\rule" + action.ToString() + ".txt";
                File.Delete(Fname);
                ShowThisRule();
            }
        }

        private void goAddMode()
        {
            editmode = 0;
            button2.Content = "ADD RULE";
            button3.Content = "RESTART";
        }

        private void goEditMode()
        {
            editmode = 1;
            button2.Content = "UPDATE";
            button3.Content = "CANCEL";

        }

        private void ruleBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int cnt = 0,  i = ruleBox.SelectedIndex;
            if (i < 0) return;
            action = listBox1.SelectedIndex;
            if (action < 0) return; 
            String Fname = @"c:\\kinectdata\\rules\rule" + action.ToString() + ".txt";
            String s;
            using (StreamReader b = new StreamReader(Fname))
            {
               while ((s = b.ReadLine()) != null) {
                   if (cnt++ == i)
                   {
                       String[] pt = s.Split('~');
                       textBox2.Text = pt[5];
                       textBox3.Text = pt[6];
                       textBox4.Text = pt[7];
                       textBox5.Text = pt[8];
                       textBox6.Text = pt[9];
                       textBox7.Text = pt[10];
                       textBox8.Text = pt[11];
                       textBox9.Text = pt[12];
                       if (pt[0] == "0") listBox2.SelectedIndex = 0;
                       else if (pt[0] == "1") listBox2.SelectedIndex = 1;
                       else listBox2.SelectedIndex = 2;
                       joint1.SelectedIndex = Int32.Parse(pt[2]);
                       joint1.ScrollIntoView(joint1.SelectedItem);
                       int t = Int32.Parse(pt[3]);
                       if (t >= 0)
                       {
                           joint2.SelectedIndex = t;
                           joint2.ScrollIntoView(joint2.SelectedItem);
                       }
                       t = Int32.Parse(pt[4]);
                       if (t >= 0)
                       {
                           joint3.SelectedIndex = t;
                           joint3.ScrollIntoView(joint3.SelectedItem);
                       }
                       t = Int32.Parse(pt[1]);
                       if (t == 2) radioButton2.IsChecked = true;
                       else if (t == 3) radioButton3.IsChecked = true;
                       else radioButton1.IsChecked = true;
                   }
                }
            }
            goEditMode();
        }

        private void saveEdit()
        {
            int cnt = 0,  i = ruleBox.SelectedIndex;
            if (i < 0) return;
            String Fname = @"c:\\kinectdata\\rules\rule" + action.ToString() + ".txt";
            String s;
            String[] old = new String[20];
            using (StreamReader b = new StreamReader(Fname))
            {
               while ((s = b.ReadLine()) != null) 
                 old[cnt++] = s;
            }
            File.Delete(Fname);
            using (StreamWriter b = new StreamWriter(Fname, true))
            {
                for (int jcnt = 0; jcnt < cnt; jcnt++)
                {
                    if (jcnt == i)
                    {
                        s = listBox2.SelectedIndex.ToString() + '~';
                        if (radioButton1.IsChecked == true) s = s + '1';
                        if (radioButton2.IsChecked == true) s = s + '2';
                        if (radioButton3.IsChecked == true) s = s + '3';
                        s = s + '~' + joint1.SelectedIndex.ToString() + '~' + joint2.SelectedIndex.ToString() + '~' + joint3.SelectedIndex.ToString() + '~' + textBox2.Text + '~' + textBox3.Text + '~' + textBox4.Text + '~' + textBox5.Text + '~' + textBox6.Text + '~' + textBox7.Text + '~' + textBox8.Text + '~' + textBox9.Text;
                        b.WriteLine(s);
                    }
                    else b.WriteLine(old[jcnt]);
                }
            }
            ShowThisRule();
        }

    
    }
}
