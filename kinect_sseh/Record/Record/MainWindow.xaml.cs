﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;
using System.IO;
using System.Diagnostics;
using System.Collections;
using System.Windows.Threading;
 
namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        IRTimer irTimer = new IRTimer();
        const int skeletonCount = 6;
        Skeleton[] allSkeletons = new Skeleton[skeletonCount];
        int cnt = 0;
        int sts = 0;
        long start = 0L, maxlim = 0;
        const double conv = 57.29577951308232; /* radians to degrees */
        const double pi = 3.141592653589793;

        System.IO.BinaryWriter bWriter = null;
        System.IO.BinaryReader bRead = null;
        private KinectSensor sensor1;
        JointCollection jp;
        /// <summary>
        /// Number of bytes taken by one skeleton record
        /// (20 *3 *8) for skeletons,
        /// (20 *3 *8) for orientatons,
        /// 8 for timestamp 
        /// 20 for validity
        /// Total=508 +480
        /// </summary>
        const int recsize = 508 + 480; 
        double[,] dta = new double[20, 3];
        char[] tst = new char[20];
        double[,] dta2 = new double[20, 3];
        char[] tst2 = new char[20];
        double th = 0, cth = 0, sth = 0;
        double[] tt = new double[3];
        double tim = 0;
        long timeup = 0L;
        int calibrating = 0;
        long secondrec = 0;

        float[,] boneOrietationHolder = new float[20, 3];

        Process secondKinect;



        string aclist = "", mainstring = "", secondstring = "", cleanstring = "";

        
       
        StreamWriter boneOrientationWriter;

        /// <summary>
        /// Bitmap that will hold color information
        /// </summary>
        private WriteableBitmap colorBitmap;

        /// <summary>
        /// Intermediate storage for the color data received from the camera
        /// </summary>
        private byte[] colorPixels;
        string projectDir = "";

        private double[] orientation_holder  = new double[60];  // to hold orientations of each frame of first kinect (used when  saving and reading from first file for merging);
        private double[] orientation_holder2  =new double[60]; // to hold orientations of each frame of second kinect (used when  reading from second file for merging);


        public MainWindow()
        {   
            InitializeComponent();
            
            this.Show();


          
            if (!ChooseFolder())
            {
                
                this.Close();
                
                return;
            }
            
            //do not display image by default
            this.DispImg.IsChecked = false;
            this.DispImg_Checked(this.DispImg, null);

            this.aclist = System.IO.Path.Combine(this.projectDir, "recordings.txt");
            if (FindSensor1() != true)
            {
                MessageBox.Show("No Kinect Sensors Found");
            }
            else
            {
                this.irTimer.TimerTask += irTimer_TimerTask;
               // this.irTimer.Start();
            }



             using (StreamWriter vWriter = new StreamWriter(GetFullFile("version.txt"), true)) { vWriter.WriteLine("1"); }
             StartSecondKinectProcess();

        }


        /**
         * Starts the external program  that handles the data from the second kinect as a process.
         * The window of the second program is  docked on to the top right corner of the main widow.
         */
        
        private  void   StartSecondKinectProcess()
        {
            
            
            string secondProgramFile = @"C:\kinectapps\\Record\Second.exe";

            if (File.Exists(secondProgramFile))
            {
                ProcessStartInfo secondProcess = new ProcessStartInfo();
                secondProcess.FileName = secondProgramFile;
                double secondWindowLeftPos = this.Width + this.Left;
                double secondWidowTopPos = this.Top;
                secondProcess.Arguments = secondWindowLeftPos + @"   " + secondWidowTopPos+@"  "+this.projectDir;
                this.secondKinect = Process.Start(secondProcess);
            }
            else
            {
                MessageBox.Show(" A program that handles the second Kinect could not be found! " + secondProgramFile);
            }
            
       
        }



        private bool FindSensor1()
        {
            foreach (var potentialSensor in KinectSensor.KinectSensors)
                if (potentialSensor.Status == KinectStatus.Connected)
                {
                    this.sensor1 = potentialSensor;
                    this.sensor1.SkeletonStream.Enable();
                    this.sensor1.SkeletonFrameReady += new EventHandler<SkeletonFrameReadyEventArgs>(sensor1_SkeletonFrameReady);
                    InitializeImageReceiver();
                    
                    try
                    {
                        this.sensor1.Start();
                        sensor1.ElevationAngle = 0;
                    }
                    catch
                    {
                        MessageBox.Show("Cannot Start Sensor 1");
                        return false;
                    }

                    return true;
                }
            return false;
        }

        private void sensor1_SkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {

        
            canvas1.Children.Clear();
            Rectangle rect = new Rectangle { Stroke = Brushes.Black, StrokeThickness = 1 };
            Canvas.SetLeft(rect, 0);
            Canvas.SetTop(rect, 0);
            rect.Width = 400;
            rect.Height = 400;
            canvas1.Children.Add(rect);

            Line l1 = new Line { Stroke = Brushes.DarkGray, StrokeThickness = 1 };
            Line l2 = new Line { Stroke = Brushes.DarkGray, StrokeThickness = 1 };

            l1.X1 = 200;
            l1.X2 = 200;
            l1.Y1 = 0;
            l1.Y2 = 400;
            l2.X1 = 0;
            l2.X2 = 400;
            l2.Y1 = 200;
            l2.Y2 = 200;

            canvas1.Children.Add(l1);
            canvas1.Children.Add(l2);

            Skeleton first = GetFirstSkeleton(e);




             

            if (first == null)
            {
                // wait
            }
            else
            {



                //store in the joint locations jp field 
                jp = first.Joints;

                drawSeg(JointType.ShoulderCenter, JointType.HipCenter);
                drawSeg(JointType.ElbowLeft, JointType.WristLeft);
                drawSeg(JointType.ShoulderCenter, JointType.ShoulderLeft);
                drawSeg(JointType.ElbowLeft, JointType.ShoulderLeft);
                drawSeg(JointType.ElbowRight, JointType.WristRight);
                drawSeg(JointType.ShoulderCenter, JointType.ShoulderRight);
                drawSeg(JointType.ElbowRight, JointType.ShoulderRight);

                drawSeg(JointType.KneeLeft, JointType.AnkleLeft);
                drawSeg(JointType.HipCenter, JointType.HipLeft);
                drawSeg(JointType.KneeLeft, JointType.HipLeft);
                drawSeg(JointType.KneeRight, JointType.AnkleRight);
                drawSeg(JointType.HipCenter, JointType.HipRight);
                drawSeg(JointType.KneeRight, JointType.HipRight);

                drawHand(JointType.HandLeft, 0);
                drawHand(JointType.HandRight, 0);
                drawHand(JointType.FootLeft, 0);
                drawHand(JointType.FootRight, 0);
                drawHand(JointType.Head, 1);

                //lead  in time
                if (sts > 1)
                    SaveSkeleton(first);//saves the skeleton in a file

            }
            //  combine recordings from two kinect
            if (sts > 2)
            {
                sts = 0;
                textBox2.Text = "ENDED!";
                bWriter.Close();
                if(this.boneOrientationWriter!=null) 
                    this.boneOrientationWriter.Close();
               
                File.Delete(System.IO.Path.Combine(this.projectDir, "config.dta"));
                if (File.Exists(secondstring))
                {
                    if (calibrating > 0)
                        Calibrate(); // compute the angles from the records, last step for calibration
                    else
                        CleanData(); //is this the last step?
                }
                return;
            }

            //if sts=0 before the start button is clicked
            //sts=1 when the start button is clicked and before a lead in time
            //sts=2 when the lead in  time has passed -starts recording
            //sts=3 when stop button  is clicked - stops recording and combine two kinect data
            //Note: same rule is applied when Calibrate button is clicked instead of Start, Sts=3  is
            //automatically set  when calibration is done or failed
            
            if (sts > 0)
            {
                long t = DateTime.Now.Ticks - start;
                textBox2.Text = "";
                if (t < timeup)
                {
                    textBox2.Text += "GET READY!";
                }
                else
                {
                    textBox2.Text += "RECORDING!"+DateTime.Now.Ticks;
                    sts = 2;
                }
                if (calibrating>0)
                    if (t > timeup + 30000000L)
                        sts = 3;//stop calibration  

                // when sts=3   Cleandata() will be called
                // SaveSkeleton saves  joints  in a*.dta file.  CleanData reads a*.dta  and  b*.dta  file and combines synchronized skeletons
                // and saves them in c*.dta file
                
                
            }

        }



        private void drawSeg(JointType a, JointType b)
        { 
            Line l = new Line { StrokeThickness = 2 };
            if ((jp[a].TrackingState == JointTrackingState.Tracked) && (jp[b].TrackingState == JointTrackingState.Tracked))
                l.Stroke = Brushes.Blue;
            else
                l.Stroke = Brushes.Red;
            l.X1 = ScreenX(jp[a].Position.X);
            l.X2 = ScreenX(jp[b].Position.X);
            l.Y1 = ScreenY(jp[a].Position.Y);
            l.Y2 = ScreenY(jp[b].Position.Y);

            canvas1.Children.Add(l);

    
        }

        private void drawHand(JointType a, int sz)
        {
            Line l1 = new Line { StrokeThickness = 2 };
            Line l2 = new Line { StrokeThickness = 2 };

            if (jp[a].TrackingState == JointTrackingState.Tracked)
                l1.Stroke = l2.Stroke = Brushes.Blue;
            else
                l1.Stroke = l2.Stroke = Brushes.Red;
            l1.X1 = ScreenX(jp[a].Position.X) - 2;
            l1.X2 = l1.X1 + 4;
            l1.Y1 = ScreenY(jp[a].Position.Y) - 2;
            l1.Y2 = l1.Y1 + 4;
            l2.X1 = l1.X1;
            l2.X2 = l1.X2;
            l2.Y1 = l1.Y2;
            l2.Y2 = l1.Y1;

            
            canvas1.Children.Add(l1);
            canvas1.Children.Add(l2);



              
          
           

        }



        private int ScreenX(double x)
        {
            return 200 + (int)(x * 100.0);
        }

        private int ScreenY(double y)
        {
            return 200 - (int)(y * 100.0);
        }

        /// <summary>
        /// Saves a skeleton in the file  designated by mainstring
        /// </summary>
        /// <param name="first"></param>
        void SaveSkeleton(Skeleton first)
        {
            double timestamp = (double)DateTime.Now.Ticks;
            string orientation_string = this.GetBoneOrientationsString(first, timestamp);// this also populates orientations in orientation_holder property
            ++cnt;
          //  if (cnt >= 3) // ####skipping frames while saving?? why??
          //  {
                JointCollection jp = first.Joints;
                double d;
                char ch;
                foreach (Joint i in jp)
                {
                    d = (double)i.Position.X;
                    bWriter.Write(d);
                    d = (double)i.Position.Y;
                    bWriter.Write(d);
                    d = (double)i.Position.Z;
                    bWriter.Write(d);
                    if (i.TrackingState == JointTrackingState.Tracked) ch = '1';
                    else ch = '0';
                    bWriter.Write(ch);
                }
                cnt = 0;
                //write relative orientations too
                for (int xx = 0; xx < 60; xx++)
                    bWriter.Write(this.orientation_holder[xx]);
                 
                //write time stamp
                bWriter.Write(timestamp);
          //  }//end cnt

            //save orientatios of the bone segments
            //@todo save in memory first and dump into a file when stop button is clicked
            if (this.boneOrientationWriter != null && this.calibrating==0)
            {
                this.boneOrientationWriter.WriteLine(orientation_string);
            } 

        }


        /// <summary>
        /// Returns a first tracked skeleton
        /// </summary>
        /// <param name="e"></param>
        /// <returns> Returns a first tracked skeleton</returns>

        Skeleton GetFirstSkeleton(SkeletonFrameReadyEventArgs e)
        {
            using (SkeletonFrame skeletonFrameData = e.OpenSkeletonFrame())
            {
                if (skeletonFrameData == null)
                {
                    return null;
                }


                skeletonFrameData.CopySkeletonDataTo(allSkeletons);

                //get the first tracked skeleton
                Skeleton first = (from s in allSkeletons
                                  where s.TrackingState == SkeletonTrackingState.Tracked
                                  select s).FirstOrDefault();

                return first;

            }
        }

  
        private void StopKinect(KinectSensor sensor)
        {
            

            if (sensor != null)
            {
                if (sensor.IsRunning)
                {
                    //stop sensor 
                    sensor.Stop();

                    //stop audio if not null
                    if (sensor.AudioSource != null)
                    {
                        sensor.AudioSource.Stop();
                    }


                }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
           // MessageBox.Show("Killing Second Process");
            if (this.secondKinect != null)
            {


                try
                {
                    this.secondKinect.Kill();
                }
                catch { };

            }

            StopKinect(this.sensor1);

            if (this.boneOrientationWriter != null)
            {
                this.boneOrientationWriter.Close();
                this.boneOrientationWriter = null;
            }


        }

  
        private void button2_Click(object sender, RoutedEventArgs e)
        {
            calibrating = 0;
            //initialize resource for recording
            dothestart();
        }

        private void dothestart()
        {

  
            int jcount = 0;
            if (!File.Exists(aclist))
                using (StreamWriter b = new StreamWriter(aclist, true))
                {
                    b.WriteLine("Calibration Only");
                    
                }
            using (StreamReader b = new StreamReader(aclist))
                {
                    string line;
                    while ((line = b.ReadLine()) != null)
                        jcount++;
                }
                
                /* Create config.dta file to signal camera 2 to start recording */
                using (FileStream fs = new FileStream(System.IO.Path.Combine(this.projectDir, "config.dta"), FileMode.Create))
                {
                    using (StreamWriter w = new StreamWriter(fs))
                    {
                        if (calibrating>0)
                        {
                            w.WriteLine("Calibration");
                            w.WriteLine("0");
                        }
                        else
                        {
                            w.WriteLine("Recording");
                            w.WriteLine("0");
                        }
                        w.WriteLine(jcount.ToString());
                       
                    }
                }

                using (FileStream fs = new FileStream(aclist, FileMode.Append))
                {
                    using (StreamWriter w = new StreamWriter(fs))
                    {
                      w.WriteLine(textBox3.Text);
                    }
                }
                String nd = "000" + jcount.ToString().PadLeft(5, '0') + ".dta";
                mainstring   = System.IO.Path.Combine(this.projectDir, "a" +nd);  //@"c:\\kinectdata\a" + nd;
                secondstring = System.IO.Path.Combine(this.projectDir, "b" + nd); // @"c:\\kinectdata\b" + nd;
                cleanstring  = System.IO.Path.Combine(this.projectDir, "c" + nd);  //@"c:\\kinectdata\c" + nd;
                
                bWriter = new BinaryWriter(File.Open(mainstring, FileMode.Create));
                start = DateTime.Now.Ticks;
                timeup = (long)Int32.Parse(textBox1.Text) * 10000000L + 200000L;
                sts = 1;

            //open a file to write orientations
                if (this.calibrating == 0)
                {
                    string boneOrientString = mainstring.Substring(0, mainstring.Length - 4) + ".orientation";
                    boneOrientationWriter = new StreamWriter(boneOrientString);
                }

                
            
        }

        /// <summary>
        /// Combine skeleton data from two kinects and saves 
        /// </summary>
        private void CleanData()
        {

            long delay = DateTime.Now.Ticks;
            while (DateTime.Now.Ticks - delay < 2000000) ;
 
            long maxlim;
            int frame;
            
            using (StreamReader b = new StreamReader(System.IO.Path.Combine(this.projectDir, "calibration.txt")))
            {
                th = Double.Parse(b.ReadLine());
                tt[0] = Double.Parse(b.ReadLine());
                tt[1] = Double.Parse(b.ReadLine());
                tt[2] = Double.Parse(b.ReadLine());
            }
            bRead = new BinaryReader(File.Open(mainstring, FileMode.Open));
            maxlim = bRead.BaseStream.Length / recsize;
            double x = 0, y = 0, z = 0, d = 0;
            char c = '0';
            cth = Math.Cos(th);
            sth = Math.Sin(th);
            bWriter = new BinaryWriter(File.Open(cleanstring, FileMode.Create));
            secondrec = 0L;
            for (frame = 0; frame < maxlim; frame++)
            {
                //load skeleton from first and second files as dta and dta2
                LoadFirst(frame);
                //dta and dta2 contains joints data
                //tst and tst2 contains flags that denotes the validity of each joint
                 
                for (int j = 0; j < 20; j++)
                {
                    // convert dta2 in the reference frame of camera 1 using calibration data
                    x = dta2[j, 0] * cth + dta2[j, 2] * sth + tt[0];
                    y = dta2[j, 1] + tt[1];
                    z = dta2[j, 2] * cth - dta2[j, 0] * sth + tt[2];
                    d = Math.Sqrt(Math.Pow(x - dta[j, 0], 2) + Math.Pow(y - dta[j, 1], 2) + Math.Pow(z - dta[j, 2], 2));
                    if (tst2[j] == '1')//if the joint from camera 2 is valid
                    {
                        c = '2';
                        if (tst[j] == '1') // joints from camera 1 is valid, i.e. both are valid, so  take average 
                        {
                            x = (x + dta[j, 0]) / 2.0;
                            y = (y + dta[j, 1]) / 2.0;
                            z = (z + dta[j, 2]) / 2.0;
                            c = '3';
                        }
                        bWriter.Write(x);
                        bWriter.Write(y);
                        bWriter.Write(z);
                        bWriter.Write(c);
                    }
                    else //if only joint from camera 1   is valid or invalid 
                    {
                        bWriter.Write(dta[j, 0]);
                        bWriter.Write(dta[j, 1]);
                        bWriter.Write(dta[j, 2]);
                        bWriter.Write(tst[j]);
                    }
                }

                //Merge ad write orientations
               MergeOrientations();
                for (int xx = 0; xx < 60; xx++)
                    bWriter.Write(this.orientation_holder[xx]);
                 

                bWriter.Write(tim);
            }
            bWriter.Close();
            bRead.Close();
        }

        
        /// <summary>
        //Merges the orietations stored in orientation_holder and orietation_holder2 into orientation_holder
        //The orientation of hip joint segment (below spine and above hip joint) is not merged. It is always  taken from the first kinect
        //The the first three elements of orietation_holder corresponds to the orientation of hip center
        /// </summary>
        private void MergeOrientations()
        {

            //save the hip center of second kinect
            orientation_holder[0] = orientation_holder2[0];
            orientation_holder[1] = orientation_holder2[1];
            orientation_holder[2] = orientation_holder2[2];
             

            //merge orientations other than hip center
            int valid1 = 0, valid2 = 0;
            for (int j = 1; j < 20; j++)
            {
                if (tst[j] == '1') valid1 = 1; else valid1 = 0;
                if (tst2[j] == '1') valid2 = 1; else valid2 = 0;
                double factor = 1.0 / (valid1 + valid2);
                orientation_holder[j*3] =     factor * (valid1 * orientation_holder[j*3] + valid2 * orientation_holder2[j*3]);
                orientation_holder[j*3 + 1] = factor * (valid1 * orientation_holder[j*3 + 1] + valid2 * orientation_holder2[j*3 + 1]);
                orientation_holder[j*3 + 2] = factor * (valid1 * orientation_holder[j*3 + 2] + valid2 * orientation_holder2[j*3 + 2]);

            }
            // can we take the orientation from one kinect.

        }

        private void LoadFirst(long frame)
        {
            bRead.BaseStream.Seek((long)(recsize * frame), SeekOrigin.Begin);

            for (int j = 0; j < 20; j++)
            {
                dta[j, 0] = bRead.ReadDouble();
                dta[j, 1] = bRead.ReadDouble();
                dta[j, 2] = bRead.ReadDouble();
                tst[j] = bRead.ReadChar();
            }
            //read orietations chunk
            for (int j = 0; j < 60; j++)
            {
                this.orientation_holder[j] = bRead.ReadDouble();
            }


            LoadSecond(tim = bRead.ReadDouble());

        }



        private void Calibrate()
        {
            long delay = DateTime.Now.Ticks;

            while (DateTime.Now.Ticks - delay < 2000000) ;
                        
            bRead = new BinaryReader(File.Open(mainstring, FileMode.Open));
            maxlim = bRead.BaseStream.Length / recsize;

            double l1, l2 = 0.0, ltot = 0.0;
            int lcnt = 0;
            secondrec = 0L;
            for (int f = 0; f < maxlim; f++)
            {
                LoadFirst(f);
                if ((tst[7] == '1') && (tst[11] == '1'))
                {
                    if ((tst2[7] == '1') && (tst2[11] == '1'))
                    {
                        l1 = Math.Sqrt(Math.Pow(dta[11, 0] - dta[7, 0], 2) + Math.Pow(dta[11, 1] - dta[7, 1], 2) + Math.Pow(dta[11, 2] - dta[7, 2], 2));
                        l2 = Math.Sqrt(Math.Pow(dta2[11, 0] - dta2[7, 0], 2) + Math.Pow(dta2[11, 1] - dta2[7, 1], 2) + Math.Pow(dta2[11, 2] - dta2[7, 2], 2));
                        if (Math.Abs(l1 - l2) < 0.1)
                        {

                            ltot += getAngles(7, 11);
                            lcnt++;
                        }
                    }

                }

            } 
            cleanstring = System.IO.Path.Combine(this.projectDir, "calibration.txt"); //@"c:\\kinectdata\calibration.txt";
            if (lcnt > 3)
            {
                th = ltot / (double)lcnt;
                cth = Math.Cos(th);
                sth = Math.Sin(th);
                int f;
                if (th < 0) th += pi * 2;
                secondrec = 0L;
                for (f = 0; CheckJoint(f, 1) == false; f++) ;
                tt[0] = dta[1, 0] - dta2[1, 0] * cth - dta2[1, 2] * sth;
                tt[2] = dta[1, 2] - dta2[1, 2] * cth + dta2[1, 0] * sth;
                tt[1] = dta[1, 1] - dta2[1, 1];
                using (FileStream fs = new FileStream(cleanstring, FileMode.Create))
                {
                    using (StreamWriter w = new StreamWriter(fs))
                    {

                        w.WriteLine(th.ToString());
                        w.WriteLine(tt[0].ToString());
                        w.WriteLine(tt[1].ToString());
                        w.WriteLine(tt[2].ToString());

                    }
                }
                th *= conv;
                MessageBox.Show("Angle=" + JShow(th) + "(" + JShow(tt[0]) + "," + JShow(tt[1]) + "," + JShow(tt[2]) + ")");

            }
            else
            {
                MessageBox.Show("Calibration Failed");
                File.Delete(cleanstring);
            }
            bRead.Close();
        }

        private bool CheckJoint(long frame, int j)
        {
            LoadFirst(frame);
            if ((tst[j] == '1') && (tst2[j] == '1')) return true;
            return false;
        }


        private double Jat(double v1, double v2)
        {
            if (v2 == 0.0)
            {
                if (v1 == 0.0) return 0.0;
                if (v1 >= 0.0) return pi / 2.0;
                else return pi * 1.5;
            }
            double ang = Math.Atan(v1 / v2);
            if (v2 < 0.0) ang += pi;
            return ang;

        }


        private double getAngles(int a, int b)
        {

            double th2 = Jat(dta[b, 0] - dta[a, 0], dta[b, 2] - dta[a, 2]) - Jat(dta2[b, 0] - dta2[a, 0], dta2[b, 2] - dta2[a, 2]);

            return th2;
        }


        private string JShow(double d)
        {
            double d2 = Math.Round(d, 4);
            return d2.ToString();
        }

        /**
         * Loads the skeleton data  saved by Second process
         * 
         */ 
        private void LoadSecond(double d)
        {
                
            using (BinaryReader b = new BinaryReader(File.Open(secondstring, FileMode.Open)))
            {
                long hi = (long)(b.BaseStream.Length / recsize)-1; //total number of records -1
                while (true)
                {
                    if (secondrec >= hi) { //end of file encountered
                                for (int j = 0; j < 20; j++) tst[j] = '-';
                                return;
                    }
                    secondrec++;
                    b.BaseStream.Seek((long)(recsize * secondrec), SeekOrigin.Begin);
                    for (int j = 0; j < 20; j++)
                      {
                                dta2[j, 0] = b.ReadDouble();
                                dta2[j, 1] = b.ReadDouble();
                                dta2[j, 2] = b.ReadDouble();
                                tst2[j] = b.ReadChar();
                      }

                    //read orietations chunk
                    for (int j = 0; j < 60; j++)
                    {
                        this.orientation_holder2[j] = b.ReadDouble();
                    }
                     
                    //check the timestamp
                    if (b.ReadDouble() > d)
                        return;

                }

            }
        }
         
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            sts = 3;
        }

        private void button3_Click_1(object sender, RoutedEventArgs e)
        {   textBox3.Text="Calibrating";
            calibrating = 1;
            dothestart();
        }


        /**
         *  Returns  1 x 60 vector of orientions of bone segments of the skeleton  in terms of Euler Angles. Each elements are separated by space.
         *  Each agles are computer in YXZ order
         *  
         */
        
        private string GetBoneOrientationsString(Skeleton skeleton,double timestamp)
        {
            //long ticks_milliseconds =   (long)Math.Round (  (double)(1.0*DateTime.Now.Ticks / TimeSpan.TicksPerSecond) * 1000,0);
            string angles = "" + timestamp + " ";
            
            //check  absolute rotation and what is the parent bone in hierarchical rotation and why they are different.

            if (skeleton != null)
            {
                int i = 0;
                string aaa="";
                foreach (BoneOrientation orientation in skeleton.BoneOrientations)
                {
                     
                    Matrix4 rot = orientation.HierarchicalRotation.Matrix;
                    Vector4 ang = new RotationMatrix(rot).ToEulerAngle("YXZ");
                    angles = angles + string.Format("{0:f2}",ang.X*180/pi)+" ";
                    angles = angles + string.Format("{0:f2}",ang.Y*180/pi)+" ";
                    angles = angles + string.Format("{0:f2}",ang.Z*180/pi)+" ";
                    
                    this.orientation_holder[i++] = ang.X;
                    this.orientation_holder[i++] = ang.Y;
                    this.orientation_holder[i++] = ang.Z;

                     aaa+=orientation.StartJoint.ToString()+":"+orientation.EndJoint.ToString()+"\n";
                     
                     
                     
                 }
                this.textBox2.Text = aaa;
            }
 

            return angles;



            //Note     rotation axes convention  used by vicon and kinect are different
            // Note    System.Windows.Media.Media3D.Matrix3D for matrix operations
            //TimeSpan.TicksPerSecond
            
        }




        //Initializes all the resources required to display image

        private void InitializeImageReceiver()
        {
            

            // Turn on the color stream to receive color frames
            this.sensor1.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);

            // Allocate space to put the pixels we'll receive
            this.colorPixels = new byte[this.sensor1.ColorStream.FramePixelDataLength];

            // This is the bitmap we'll display on-screen
            this.colorBitmap =  new WriteableBitmap(this.sensor1.ColorStream.FrameWidth, this.sensor1.ColorStream.FrameHeight, 96.0, 96.0, PixelFormats.Pbgra32, null);

           
            // Set the image we display to point to the bitmap where we'll put the image data
            this.Image.Source = this.colorBitmap;

            // Add an event handler to be called whenever there is new color frame data
            this.sensor1.ColorFrameReady += this.SensorColorFrameReady;

        }

        /// <summary>
        /// Event handler for Kinect sensor's ColorFrameReady event
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void SensorColorFrameReady(object sender, ColorImageFrameReadyEventArgs e)
        {
            if (this.DispImg.IsChecked.Equals(true))
            {
                using (ColorImageFrame colorFrame = e.OpenColorImageFrame())
                {
                    if (colorFrame != null)
                    {
                        // Copy the pixel data from the image to a temporary array
                        colorFrame.CopyPixelDataTo(this.colorPixels);

                        //set the alpha values to 255
                        for (int i = 0; i < this.colorPixels.Length; )
                        {
                            i = i + 4;
                            colorPixels[i - 1] = 0xff;
                        }


                        // Write the pixel data into our bitmap 

                        this.colorBitmap.WritePixels(
                            new Int32Rect(0, 0, this.colorBitmap.PixelWidth, this.colorBitmap.PixelHeight),
                            this.colorPixels,
                            this.colorBitmap.PixelWidth * sizeof(int),
                            0);
                        //  this.colorBitmap=BitmapFactory.ConvertToPbgra32Format(this.colorBitmap);




                    }

                }
            }
 
            
            
        }

 
        /// <summary>
        /// This method is called when the display image check box is checked or unchecked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void DispImg_Checked(object sender, RoutedEventArgs e)
        {

            using (FileStream fs = new FileStream(System.IO.Path.Combine(this.projectDir, "display_setting.dta"), FileMode.Create))
            {
                using (StreamWriter w = new StreamWriter(fs))
                {

                    if (this.DispImg.IsChecked.Equals(true))
                    {
                        this.Image.Source = this.colorBitmap;
                        w.WriteLine("DisPlayImage");
                    }
                    else
                    {
                        this.Image.Source = null;
                        w.WriteLine("");
                    }


                }
            }

        }


  

       
        /// <summary>
        ///  Opens a  dialog box to ask user for the location to save recording
        /// </summary>
        /// <returns>Returns true if a folder is selected and returns false otherwise</returns>
        public bool ChooseFolder()
        {   
           
            System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            string root_dir = @"C:\kinectdata";
            
            if (Directory.Exists(root_dir))
            {
                
                folderBrowserDialog1.SelectedPath = root_dir;
                 

            }


            folderBrowserDialog1.Description = "Choose a folder for the recording";
            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                /*if (!folderBrowserDialog1.SelectedPath.Contains(root_dir) || folderBrowserDialog1.SelectedPath.Equals(root_dir))
                {
                    MessageBox.Show("The selected directory should  be under " + root_dir+". Program will now exit.","Invalid action");
                    
                    return false;
                }
                else
                {*/
                    projectDir = folderBrowserDialog1.SelectedPath;
                    this.Title = "Working on "+projectDir;
                    return true;
                //}
            }
            else{

                MessageBox.Show("A directory must be selected. Program will  now  exit.","Invalid action");
                return false;
            }

             
            
        }

        private string GetFullFile(string aFile)
        {

            return System.IO.Path.Combine(this.projectDir, aFile);

        }

        private bool currentIRState = true;

        //turn the IR state in each interval
        void irTimer_TimerTask(object sender, EventArgs e)
        {
            currentIRState = !currentIRState;
            this.sensor1.ForceInfraredEmitterOff = currentIRState;
        }



}



    /*
     * relative orientation of limbs other than torso does not require angle normalization
     * if the first kinect can see torso then, we dont need calibration
     * 
     */

    public delegate void AutoTimerUp(object sender, EventArgs e);
    
    /// <summary>
    /// Implements a timer to turn the IR emmiter ON/OF in each interval
    /// @todo: synchronize the timer so to the second program (use kinectstudio of v1.6 api to check)
    /// 
    /// </summary>
    public class IRTimer
    {
        private DispatcherTimer timer;
        public event AutoTimerUp TimerTask;

        
        public IRTimer()
        {
            timer = new DispatcherTimer();
        }
        public void Start()
        {   
            timer.Interval =  TimeSpan.FromMilliseconds(0.01);//TimeSpan.FromTicks(1);
             
            timer.Tick += timer_Task;
            timer.Start();




        }

        public void Stop()
        {
            timer.Stop();
        }
        private void timer_Task(object sender, EventArgs e)
        {

            TimerTask(sender, EventArgs.Empty);
        }


    }

}


