﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Kinect;

namespace WpfApplication1
{
    class RotationMatrix
    {
        
        private Matrix4 aMatrix;

        public RotationMatrix(Matrix4 rot_)
        {
            this.aMatrix = rot_;
        }


        public Vector4 ToEulerAngle(string order) 
        {


            if (order == "XYZ")
            {

                return XYZ(this.aMatrix);

            } 
            else  if(order=="YXZ")
            {
                return YXZ(this.aMatrix);
                
            }
            else if (order == "ZXY")
            {
                return ZXY(this.aMatrix);

            }
            else if (order == "ZYX")
            {
                return ZYX(this.aMatrix);

            }
            else
            {
                throw (new Exception("Invalid order " +order));
            }




        }//ToEulerAngle



        private  Vector4 XYZ(Matrix4 M)
        {
             double thetaX;
             double thetaY;
             double thetaZ;
            Vector4 v=new Vector4();
            thetaY = Math.Asin(M.M13);
            if (thetaY < (Math.PI / 2))
            {
                if (thetaY > (-Math.PI / 2))
                {
                    thetaX = Math.Atan2(-M.M23, M.M33);
                    thetaZ = Math.Atan2(-M.M12, M.M11);
                }
                else
                {
                    //warning('Not unique solution.');
                    thetaX = -Math.Atan2(M.M21, M.M22);
                    thetaZ = 0;
                }
            }
            else{
                //warning('Not unique solution.');
                thetaX = Math.Atan2(M.M21, M.M22);
                thetaZ = 0;
             }
            
            v.X = (float)thetaX;
            v.Y = (float)thetaY;
            v.Z = (float)thetaZ;
            return v;

         }


        private Vector4 YXZ(Matrix4 M)
        {
         double thetaX;
         double thetaY;
         double thetaZ;
         Vector4 v = new Vector4();


         thetaX = Math.Asin(-M.M23);
         if (thetaX < (Math.PI / 2))
         {
             if (thetaX > (-Math.PI / 2))
             {
                 thetaY = Math.Atan2(M.M13, M.M33);
                 thetaZ = Math.Atan2(M.M21, M.M22);

             }
             else
             {
                 //warning('Not unique solution.');
                 thetaY = -Math.Atan2(-M.M12, M.M11);
                 thetaZ = 0;
             }
         }
         else
         {
             //warning('Not unique solution.');
             thetaY = Math.Atan2(-M.M12, M.M11);
             thetaZ = 0;
         }

         v.X = (float)thetaX;
         v.Y = (float)thetaY;
         v.Z = (float)thetaZ;
         return v;

     }





        private Vector4 ZXY(Matrix4 M)
        {
            double thetaX;
            double thetaY;
            double thetaZ;
            Vector4 v = new Vector4();


            thetaX = Math.Asin(-M.M32);
            if (thetaX < (Math.PI / 2))
            {
                if (thetaX > (-Math.PI / 2))
                {
                    thetaZ = Math.Atan2(-M.M12, M.M22);
                    thetaY = Math.Atan2(-M.M31, M.M33);

                }
                else
                {
                    //warning('Not unique solution.');
                    thetaZ = -Math.Atan2(M.M13, M.M11);
                    thetaY = 0;
                }
            }
            else
            {
                //warning('Not unique solution.');
                thetaZ = Math.Atan2(M.M13, M.M11);
                thetaY = 0;
            }

            v.X = (float)thetaX;
            v.Y = (float)thetaY;
            v.Z = (float)thetaZ;
            return v;

        }


        private Vector4 ZYX(Matrix4 M)
        {
            double thetaX;
            double thetaY;
            double thetaZ;
            Vector4 v = new Vector4();


            thetaY = Math.Asin(-M.M31);
            if (thetaY < (Math.PI / 2))
            {
                if (thetaY > (-Math.PI / 2))
                {
                    thetaZ = Math.Atan2(M.M21, M.M11);
                    thetaX = Math.Atan2(M.M32, M.M33);

                }
                else
                {
                    //warning('Not unique solution.');
                    thetaZ = -Math.Atan2(-M.M12, M.M13);
                    thetaX = 0;
                }
            }
            else
            {
                //warning('Not unique solution.');
                thetaZ = Math.Atan2(-M.M12, M.M13);
                thetaX = 0;
            }

            v.X = (float)thetaX;
            v.Y = (float)thetaY;
            v.Z = (float)thetaZ;
            return v;

        }




    }//RotationMatrix
}//WpfApp
