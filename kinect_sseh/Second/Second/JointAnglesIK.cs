﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WpfApplication1
{ 
    /// <summary>
    /// This class computes the joint angles from joint location using inverse kinematics and stores them.
    /// </summary>
      public class JointAnglesIK
    {
       //
       static  int[,] keyj = new int[,] { { 4, 5, 6 }, { 8, 9, 10 }, { 12, 13, 14 }, { 16, 17, 18 }, { 1, 0, 13 }, { 1, 0, 17 }, { 1, 2, 5 }, { 1, 2, 9 }, { 12, 4, 5 }, { 2, 0, 20 }, { 0, 2, 10 } };

       private  static    double conv = 57.29577951308232; /* radians to degrees */

       private double[]   angles_ik = new double[11];


          /// <summary>
          /// Creates JointAnglesIK instance from 3D joint locatioins
          /// </summary>
          /// <param name="dtaj">20 x 3 matrix of 3D joint locations </param>
       public JointAnglesIK(double[,] joints)
       {
           this.SkeletonJointsToAnglesIK(joints);
       }

       public static string JName(int s)
       {
           switch (s)
           {
               case 0: return "hip centre";
               case 1: return "spine";
               case 2: return "shoulder centre";
               case 3: return "head";
               case 4: return "left shoulder";
               case 5: return "left elbow";
               case 6: return "left wrist";
               case 7: return "left hand";
               case 8: return "right shoulder";
               case 9: return "right elbow";
               case 10: return "right wrist";
               case 11: return "right hand";
               case 12: return "left hip";
               case 13: return "left knee";
               case 14: return "left ankle";
               case 15: return "left foot";
               case 16: return "right hip";
               case 17: return "right knee";
               case 18: return "right ankle";
               case 19: return "right foot";
               case 20: return "straight up";
               case 21: return "straight down";

           }
           return "ERROR!";
       }
          /// <summary>
          /// Returns the joint name specified by ith row and jth column of keyj matrix
          /// </summary>
          /// <param name="i"></param>
          /// <param name="j"></param>
          /// <returns></returns>
       public static  string JointNameForKey(int i, int j)
       {
           return JName(keyj[i, j]);
       }

        /// <summary>
        /// Computes an angle  between lines j1-j2 and j2-j3
        /// </summary>
        /// <param name="j1"></param>
        /// <param name="j2"></param>
        /// <param name="j3"></param>
        /// <returns></returns>
        /// <remarks> Jonathons method</remarks>
       private static double JKAngle(double[] j1, double[] j2, double[] j3)
       // p1x, double p1y, double p1z, double p2x, double p2y, double p2z, double p3x, double p3y, double p3z)
       {
           double[] v1 = new double[3];
           double[] v2 = new double[3];
           v1[0] = j2[0] - j1[0];
           v1[1] = j2[1] - j1[1];
           v1[2] = j2[2] - j1[2];
           v2[0] = j3[0] - j1[0];
           v2[1] = j3[1] - j1[1];
           v2[2] = j3[2] - j1[2];
           double dp = v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
           double av1 = v1[0] * v1[0] + v1[1] * v1[1] + v1[2] * v1[2];
           double av2 = v2[0] * v2[0] + v2[1] * v2[1] + v2[2] * v2[2];
           double cth = Math.Acos(dp / (Math.Sqrt(av1) * Math.Sqrt(av2)));
           cth *= conv;
           return cth;
       }


        /// <summary>
        /// Computes angles using inverse kinematics
        /// </summary>
        /// <param name="dtaj"></param>
        /// <param name="angles_ik"> 11 dimensional array to store angles</param>
       private    void  SkeletonJointsToAnglesIK(double[,] dtaj)
       {



          
           //number of rows of keyj matrix is 11
           for (int i = 0; i < 11; i++)
           {
               double[] ang1 = new double[] { dtaj[keyj[i, 0], 0], dtaj[keyj[i, 0], 1], dtaj[keyj[i, 0], 2] };
               double[] ang2 = new double[] { dtaj[keyj[i, 1], 0], dtaj[keyj[i, 1], 1], dtaj[keyj[i, 1], 2] };
               double[] ang3 = new double[] { 0.0000, 0.00000, 0.00000 };
               int kj = keyj[i, 2];
               if (kj < 20)
               {
                   ang3[0] = dtaj[kj, 0];
                   ang3[1] = dtaj[kj, 1];
                   ang3[2] = dtaj[kj, 2];
               }
               if (kj == 20)
               {
                   ang3[0] = dtaj[keyj[i, 0], 0];
                   ang3[1] = 10000;
                   ang3[2] = dtaj[keyj[i, 0], 2];
               }
               if (kj == 21)
               {
                   ang3[0] = dtaj[keyj[i, 0], 0];
                   ang3[1] = -100000;
                   ang3[2] = dtaj[keyj[i, 0], 2];
               }
               double ang_value = JKAngle(ang2, ang1, ang3);
               angles_ik[i] = ang_value;
               //nw += "Angle of " + JName(keyj[i, 1]) + " (" + JName(keyj[i, 0]) + " to " + JName(keyj[i, 2]) + ") : " + ang_value + Environment.NewLine;
           }

           

           

       }
           
       public double GetAngle(int index)
       {
           return 180-this.angles_ik[index];
       }


    }
}
