@ECHO off

:: need to find the FMS.csv file to extract the start/finish frames and score from it
:: set /p filepath= Enter the path to FMS.csv: 

:: assuming fms.csv in current dir
python extract_fms.py fms.csv dataSegments.csv labels.csv
:: add second statement here with different out files if multiple FMS files

set /p hey = wait

:: readRecording needs to be modified to extract only the data required for the movement
echo Parsing data from raw input files...
matlab -nodisplay -nojvm -nosplash -nodesktop -r 'extract_jumps())'
:: matlab -nodisplay -nojvm -nosplash -nodesktop -r 'readRecording(%filepath%))'

echo Transforming jump results...
transform.py jumps_csv jumps_csv_processed
extract_features.py jumps_csv_processed labels.csv jumps_extracted_features

echo Transforming kick results...
transform.py kt_csv kt_csv_processed
extract_features.py kt_csv_processed kt_labels.csv kt_extracted_features

:: now train SVM using transformed_experiment.bat
echo Training SVM with raw results...
call raw_experiment.bat

echo Training SVM with transformed results...
call transformed_experiment.bat

:: this is where any output could be displayed based on the results, which are
:: stored in transformed_results.csv and raw_results.csv
