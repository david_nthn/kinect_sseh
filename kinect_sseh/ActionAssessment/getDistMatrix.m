function [ D ] = getDistMatrix(jumps)
%getDistMatrix Summary of this function goes here
%   Returns struct which contains a njumps by njoints matrix for dtw
%   distance for each jump. 

D = struct;
names = fieldnames(jumps);

for i=1:length(names)
    D.(names{i,1}) = [];
    for row=1:length(names)
        for col=1:20           
            D.(names{i,1})(row,col) = dtw(jumps.(names{i,1}){col,1},jumps.(names{row,1}){col,1});
        end
    end
end

end

