function [ RP, Ratings ] = jointRatingPredict(DTW,jrating)
%UNTITLED3 Find lowest distance for each joint in a jump and its rating
%   Detailed explanation goes here

RP = struct; Ratings = struct;
dtwnames = fieldnames(DTW);
ratingnames = fieldnames(jrating);
jointnames = dtaJointBoneNames;

for i=1:length(dtwnames)
    RP.(dtwnames{i,1}) = cell(1,1);
    Ratings.(dtwnames{i,1}) = [];
    for col=1:20
        
        dist = sort(DTW.(dtwnames{i,1})(:,col));
        min_dist = 0;
        %{
        r = find(dist(2,1) == DTW.(dtwnames{i,1})(:,col));
        jname = dtwnames{r,1};
        jname = upper(jname);
        %}
        
        jname = '';
        r = 0;
        for j=2:numel(dist)              
            r = find( dist(j,1) == DTW.(dtwnames{i,1})(:,col));
            jname = dtwnames{r,1};
            jname = upper(jname);
            % Ensure that predicted instance is not from same participant
            if ~strcmp(jname(end-2:end),dtwnames{i,1}(end-2:end))
                min_dist = dist(j,1);
                break;
            end           
        end
        
        ratingindex = find(strcmp(jname,ratingnames));
        rating = jrating.(ratingnames{ratingindex,1});
        
        RP.(dtwnames{i,1}){1,col} = jointnames{col,1};
        RP.(dtwnames{i,1}){2,col} = jname;
        RP.(dtwnames{i,1}){3,col} = rating;
        RP.(dtwnames{i,1}){4,col} = min_dist;
        Ratings.(dtwnames{i,1})(1,col) = rating;
        Ratings.(dtwnames{i,1})(2,col) = min_dist;
    end
end


end

