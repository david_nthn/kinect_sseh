clear all
A = importdata('MtHawthorneKT.txt');
list = findFMS('c:\kinectdata\MtHawthorne2013\recordings.txt');
KTHawthorne = struct;
prefix = 'kt';

for i=1:length(A.data)
    filename = 'c:\kinectdata\MtHawthorne2013\';
    str ='c00000000.dta';
    if strcmp(list{i,2},num2str(A.data(i,1)))
        cstr = num2str(list{i,1});
        if(length(cstr)==1)
            str(end-4)= cstr;
        end
        if(length(cstr) > 1)
            l = 3+length(cstr);
            str(end-l:end-4) = cstr;
        end
        filename = strcat(filename,str);
        mot = readRecording(filename);
        if A.data(i,2)==0 && A.data(i,3) ==0
            a=NaN;b=NaN;c=NaN;
        end
        if A.data(i,2)~=0 && A.data(i,3) ~=0 
            [a,b,c] = extract3(mot,A.data(i,2),A.data(i,3),A.data(i,4),A.data(i,5),A.data(i,6),A.data(i,7));
        end
        
        KTHawthorne.(genvarname([strcat(prefix,'1_') list{i,2}])) = a;
        KTHawthorne.(genvarname([strcat(prefix,'2_') list{i,2}])) = b;
        KTHawthorne.(genvarname([strcat(prefix,'3_') list{i,2}])) = c;
    end    
end

fields = fieldnames(KTHawthorne);

for i=1:numel(fields)
    J = [];
    filename = strcat('/kt_csv/',fields{i},'.csv');

    A = KTHawthorne.(fields{i});
    if iscell(A)
        for j=1:20
            T = A{j,1};
            T = T';
            J = [J T];
        end
        csvwrite(filename, J);
    end
end
