% Script to calculate average trajectories of jump. Inefficient use of DTW.

clear all;

load('Jumps');

% Will need to warp and unwarp between each trajectory
jumps = who;

jmp_mean = cell(1,1);
%dist = [];
holder = eval(jumps{1,1});

for i=1:20
    x = num2cell(holder{i,1}(1,:));
    y = num2cell(holder{i,1}(2,:));
    z = num2cell(holder{i,1}(3,:));
    jmp_mean{i,1} = {x;y;z};
end
    
     

for i=2:numel(jumps)
    for j=1:20
        v = eval(jumps{i,1});
       
        x=cell2mat(jmp_mean{j,1}{1,:});
        y=cell2mat(jmp_mean{j,1}{2,:});
        z=cell2mat(jmp_mean{j,1}{3,:});
        
        avg_x = avg_trajectory(x, v{j,1}(1,:))';
        avg_y = avg_trajectory(y, v{j,1}(2,:))';
        avg_z = avg_trajectory(z, v{j,1}(3,:))';
        %dx = avg_x(end);
        %dy = avg_y(end);
        %dz = avg_z(end);
        jmp_mean{j,1} = {num2cell(avg_x); num2cell(avg_y); num2cell(avg_z)};
        %dist(:,(i-1)*20 - (20-j)) = [dx dy dz]'; 
    end
end

save('jmp_mean', 'jmp_mean');
%save('dist','dist');
clear all;
load('jmp_mean');
%load('dist');
delete('jmp_mean.mat');
%delete('dist.mat');
        
        
        