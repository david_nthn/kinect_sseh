%% Takes JumpHawthorne struct and writes each jump as a csv file

fields = fieldnames(JumpsHawthorne);

for i=1:numel(fields)
    J = [];
    filename = strcat('./jumps_csv/',fields{i},'.csv');

        A = JumpsHawthorne.(fields{i});
        for j=1:20
            T = A{j,1};
            T = T';
            J = [J T];
        end
        csvwrite(filename, J);    
end
