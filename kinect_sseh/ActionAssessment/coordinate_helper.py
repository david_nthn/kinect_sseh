import numpy
import math as m

#=================
#  MAIN FUNCTION
#=================

#  
def cartToSph(x,y,z):
	x2y2 = x**2 + y**2
    r = m.sqrt(z**2 + x2y2)
    theta = m.atan2(z/m.sqrt(x2y2))
    phi = m.atan2(y/x)
    return r, theta, phi
    