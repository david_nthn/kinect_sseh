function [kicktarget] = readcsv2mot(dirname)
%UNTITLED Summary of this function goes here
%  Reads in the processed data and creates jump struct with same format
%  jumps= readcsv2mot('C:\Users\David\Dropbox\SSEH_Kinect\Data\jumps_csv\');

contents = dir(dirname);
kicktarget = struct;

for i=3:length(contents)
    A = csvread(strcat(dirname,contents(i,1).name));
    name = contents(i,1).name;
    name = name(1:end-4);
    kicktarget.(name) = cell(1,1);
    
    for j=1:20
        kicktarget.(name){j,1} = A(:,3*j-2:3*j)';
    end
    
end

