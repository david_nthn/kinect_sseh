clear all;

jrating = struct;
filename = strcat(pwd,'\ratings_labels\jump_labels.csv');
A = importdata(filename);

for i=1:length(A.data)
    id = num2str(A.data(i,1));
    
    for j=2:4
        name = strcat(A.textdata{1,j},'_',id);
        jrating.(name) = A.data(i,j);
    end
end

% calculated DTW data
load('MTHW_JMPS_DTW.mat');

% Alternative
%foldername = strcat(pwd, '\jumps_csv\');
%foldername = strcat(pwd, '\jumps_csv_processed\transformed\');
%jumps= readcsv2mot(foldername);
%M = getDistMatrix(jumps);


[RP, Rating] = jointRatingPredict(M,jrating);

fnames = fieldnames(Rating);
prating = [];
arating = [];
Z = zeros(10);
se = [];
mse =0;

for i=1:length(fnames)
    prating(i) = mode(Rating.(fnames{i,1})(1,:));
    arating(i) = jrating.(upper(fnames{i,1}));
end

for i=1:length(fnames)
    se(i) = (prating(1,i) - arating(1,i))^2;
    mse = mse + se(i);
end

mse = mse/length(fnames);

for i=1:length(fnames)
    Z(prating(1,i),arating(1,i)) = Z(prating(1,i),arating(1,i)) +1;
end

for i=1:length(Z)
    s=sum(Z(:,i));
    if s ~= 0
        Z(:,i) = Z(:,i)./s;
        Z(:,i) = Z(:,i).*100;
    end
end

contourf(Z);

colorbar;
title('Jump DTW Accuracy');
xlabel('Actual Rating');
ylabel('Predicted Rating');