function [ list ] = findFMS( filename )
%findFMS Returns the linenumbers in recordings.txt that are FMS. Used to 
% find the FMS c...dta files.
% e.g. list = findFMS('c:\kinectdata\July2013_Holidays\recordings.txt');
% and list(1) = 8, so
% mot = readRecording('c:\kinectdata\July2013_Holidays\c00000008.dta');
% 
list =cell(1,1);

fid = fopen(filename);
filecount = 1;
count = 0;
while feof(fid) == 0
    aline = fgetl(fid);    
     if strcmp(aline(end-2:end),'FMS')
         list{filecount,1} = count;
         list{filecount,2} = aline(end-5:end-3);
         filecount = filecount +1;
     end
     count = count+1;
end
        
end

