import sys
import os
import shutil
import csv
import re

#==============
#  CONSTANTS
#==============
ROOT_PATH = os.path.dirname(os.path.abspath(__file__))
OUT_PATH = ROOT_PATH + '\\' + 'parsed'

FMS_FILENAME = ROOT_PATH + '\\' + sys.argv[1]
DATA_FILENAME = sys.argv[2]
LABELS_FILENAME = sys.argv[3]

LABELS_PATH = OUT_PATH + '\\' + LABELS_FILENAME
DATA_PATH = OUT_PATH + '\\' + DATA_FILENAME
SPACER = '=================================='

#=================
#  MAIN FUNCTIONS
#=================

# read in FMS.csv and output 
def run():
	with open(FMS_FILENAME,'r') as input, \
            open(LABELS_PATH,'wb') as labels, \
            open(DATA_PATH,'wb') as data:
            print("Reading in metadata from: " + FMS_FILENAME)
            csvReader = csv.reader(input)
            dataWriter = csv.writer(data) # for use with readRecording
            labelWriter = csv.writer(labels) # for use in extract_features
            
            print("Writing in metadata to: " + OUT_PATH)
            # assuming 3 trials in headers for the moment
            dataWriter.writerow(['ID','Type','Path','#Trials','1s','1e','2s','2e','3s','3e','Row in ']) # prepend this once #trials is known?
            # labelWriter.writerow(['ID','Trial1','Trial2','Trial3']) # prepend this once #trials is known?
            
            id = ''
            path = ''
            for row in csvReader:
                # need this first as csvReader.next() call will get Description row
                if 'FMS' in row[0] and id is not '':
                    # extract from data rows, ignoring header
                    row = csvReader.next()
                    fms = row[0] # movement type (e.g. 'Jump')
                    dataRow = [id, fms, path]
                    labelRow = [id]
                    tempData = []
                    numTrials = 0
                    while ('Description:' not in row):
                        numTrials += 1
                        tempData.extend([row[1],row[2]])
                        labelRow.extend([row[3]])
                        try:
                            row = csvReader.next()
                        except StopIteration:
                            break # end of file
                    dataRow.extend([numTrials])
                    dataRow.extend(tempData)
                    # write new row
                    dataRow.extend([int(path[-12:-4])])
                    dataWriter.writerow(dataRow)
                    labelWriter.writerow(labelRow)
                
                # prepare next id, path
                if 'Description:' in row[0]:
                    regex = re.match(r"(\D)*0*(\d+)", row[1])
                    if regex is not None:
                        id = regex.group(2)
                        path = row[3]
                    
	print('Done.')

#========================
# KICK-OFF MAIN FUNCTION
#========================

# prepare export folders
if os.path.exists(OUT_PATH):
    shutil.rmtree(OUT_PATH)
os.mkdir(OUT_PATH)
    
#run
run()