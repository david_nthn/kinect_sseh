%  Takes the pathname to c....dta as input and returns a mot structure
%
%A single frame in the DTA binary file has three components:
% 1. Joint Position - composed of three doubles for XYZ and a single byte
%                     validity char for each of the 20 joints.
%                     #########
%                     #8#8#8#1#
%                     #########
%                     The are four possible validity char:
%                     (a) '0' - camera 1 position used and was invalid
%                     (b) '1' - camera 1 position used and was valid
%                     (c) '2' - camera 2 position used and was valid
%                     (d) '3' - average position from camera 1 and 2 since
%                               both camera positions were valid.
%                     For a single frame this gives a total of 20x25=500
%                     bytes.
% 2. Orientation    - composed of three doubles for YXZ Euler angle for
%                     root hip centre and the 19 bones derived from the
%                     joint hierarchy.
%                     #######
%                     #8#8#8#
%                     #######
%                     For a single frame this gives a total of 20x24= 480 
%                     bytes.
% 3. Timestamp      - composed of a single double that represents the
%                     number of 100-nanosecond intervals that have elapsed
%                     since January 1, 0001 at 00:00:00.000 in the
%                     Gregorian calendar.
%                     ###
%                     #8#
%                     ###
%                     TODO: A matlab SerialDateNumber is equal to the
%                     number of days since January 0, 0000. SerialTime is 
%                     then represented as a fraction of day after midnight.
%                     For a single frame this gives a total of 8 bytes.
%
% The total memory occupied by a single frame in a DTA binary file is thus
% 500 + 480 + 8 = 988 bytes.

function [ mot ] = readRecording( filename )
% readRecording Creates a mot struct from a merged DTA file.
mot = initMot;
dta_framesize = 988;
dta_njoints = 20; 

file_info = dir(filename);
% Extract what line number the description is in the recordings.txt file.
rec_linenum = filename(end-11:end-4); 
rec_linenum = str2double(rec_linenum);

% File path for the recordings.txt
recording_filename = filename;
recording_filename(end-12:end) = '';
recording_filename = strcat(recording_filename,'recordings.txt');

% parse the filename for error checks
%filetype = upper(filename(end-3:end));
%cleandata = upper(filename(1));
%if ~strcmp(filetype, '.DTA')
%    error('readRecording:chkFileType', 'Unknown file type');
%elseif ~strcmp(cleandata , 'C')
%    error('readRecording:chkClean', 'File is not clean data');
%    if  mod(file_info.bytes, dta_framesize) ~= 0
%    error('readRecording:chkCorrupt', 'File is corrupt');
%    end
%end
       
fid = fopen(filename , 'rb'); % Open file with read and binary permission
    % Open recording file to get description
    f2id = fopen(recording_filename);
   
    for i=0:rec_linenum
        mot.description = fgetl(f2id);
    end
    
    mot.filename = file_info.name;
    mot.njoints = dta_njoints;
    mot.nframes = file_info.bytes/dta_framesize;
    
    % Value to subtract from Time ticks to get duration between frames.
    firstTimeStamp = 0;
    
    strlen = 0;
    disp('Reading frame number: ');
    
    for  nFramesRead =1:mot.nframes
        for i=1:20
            % The XYZ position for each joint
            mot.jointTrajectories{i,1}(:, nFramesRead)=fread(fid,3,'double');
            mot.frameValidity{i,1}(1, nFramesRead) = fread(fid,1,'uint8=>char');
        end
        
        for i=1:20
            % The YXZ Euler angles for each joint's orientation
            mot.rotationEuler{i,1}(:, nFramesRead)=fread(fid,3,'double');
        end
        
        ticks = fread(fid,1,'double=>int64'); % Number of 100nanosecond ticks
        ticks = double(ticks); % Cast int64 to double
        days= ticks/8.64e+11 + 367; % Convert to MATLAB time
        mot.frameTimeStamp{nFramesRead,1} = datestr(days,'dd/mm/yyyy HH:MM:SS.FFF AM');
   
        % Subtract previous ticks and divided by 10000 to get duration in
        % milliseconds.
        mot.frameDuration(nFramesRead) = (ticks - firstTimeStamp)/10000;   
        firstTimeStamp = ticks;
        
        % Display what frame has been written to commandline.        
        s = [num2str(nFramesRead) '/' num2str(mot.nframes)];
        disp(char(8*ones(1,strlen+2)));
        strlen = length(s);
        disp(s);
                  
    end
    
    mot.frameDuration(1) = 0; % Ensure frameDuration begins at 0.
    mot.rootTranslation = mot.jointTrajectories{1,1};
    [mot.jointNames, mot.boneNames] = dtaJointBoneNames;
      
fclose(fid);

end

