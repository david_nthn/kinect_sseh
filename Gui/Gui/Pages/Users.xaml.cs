﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Common;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Navigation;

namespace Gui
{
    /// <summary>
    /// Interaction logic for Users.xaml
    /// </summary>
    public partial class Users : UserControl, IContent
    {
        private object GetDataContext()
        {
            return new DatabaseHelper().GetSubjectTable();
        }
        private int GetID(object row)
        {
            return DatabaseHelper.GetSubject_ID(row);
        }
        private string formName = "UsersForm";

        public Users()
        {
            InitializeComponent();

            //avoid designer complaining about not being able to find database
            if (!DesignerProperties.GetIsInDesignMode(this))
            {
                DataContext = GetDataContext();
            }
        }

        #region IContent
        /// <summary>
        /// Handle sub-pages
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnFragmentNavigation(FragmentNavigationEventArgs e)
        {
            string[] subFragments = FragmentHelper.GetFragments(e.Fragment);
            if(subFragments.Length > 1)
            {
                UpdateDropDownListView(string.IsNullOrWhiteSpace(subFragments[1]) ? (int?)null : int.Parse(subFragments[1]));
            }
        }

        /// <summary>
        /// Unload event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedFrom(NavigationEventArgs e)
        {
        }

        /// <summary>
        /// Load event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedTo(NavigationEventArgs e)
        {
            CommandsListView.SelectedIndex = 0;
        }

        /// <summary>
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
        }
        #endregion

        #region EventHandlers
        /// <summary>
        /// Event handler to select an item in the 'command' list
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void OnSelectionChanged_Commands(object sender, RoutedEventArgs e)
        {
            if ((e.OriginalSource as ListView).SelectedItem == null)
            {
                return;
            }
            listView.SelectedItem = null;


            switch ((e.OriginalSource as ListView).SelectedIndex)
            {
                case 0:
                default:
                    {
                        Form.Source = new Uri("/Content/" + formName + ".xaml#add", UriKind.Relative);
                        break;
                    }
                case 1:
                    {
                        Form.Source = new Uri("/Content/" + formName + ".xaml#import", UriKind.Relative);
                        break;
                    }
            }
        }

        /// <summary>
        /// Event handler to select an item in the 'user' list
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void OnSelectionChanged(object sender, RoutedEventArgs e)
        {
            if ((e.OriginalSource as ListView).SelectedItem == null)
            {
                return;
            }
            CommandsListView.SelectedItem = null;

            string uri = "/Content/" + formName + ".xaml#edit"
                + FragmentHelper.Separator + GetID(listView.SelectedItem).ToString();
            Form.Source = new Uri(uri, UriKind.Relative);
        }

        /// <summary>
        /// Helper to update the contents of the contained listview
        /// </summary>
        /// <param name="id">Id of the subject row to set the selected item to</param>
        private void UpdateDropDownListView(int? id = null)
        {
            //update binding
            DataContext = GetDataContext();

            //select the appropriate item
            if (id != null)
            {
                for (int i = 0; i < listView.Items.Count; i++)
                {
                    if (GetID(listView.Items.GetItemAt(i)) == (int)id)
                    {
                        listView.SelectedItem = listView.Items.GetItemAt(i);
                        break;
                    }
                }
            }

            //resort if needed
            listView.DoSort();

            //ensure columns are wide enough for contents
            listView.RefreshView();
        }
        #endregion
    }
}
