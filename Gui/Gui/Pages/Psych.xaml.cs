﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using Common;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Navigation;
using Microsoft.Kinect;

namespace Gui
{
    /// <summary>
    /// Interaction logic for Psych.xaml
    /// </summary>
    public partial class Psych : UserControl, IContent
    {
        /// <summary>
        /// Helper class that handles reading / writing of the Survey list
        /// </summary>
        private SurveyHelper surveys = new SurveyHelper();

        public Psych()
        {
            InitializeComponent();
        }

        #region IContent
        /// <summary>
        /// Handle sub-pages
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnFragmentNavigation(FragmentNavigationEventArgs e)
        {
            switch(FragmentHelper.GetFragmentName(e.Fragment))
            {
                //'select' page
                case "select":
                    {
                        Selection.Visibility = Visibility.Visible;
                        Survey.Visibility = Visibility.Hidden;
                        break;
                    }
                //'survey' page
                case "survey":
                    {

                        //make survey 'panel' visible
                        Survey.Visibility = Visibility.Visible;
                        //and hide the survey selection 'panel'
                        Selection.Visibility = Visibility.Hidden;

                        //get url from fragment
                        string[] subFragments = FragmentHelper.GetFragments(e.Fragment);

                        if (subFragments.Length > 0
                            && !string.IsNullOrWhiteSpace(subFragments[0]))
                        {
                            webBrowser.Navigate(subFragments[0]);
                        }
                        break;
                    }
            }
        }

        /// <summary>
        /// Unload event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedFrom(NavigationEventArgs e)
        {
            //cleanup Kinect
            KinectSensor.KinectSensors.StatusChanged -= OnKinectSensorsStatusChanged;

            SkeletonMouse.Sensor = null;
        }

        /// <summary>
        /// Load event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedTo(NavigationEventArgs e)
        {
            MainWindow window = Application.Current.MainWindow as MainWindow;
            if(window != null)
            {
                //try to find existing LinkGroup
                bool contains = false;
                foreach (LinkGroup group in window.Menu.LinkGroups)
                {
                    if (group.DisplayName == "Wellness")
                    {
                        contains = true;
                        break;
                    }
                }

                //and add if not found
                if (!contains)
                {
                    LinkGroup psych = new LinkGroup { DisplayName = "Wellness" };

                    Link select = new Link { DisplayName = "Select", Source = new Uri("/Pages/Psych.xaml#select", UriKind.Relative) };
                    psych.Links.Add(select);

                    window.Menu.LinkGroups.Add(psych);
                    window.Menu.SelectedLink = select;
                }
            }

            Participant_SelectBox.RefreshDataContext();
            
            surveys.Refresh();
            Survey_SelectBox.DataContext = surveys.data.Surveys;
            Survey_SelectBox.SelectedIndex = -1;

            //setup Kinect
            ThreadPool.QueueUserWorkItem(
                (o) =>
                {
                    KinectSensorCollection sensors = KinectSensor.KinectSensors;

                    //hide processing 'panel' and show save 'panel'
                    //do this on the GUI thread
                    Dispatcher.Invoke((Action)delegate
                    {
                        sensors.StatusChanged += OnKinectSensorsStatusChanged;
                        OnKinectSensorsStatusChanged(sensors, null);
                    });
                });
        }

        /// <summary>
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
        }
        #endregion

        #region EventHandlers
        /// <summary>
        /// Event to handle Kinect Sensor status changes
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="args">event arguments</param>
        private void OnKinectSensorsStatusChanged(object sender, StatusChangedEventArgs args)
        {
            KinectSensorCollection sensors = (KinectSensorCollection)sender;

            int index = -1;

            //map kinects to views by id
            //this allows the order of sensors in the collection to change
            for (int i = 0; i < sensors.Count; i++)
            {
                if (sensors[i].Status == KinectStatus.Connected)
                {
                    if (SkeletonMouse.Sensor != null
                        && SkeletonMouse.Sensor.UniqueKinectId == sensors[i].UniqueKinectId)
                    {
                        index = i;
                    }
                }
            }

            //map newly connected kinects
            if (index == -1)
            {
                for (int i = 0; i < sensors.Count; i++)
                {
                    if (sensors[i].Status == KinectStatus.Connected)
                    {
                        index = i;
                        SkeletonMouse.Sensor = sensors[i];
                    }
                }
            }

            //account for disconnected kinects
            if (index == -1
                && SkeletonMouse.Sensor != null)
            {
                SkeletonMouse.Sensor = null;
            }
        }
        
        /// <summary>
        /// Start the survey
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void OnClick(object sender, RoutedEventArgs e)
        {
            object selectedUser = Participant_SelectBox.SelectedItem;
            if (selectedUser == null)
            {
                return;
            }

            Survey selectedSurvey = Survey_SelectBox.SelectedItem as Survey;
            if(selectedSurvey == null)
            {
                return;
            }

            MainWindow window = Application.Current.MainWindow as MainWindow;
            if (window == null)
            {
                return;
            }

            //try to find an existing 'survey' link
            Link survey = null;
            foreach (Link link in window.Menu.SelectedLinkGroup.Links)
            {
                if (link.DisplayName == "Survey")
                {
                    survey = link;
                    break;
                }
            }

            //prepare address to navigate to
            string uri = "/Pages/Psych.xaml#survey"
                + FragmentHelper.Separator + selectedSurvey.Url
                + "&UUID=" + DatabaseHelper.GetSubject_UUID(selectedUser);
            
            if (survey != null)
            {
                //Link was found, overwrite with new address
                survey.Source = new Uri(uri, UriKind.Relative);
                window.Menu.SelectedLink = survey;
            }
            else
            {
                //new Link needed
                survey = new Link { DisplayName = "Survey", Source = new Uri(uri, UriKind.Relative) };
                window.Menu.SelectedLinkGroup.Links.Add(survey);
                window.Menu.SelectedLink = survey;
            }
        }
        #endregion
    }
}
