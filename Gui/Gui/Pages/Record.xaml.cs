﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Xml;
using Common;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Navigation;
using Microsoft.Kinect;

namespace Gui
{
    /// <summary>
    /// Interaction logic for Record.xaml
    /// </summary>
    public partial class Record : UserControl, IContent
    {
        public Record()
        {
            InitializeComponent();

            //make sure DPs work
            LayoutRoot.DataContext = this;
        }

        #region IContent
        /// <summary>
        /// Cached value for session / location link
        /// </summary>
        private Link session;

        /// <summary>
        /// Cached value for calibrate link
        /// </summary>
        private Link calibrate;

        /// <summary>
        /// Cached value for record link
        /// </summary>
        private Link record;

        /// <summary>
        /// Avoid switching pages while recording
        /// </summary>
        private Link stayOnPage;

        /// <summary>
        /// Avoid switching pages while recording
        /// </summary>
        private bool calibrated = false;

        /// <summary>
        /// Avoid showing / recording video if activity doesn't want it
        /// </summary>
        private bool NoVideo = false;

        /// <summary>
        /// Handle sub-pages
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnFragmentNavigation(FragmentNavigationEventArgs e)
        {
            MainWindow window = Application.Current.MainWindow as MainWindow;

            if (RecStartStop.IsChecked == true)
            {
                window.Menu.SelectedLink = stayOnPage;
                return;
            }

            if (Participant != SaveRecordingData.Subject)
            {
                Participant = SaveRecordingData.Subject;
                calibrated = false;
            }

            switch (FragmentHelper.GetFragmentName(e.Fragment))
            {
                //'session' page
                case "session":
                default:
                    {
                        Form.Visibility = Visibility.Visible;
                        KinectGrid.Visibility = Visibility.Hidden;
                        CalibrateGrid.Visibility = Visibility.Hidden;
                        RecordGrid.Visibility = Visibility.Hidden;

                        //ensure the correct link is selected
                        window.Menu.SelectedLink = session;
                        break;
                    }
                //'calibrate' page
                case "calibrate":
                    {
                        Form.Visibility = Visibility.Hidden;
                        KinectGrid.Visibility = Visibility.Visible;
                        KinectViewGrid.Visibility = Visibility.Visible;
                        CalibrateGrid.Visibility = Visibility.Visible;
                        RecordGrid.Visibility = Visibility.Hidden;
                        NoVideo = false;

                        //ensure the correct link is selected
                        window.Menu.SelectedLink = calibrate;
                        break;
                    }
                //'record' page
                case "record":
                    {
                        //subFragments[0] = activity if valid
                        //subFragments[1] = right limb
                        if (!calibrated)
                        {
                            window.Menu.SelectedLink = calibrate;
                            break;
                        }
                        Form.Visibility = Visibility.Hidden;
                        KinectGrid.Visibility = Visibility.Visible;
                        KinectViewGrid.Visibility = Visibility.Visible;
                        CalibrateGrid.Visibility = Visibility.Hidden;
                        RecordGrid.Visibility = Visibility.Visible;

                        if (MovementSkill.Current == MovementSkill.Index.UniActive)
                        {
                            Activities_SelectBox.IsEnabled = false;
                            if (Activities_SelectBox.SelectedItem == null)
                            {
                                Activities_SelectBox.SelectedIndex = 0;
                            }
                        }

                        //ensure the correct link is selected
                        window.Menu.SelectedLink = record;

                        //update count if appropriate
                        string[] subFragments = FragmentHelper.GetFragments(e.Fragment);
                        if (subFragments.Length > 0
                            && !string.IsNullOrWhiteSpace(subFragments[0]))
                        {
                            //find the approriate activity in the listview
                            foreach (CountItem item in countList)
                            {
                                if (item.activity == subFragments[0])
                                {
                                    //and update its count
                                    if (item.count.right != null
                                        && subFragments.Length > 1
                                        && !string.IsNullOrWhiteSpace(subFragments[1])
                                        && Convert.ToBoolean(subFragments[1]))
                                    {
                                        item.count.right++;
                                    }
                                    else
                                    {
                                        item.count.left++;
                                    }
                                    RefreshActivityCountListView();

                                    if (MovementSkill.Current == MovementSkill.Index.UniActive)
                                    {
                                        if (item.count.left >= item.count.trials
                                            && (item.count.right == null || item.count.right >= item.count.trials)
                                            )
                                        {
                                            int previousIndex = Activities_SelectBox.SelectedIndex;
                                            Activities_SelectBox.SelectedIndex++;

                                            //check if reached end of activities
                                            if (Activities_SelectBox.SelectedIndex == previousIndex)
                                            {
                                                new DatabaseHelper().MarkSessionComplete(SaveRecordingData.Session);

                                                SaveRecordingData.Subject = null;

                                                string uri = "/Pages/ReportSessions.xaml#report"
                                                                + FragmentHelper.Separator
                                                                + DatabaseHelper.GetSession_ID(SaveRecordingData.Session).ToString();
                                                Form.Source = new Uri(uri, UriKind.Relative);
                                                window.Menu.SelectedLink = session; ;
                                            }
                                        }
                                    }
                                    break;
                                }
                            }
                        }

                        if (MovementSkill.Current == MovementSkill.Index.UniActive)
                        {
                            XmlNode noVideo = ActivityNodes[Activities_SelectBox.SelectedIndex].Attributes["noVideo"];
                            NoVideo = noVideo != null && Convert.ToBoolean(noVideo.Value);
                            if (NoVideo)
                            {
                                KinectViewGrid.Visibility = Visibility.Hidden;
                            }
                            else
                            {
                                KinectViewGrid.Visibility = Visibility.Visible;
                            }
                        }

                        break;
                    }
            }
        }

        /// <summary>
        /// Unload event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedFrom(NavigationEventArgs e)
        {
            //cleanup Kinects
            KinectSensor.KinectSensors.StatusChanged -= OnKinectSensorsStatusChanged;

            LeftView.Sensor = null;
            RightView.Sensor = null;
        }

        /// <summary>
        /// Load event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedTo(NavigationEventArgs e)
        {
            MainWindow window = Application.Current.MainWindow as MainWindow;
            if (window != null)
            {
                //try to find existing LinkGroup
                LinkGroup FMS = null;
                foreach (LinkGroup group in window.Menu.LinkGroups)
                {
                    if (group.DisplayName == MovementSkill.DisplayName)
                    {
                        FMS = group;
                        break;
                    }
                }

                //add if not found
                if (FMS == null)
                {
                    SaveRecordingData.Subject = null;
                    ResetAllComponents();

                    FMS = new LinkGroup { DisplayName = MovementSkill.DisplayName };

                    session = new Link { DisplayName = (MovementSkill.Current == MovementSkill.Index.UniActive) ? "Session" : "Location", Source = new Uri("/Pages/Record.xaml#session", UriKind.Relative) };
                    FMS.Links.Add(session);
                    if (MovementSkill.Current == MovementSkill.Index.UniActive)
                    {
                        Form.Source = new Uri("/Pages/ReportSessions.xaml", UriKind.Relative);
                    }
                    else
                    {
                        Form.Source = new Uri("/Pages/NewSession.xaml", UriKind.Relative);
                    }

                    calibrate = new Link { DisplayName = "Calibrate", Source = new Uri("/Pages/Record.xaml#calibrate", UriKind.Relative) };
                    FMS.Links.Add(calibrate);

                    record = new Link { DisplayName = "Record", Source = new Uri("/Pages/Record.xaml#record", UriKind.Relative) };
                    FMS.Links.Add(record);

                    window.Menu.LinkGroups.Add(FMS);
                    window.Menu.SelectedLink = session;
                }
                else
                {
                    //make sure caches are correct
                    foreach (Link link in FMS.Links)
                    {
                        if (link.DisplayName == "Session" || link.DisplayName == "Location")
                        {
                            session = link;
                        }
                        else if (link.DisplayName == "Calibrate")
                        {
                            calibrate = link;
                        }
                        else if (link.DisplayName == "Record")
                        {
                            record = link;
                        }
                    }
                }
            }

            //setup Kinects
            ThreadPool.QueueUserWorkItem(
                (o) =>
                {
                    KinectSensorCollection sensors = KinectSensor.KinectSensors;

                    //hide processing 'panel' and show save 'panel'
                    //do this on the GUI thread
                    Dispatcher.Invoke((Action)delegate
                    {
                        sensors.StatusChanged += OnKinectSensorsStatusChanged;
                        OnKinectSensorsStatusChanged(sensors, null);
                    });
                });
        }

        /// <summary>
        /// Disables navigation while recording
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            if (RecStartStop.IsChecked == true)
            {
                e.Cancel = true;
            }
        }
        #endregion

        #region EventHandlers
        /// <summary>
        /// Event to handle Kinect Sensor status changes
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="args">event arguments</param>
        private void OnKinectSensorsStatusChanged(object sender, StatusChangedEventArgs args)
        {
            KinectSensorCollection sensors = (KinectSensorCollection)sender;

            int leftIndex = -1;
            int rightIndex = -1;

            //map kinects to views by id
            //this allows the order of sensors in the collection to change
            for (int i = 0; i < sensors.Count; i++)
            {
                if (sensors[i].Status == KinectStatus.Connected)
                {
                    if (LeftView.Sensor != null
                        && LeftView.Sensor.UniqueKinectId == sensors[i].UniqueKinectId)
                    {
                        leftIndex = i;
                    }
                    if (RightView.Sensor != null
                        && RightView.Sensor.UniqueKinectId == sensors[i].UniqueKinectId)
                    {
                        rightIndex = i;
                    }
                }
            }

            //map newly connected kinects
            for (int i = 0; i < sensors.Count; i++)
            {
                if (sensors[i].Status == KinectStatus.Connected)
                {
                    if (!swapped
                        && leftIndex == -1
                        && rightIndex != i)
                    {
                        leftIndex = i;
                        LeftView.Sensor = sensors[i];
                    }
                    if (rightIndex == -1
                        && leftIndex != i)
                    {
                        rightIndex = i;
                        RightView.Sensor = sensors[i];
                    }
                    if (swapped
                        && leftIndex == -1
                        && rightIndex != i)
                    {
                        leftIndex = i;
                        LeftView.Sensor = sensors[i];
                    }
                }
            }

            //account for disconnected kinects
            if (leftIndex == -1
                && LeftView.Sensor != null)
            {
                LeftView.Sensor = null;
            }
            if (rightIndex == -1
                && RightView.Sensor != null)
            {
                RightView.Sensor = null;
            }
        }

        /// <summary>
        /// Keep track of swapped state, so that the user doesn't have to swap every time
        /// </summary>
        private bool swapped = false;

        /// <summary>
        /// Event handler to handle swapping kinect views
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void OnClick_Swap(object sender, RoutedEventArgs e)
        {
            swapped = !swapped;

            KinectSensor right = LeftView.Sensor;
            KinectSensor left = RightView.Sensor;

            //stop old left (new right)
            LeftView.Sensor = null;

            //stop old right (new left) and set new right
            RightView.Sensor = right;

            //set new left
            LeftView.Sensor = left;
        }

        /// <summary>
        /// Event handler to handle recording of data
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void OnClick_RecStartStop(object sender, RoutedEventArgs e)
        {
            MainWindow window = Application.Current.MainWindow as MainWindow;

            bool? isRecording = (e.OriginalSource as ToggleButton).IsChecked;
            if (isRecording == true)
            {
                stayOnPage = window.Menu.SelectedLink;

                if (RecordGrid.Visibility == Visibility.Visible)
                {
                    if (NoVideo)
                    {
                        (e.OriginalSource as ToggleButton).IsChecked = false;
                        isRecording = false;
                    }
                    else
                    {
                        XmlNode noPreTime = ActivityNodes[Activities_SelectBox.SelectedIndex].Attributes["noPreTime"];
                        if (noPreTime != null && Convert.ToBoolean(noPreTime.Value))
                        {
                            OnStartComplete(null, null);
                        }
                        else
                        {
                            CountDown.Visibility = Visibility.Visible;
                        }
                    }
                }
            }
            if (isRecording == false)
            {
                CountDown.Visibility = Visibility.Hidden;
                CountDown2.Visibility = Visibility.Hidden;
                if (CalibrateGrid.Visibility == Visibility.Visible)
                {
                    ResetAllComponents();
                    InitializeListCounts();

                    //save calibration streams
                    SaveRecordingData.LeftCalibration = LeftViewRecorder.GetAllSkeletonData();
                    SaveRecordingData.RightCalibration = RightViewRecorder.GetAllSkeletonData();

                    //clean up temp video file
                    if (!string.IsNullOrWhiteSpace(Camera.videostring)
                        && File.Exists(Camera.videostring))
                    {
                        File.Delete(Camera.videostring);
                    }

                    //stop recording
                    calibrated = true;
                    window.Menu.SelectedLink = record;
                }
                else if (RecordGrid.Visibility == Visibility.Visible)
                {
                    if (Timer.Visibility == Visibility.Visible)
                    {
                        SaveRecordingData.Time = Timer.GetTime();
                        Timer.Visibility = Visibility.Hidden;
                    }
                    else
                    {
                        SaveRecordingData.Time = null;
                    }
                    SaveRecordingData.Activity = (XmlElement)Activities_SelectBox.SelectedItem;
                    if (NoVideo)
                    {
                        SaveRecordingData.LeftSkeletonRecorder = null;
                        SaveRecordingData.RightSkeletonRecorder = null;
                        SaveRecordingData.VideoPath = null;
                    }
                    else
                    {
                        SaveRecordingData.LeftSkeletonRecorder = LeftViewRecorder;
                        SaveRecordingData.RightSkeletonRecorder = RightViewRecorder;
                        SaveRecordingData.VideoPath = Camera.videostring;
                    }

                    //stop recording
                    window.ContentSource = new Uri("/Pages/SaveRecording.xaml", UriKind.Relative);
                }
            }
        }

        /// <summary>
        /// Event handler to handle completion of the 'start' countdown timer
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void OnStartComplete(object sender, RoutedEventArgs e)
        {
            XmlNode countdown = ((XmlElement)Activities_SelectBox.SelectedItem).Attributes["countdown"];
            XmlNode timer = ((XmlElement)Activities_SelectBox.SelectedItem).Attributes["timer"];
            if (countdown != null)
            {
                CountDown2.CountStart = Convert.ToInt32(countdown.Value);
                CountDown2.Visibility = Visibility.Visible;
            }
            else if (timer != null)
            {
                Timer.startTime = DateTime.Now;
                XmlNode timerLimit = ((XmlElement)Activities_SelectBox.SelectedItem).Attributes["timerLimit"];
                if (timerLimit != null)
                {
                    Timer.timerLimit = Convert.ToInt32(timerLimit.Value);
                }

                Timer.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Event handler to handle completion of the 'start' countdown timer
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void OnCountdownComplete(object sender, RoutedEventArgs e)
        {
            //stop recording?
        }
        #endregion

        #region UI
        /// <summary>
        /// Item for the 'count' listview collection
        /// </summary>
        public class Count
        {
            public int left;
            public int? right;
            public int? trials;

            public override string ToString()
            {
                string retval = left.ToString();
                if (trials != null)
                {
                    retval += "/" + trials;
                }
                if (right != null)
                {
                    retval += ", " + right;
                    if (trials != null)
                    {
                        retval += "/" + trials;
                    }
                }
                return retval;
            }
        }
        public class CountItem
        {
            public string activity { get; set; }
            public Count count { get; set; }
        }
        public ObservableCollection<CountItem> countList { get; private set; }

        /// <summary>
        /// Reset the control
        /// </summary>
        private void ResetAllComponents()
        {
            Activities_SelectBox.IsEnabled = true;

            countList = new ObservableCollection<CountItem>();

            //get the list of activities
            XmlDataProvider xmlActivities = (XmlDataProvider)this.FindResource("Activities");
            xmlActivities.IsInitialLoadEnabled = true;
            xmlActivities.IsAsynchronous = false;
            xmlActivities.Refresh();

            ActivityNodes = xmlActivities.Document.SelectNodes("Activities/" + MovementSkill.ID + "/Activity");

            //and use that to populate the collection
            foreach (XmlNode node in ActivityNodes)
            {
                CountItem item = new CountItem();
                item.count = new Count();

                item.activity = node.Attributes["name"].Value;
                item.count.left = 0;

                item.count.right = null;
                XmlNode eachSide = node.Attributes["eachSide"];
                if (eachSide != null && Convert.ToBoolean(eachSide.Value))
                {
                    item.count.right = 0;
                }

                item.count.trials = null;
                if (MovementSkill.Current == MovementSkill.Index.UniActive)
                {
                    item.count.trials = Convert.ToInt32(node.Attributes["trials"].Value);
                }
                countList.Add(item);
            }

            //apply the collection to the listview
            ActivityCountListView.ItemsSource = countList;
        }

        private void InitializeListCounts()
        {
            DatabaseHelper db = new DatabaseHelper();
            var SessionActivities = db.GetSessionActivities(SaveRecordingData.Session);
            if (SessionActivities == null)
            {
                RefreshActivityCountListView();
                return;
            }
            foreach (object row in SessionActivities)
            {
                string name = DatabaseHelper.GetActivity_ActivityType(row);

                //find the approriate activity in the listview
                for (int i = 0; i < countList.Count; i++)
                {
                    if (countList[i].activity == name)
                    {
                        //and update its count
                        if (countList[i].count.right == null)
                        {
                            countList[i].count.left++;
                        }
                        else
                        {
                            string leftRight = null;
                            foreach (XmlElement node in ActivityNodes[i].ChildNodes)
                            {
                                XmlNode hasLeftRight = node.Attributes["leftRight"];
                                if (hasLeftRight != null && Convert.ToBoolean(hasLeftRight.Value))
                                {
                                    leftRight = node.Attributes["dbName"].Value;
                                    break;
                                }
                            }

                            var ActivityData = db.GetActivityData((int)DatabaseHelper.GetActivity_ID(row));
                            foreach (var dataRow in ActivityData)
                            {
                                string dataName = DatabaseHelper.GetActivityData_Name(dataRow);
                                if (dataName == leftRight)
                                {
                                    int? boolData = DatabaseHelper.GetActivityData_BoolData(dataRow);
                                    if (boolData != null)
                                    {
                                        if (boolData == 1)
                                        {
                                            countList[i].count.right++;
                                        }
                                        else
                                        {
                                            countList[i].count.left++;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                        break;
                    }
                }
            }
            RefreshActivityCountListView();

            if (MovementSkill.Current == MovementSkill.Index.UniActive)
            {
                Activities_SelectBox.SelectedIndex = countList.Count - 1;
                for (int i = 0; i < countList.Count; i++)
                {
                    if (countList[i].count.left < countList[i].count.trials
                        || (countList[i].count.right != null && countList[i].count.right < countList[i].count.trials)
                        )
                    {
                        Activities_SelectBox.SelectedIndex = i;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Refresh the listview and ensure that the columns are wide enough to read the contents
        /// </summary>
        private void RefreshActivityCountListView()
        {
            ActivityCountListView.Items.Refresh();

            foreach (GridViewColumn column in ((GridView)ActivityCountListView.View).Columns)
            {
                if (double.IsNaN(column.Width))
                {
                    column.Width = column.ActualWidth;
                }

                column.Width = double.NaN;
            }
        }
        #endregion

        /// <summary>
        /// The participant that is being used / calibrated
        /// </summary>
        public object Participant
        {
            get { return this.GetValue(ParticipantProperty); }
            set { this.SetValue(ParticipantProperty, value); }
        }
        public static readonly DependencyProperty ParticipantProperty = DependencyProperty.Register("Participant", typeof(object), typeof(Record), new PropertyMetadata(null));

        /// <summary>
        /// The node of the Activities XML used
        /// </summary>
        public XmlNodeList ActivityNodes
        {
            get { return (XmlNodeList)this.GetValue(ActivityNodesProperty); }
            set { this.SetValue(ActivityNodesProperty, value); }
        }
        public static readonly DependencyProperty ActivityNodesProperty = DependencyProperty.Register("ActivityNodes", typeof(XmlNodeList), typeof(Record), new PropertyMetadata(null));
    }
}
