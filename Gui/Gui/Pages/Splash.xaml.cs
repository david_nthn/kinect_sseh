﻿using System;
using System.Windows;
using System.Windows.Controls;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Navigation;

namespace Gui
{
    /// <summary>
    /// Interaction logic for Splash.xaml
    /// </summary>
    public partial class Splash : UserControl, IContent
    {
        public Splash()
        {
            InitializeComponent();
        }

        #region IContent
        /// <summary>
        /// Handle sub-pages
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnFragmentNavigation(FragmentNavigationEventArgs e)
        {
        }

        /// <summary>
        /// Unload event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedFrom(NavigationEventArgs e)
        {
        }

        /// <summary>
        /// Load event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedTo(NavigationEventArgs e)
        {
            MainWindow window = Application.Current.MainWindow as MainWindow;
            if (window != null)
            {
                //add LinkGroup
                if (window.Menu.LinkGroups.Count != 1)
                {
                    window.Menu.LinkGroups.Clear();

                    LinkGroup linkgroup = new LinkGroup { DisplayName = "Home" };
                    Link link = new Link { DisplayName = "Welcome", Source = new Uri("/Pages/Splash.xaml", UriKind.Relative) };
                    linkgroup.Links.Add(link);
                    window.Menu.LinkGroups.Add(linkgroup);
                    window.Menu.SelectedLink = link;
                }
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
        }
        #endregion

        #region EventHandlers
        /// <summary>
        /// Event handler for pressing the FMS button
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void OnFMS(object sender, RoutedEventArgs e)
        {
            MainWindow window = Application.Current.MainWindow as MainWindow;
            if(window == null)
            {
                return;
            }

            MovementSkill.Current = MovementSkill.Index.FMS;
            window.ContentSource = new Uri("/Pages/Record.xaml", UriKind.Relative);
        }

        /// <summary>
        /// Event handler for pressing the Uni Active button
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void OnUniActive(object sender, RoutedEventArgs e)
        {
            MainWindow window = Application.Current.MainWindow as MainWindow;
            if(window == null)
            {
                return;
            }

            MovementSkill.Current = MovementSkill.Index.UniActive;
            window.ContentSource = new Uri("/Pages/Record.xaml", UriKind.Relative);
        }

        /// <summary>
        /// Event handler for pressing the Psych button
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void OnPsych(object sender, RoutedEventArgs e)
        {
            MainWindow window = Application.Current.MainWindow as MainWindow;
            if (window == null)
            {
                return;
            }

            window.ContentSource = new Uri("/Pages/Psych.xaml#select", UriKind.Relative);
        }
        #endregion
    }
}
