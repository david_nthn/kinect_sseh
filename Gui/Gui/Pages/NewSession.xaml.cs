﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Common;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Navigation;

namespace Gui
{
    /// <summary>
    /// Interaction logic for NewSession.xaml
    /// </summary>
    public partial class NewSession : UserControl, IContent
    {
        public NewSession()
        {
            InitializeComponent();

            if(MovementSkill.Current == MovementSkill.Index.UniActive)
            {
                Continue.Visibility = Visibility.Visible;
            }

            //make sure DPs work
            LayoutRoot.DataContext = this;
        }

        #region IContent
        /// <summary>
        /// Handle sub-pages
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnFragmentNavigation(FragmentNavigationEventArgs e)
        {
            Participant_SelectBox.SelectedItem = null;
        }

        /// <summary>
        /// Unload event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedFrom(NavigationEventArgs e)
        {
        }

        /// <summary>
        /// Load event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedTo(NavigationEventArgs e)
        {
            Participant_SelectBox.RefreshDataContext();
        }

        /// <summary>
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
        }
        #endregion


        /// <summary>
        /// The active SessionID of the current participant
        /// </summary>
        private int? SessionID = null;

        #region EventHandler
        /// <summary>
        /// Check if the currently selected participant has an incomplete session
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Participant_SelectBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Continue.IsEnabled = false;
            SessionID = new DatabaseHelper().GetActiveSessionID((System.Data.DataRowView)Participant_SelectBox.SelectedItem);
            if(SessionID != null)
            {
                Continue.IsEnabled = true;
            }
        }

        /// <summary>
        /// Continue previous session
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void ContinueSession(object sender, RoutedEventArgs e)
        {
            DoSession();
        }

        /// <summary>
        /// Start a new session
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void StartSession(object sender, RoutedEventArgs e)
        {
            if (SessionID != null)
            {
                new DatabaseHelper().MarkSessionInvalid((int)SessionID);
                SessionID = null;
            }

            DoSession();
        }
        #endregion

        /// <summary>
        /// Get the session underway
        /// </summary>
        private void DoSession()
        {
            //add entry to database
            SaveRecordingData.Subject = (System.Data.DataRowView)Participant_SelectBox.SelectedItem;

            if (SessionID == null)
            {
                SessionID = new DatabaseHelper().AddSession(SaveRecordingData.Subject, DateTime.Now, MovementSkill.ID);
            }
            SaveRecordingData.Session = new DatabaseHelper().GetSession((int)SessionID);

            (Application.Current.MainWindow as MainWindow).ContentSource = new Uri("/Pages/Record.xaml#calibrate", UriKind.Relative);
        }
    }
}
