﻿using System;
using System.Data;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Xml;
using Common;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Navigation;

namespace Gui
{
    /// <summary>
    /// Class to contain data required by SaveRecording
    /// This is done as it is not desirable to pass objects via Fragments
    /// </summary>
    public static class SaveRecordingData
    {
        /// <summary>
        /// Information about the participant that performed the activity
        /// </summary>
        public static DataRowView Subject;

        /// <summary>
        /// Information about the participant that performed the activity
        /// </summary>
        public static DataRowView Session;
        
        /// <summary>
        /// Information about the activity performed
        /// </summary>
        public static XmlElement Activity;

        /// <summary>
        /// Extra info about the activity performed
        /// </summary>
        public static double? Time;

        /// <summary>
        /// Path to recorded video
        /// </summary>
        public static string VideoPath;

        /// <summary>
        /// Left calibration stream
        /// </summary>
        public static byte[] LeftCalibration;
        
        /// <summary>
        /// Recorder that handles recording of the left skeleton view
        /// </summary>
        public static SkeletonRecorder LeftSkeletonRecorder;

        /// <summary>
        /// Right calibration stream
        /// </summary>
        public static byte[] RightCalibration;
        
        /// <summary>
        /// Recorder that handles recording of the right skeleton view
        /// </summary>
        public static SkeletonRecorder RightSkeletonRecorder;
    }

    /// <summary>
    /// Interaction logic for SaveRecording.xaml
    /// </summary>
    public partial class SaveRecording : UserControl, IContent
    {
        public SaveRecording()
        {
            InitializeComponent();
            Popup.DataContext = this;
        }

        #region IContent
        /// <summary>
        /// Whether to allow navigation away from this page
        /// </summary>
        private bool allowNav = false;

        /// <summary>
        /// Cached value for save Link (ie. the link to this page)
        /// </summary>
        private Link save;

        /// <summary>
        /// Handle sub-pages
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnFragmentNavigation(FragmentNavigationEventArgs e)
        {
        }

        /// <summary>
        /// Unload event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedFrom(NavigationEventArgs e)
        {
        }

        /// <summary>
        /// Load event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedTo(NavigationEventArgs e)
        {
            MainWindow window = Application.Current.MainWindow as MainWindow;

            if (window == null)
            {
                return;
            }

            bool contains = false;
            foreach (Link link in window.Menu.SelectedLinkGroup.Links)
            {
                if (link.DisplayName == "Save")
                {
                    save = link;
                    contains = true;
                    break;
                }
            }

            save = new Link { DisplayName = "Save", Source = window.ContentSource };
            if (!contains)
            {
                window.Menu.SelectedLinkGroup.Links.Add(save);
            }
            window.Menu.SelectedLink = save;

            OnVisibleChanged();

            allowNav = false;
        }

        /// <summary>
        /// Disables navigation until save complete
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            if (!allowNav)
            {
                e.Cancel = true;
            }
        }
        #endregion

        /// <summary>
        /// Time that saving started
        /// </summary>
        private DateTime timestamp;

        private int leftRightCheck = 0;

        /// <summary>
        /// Process data in preparation to save
        /// </summary>
        private void OnVisibleChanged()
        {
            //store the time that saving started
            timestamp = DateTime.Now;
            
            //make the 'processing' panel visible
            Processing.Visibility = Visibility.Visible;
            MainGrid.Visibility = Visibility.Collapsed;

            //reset the control
            Title.Text = SaveRecordingData.Activity.Attributes.GetNamedItem("name").Value;

            Classifier.Visibility = Visibility.Collapsed;
            Number.Visibility = Visibility.Collapsed;
            Check.Visibility = Visibility.Collapsed;
            Check2.Visibility = Visibility.Collapsed;
            if(MovementSkill.Current == MovementSkill.Index.FMS)
            {
                Classifier.Visibility = Visibility.Visible;
            }
            else if (MovementSkill.Current == MovementSkill.Index.UniActive)
            {
                leftRightCheck = 0;
                foreach(XmlNode node in SaveRecordingData.Activity.ChildNodes)
                {
                    if(node.Name == "Number")
                    {
                        Number.Visibility = Visibility.Visible;
                        NumberText.Text = node.Attributes.GetNamedItem("name").Value;
                        NumberDB.Text = node.Attributes.GetNamedItem("dbName").Value;
                        NumberField.Text = null;
                        if(SaveRecordingData.Time != null)
                        {
                            NumberField.Text = ((double)SaveRecordingData.Time).ToString("0.##");
                        }
                    }
                    else if(node.Name == "Check")
                    {
                        Check.Visibility = Visibility.Visible;
                        Check.Content = node.Attributes.GetNamedItem("name").Value;
                        CheckDB.Text = node.Attributes.GetNamedItem("dbName").Value;

                        XmlNode hasDefault = node.Attributes.GetNamedItem("default");
                        if (hasDefault != null)
                        {
                            Check.IsChecked = Convert.ToBoolean(hasDefault.Value);
                        }

                        XmlNode hasLeftRight = node.Attributes.GetNamedItem("leftRight");
                        if(hasLeftRight != null && Convert.ToBoolean(hasLeftRight.Value))
                        {
                            leftRightCheck = 1;
                        }
                    }
                    else if (node.Name == "Check2")
                    {
                        Check2.Visibility = Visibility.Visible;
                        Check2.Content = node.Attributes.GetNamedItem("name").Value;
                        Check2DB.Text = node.Attributes.GetNamedItem("dbName").Value;

                        XmlNode hasDefault = node.Attributes.GetNamedItem("default");
                        if (hasDefault != null)
                        {
                            Check.IsChecked = Convert.ToBoolean(hasDefault.Value);
                        }

                        XmlNode hasLeftRight = node.Attributes.GetNamedItem("leftRight");
                        if (hasLeftRight != null && Convert.ToBoolean(hasLeftRight.Value))
                        {
                            leftRightCheck = 2;
                        }
                    }
                }
            }
            Invalid_CheckBox.IsChecked = false;
            Notes_TextBox.Text = "";

            //do actual processing in a seperate thread
            ThreadPool.QueueUserWorkItem(
                (o) =>
                {
                    //do processing
                    DoProcessing();

                    //hide processing 'panel' and show save 'panel'
                    //do this on the GUI thread
                    Dispatcher.Invoke((Action)delegate
                    {
                        Processing.Visibility = Visibility.Collapsed;
                        MainGrid.Visibility = Visibility.Visible;
                    });
                });
        }

        /// <summary>
        /// Cache for left skeleton view calibration
        /// </summary>
        byte[] LeftCalibration = null;

        /// <summary>
        /// Cache for left skeleton view recording
        /// </summary>
        byte[] LeftStream = null;

        /// <summary>
        /// Cache for right skeleton view calibration
        /// </summary>
        byte[] RightCalibration = null;

        /// <summary>
        /// Cache for right skeleton view recording
        /// </summary>
        byte[] RightStream = null;

        /// <summary>
        /// Cache for merged skeleton data
        /// </summary>
        byte[] MergedStream = null;

        /// <summary>
        /// Process data in preparation to save
        /// </summary>
        private void DoProcessing()
        {
            string activity = null;

            //Get skeleton streams for processing
            Dispatcher.Invoke((Action)delegate
            {
                LeftCalibration = SaveRecordingData.LeftCalibration;
                RightCalibration = SaveRecordingData.RightCalibration;

                if (SaveRecordingData.LeftSkeletonRecorder == null)
                {
                    LeftStream = null;
                }
                else
                {
                    LeftStream = SaveRecordingData.LeftSkeletonRecorder.GetAllSkeletonData();
                }
                if (SaveRecordingData.RightSkeletonRecorder == null)
                {
                    RightStream = null;
                }
                else
                {
                    RightStream = SaveRecordingData.RightSkeletonRecorder.GetAllSkeletonData();
                }

                activity = SaveRecordingData.Activity.Attributes.GetNamedItem("name").Value;
            });
            
            //merge skeleton streams into a combined stream
            MergedStream = SkeletonMultiplexer.Multiplex(LeftCalibration, LeftStream, RightCalibration, RightStream);

            if (MovementSkill.Current == MovementSkill.Index.FMS)
            {
                //pass the merged stream through the classifier
                int rating = ActivityClassifier.Classify(MergedStream, activity);

                //apply the results to the UI
                Dispatcher.Invoke((Action)delegate
                {
                    ClassifierRating = rating.ToString();
                    HumanRating = ClassifierRating;
                });
            }
        }

        /// <summary>
        /// Helper that requests a folder location from the user
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void BrowseFolder(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            dialog.SelectedPath = Folder_TextBox.Text;

            System.Windows.Forms.DialogResult result = dialog.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                Folder_TextBox.Text = dialog.SelectedPath;
            }
        }

        /// <summary>
        /// Do the actual saving
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void OnClick_OK(object sender, RoutedEventArgs e)
        {
            string activity = SaveRecordingData.Activity.Attributes.GetNamedItem("name").Value;
            //whether the activity instance is considered valid
            bool isValid = Invalid_CheckBox.IsChecked != true;
            
            //path to save the data into
            string folderPath = string.Format("{0}\\{1}_{2}{3}_{4}",
                                    Folder_TextBox.Text,
                                    timestamp.ToString("yyyyMMdd_HHmmss"),
                                    DatabaseHelper.GetSubject_LastName(SaveRecordingData.Subject),
                                    DatabaseHelper.GetSubject_FirstName(SaveRecordingData.Subject),
                                    activity);
            Directory.CreateDirectory(folderPath);
            
            //save the data
            if (LeftCalibration != null)
            {
                using (FileStream file = new FileStream(folderPath + "\\LeftCalibration.bin", FileMode.Create, System.IO.FileAccess.Write))
                {
                    file.Write(LeftCalibration, 0, LeftCalibration.Length);
                }
            }
            if (LeftStream != null)
            {
                using (FileStream file = new FileStream(folderPath + "\\Left.bin", FileMode.Create, System.IO.FileAccess.Write))
                {
                    file.Write(LeftStream, 0, LeftStream.Length);
                }
            }
            if (RightCalibration != null)
            {
                using (FileStream file = new FileStream(folderPath + "\\RightCalibration.bin", FileMode.Create, System.IO.FileAccess.Write))
                {
                    file.Write(RightCalibration, 0, RightCalibration.Length);
                }
            }
            if (RightStream != null)
            {
                using (FileStream file = new FileStream(folderPath + "\\Right.bin", FileMode.Create, System.IO.FileAccess.Write))
                {
                    file.Write(RightStream, 0, RightStream.Length);
                }
            }
            if (MergedStream != null)
            {
                using (FileStream file = new FileStream(folderPath + "\\Merged.bin", FileMode.Create, System.IO.FileAccess.Write))
                {
                    file.Write(MergedStream, 0, MergedStream.Length);
                }
            }
            if (!string.IsNullOrWhiteSpace(SaveRecordingData.VideoPath)
                        && File.Exists(SaveRecordingData.VideoPath))
            {
                File.Move(SaveRecordingData.VideoPath, folderPath + "\\Video.mp4");
            }

            DatabaseHelper db = new DatabaseHelper();
            int id = db.AddActivity(SaveRecordingData.Session, timestamp, folderPath, activity, Notes_TextBox.Text, isValid);

            bool leftRight = false;
            //add activity information into the database
            if (MovementSkill.Current == MovementSkill.Index.FMS)
            {
                db.AddActivityDataFloat(id, "Classifier Rating", ClassifierRating);
                db.AddActivityDataFloat(id, "Human Rating", HumanRating);
            }
            else if (MovementSkill.Current == MovementSkill.Index.UniActive)
            {
                if (Number.Visibility == Visibility.Visible)
                {
                    db.AddActivityDataFloat(id, NumberDB.Text, NumberField.Text);
                }
                if (Check.Visibility == Visibility.Visible)
                {
                    db.AddActivityDataBool(id, CheckDB.Text, (bool)Check.IsChecked);
                    if(leftRightCheck == 1)
                    {
                        leftRight = (bool)Check.IsChecked;
                    }
                }
                if (Check2.Visibility == Visibility.Visible)
                {
                    db.AddActivityDataBool(id, Check2DB.Text, (bool)Check2.IsChecked);
                    if (leftRightCheck == 2)
                    {
                        leftRight = (bool)Check2.IsChecked;
                    }
                }
            }
            
            //'close'
            string uri = "/Pages/Record.xaml#record";

            if (isValid)
            {
                uri = uri + FragmentHelper.Separator + activity + FragmentHelper.Separator + leftRight;
            }
            allowNav = true;


            MainWindow window = Application.Current.MainWindow as MainWindow;
            window.ContentSource = new Uri(uri, UriKind.Relative);
            if (save != null)
            {
                window.Menu.SelectedLinkGroup.Links.Remove(save);
            }
        }

        /// <summary>
        /// Rating returned by the classifier
        /// </summary>
        public string ClassifierRating
        {
            get { return (string)this.GetValue(ClassifierRatingProperty); }
            set { this.SetValue(ClassifierRatingProperty, value); }
        }
        public static readonly DependencyProperty ClassifierRatingProperty = DependencyProperty.Register("ClassifierRating", typeof(string), typeof(SaveRecording), new PropertyMetadata(null));

        /// <summary>
        /// Rating that the human rater enters
        /// </summary>
        public string HumanRating
        {
            get { return (string)this.GetValue(HumanRatingProperty); }
            set { this.SetValue(HumanRatingProperty, value); }
        }
        public static readonly DependencyProperty HumanRatingProperty = DependencyProperty.Register("HumanRating", typeof(string), typeof(SaveRecording), new PropertyMetadata(null));
    }
}
