﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Common;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Navigation;

namespace Gui
{
    /// <summary>
    /// Interaction logic for Surveys.xaml
    /// </summary>
    public partial class Surveys : UserControl, IContent
    {
        /// <summary>
        /// Helper class that handles reading / writing of the Survey list
        /// </summary>
        private SurveyHelper surveys = new SurveyHelper();

        public Surveys()
        {
            InitializeComponent();

            DataContext = surveys.data.Surveys;
        }

        #region IContent
        /// <summary>
        /// Handle sub-pages
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnFragmentNavigation(FragmentNavigationEventArgs e)
        {
            string[] subFragments = FragmentHelper.GetFragments(e.Fragment);
            if (subFragments.Length > 1)
            {
                UpdateDropDownListView(string.IsNullOrWhiteSpace(subFragments[1]) ? (int?)null : int.Parse(subFragments[1]));
            }
        }

        /// <summary>
        /// Unload event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedFrom(NavigationEventArgs e)
        {
        }

        /// <summary>
        /// Load event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedTo(NavigationEventArgs e)
        {
            CommandsListView.SelectedIndex = 0;
        }

        /// <summary>
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
        }
        #endregion

        #region EventHandlers
        /// <summary>
        /// Event handler to select an item in the 'command' list
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void OnSelectionChanged_Commands(object sender, RoutedEventArgs e)
        {
            object item = (e.OriginalSource as ListView).SelectedItem;
            if (item == null)
            {
                return;
            }
            SurveysListView.SelectedItem = null;

            //currently only one command in the list
            Form.Source = new Uri("/Content/SurveysForm.xaml#add", UriKind.Relative);
        }

        /// <summary>
        /// Event handler to select an item in the 'survey' list
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void OnSelectionChanged_Surveys(object sender, RoutedEventArgs e)
        {
            object item = (e.OriginalSource as ListView).SelectedItem;
            if (item == null)
            {
                return;
            }
            CommandsListView.SelectedItem = null;

            //account for column sorting
            for (int i = 0; i < surveys.data.Surveys.Count; i++ )
            {
                if (item == surveys.data.Surveys[i])
                {
                    string uri = "/Content/SurveysForm.xaml#edit"
                        + FragmentHelper.Separator + i.ToString();
                    Form.Source = new Uri(uri, UriKind.Relative);
                    break;
                }
            }
        }

        /// <summary>
        /// Helper to update the contents of the contained listview
        /// </summary>
        /// <param name="id">Index in the helper to select</param>
        private void UpdateDropDownListView(int? id = null)
        {
            //update binding
            surveys.Refresh();
            DataContext = surveys.data.Surveys;

            //select the appropriate item
            if (id != null)
            {
                SurveysListView.SelectedIndex = (int)id;
            }

            //resort if needed
            SurveysListView.DoSort();

            //ensure columns are wide enough for contents
            SurveysListView.RefreshView();
        }
        #endregion
    }
}
