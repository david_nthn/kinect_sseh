﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Xml;
using Common;
using CsvHelper;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Navigation;

namespace Gui
{
    public static class Average
    {
        public static double Mean(this IEnumerable<double> list)
        {
            return list.Average(); // :-)
        }

        public static double Median(this IEnumerable<double> list)
        {
            List<double> orderedList = list
                .OrderBy(numbers => numbers)
                .ToList();

            int listSize = orderedList.Count;
            double result;

            if (listSize % 2 == 0) // even
            {
                int midIndex = listSize / 2;
                result = ((orderedList.ElementAt(midIndex - 1) +
                           orderedList.ElementAt(midIndex)) / 2);
            }
            else // odd
            {
                double element = (double)listSize / 2;
                element = Math.Round(element, MidpointRounding.AwayFromZero);

                result = orderedList.ElementAt((int)(element - 1));
            }

            return result;
        }

        public static IEnumerable<double> Modes(this IEnumerable<double> list)
        {
            var modesList = list
                .GroupBy(values => values)
                .Select(valueCluster =>
                        new
                        {
                            Value = valueCluster.Key,
                            Occurrence = valueCluster.Count(),
                        })
                .ToList();

            int maxOccurrence = modesList
                .Max(g => g.Occurrence);

            return modesList
                .Where(x => x.Occurrence == maxOccurrence && maxOccurrence > 1) // Thanks Rui!
                .Select(x => x.Value);
        }

        public static double StdDev(this IEnumerable<double> list)
        {
            double ret = 0;
            if (list.Count() > 1)
            {
                //Compute the Average      
                double avg = list.Average();
                //Perform the Sum of (value-avg)_2_2      
                double sum = list.Sum(d => Math.Pow(d - avg, 2));
                //Put it all together      
                ret = Math.Sqrt((sum) / (list.Count() - 1));
            }
            return ret;
        }
    }

    /// <summary>
    /// Interaction logic for Report.xaml
    /// </summary>
    public partial class Report : UserControl, IContent
    {
        public Report()
        {
            InitializeComponent();

            //make sure DPs work
            LayoutRoot.DataContext = this;

            //avoid designer complaining about not being able to find database
            if (!DesignerProperties.GetIsInDesignMode(this))
            {
                School_Combo.ItemsSource = new DatabaseHelper().GetSchoolTable();
            }
        }

        #region IContent
        /// <summary>
        /// Handle sub-pages
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnFragmentNavigation(FragmentNavigationEventArgs e)
        {
            type.isSubject = false;
            type.isClass = false;
            type.isYear = false;
            type.ID = null;

            SummaryReport.Visibility = Visibility.Collapsed;
            SchoolClass_Grid.Visibility = Visibility.Collapsed;
            SchoolClass_Text.Visibility = Visibility.Collapsed;

            School_Combo.SelectedItem = null;

            switch (FragmentHelper.GetFragmentName(e.Fragment))
            {
                case "subject":
                default:
                    {
                        type.isSubject = true;

                        string[] subFragments = FragmentHelper.GetFragments(e.Fragment);
                        if (subFragments.Length > 0
                            && !string.IsNullOrWhiteSpace(subFragments[0]))
                        {
                            type.ID = int.Parse(subFragments[0]);
                        }
                        break;
                    }
                case "class":
                    {
                        type.isClass = true;
                        SummaryReport.Visibility = Visibility.Visible;
                        SchoolClass_Grid.Visibility = Visibility.Visible;
                        SchoolClass_Text.Visibility = Visibility.Visible;
                        break;
                    }
                case "year":
                    {
                        type.isYear = true;
                        SummaryReport.Visibility = Visibility.Visible;
                        break;
                    }
            }
                
            UpdatePage();
        }

        /// <summary>
        /// Unload event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedFrom(NavigationEventArgs e)
        {
        }

        /// <summary>
        /// Load event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        /// <summary>
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
        }
        #endregion

        #region EventHandler

        private void School_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (School_Combo.SelectedItem == null)
            {
                Year_Combo.ItemsSource = null;
            }
            else
            {
                Year_Combo.ItemsSource = new DatabaseHelper().GetSchoolYearTable(DatabaseHelper.GetSchool_ID(School_Combo.SelectedItem));
            }
            Year_Combo.SelectedItem = null;
        }

        private void Year_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Year_Combo.SelectedItem == null)
            {
                SchoolClass_Combo.ItemsSource = null;
            }
            else
            {
                if(type.isYear)
                {
                    type.ID = (int)Year_Combo.SelectedValue;
                    UpdatePage();
                    return;
                }
                SchoolClass_Combo.ItemsSource = new DatabaseHelper().GetSchoolClassTable(DatabaseHelper.GetSchoolYear_ID(Year_Combo.SelectedItem));
            }
            SchoolClass_Combo.SelectedItem = null;
        }

        private void SchoolClass_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (SchoolClass_Combo.SelectedItem != null
                && type.isClass)
            {
                type.ID = (int)SchoolClass_Combo.SelectedValue;
                UpdatePage();
            }
        }

        private void AfterDate_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdatePage();
        }

        private void BeforeDate_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdatePage();
        }

        /// <summary>
        /// Helper that requests a folder location from the user
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Export(object sender, RoutedEventArgs e)
        {
            // Configure open file dialog box
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.OverwritePrompt = true;
            dlg.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;
            dlg.FileName = ""; // Default file name
            dlg.DefaultExt = ".xls"; // Default file extension
            dlg.Filter = "Excel (.xls)|*.xls"; // Filter files by extension 

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results 
            if (result != true)
            {
                return;
            }

            //do actual export
            using (StreamWriter sw = new StreamWriter(dlg.FileName))
            {
                using (CsvWriter writer = new CsvWriter(sw))
                {
                    DatabaseHelper db = new DatabaseHelper();

                    if (!type.isSubject)
                    {
                        writer.WriteField("School");
                        writer.WriteField(School_Combo.Text);
                        writer.NextRecord();

                        writer.WriteField("Year");
                        writer.WriteField(Year_Combo.Text);
                        writer.NextRecord();

                        if (!type.isYear)
                        {
                            writer.WriteField("Class");
                            writer.WriteField(SchoolClass_Combo.Text);
                            writer.NextRecord();
                        }
                    }
                    else
                    {
                        var subject = db.GetSubject(
                                            DatabaseHelper.GetSession_SubjectID(
                                                db.GetSession((int)type.ID)
                                            )
                                        );
                        writer.WriteField("First Name");
                        writer.WriteField(DatabaseHelper.GetSubject_FirstName(subject));
                        writer.NextRecord();

                        writer.WriteField("Last Name");
                        writer.WriteField(DatabaseHelper.GetSubject_LastName(subject));
                        writer.NextRecord();

                        int? schoolClassID = DatabaseHelper.GetSession_SchoolClassID(
                                                            db.GetSession((int)type.ID)
                                                        );
                        if (schoolClassID != null)
                        {
                            int? schoolYearID = DatabaseHelper.GetSchoolClass_SchoolYearID(
                                                            db.GetSchoolClass((int)schoolClassID)
                                                        );
                            if (schoolYearID != null)
                            {
                                int? schoolID = DatabaseHelper.GetSchoolYear_SchoolID(
                                                            db.GetSchoolYear((int)schoolYearID)
                                                        );
                                if (schoolID != null)
                                {
                                    writer.WriteField("School");
                                    writer.WriteField(DatabaseHelper.GetSchool_Name(
                                                            db.GetSchool((int)schoolID)
                                                        )
                                                    );
                                    writer.NextRecord();

                                    writer.WriteField("Year");
                                    writer.WriteField(DatabaseHelper.GetSchoolYear_Year(
                                                            db.GetSchoolYear((int)schoolYearID)
                                                        )
                                                    );
                                    writer.NextRecord();

                                    writer.WriteField("Class");
                                    writer.WriteField(DatabaseHelper.GetSchoolClass_Class(
                                                            db.GetSchoolClass((int)schoolClassID)
                                                        )
                                                    );
                                    writer.NextRecord();
                                }
                            }
                        }
                    }

                    writer.NextRecord();

                    if (summary != null)
                    {
                        writer.WriteRecords(summary);
                        writer.NextRecord();
                    }

                    if (reports != null)
                    {
                        //get and write headers
                        List<string> headers = new List<string>();
                        foreach (var record in reports)
                        {
                            foreach (var item in record)
                            {
                                int? h = null;
                                for(int i = 0; i < headers.Count; i++)
                                {
                                    if(headers[i] == item.activity)
                                    {
                                        h = i;
                                        break;
                                    }
                                }
                                if (h == null)
                                {
                                    headers.Add(item.activity);
                                }
                            }
                        }
                        headers.Sort();
                        if(!type.isSubject)
                        {
                            writer.WriteField("First Name");
                            writer.WriteField("Last Name");
                        }
                        if (type.isYear)
                        {
                            writer.WriteField("Class");
                        }
                        foreach (var h in headers)
                        {
                            writer.WriteField(h);
                        }
                        writer.NextRecord();

                        //write reports
                        foreach (var record in reports)
                        {
                            if (!type.isSubject)
                            {
                                var subject = db.GetSubject(
                                                    DatabaseHelper.GetSession_SubjectID(
                                                        db.GetSession(
                                                            DatabaseHelper.GetActivity_SessionID(record[0].rows[0])
                                                        )
                                                    )
                                                );
                                writer.WriteField(DatabaseHelper.GetSubject_FirstName(subject));
                                writer.WriteField(DatabaseHelper.GetSubject_LastName(subject));
                            }
                            if (type.isYear)
                            {
                                int? schoolClassID = DatabaseHelper.GetSession_SchoolClassID(
                                                            db.GetSession(
                                                                DatabaseHelper.GetActivity_SessionID(record[0].rows[0])
                                                            )
                                                        );
                                string schoolClass = null;
                                if (schoolClassID != null)
                                {
                                    schoolClass = DatabaseHelper.GetSchoolClass_Class(
                                                        db.GetSchoolClass((int)schoolClassID)
                                                    );
                                }
                                writer.WriteField(schoolClass);
                            }
                        
                            float?[] scores = new float?[headers.Count];
                            foreach (var item in record)
                            {
                                for (int i = 0; i < headers.Count; i++)
                                {
                                    if (headers[i] == item.activity)
                                    {
                                        scores[i] = item.score;
                                        break;
                                    }
                                }
                            }

                            foreach (var item in scores)
                            {
                                writer.WriteField(item);
                            }
                            writer.NextRecord();
                        }
                    }
                }
            }
        }
        #endregion

        private class ReportType
        {
            public bool isSubject = false;
            public bool isClass = false;
            public bool isYear = false;
            public int? ID = null;
        }
        private ReportType type = new ReportType();

        public class ActivityEntry
        {
            public string activity { get; set; }
            public List<System.Data.DataRowView> rows = new List<System.Data.DataRowView>();
            public float? score { get; set; }
        };
        List<ObservableCollection<ActivityEntry>> reports = null;

        public class ActivitySummary
        {
            public string activity { get; set; }
            public string activityWithRowCount
            {
                get { return activity + " (" + rows.Count + ")"; }
            }
            public List<double> rows = new List<double>();
            public double? mean { get; set; }
            public double? median { get; set; }
            public double? mode { get; set; }
            public double? stddev { get; set; }
        };
        ObservableCollection<ActivitySummary> summary = null;

        public class ActivityNodeEntry
        {
            //activity
            public string name;
            public bool eachSide = false;
            //number
            public string number = null;
            public bool lessIsBetter = false;
            //check
            public string counter = null;
            public string side = null;
        }
        public List<ActivityNodeEntry> nodeCache = new List<ActivityNodeEntry>();

        private Thread thread = null;
        private void UpdatePage()
        {
            listView.Visibility = Visibility.Collapsed;
            summaryListView.Visibility = Visibility.Collapsed;
            listView.ItemsSource = null;
            summaryListView.ItemsSource = null;

            if (type.ID == null)
            {
                return;
            }

            if (thread != null)
            {
                thread.Abort();
            }

            thread = new Thread(DoScoring);
            thread.Start();
        }

        private void DoScoring()
        {
            if(nodeCache.Count == 0)
            {
                //get the list of activities
                XmlDataProvider xmlActivities = (XmlDataProvider)this.FindResource("Activities");
                xmlActivities.IsInitialLoadEnabled = true;
                xmlActivities.IsAsynchronous = false;
                xmlActivities.Refresh();
                XmlNodeList ActivityNodes = xmlActivities.Document.SelectNodes("Activities/" + MovementSkill.ID + "/Activity");

                foreach (XmlNode activity in ActivityNodes)
                {
                    ActivityNodeEntry a = new ActivityNodeEntry();
                    a.name = activity.Attributes["name"].Value;

                    XmlNode hasEachSide = activity.Attributes["eachSide"];
                    a.eachSide = hasEachSide != null && Convert.ToBoolean(hasEachSide.Value);

                    foreach (XmlNode node in activity.ChildNodes)
                    {
                        if (node.Name == "Number")
                        {
                            a.number = node.Attributes["dbName"].Value;
                            XmlNode hasLessIsBetter = node.Attributes["lessIsBetter"];
                            a.lessIsBetter = hasLessIsBetter != null && Convert.ToBoolean(hasLessIsBetter.Value);
                        }
                        else
                        {
                            XmlNode hasLeftRight = node.Attributes["leftRight"];
                            if (hasLeftRight != null && Convert.ToBoolean(hasLeftRight.Value))
                            {
                                a.side = node.Attributes["dbName"].Value;
                            }

                            XmlNode hasCounter = node.Attributes["counter"];
                            if (hasCounter != null && Convert.ToBoolean(hasCounter.Value))
                            {
                                a.counter = node.Attributes["dbName"].Value;
                            }
                        }
                    }

                    nodeCache.Add(a);
                }
            }

            if (type.isSubject)
            {
                ObservableCollection<ActivityEntry> results = CalculateScores((int)type.ID);

                reports = new List<ObservableCollection<ActivityEntry>>();
                reports.Add(results);
                summary = null;

                Dispatcher.Invoke((Action)delegate
                {
                    listView.ItemsSource = results;
                    listView.Visibility = Visibility.Visible;
                });
            }
            else if (type.isClass || type.isYear)
            {
                DateTime? afterDate = null;
                DateTime? beforeDate = null;

                Dispatcher.Invoke((Action)delegate
                {
                    afterDate = After_DatePicker.SelectedDate;
                    beforeDate = Before_DatePicker.SelectedDate;
                });

                reports = GetReports((int)type.ID, afterDate, beforeDate);
                summary = CalculateSummary(reports);

                Dispatcher.Invoke((Action)delegate
                {
                    summaryListView.ItemsSource = summary;
                    summaryListView.Visibility = Visibility.Visible;
                });
            }
        }

        private ObservableCollection<ActivityEntry> CalculateScores(int sessionID)
        {
            ObservableCollection<ActivityEntry> retval = new ObservableCollection<ActivityEntry>();

            DatabaseHelper db = new DatabaseHelper();
            var dbActivities = db.GetSessionActivities(db.GetSession(sessionID));
            if (dbActivities == null)
            {
                //no data to show...
                return retval;
            }

            //sort rows into activity types to make scoring easier
            foreach (System.Data.DataRowView row in dbActivities)
            {
                if (!DatabaseHelper.GetActivity_IsValid(row))
                {
                    continue;
                }

                string name = DatabaseHelper.GetActivity_ActivityType(row);
                int index = -1;
                for (int i = 0; i < retval.Count; i++)
                {
                    if (retval[i].activity == name)
                    {
                        index = i;
                        break;
                    }
                }
                if (index == -1)
                {
                    ActivityEntry e = new ActivityEntry();
                    e.activity = name;
                    e.score = null;
                    retval.Add(e);
                    index = retval.Count - 1;
                }
                retval[index].rows.Add(row);
            }

            foreach (ActivityEntry entry in retval)
            {
                ActivityNodeEntry xml = null;
                for (int i = 0; i < nodeCache.Count; i++)
                {
                    if (nodeCache[i].name == entry.activity)
                    {
                        xml = nodeCache[i];
                        break;
                    }
                }

                if(xml == null)
                {
                    continue;
                }

                float? rightScore = null;
                foreach (object row in entry.rows)
                {
                    float? score = null;
                    bool isLeft = true;
                    var ActivityData = db.GetActivityData(DatabaseHelper.GetActivity_ID(row));
                    foreach (object data in ActivityData)
                    {
                        string dataName = DatabaseHelper.GetActivityData_Name(data);
                        if (dataName == xml.number)
                        {
                            score = DatabaseHelper.GetActivityData_FloatData(data);
                        }
                        if (dataName == xml.counter)
                        {
                            score = DatabaseHelper.GetActivityData_BoolData(data);
                        }
                        if (xml.eachSide && dataName == xml.side)
                        {
                            isLeft = DatabaseHelper.GetActivityData_BoolData(data) != 1;
                        }
                    }

                    if (score != null)
                    {
                        if (xml.counter != null) //just add everything
                        {
                            entry.score = entry.score == null
                                            ? score
                                            : entry.score + score;
                        }
                        else if (xml.eachSide && !isLeft)//keep seperate score for right if needed
                        {
                            rightScore = rightScore == null
                                            ? (float)score
                                            : xml.lessIsBetter
                                                ? Math.Min((float)rightScore, (float)score)
                                                : Math.Max((float)rightScore, (float)score);
                        }
                        else
                        {
                            entry.score = entry.score == null
                                            ? (float)score
                                            : xml.lessIsBetter
                                                ? Math.Min((float)entry.score, (float)score)
                                                : Math.Max((float)entry.score, (float)score);
                        }
                    }
                }

                //recombine right score
                if (xml.eachSide)
                {
                    entry.score = entry.score == null
                                    ? rightScore
                                    : rightScore == null
                                        ? entry.score
                                        : entry.score + rightScore;
                }
            }

            return retval;
        }
        private List<ObservableCollection<ActivityEntry>> GetReports(int ID, DateTime? afterDate, DateTime? beforeDate)
        {
            System.Data.DataView sessions = null;
            if (type.isClass)
            {
                sessions = new DatabaseHelper().GetValidSessionsByClass(MovementSkill.ID, ID);
            }
            else if (type.isYear)
            {
                sessions = new DatabaseHelper().GetValidSessionsByYear(MovementSkill.ID, ID);
            }
            if(sessions == null)
            {
                return null;
            }

            List<ObservableCollection<ActivityEntry>> results = new List<ObservableCollection<ActivityEntry>>();
            List<int> subjects = new List<int>();
            foreach (var s in sessions)
            {
                DateTime? time = DatabaseHelper.GetSession_SessionDate(s);
                if(time == null
                    || (afterDate != null && ((DateTime)time).CompareTo((DateTime)afterDate) < 0)
                    || (beforeDate != null && ((DateTime)time).CompareTo((DateTime)beforeDate) > 0)
                    )
                {
                    continue;
                }
                int id = DatabaseHelper.GetSession_SubjectID(s);
                bool found = false;
                foreach(int sID in subjects)
                {
                    if (sID == id)
                    {
                        found = true;
                        break;
                    }
                }
                if(found)
                {
                    continue;
                }
                subjects.Add(id);
                results.Add(CalculateScores(DatabaseHelper.GetSession_ID(s)));
            }

            return results;
        }
        private ObservableCollection<ActivitySummary> CalculateSummary(List<ObservableCollection<ActivityEntry>> reports)
        {
            if(reports == null 
                || reports.Count == 0)
            {
                return null;
            }

            ObservableCollection<ActivitySummary> summary = new ObservableCollection<ActivitySummary>();

            //sort rows into activity types to make scoring easier
            foreach (ObservableCollection<ActivityEntry> subject in reports)
            {
                foreach (ActivityEntry row in subject)
                {
                    int index = -1;
                    for (int i = 0; i < summary.Count; i++)
                    {
                        if (summary[i].activity == row.activity)
                        {
                            index = i;
                            break;
                        }
                    }
                    if (index == -1)
                    {
                        ActivitySummary e = new ActivitySummary();
                        e.activity = row.activity;
                        e.mean = null;
                        e.median = null;
                        e.mode = null;
                        e.stddev = null;
                        summary.Add(e);
                        index = summary.Count - 1;
                    }
                    if (row.score != null)
                    {
                        summary[index].rows.Add((float)row.score);
                    }
                }
            }
            foreach (ActivitySummary s in summary)
            {
                if(s.rows.Count() == 0)
                {
                    continue;
                }
                s.mean = s.rows.Mean();
                s.median = s.rows.Median();
                var modes = s.rows.Modes();
                if (modes.Count() > 0)
                {
                    s.mode = modes.ElementAt(0);
                }
                s.stddev = s.rows.StdDev();
            }

            return summary;
        }
    }
}
