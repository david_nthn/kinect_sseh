﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using AForge.Video.FFMPEG;
using Microsoft.Kinect;
using Drawing = System.Drawing;

namespace Gui
{
    /// <summary>
    /// Control to display the camera image from the Kinect
    /// </summary>
    class KinectCamera : KinectControl
    {
        /// <summary>
        /// Width of output drawing
        /// </summary>
        private const float RenderWidth = 640.0f;

        /// <summary>
        /// Height of our output drawing
        /// </summary>
        private const float RenderHeight = 480.0f;

        /// <summary>
        /// Image control to show camera data in the user interface
        /// </summary>
        private Image image = new Image();

        /// <summary>
        /// Helper to detect changes in the image format
        /// </summary>
        private ColorImageFormat lastImageFormat = ColorImageFormat.Undefined;

        /// <summary>
        /// Cache for raw image data
        /// </summary>
        private byte[] rawPixelData;

        /// <summary>
        /// Cache for image data
        /// </summary>
        private byte[] pixelData;

        /// <summary>
        /// Underlying image data
        /// </summary>
        private WriteableBitmap colorBitmap;

        /// <summary>
        /// Video writer object
        /// </summary>
        private VideoFileWriter videoFileWriter;

        /// <summary>
        /// path to the temp video file
        /// </summary>
        public string videostring;

        /// <summary>
        /// Thread that will process video frame data
        /// </summary>
        private Thread ProcessFrameThread;

        /// <summary>
        /// Inter-thread video buffer
        /// </summary>
        private List<byte[]> frames = new List<byte[]>();

        /// <summary>
        /// object to handle access to the inter-thread buffer
        /// </summary>
        private Mutex framesMutex = new Mutex();

        /// <summary>
        /// Tell the video processing thread to finish
        /// </summary>
        private bool exit;
        
        public KinectCamera()
        {
            //Add image control to interface
            AddChild(image);

            DataContext = this;
        }

        /// <summary>
        /// Convert from RawBayer* ImageFormats to RGB32
        /// </summary>
        /// <param name="width">Width of image</param>
        /// <param name="height">Height of image</param>
        private void ConvertBayerToRgb32(int width, int height)
        {
            // Demosaic using a basic nearest-neighbor algorithm, operating on groups of four pixels.
            for (int y = 0; y < height; y += 2)
            {
                for (int x = 0; x < width; x += 2)
                {
                    int firstRowOffset = (y * width) + x;
                    int secondRowOffset = firstRowOffset + width;

                    // Cache the Bayer component values.
                    byte red = rawPixelData[firstRowOffset + 1];
                    byte green1 = rawPixelData[firstRowOffset];
                    byte green2 = rawPixelData[secondRowOffset + 1];
                    byte blue = rawPixelData[secondRowOffset];

                    // Adjust offsets for RGB.
                    firstRowOffset *= 4;
                    secondRowOffset *= 4;

                    // Top left
                    pixelData[firstRowOffset] = blue;
                    pixelData[firstRowOffset + 1] = green1;
                    pixelData[firstRowOffset + 2] = red;

                    // Top right
                    pixelData[firstRowOffset + 4] = blue;
                    pixelData[firstRowOffset + 5] = green1;
                    pixelData[firstRowOffset + 6] = red;

                    // Bottom left
                    pixelData[secondRowOffset] = blue;
                    pixelData[secondRowOffset + 1] = green2;
                    pixelData[secondRowOffset + 2] = red;

                    // Bottom right
                    pixelData[secondRowOffset + 4] = blue;
                    pixelData[secondRowOffset + 5] = green2;
                    pixelData[secondRowOffset + 6] = red;
                }
            }
        }

        /// <summary>
        /// Event handler for Kinect sensor's ColorFrameReady event
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void ColorFrameReady(object sender, ColorImageFrameReadyEventArgs e)
        {
            using (ColorImageFrame imageFrame = e.OpenColorImageFrame())
            {
                if (imageFrame != null)
                {
                    // We need to detect if the format has changed.
                    bool haveNewFormat = this.lastImageFormat != imageFrame.Format;
                    bool convertToRgb = false;
                    int bytesPerPixel = imageFrame.BytesPerPixel;

                    //detect if conversion is neccessary
                    if (imageFrame.Format == ColorImageFormat.RawBayerResolution640x480Fps30 ||
                        imageFrame.Format == ColorImageFormat.RawBayerResolution1280x960Fps12)
                    {
                        convertToRgb = true;
                        bytesPerPixel = 4;
                    }

                    //allocate space for caches
                    if (haveNewFormat)
                    {
                        if (convertToRgb)
                        {
                            this.rawPixelData = new byte[imageFrame.PixelDataLength];
                            this.pixelData = new byte[bytesPerPixel * imageFrame.Width * imageFrame.Height];
                        }
                        else
                        {
                            this.pixelData = new byte[imageFrame.PixelDataLength];
                        }
                    }

                    //store image data into cache
                    if (convertToRgb)
                    {
                        imageFrame.CopyPixelDataTo(this.rawPixelData);
                        ConvertBayerToRgb32(imageFrame.Width, imageFrame.Height);
                    }
                    else
                    {
                        imageFrame.CopyPixelDataTo(this.pixelData);
                    }

                    // A WriteableBitmap is a WPF construct that enables resetting the Bits of the image.
                    // This is more efficient than creating a new Bitmap every frame.
                    if (haveNewFormat)
                    {
                        PixelFormat format = PixelFormats.Bgr32;
                        if (imageFrame.Format == ColorImageFormat.InfraredResolution640x480Fps30)
                        {
                            format = PixelFormats.Gray16;
                        }

                        colorBitmap = new WriteableBitmap(
                            imageFrame.Width,
                            imageFrame.Height,
                            96,  // DpiX
                            96,  // DpiY
                            format,
                            null);

                        image.Source = colorBitmap;
                    }

                    colorBitmap.WritePixels(
                        new Int32Rect(0, 0, imageFrame.Width, imageFrame.Height),
                        this.pixelData,
                        imageFrame.Width * bytesPerPixel,
                        0);

                    this.lastImageFormat = imageFrame.Format;
                }
            }

            if(IsRecording)
            {
                using (MemoryStream outStream = new MemoryStream())
                {
                    //This needs to be done on the UI thread, so may as well be done here
                    BitmapEncoder enc = new BmpBitmapEncoder();
                    enc.Frames.Add(BitmapFrame.Create((BitmapSource)colorBitmap));
                    enc.Save(outStream);
                    
                    //add frame data for thread to process
                    framesMutex.WaitOne();
                    frames.Add(outStream.GetBuffer());
                    framesMutex.ReleaseMutex();
                }
            }
        }

        /// <summary>
        /// Thread that will process bitmaps into a video file
        /// </summary>
        private void ProcessVideo()
        {
            //create new video file
            videoFileWriter = new VideoFileWriter();
            videostring = DateTime.Now.Ticks.ToString() + ".mp4";
            videoFileWriter.Open(videostring, 640, 480, 30, VideoCodec.MPEG4);

            //clean up inter-thread buffer (not that there should be anything in it)
            framesMutex.WaitOne();
            frames.Clear();
            framesMutex.ReleaseMutex();

            bool frameAvailable = false;
            byte[] bmap = null;

            while (true)
            {
                //check if there is a frame to process
                framesMutex.WaitOne();
                frameAvailable = frames.Count > 0;
                if (frameAvailable)
                {
                    //and retrieve it
                    bmap = frames[0];
                    frames.RemoveAt(0);
                }
                framesMutex.ReleaseMutex();

                if (frameAvailable)
                {
                    //process the frame
                    Drawing.Bitmap bmp;
                    using (MemoryStream outStream = new MemoryStream(bmap))
                    {
                        bmp = new Drawing.Bitmap(outStream);
                    }

                    videoFileWriter.WriteVideoFrame(bmp);
                }
                else if(exit)
                {
                    //no more frames will be added
                    break;
                }
                else
                {
                    //avoid tying up the cpu processing nothing
                    //it's possible this should be a larger timestep
                    //but there 'may' be performance implications if it's too big
                    Thread.Sleep(10);
                }
            }

            videoFileWriter.Close();
        }

        /// <summary>
        /// Clean up old sensor when it changes
        /// </summary>
        protected override void PreSensorSet()
        {
            if (Sensor == null)
            {
                return;
            }

            //clean up old sensor
            Sensor.ColorStream.Disable();
            Sensor.ColorFrameReady -= ColorFrameReady;

            //clean up control
            image.Source = null;
            lastImageFormat = ColorImageFormat.Undefined;
        }

        /// <summary>
        /// Set up new sensor when it changes
        /// </summary>
        protected override void PostSensorSet()
        {
            if (Sensor == null)
            {
                return;
            }
            Sensor.ColorStream.Enable(ImageFormat);

            //some formats are not supported
            if (ImageFormat != ColorImageFormat.Undefined
                && ImageFormat != ColorImageFormat.RawYuvResolution640x480Fps15)
            {
                Sensor.ColorFrameReady += ColorFrameReady;
            }
        }

        /// <summary>
        /// Whether to save image frame
        /// </summary>
        public bool IsRecording
        {
            get { return (bool)GetValue(IsRecordingProperty); }
            set { SetValue(IsRecordingProperty, value); }
        }
        public static readonly DependencyProperty IsRecordingProperty = DependencyProperty.Register("IsRecording", typeof(bool), typeof(KinectCamera), new PropertyMetadata(false, OnRecordingChanged));

        /// <summary>
        /// Handle changes to the Recording state
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="args">event arguments</param>
        private static void OnRecordingChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            //sender should be this class
            var view = sender as KinectCamera;

            //if not, ignore it
            if (null == view)
            {
                return;
            }

            if (view.IsRecording)
            {
                //start the thread that will process the video frames
                view.exit = false;
                view.ProcessFrameThread = new Thread(() =>
                {
                    view.ProcessVideo();
                });
                view.ProcessFrameThread.Start();
            }
            else
            {
                //tell the processing thread to stop
                view.exit = true;

                //and wait for it to do so
                view.ProcessFrameThread.Join();
            }
        }

        /// <summary>
        /// Image format to tell the Kinect to use
        /// </summary>
        public ColorImageFormat ImageFormat
        {
            get { return (ColorImageFormat)GetValue(ImageFormatProperty); }
            set { SetValue(ImageFormatProperty, value); }
        }
        public static readonly DependencyProperty ImageFormatProperty = DependencyProperty.Register("ImageFormat", typeof(ColorImageFormat), typeof(KinectCamera), new PropertyMetadata(ColorImageFormat.RgbResolution640x480Fps30));
    }
}
