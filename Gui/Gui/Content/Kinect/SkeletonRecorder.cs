﻿using System.IO;
using System.Linq;
using System.Windows;
using Microsoft.Kinect;

namespace Gui
{
    /// <summary>
    /// Control that allows for recording of the skeleton data obtained from the Kinect
    /// </summary>
    public class SkeletonRecorder : KinectControl
    {
        /// <summary>
        /// Cache to store the recorded data
        /// </summary>
        private MemoryStream RecordingStream = null;

        /// <summary>
        /// Helper class to write the data into the cache
        /// </summary>
        private SkeletonReaderWriter.SkeletonWriter Writer = null;

        /// <summary>
        /// State to help identify when recording starts / stops
        /// </summary>
        private bool WasRecording = false;

        public SkeletonRecorder()
        {
            DataContext = this;
        }

        /// <summary>
        /// Get the recorded data
        /// </summary>
        /// <returns>recorded data</returns>
        public byte[] GetAllSkeletonData()
        {
            if (RecordingStream == null)
            {
                return null;
            }
            return RecordingStream.ToArray();
        }

        /// <summary>
        /// Record data
        /// </summary>
        /// <param name="skeletons">skeleton data to process</param>
        public override void ProcessSkeletonFrame(Skeleton[] skeletons)
        {
            //check if recording
            if (!IsRecording)
            {
                //check if recording stopped
                WasRecording = IsRecording;
                return;
            }

            //check if recording started
            if (!WasRecording)
            {
                //clean up previous cache
                if (RecordingStream != null)
                {
                    RecordingStream.Dispose();
                }

                //set up new cache
                RecordingStream = new MemoryStream();
                Writer = new SkeletonReaderWriter.SkeletonWriter(RecordingStream);

                //write recording header
                Writer.WriteHeader();
            }
            WasRecording = IsRecording;

            if (skeletons.Length != 0)
            {
                //get the first tracked skeleton
                Skeleton skel = (from s in skeletons
                                 where s.TrackingState == SkeletonTrackingState.Tracked
                                 select s).FirstOrDefault();
                if (skel != null)
                //foreach (Skeleton skel in skeletons)
                {
                    Writer.WriteFrame(Sensor, skel);
                }
            }
        }

        /// <summary>
        /// Whether to save skeleton frame
        /// </summary>
        public bool IsRecording
        {
            get { return (bool)GetValue(IsRecordingProperty); }
            set { SetValue(IsRecordingProperty, value); }
        }
        public static readonly DependencyProperty IsRecordingProperty = DependencyProperty.Register("IsRecording", typeof(bool), typeof(SkeletonRecorder), new PropertyMetadata(false));
    }
}
