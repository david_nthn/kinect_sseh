﻿using System.Windows.Controls;
using Microsoft.Kinect;

namespace Gui
{
    /// <summary>
    /// Helper to provide a common interface to controls that need access to a Kinect Sensor
    /// </summary>
    public abstract class KinectControl : UserControl
    {
        /// <summary>
        /// Alow derived classes to process skeleton frames
        /// </summary>
        public virtual void ProcessSkeletonFrame(Skeleton[] skeletons) { }

        /// <summary>
        /// Kinect Sensor object
        /// </summary>
        protected KinectSensor Sensor;

        /// <summary>
        /// Set the Sensor, and allow the derived class to handle it
        /// </summary>
        public void SetSensor(KinectSensor sensor)
        {
            PreSensorSet();
            Sensor = sensor;
            PostSensorSet();
        }

        /// <summary>
        /// Called before the sensor is set, to allow derived classes to clean up the old sensor
        /// </summary>
        protected virtual void PreSensorSet() { }

        /// <summary>
        /// Called after the sensor is set, to allow derived classes to set up the new sensor
        /// </summary>
        protected virtual void PostSensorSet() { }
    }
}
