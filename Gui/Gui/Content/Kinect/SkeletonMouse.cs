﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using Microsoft.Kinect;
using Microsoft.Speech.AudioFormat;
using Microsoft.Speech.Recognition;

//#pragma warning disable 0649

namespace Gui
{
    /// <summary>
    /// Control that uses the skeleton data from the Kinect to control the system mouse
    /// </summary>
    class SkeletonMouse : KinectControl
    {
        /// <summary>
        /// Distance *Hand joint must be in front of CenterSholder joint to be tracked (ie. to control the mouse)
        /// </summary>
        private const float TrackThreshold = 0.33f;

        /// <summary>
        /// Was the previously tracked hand the right hand?
        /// Used to handle cases when both hands are being tracked to avoid unexpected changes to which hand is used to control the mouse
        /// </summary>
        private bool PreviousRight = true;
        
        /// <summary>
        /// Mouse click handling
        /// </summary>
        private bool ClickMouseDown = false;

        public SkeletonMouse()
        {
            DataContext = this;
        }

        /// <summary>
        /// Process skeleton data to control the mouse
        /// </summary>
        /// <param name="skeletons">skeleton data to process</param>
        public override void ProcessSkeletonFrame(Skeleton[] skeletons)
        {
            if (skeletons.Length != 0)
            {
                //get the first tracked skeleton
                Skeleton skeleton = (from s in skeletons
                                 where s.TrackingState == SkeletonTrackingState.Tracked
                                 select s).FirstOrDefault();
                if (skeleton != null)
                //foreach (Skeleton skel in skeletons)
                {
                    //default full screen control
                    Point offset = new Point(0, 0);
                    Point size = new Point(SystemParameters.PrimaryScreenWidth, SystemParameters.PrimaryScreenHeight);

                    //limit to bounds if supplied
                    if (Bounds != null)
                    {
                        if (!Bounds.IsVisible)
                        {
                            return;
                        }

                        offset = Bounds.PointToScreen(offset);
                        size = new Point(Bounds.ActualWidth, Bounds.ActualHeight);
                    }

                    Joint jointRight = skeleton.Joints[JointType.HandRight];
                    Joint jointLeft = skeleton.Joints[JointType.HandLeft];
                    Joint jointShoulderCenter = skeleton.Joints[JointType.ShoulderCenter];

                    //handedness agnostic
                    bool useRight = (jointRight.TrackingState == JointTrackingState.Tracked) && ((jointShoulderCenter.Position.Z - jointRight.Position.Z) > TrackThreshold);
                    bool useLeft = (jointLeft.TrackingState == JointTrackingState.Tracked) && ((jointShoulderCenter.Position.Z - jointLeft.Position.Z) > TrackThreshold);

                    //but only one hand can control at a time
                    if (useLeft && useRight)
                    {
                        useRight = PreviousRight;
                        useLeft = !PreviousRight;
                    }

                    if (useLeft || useRight)
                    {
                        // figure out the cursor position based on tracked hand
                        Point cursorPos = ScaleXY(size, jointShoulderCenter, useRight ? jointRight : jointLeft);
                        cursorPos.X += offset.X;
                        cursorPos.Y += offset.Y;

                        SendMouseInput(cursorPos, ClickMouseDown);
                        ClickMouseDown = false;

                        PreviousRight = useRight;
                    }
                }
            }
        }

        #region Voice
        /// <summary>
        /// Clean up old sensor when it changes
        /// </summary>
        protected override void PreSensorSet()
        {
            if(Sensor == null)
            {
                return;
            }
            if (speechEngine != null)
            {
                Sensor.AudioSource.Stop();

                speechEngine.SpeechRecognized -= SpeechRecognized;
                speechEngine.RecognizeAsyncCancel();
                speechEngine = null;
            }
        }

        /// <summary>
        /// Sets up new sensor when it changes
        /// Sets up speech support, which is used for mouse left click support
        /// </summary>
        protected override void PostSensorSet()
        {
            if (Sensor == null)
            {
                return;
            }

            RecognizerInfo ri = GetKinectRecognizer();

            if (null != ri)
            {
                speechEngine = new SpeechRecognitionEngine(ri.Id);

                // Create a grammar from grammar definition XML file.
                using (var memoryStream = new MemoryStream(File.ReadAllBytes("Data/SpeechGrammar.xml")))
                {
                    var g = new Grammar(memoryStream);
                    speechEngine.LoadGrammar(g);
                }

                speechEngine.SpeechRecognized += SpeechRecognized;
                //speechEngine.SpeechRecognitionRejected += SpeechRejected;

                // For long recognition sessions (a few hours or more), it may be beneficial to turn off adaptation of the acoustic model. 
                // This will prevent recognition accuracy from degrading over time.
                ////speechEngine.UpdateRecognizerSetting("AdaptationOn", 0);

                speechEngine.SetInputToAudioStream(Sensor.AudioSource.Start(), new SpeechAudioFormatInfo(EncodingFormat.Pcm, 16000, 16, 1, 32000, 2, null));
                speechEngine.RecognizeAsync(RecognizeMode.Multiple);
            }
        }

        /// <summary>
        /// Speech recognition engine using audio data from Kinect.
        /// </summary>
        private SpeechRecognitionEngine speechEngine;

        /// <summary>
        /// Gets the metadata for the speech recognizer (acoustic model) most suitable to
        /// process audio from Kinect device.
        /// </summary>
        /// <returns>
        /// RecognizerInfo if found, <code>null</code> otherwise.
        /// </returns>
        private static RecognizerInfo GetKinectRecognizer()
        {
            foreach (RecognizerInfo recognizer in SpeechRecognitionEngine.InstalledRecognizers())
            {
                string value;
                recognizer.AdditionalInfo.TryGetValue("Kinect", out value);
                if ("True".Equals(value, StringComparison.OrdinalIgnoreCase) && "en-US".Equals(recognizer.Culture.Name, StringComparison.OrdinalIgnoreCase))
                {
                    return recognizer;
                }
            }

            return null;
        }

        /// <summary>
        /// Handler for recognized speech events.
        /// </summary>
        /// <param name="sender">object sending the event.</param>
        /// <param name="e">event arguments.</param>
        private void SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            // Speech utterance confidence below which we treat speech as if it hadn't been heard
            const double ConfidenceThreshold = 0.3;

            if (e.Result.Confidence >= ConfidenceThreshold)
            {
                switch (e.Result.Semantics.Value.ToString())
                {
                    case "CLICK":
                        ClickMouseDown = true;
                        break;
                }
            }
        }
        #endregion

        #region scalePos
        /// <summary>
        /// Scale the position of a joint to within the bounds provided
        /// </summary>
        /// <param name="scaleTo">Size of area to scale to</param>
        /// <param name="shoulderCenter">Joint to use as frame of reference</param>
        /// <param name="joint">Joint whose position is to be scaled</param>
        /// <returns>scaled point</returns>
        private static Point ScaleXY(Point scaleTo, Joint shoulderCenter, Joint joint)
        {
            //scale position
            //Note:
            //x is scaled such that 0 is half way within bounds
            //y is scaled such that 0 is a quarter from the top of the bounds and the range of movement required is reduced
            Point scaledXY = new Point(((joint.Position.X - shoulderCenter.Position.X) * scaleTo.X) + (scaleTo.X / 2.0), 
                                ((shoulderCenter.Position.Y - joint.Position.Y) * scaleTo.Y * 2.0) + (scaleTo.Y / 4.0));

            //handle out of range conditions caused by scaling
            if (scaledXY.X < 0)
            {
                scaledXY.X = 0;
            }
            else if (scaledXY.X > scaleTo.X - 5)
            {
                scaledXY.X = scaleTo.X - 5;
            }

            if (scaledXY.Y < 0)
            {
                scaledXY.Y = 0;
            }
            else if (scaledXY.Y > scaleTo.Y - 5)
            {
                scaledXY.Y = scaleTo.Y - 5;
            }

            return scaledXY;
        }
        #endregion

        #region SendMouseInput
        /// <summary>
        /// Mouse state structure, as used by system
        /// </summary>
        internal struct MouseInput
        {
            public int X;
            public int Y;
            public uint MouseData;
            public uint Flags;
            public uint Time;
            public IntPtr ExtraInfo;
        }

        /// <summary>
        /// Input state specifying structure, as used by system
        /// </summary>
        internal struct Input
        {
            public int Type;
            public MouseInput MouseInput;
        }

        /// <summary>
        /// Type id of mouse state structure, as used by system
        /// </summary>
        private const int InputMouse = 0;

        /// <summary>
        /// Mouse state flags, as used by system
        /// </summary>
        private const int MouseEventMove = 0x01;
        private const int MouseEventLeftDown = 0x02;
        private const int MouseEventLeftUp = 0x04;
        private const int MouseEventRightDown = 0x08;
        private const int MouseEventRightUp = 0x10;
        private const int MouseEventAbsolute = 0x8000;

        /// <summary>
        /// Cached state of left mouse button, so that only state changes get 'sent'
        /// </summary>
        private static bool lastLeftDown;

        /// <summary>
        /// System function to call that will actually do the mouse position / state changes
        /// </summary>
        [DllImport("user32.dll", SetLastError = true)]
        private static extern uint SendInput(uint numInputs, Input[] inputs, int size);

        /// <summary>
        /// Update the system mouse position / state
        /// </summary>
        /// <param name="pos">Absolute position in screen pixel co-ordinates to move the mouse to</param>
        /// <param name="leftDown">Whether the left mouse button is down</param>
        private static void SendMouseInput(Point pos, bool leftDown)
        {
            Input[] i = new Input[2];

            // move the mouse to the position specified
            i[0] = new Input();
            i[0].Type = InputMouse;
            i[0].MouseInput.X = (int)((pos.X * 65535.0) / SystemParameters.PrimaryScreenWidth);
            i[0].MouseInput.Y = (int)((pos.Y * 65535.0) / SystemParameters.PrimaryScreenHeight);
            i[0].MouseInput.Flags = MouseEventAbsolute | MouseEventMove;

            // determine if we need to send a mouse down or mouse up event
            if (!lastLeftDown && leftDown)
            {
                i[1] = new Input();
                i[1].Type = InputMouse;
                i[1].MouseInput.Flags = MouseEventLeftDown;
            }
            else if (lastLeftDown && !leftDown)
            {
                i[1] = new Input();
                i[1].Type = InputMouse;
                i[1].MouseInput.Flags = MouseEventLeftUp;
            }

            lastLeftDown = leftDown;

            // send it off
            uint result = SendInput(2, i, Marshal.SizeOf(i[0]));
            if (result == 0)
                throw new Win32Exception(Marshal.GetLastWin32Error());
        }
        #endregion

        /// <summary>
        /// Bounds to confine mouse to while being controlled
        /// </summary>
        public FrameworkElement Bounds
        {
            get { return (FrameworkElement)GetValue(BoundsProperty); }
            set { SetValue(BoundsProperty, value); }
        }
        public static readonly DependencyProperty BoundsProperty = DependencyProperty.Register("Bounds", typeof(FrameworkElement), typeof(SkeletonMouse), new PropertyMetadata(null));
    }
}
