﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Threading;
using Microsoft.Kinect;

namespace Gui
{
    /// <summary>
    /// A container to manage KinectControls
    /// </summary>
    [ContentProperty("SubControls")]
    public class KinectSkeletonView : UserControl
    {
        /// <summary>
        /// Visual container of the child controls
        /// </summary>
        private Grid grid = new Grid();

        public KinectSkeletonView()
        {
            //set up a viewbox to handle uniform scaling
            Viewbox viewbox = new Viewbox();
            viewbox.Stretch = Stretch.Uniform;
            viewbox.HorizontalAlignment = HorizontalAlignment.Center;
            viewbox.VerticalAlignment = VerticalAlignment.Center;
            viewbox.Child = grid;
            Content = viewbox;

            //bind events
            Loaded += OnLoaded;
            Dispatcher.ShutdownStarted += OnUnloaded;
        }

        /// <summary>
        /// Execute startup tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            if (grid.Children.Count > 0)
            {
                return;
            }

            //add child controls to the grid, to make thm visual children
            foreach (KinectControl control in SubControls)
            {
                grid.Children.Add(control);
            }
        }

        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void OnUnloaded(object sender, EventArgs e)
        {
            Sensor = null;
            Dispatcher.ShutdownStarted -= OnUnloaded;
        }

        /// <summary>
        /// Event handler for Kinect sensor's SkeletonFrameReady event
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void SkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            Skeleton[] skeletons = new Skeleton[0];

            //get skeleton data
            using (SkeletonFrame skeletonFrame = e.OpenSkeletonFrame())
            {
                if (skeletonFrame != null)
                {
                    skeletons = new Skeleton[skeletonFrame.SkeletonArrayLength];
                    skeletonFrame.CopySkeletonDataTo(skeletons);
                }
            }

            //and let all of the children process it
            foreach (KinectControl control in SubControls)
            {
                control.ProcessSkeletonFrame(skeletons);
            }
        }

        /// <summary>
        /// KinectControls that this class manages
        /// </summary>
        private List<KinectControl> _subControls;
        public List<KinectControl> SubControls
        {
            get
            {
                if (null == _subControls)
                {
                    _subControls = new List<KinectControl>();
                }

                return _subControls;
            }
        }

        /// <summary>
        /// Kinect Sensor to use
        /// </summary>
        public KinectSensor Sensor
        {
            get { return (KinectSensor)GetValue(SensorProperty); }
            set { SetValue(SensorProperty, value); }
        }
        public static readonly DependencyProperty SensorProperty = DependencyProperty.Register("Sensor", typeof(KinectSensor), typeof(KinectSkeletonView), new PropertyMetadata(null, OnSensorChanged));

        /// <summary>
        /// Handle changes to the Kinect Sensor
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="args">event arguments</param>
        private static void OnSensorChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            //sender should be this class
            var view = sender as KinectSkeletonView;

            //if not, ignore it
            if (null == view)
            {
                return;
            }

            //clean up old sensor
            {
                KinectSensor old = (KinectSensor)args.OldValue;
                if (old != null)
                {
                    old.Stop();

                    //cleanup Skeleton Stream
                    old.SkeletonFrameReady -= view.SkeletonFrameReady;
                    old.SkeletonStream.Disable();
                }
            }

            //set up new sensor
            if (view.Sensor != null)
            {
                view.Sensor.Start();

                //set Elevation
                view.Sensor.ElevationAngle = 0;

                //setup Skeleton Stream
                TransformSmoothParameters smoothingParam = new TransformSmoothParameters();
                smoothingParam.Smoothing = 0.5f;
                smoothingParam.Correction = 0.5f;
                smoothingParam.Prediction = 0.5f;
                smoothingParam.JitterRadius = 0.05f;
                smoothingParam.MaxDeviationRadius = 0.04f;

                view.Sensor.SkeletonStream.Enable(smoothingParam);
            }

            //allow child controls to handle changes to the sensor
            foreach (KinectControl control in view.SubControls)
            {
                control.SetSensor(view.Sensor);
            }

            //Add callback _after_ children have been informed of Sensor change
            //to avoid them being called with old sensor information
            if (view.Sensor != null)
            {
                view.Sensor.SkeletonFrameReady += view.SkeletonFrameReady;
            }
        }
    }
}
