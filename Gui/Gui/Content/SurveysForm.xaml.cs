﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Common;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using FirstFloor.ModernUI.Windows.Navigation;

namespace Gui
{
    /// <summary>
    /// Helper class that handles data validation of the data
    /// </summary>
    public class SurveysFormViewModel
        : NotifyPropertyChanged, IDataErrorInfo
    {
        #region Data
        private int Id;

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                if (name != value)
                {
                    name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        private string url;
        public string Url
        {
            get { return url; }
            set
            {
                if (url != value)
                {
                    url = value;
                    OnPropertyChanged("Url");
                }
            }
        }
        #endregion

        #region IDataErrorInfo
        public string Error
        {
            get { return string.Empty; }
        }

        public string this[string columnName]
        {
            get
            {
                if (columnName == "Name")
                {
                    return string.IsNullOrEmpty(Name) ? "Required value" : string.Empty;
                }
                if (columnName == "Url")
                {
                    return string.IsNullOrEmpty(Url) ? "Required value" : string.Empty;
                }
                return string.Empty;
            }
        }
        #endregion

        /// <summary>
        /// Reset the control
        /// </summary>
        public void ClearData()
        {
            Name = null;
            Url = null;
        }

        /// <summary>
        /// Populate the control
        /// </summary>
        /// <param name="userId">Id of the user to populate the control</param>
        public void PopulateData(int id)
        {
            Id = id;

            SurveyHelper helper = new SurveyHelper();

            Name = helper.data.Surveys[id].Name;
            Url = helper.data.Surveys[id].Url;
        }

        /// <summary>
        /// Add a survey and commit changes
        /// </summary>
        public void AddSurvey()
        {
            SurveyHelper helper = new SurveyHelper();

            Survey survey = new Survey(Name, Url);
            helper.data.Surveys.Add(survey);

            helper.Commit();

            ClearData();
        }

        /// <summary>
        /// Edit a survey and commit changes
        /// </summary>
        public int EditSurvey()
        {
            SurveyHelper helper = new SurveyHelper();

            helper.data.Surveys[Id].Name = Name;
            helper.data.Surveys[Id].Url = Url;

            helper.Commit();

            return Id;
        }
    }

    /// <summary>
    /// Interaction logic for SurveysForm.xaml
    /// </summary>
    public partial class SurveysForm : UserControl, IContent
    {
        public SurveysForm()
        {
            InitializeComponent();
        }

        #region IContent
        /// <summary>
        /// Handle sub-pages
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnFragmentNavigation(FragmentNavigationEventArgs e)
        {
            switch (FragmentHelper.GetFragmentName(e.Fragment))
            {
                case "add":
                    {
                        Add.Visibility = Visibility.Visible;
                        Apply.Visibility = Visibility.Collapsed;

                        SurveysFormViewModel vm = Form.DataContext as SurveysFormViewModel;
                        if (vm != null)
                        {
                            vm.ClearData();
                        }

                        break;
                    }
                case "edit":
                    {
                        Add.Visibility = Visibility.Collapsed;
                        Apply.Visibility = Visibility.Visible;

                        string[] subFragments = FragmentHelper.GetFragments(e.Fragment);
                        if (subFragments.Length > 0
                            && !string.IsNullOrWhiteSpace(subFragments[0]))
                        {
                            SurveysFormViewModel vm = Form.DataContext as SurveysFormViewModel;
                            if (vm != null)
                            {
                                vm.PopulateData(int.Parse(subFragments[0]));
                            }
                        }
                        break;
                    }
            }
        }

        /// <summary>
        /// Unload event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedFrom(NavigationEventArgs e)
        {
        }

        /// <summary>
        /// Load event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        /// <summary>
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
        }
        #endregion

        #region EventHandler
        /// <summary>
        /// Add a subject and commit changes
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void AddSurvey(object sender, RoutedEventArgs e)
        {
            SurveysFormViewModel vm = Form.DataContext as SurveysFormViewModel;
            if (vm == null)
            {
                return;
            }

            vm.AddSurvey();

            RefreshPage(null);
        }

        /// <summary>
        /// Edit a subject and commit changes
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void EditSurvey(object sender, RoutedEventArgs e)
        {
            SurveysFormViewModel vm = Form.DataContext as SurveysFormViewModel;
            if (vm == null)
            {
                return;
            }

            int id = vm.EditSurvey();

            RefreshPage(id);
        }

        private void RefreshPage(int? id)
        {
            FrameworkElement window = this;

            while (window != null)
            {
                window = (FrameworkElement)window.Parent;
                if (window as ModernFrame != null)
                {
                    ModernFrame frame = window as ModernFrame;
                    if (frame.Source != null
                        && string.Compare(frame.Source.OriginalString, 0, "/Pages/Surveys.xaml", 0, 19, true) == 0)
                    {
                        string uri = "/Pages/Surveys.xaml#"
                            + FragmentHelper.Separator + GetToggle(frame.Source.OriginalString)
                            + FragmentHelper.Separator;
                        if (id != null)
                        {
                            uri += id;
                        }
                        frame.Source = new Uri(uri, UriKind.Relative);
                        break;
                    }
                }
            }
        }

        private bool GetToggle(string fragment)
        {
            bool toggle = false;

            string[] subFragments = FragmentHelper.GetFragments(fragment);
            if (subFragments.Length > 0
                && !string.IsNullOrWhiteSpace(subFragments[0]))
            {
                toggle = !bool.Parse(subFragments[0]);
            }

            return toggle;
        }
        #endregion
    }
}
