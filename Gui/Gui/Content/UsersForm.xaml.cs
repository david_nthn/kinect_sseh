﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Common;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using FirstFloor.ModernUI.Windows.Navigation;

namespace Gui
{
    /// <summary>
    /// Helper class that handles data validation of the data
    /// </summary>
    public class UsersFormViewModel
        : NotifyPropertyChanged, IDataErrorInfo
    {
        #region Data
        private int? Id;

        private string firstName;
        public string FirstName
        {
            get { return firstName; }
            set
            {
                if (firstName != value)
                {
                    firstName = value;
                    OnPropertyChanged("FirstName");
                }
            }
        }

        private string lastName;
        public string LastName
        {
            get { return lastName; }
            set
            {
                if (lastName != value)
                {
                    lastName = value;
                    OnPropertyChanged("LastName");
                }
            }
        }

        private DateTime? birthday;
        public DateTime? Birthday
        {
            get { return birthday; }
            set
            {
                if (birthday != value)
                {
                    birthday = value;
                    OnPropertyChanged("Birthday");
                }
            }
        }

        private bool? male;
        public bool? Male
        {
            get { return male; }
            set
            {
                if (male != value)
                {
                    male = value;
                    OnPropertyChanged("Male");
                }
            }
        }

        private bool? female;
        public bool? Female
        {
            get { return female; }
            set
            {
                if (female != value)
                {
                    female = value;
                    OnPropertyChanged("Female");
                }
            }
        }

        private int? school;
        public int? School
        {
            get { return school; }
            set
            {
                if (school != value)
                {
                    school = value;
                    OnPropertyChanged("School");
                }
            }
        }

        private int? year;
        public int? Year
        {
            get { return year; }
            set
            {
                if (year != value)
                {
                    year = value;
                    OnPropertyChanged("Year");
                }
            }
        }

        private int? schoolClass;
        public int? SchoolClass
        {
            get { return schoolClass; }
            set
            {
                if (schoolClass != value)
                {
                    schoolClass = value;
                    OnPropertyChanged("SchoolClass");
                }
            }
        }

        private string homeAddress;
        public string HomeAddress
        {
            get { return homeAddress; }
            set
            {
                if (homeAddress != value)
                {
                    homeAddress = value;
                    OnPropertyChanged("HomeAddress");
                }
            }
        }

        private string postcode;
        public string Postcode
        {
            get { return postcode; }
            set
            {
                if (postcode != value)
                {
                    postcode = value;
                    OnPropertyChanged("Postcode");
                }
            }
        }

        private string phone;
        public string Phone
        {
            get { return phone; }
            set
            {
                if (phone != value)
                {
                    phone = value;
                    OnPropertyChanged("Phone");
                }
            }
        }

        private string parentDetails;
        public string ParentDetails
        {
            get { return parentDetails; }
            set
            {
                if (parentDetails != value)
                {
                    parentDetails = value;
                    OnPropertyChanged("ParentDetails");
                }
            }
        }

        private string parentContact;
        public string ParentContact
        {
            get { return parentContact; }
            set
            {
                if (parentContact != value)
                {
                    parentContact = value;
                    OnPropertyChanged("ParentContact");
                }
            }
        }

        private string parentEmail;
        public string ParentEmail
        {
            get { return parentEmail; }
            set
            {
                if (parentEmail != value)
                {
                    parentEmail = value;
                    OnPropertyChanged("ParentEmail");
                }
            }
        }
        #endregion

        #region IDataErrorInfo
        public string Error
        {
            get { return string.Empty; }
        }

        public string this[string columnName]
        {
            get
            {
                if (columnName == "FirstName")
                {
                    return string.IsNullOrEmpty(firstName) ? "Required value" : string.Empty;
                }
                if (columnName == "LastName")
                {
                    return string.IsNullOrEmpty(lastName) ? "Required value" : string.Empty;
                }
                if (columnName == "Birthday")
                {
                    return birthday == null ? "Required value" : string.Empty;
                }
                if (columnName == "School")
                {
                    return school == null ? "Required value" : string.Empty;
                }
                if(columnName == "Year")
                {
                    return year == null ? "Required value" : string.Empty;
                }
                if (columnName == "SchoolClass")
                {
                    return schoolClass == null ? "Required value" : string.Empty;
                }
                return string.Empty;
            }
        }
        #endregion

        /// <summary>
        /// Reset the control
        /// </summary>
        public void ClearData()
        {
            Id = null;
            FirstName = null;
            LastName = null;
            Birthday = DateTime.Now;
            Birthday = null;
            Male = true;
            Female = false;
            School = null;
            HomeAddress = null;
            Postcode = null;
            Phone = null;
            ParentDetails = null;
            ParentContact = null;
            ParentEmail = null;
        }

        /// <summary>
        /// Populate the control
        /// </summary>
        /// <param name="userId">Id of the user to populate the control</param>
        public void PopulateData(int userId)
        {
            DatabaseHelper db = new DatabaseHelper();
            Id = userId;
            object user = db.GetSubject(userId);

            FirstName = DatabaseHelper.GetSubject_FirstName(user);
            LastName = DatabaseHelper.GetSubject_LastName(user);
            Birthday = DatabaseHelper.GetSubject_Birthday(user);
            Male = DatabaseHelper.GetSubject_isMale(user);
            Female = DatabaseHelper.GetSubject_isFemale(user);
            HomeAddress = DatabaseHelper.GetSubject_HomeAddress(user);
            Postcode = DatabaseHelper.GetSubject_Postcode(user);
            Phone = DatabaseHelper.GetSubject_Phone(user);
            ParentDetails = DatabaseHelper.GetSubject_ParentDetails(user);
            ParentContact = DatabaseHelper.GetSubject_ParentContact(user);
            ParentEmail = DatabaseHelper.GetSubject_ParentEmail(user);

            int? schoolClassID = DatabaseHelper.GetSubject_CurrentSchoolClassID(user);
            int? schoolYearID = null;
            if (schoolClassID != null)
            {
                schoolYearID = DatabaseHelper.GetSchoolClass_SchoolYearID(db.GetSchoolClass((int)schoolClassID));
            }
            int? schoolID = null;
            if (schoolYearID != null)
            {
                schoolID = DatabaseHelper.GetSchoolYear_SchoolID(db.GetSchoolYear((int)schoolYearID));
            }
            if(schoolID != null)
            {
                School = schoolID;
                Year = schoolYearID;
                SchoolClass = schoolClassID;
            }
            else
            {
                School = null;
            }
        }

        /// <summary>
        /// Add a subject to the database
        /// </summary>
        public int AddSubject()
        {
            bool isMale = Male != false;
            int id = new DatabaseHelper().AddSubject(FirstName, LastName, Birthday,
                                            isMale,
                                            SchoolClass,
                                            HomeAddress, Postcode, Phone,
                                            ParentDetails, ParentContact, ParentEmail);
            ClearData();
            return id;
        }

        /// <summary>
        /// Edit a subject in the database
        /// </summary>
        public int EditSubject()
        {
            bool isMale = Male != false;
            new DatabaseHelper().EditSubject((int)Id, FirstName, LastName, Birthday,
                                            isMale,
                                            SchoolClass,
                                            HomeAddress, Postcode, Phone,
                                            ParentDetails, ParentContact, ParentEmail);

            return (int)Id;
        }
    }

    /// <summary>
    /// Interaction logic for UsersForm.xaml
    /// </summary>
    public partial class UsersForm : UserControl, IContent
    {
        public UsersForm()
        {
            InitializeComponent();

            //avoid designer complaining about not being able to find database
            if (!DesignerProperties.GetIsInDesignMode(this))
            {
                School_Combo.ItemsSource = new DatabaseHelper().GetSchoolTable();
            }
        }

        #region IContent
        /// <summary>
        /// Handle sub-pages
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnFragmentNavigation(FragmentNavigationEventArgs e)
        {
            Form.Visibility = Visibility.Collapsed;
            Add.Visibility = Visibility.Collapsed;
            Apply.Visibility = Visibility.Collapsed;

            ImportGrid.Visibility = Visibility.Collapsed;

            switch(FragmentHelper.GetFragmentName(e.Fragment))
            {
                case "add":
                    {
                        Form.Visibility = Visibility.Visible;
                        Add.Visibility = Visibility.Visible;

                        UsersFormViewModel vm = Form.DataContext as UsersFormViewModel;
                        if (vm != null)
                        {
                            vm.ClearData();
                        }

                        break;
                    }
                case "edit":
                    {
                        Form.Visibility = Visibility.Visible;
                        Apply.Visibility = Visibility.Visible;

                        string[] subFragments = FragmentHelper.GetFragments(e.Fragment);
                        if (subFragments.Length > 0
                            && !string.IsNullOrWhiteSpace(subFragments[0]))
                        {
                            UsersFormViewModel vm = Form.DataContext as UsersFormViewModel;
                            if (vm != null)
                            {
                                vm.PopulateData(int.Parse(subFragments[0]));
                            }
                        }
                        break;
                    }
                case "import":
                    {
                        ImportGrid.Visibility = Visibility.Visible;
                        break;
                    }
            }
        }

        /// <summary>
        /// Unload event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedFrom(NavigationEventArgs e)
        {
        }

        /// <summary>
        /// Load event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        /// <summary>
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
        }
        #endregion

        #region EventHandler
        /// <summary>
        /// Add a subject to the database
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void AddSubject(object sender, RoutedEventArgs e)
        {
            UsersFormViewModel vm = Form.DataContext as UsersFormViewModel;
            if (vm == null)
            {
                return;
            }

            int id = vm.AddSubject();

            RefreshPage(null);
        }

        /// <summary>
        /// Edit a subject to the database
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void EditSubject(object sender, RoutedEventArgs e)
        {
            UsersFormViewModel vm = Form.DataContext as UsersFormViewModel;
            if (vm == null)
            {
                return;
            }

            int id = vm.EditSubject();

            RefreshPage(id);
        }

        /// <summary>
        /// Import Participants
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Import(object sender, RoutedEventArgs e)
        {
            // Configure open file dialog box
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.Multiselect = false;
            dlg.CheckFileExists = true;
            dlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            dlg.FileName = "Participants"; // Default file name
            dlg.DefaultExt = ".xml"; // Default file extension
            dlg.Filter = "Xml (.xml)|*.xml"; // Filter files by extension 

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results 
            if (result != true)
            {
                return;
            }

            new DatabaseHelper().ImportSubjects(dlg.FileName, Overwrite.IsChecked == true);
            RefreshPage(null);
        }

        /// <summary>
        /// Export Participants
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Export(object sender, RoutedEventArgs e)
        {
            // Configure open file dialog box
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.OverwritePrompt = true;
            dlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            dlg.FileName = "Participants"; // Default file name
            dlg.DefaultExt = ".xml"; // Default file extension
            dlg.Filter = "Xml (.xml)|*.xml"; // Filter files by extension 

            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results 
            if (result != true)
            {
                return;
            }

            new DatabaseHelper().ExportSubjects(dlg.FileName);
        }

        private void RefreshPage(int? id)
        {
            FrameworkElement window = this;

            while (window != null)
            {
                window = (FrameworkElement)window.Parent;
                if (window as ModernFrame != null)
                {
                    ModernFrame frame = window as ModernFrame;
                    if (frame.Source != null
                        && string.Compare(frame.Source.OriginalString, 0, "/Pages/Users.xaml", 0, 17, true) == 0)
                    {
                        string uri = "/Pages/Users.xaml#"
                            + FragmentHelper.Separator + GetToggle(frame.Source.OriginalString)
                            + FragmentHelper.Separator;
                        if (id != null)
                        {
                            uri += id;
                        }
                        frame.Source = new Uri(uri, UriKind.Relative);
                        break;
                    }
                }
            }
        }

        private bool GetToggle(string fragment)
        {
            bool toggle = false;

            string[] subFragments = FragmentHelper.GetFragments(fragment);
            if (subFragments.Length > 0
                && !string.IsNullOrWhiteSpace(subFragments[0]))
            {
                toggle = !bool.Parse(subFragments[0]);
            }

            return toggle;
        }

        private void School_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (School_Combo.SelectedItem == null)
            {
                Year_Combo.ItemsSource = null;
            }
            else
            {
                Year_Combo.ItemsSource = new DatabaseHelper().GetSchoolYearTable(DatabaseHelper.GetSchool_ID(School_Combo.SelectedItem));
            }
            Year_Combo.SelectedItem = null;
        }

        private void Year_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Year_Combo.SelectedItem == null)
            {
                SchoolClass_Combo.ItemsSource = null;
            }
            else
            {
                SchoolClass_Combo.ItemsSource = new DatabaseHelper().GetSchoolClassTable(DatabaseHelper.GetSchoolYear_ID(Year_Combo.SelectedItem));
            }
            SchoolClass_Combo.SelectedItem = null;
        }
        #endregion
    }
}
