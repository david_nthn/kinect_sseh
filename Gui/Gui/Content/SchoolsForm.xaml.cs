﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Common;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using FirstFloor.ModernUI.Windows.Navigation;

namespace Gui
{
    /// <summary>
    /// Helper class that handles data validation of the data
    /// </summary>
    public class SchoolsFormViewModel
        : NotifyPropertyChanged, IDataErrorInfo
    {
        #region Data
        private int? Id;

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                if (name != value)
                {
                    name = value;
                    OnPropertyChanged("Name");
                }
            }
        }
        #endregion

        #region IDataErrorInfo
        public string Error
        {
            get { return string.Empty; }
        }

        public string this[string columnName]
        {
            get
            {
                if(columnName == "Name"
                    && string.IsNullOrWhiteSpace(name))
                {
                    return "Must have a name";
                }
                return string.Empty;
            }
        }
        #endregion

        /// <summary>
        /// Reset the control
        /// </summary>
        public void ClearData()
        {
            Id = null;
            Name = null;
        }

        /// <summary>
        /// Populate the control
        /// </summary>
        /// <param name="id">Id of the row to populate the control</param>
        public void PopulateData(int id)
        {
            Id = id;
            object school = new DatabaseHelper().GetSchool(id);

            Name = DatabaseHelper.GetSchool_Name(school);
        }

        /// <summary>
        /// Add a subject to the database
        /// </summary>
        public int AddSchool()
        {
            int id = new DatabaseHelper().AddSchool(Name);

            ClearData();
            return id;
        }

        /// <summary>
        /// Edit a subject in the database
        /// </summary>
        public int EditSchool()
        {
            new DatabaseHelper().EditSchool((int)Id, Name);

            return (int)Id;
        }
    }

    /// <summary>
    /// Interaction logic for UsersForm.xaml
    /// </summary>
    public partial class SchoolsForm : UserControl, IContent
    {
        public SchoolsForm()
        {
            InitializeComponent();
        }

        #region IContent
        /// <summary>
        /// Handle sub-pages
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnFragmentNavigation(FragmentNavigationEventArgs e)
        {
            switch(FragmentHelper.GetFragmentName(e.Fragment))
            {
                case "add":
                default:
                    {
                        Add.Visibility = Visibility.Visible;
                        Apply.Visibility = Visibility.Collapsed;

                        SchoolsFormViewModel vm = Form.DataContext as SchoolsFormViewModel;
                        if (vm != null)
                        {
                            vm.ClearData();
                        }
                        ChildForm.Source = null;
                        EditSchoolData.SchoolID = null;
                        EditSchoolData.YearID = null;
                        EditSchoolData.ClassID = null;

                        break;
                    }
                case "edit":
                    {
                        Add.Visibility = Visibility.Collapsed;
                        Apply.Visibility = Visibility.Visible;

                        string[] subFragments = FragmentHelper.GetFragments(e.Fragment);
                        if (subFragments.Length > 0
                            && !string.IsNullOrWhiteSpace(subFragments[0]))
                        {
                            SchoolsFormViewModel vm = Form.DataContext as SchoolsFormViewModel;
                            if (vm != null)
                            {
                                vm.PopulateData(int.Parse(subFragments[0]));
                            }

                            EditSchoolData.SchoolID = int.Parse(subFragments[0]);
                            EditSchoolData.YearID = null;
                            EditSchoolData.ClassID = null;

                            string uri = "/Pages/SchoolYears.xaml#"
                                        + FragmentHelper.Separator + GetToggle(ChildForm.Source == null ? null : ChildForm.Source.OriginalString)
                                        + FragmentHelper.Separator;
                            ChildForm.Source = new Uri(uri, UriKind.Relative);
                        }
                        else
                        {
                            ChildForm.Source = null;
                        }
                        break;
                    }
            }
        }

        /// <summary>
        /// Unload event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedFrom(NavigationEventArgs e)
        {
        }

        /// <summary>
        /// Load event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        /// <summary>
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
        }
        #endregion

        #region EventHandler
        /// <summary>
        /// Add a subject to the database
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void AddSchool(object sender, RoutedEventArgs e)
        {
            SchoolsFormViewModel vm = Form.DataContext as SchoolsFormViewModel;
            if (vm == null)
            {
                return;
            }

            int id = vm.AddSchool();

            RefreshPage(id);
        }

        /// <summary>
        /// Edit a subject to the database
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void EditSchool(object sender, RoutedEventArgs e)
        {
            SchoolsFormViewModel vm = Form.DataContext as SchoolsFormViewModel;
            if (vm == null)
            {
                return;
            }

            int id = vm.EditSchool();

            RefreshPage(id);
        }

        private void RefreshPage(int? id)
        {
            FrameworkElement window = this;

            while (window != null)
            {
                window = (FrameworkElement)window.Parent;
                if (window as ModernFrame != null)
                {
                    ModernFrame frame = window as ModernFrame;
                    if (frame.Source != null
                        && string.Compare(frame.Source.OriginalString, 0, "/Pages/Schools.xaml", 0, 19, true) == 0)
                    {
                        string uri = "/Pages/Schools.xaml#"
                            + FragmentHelper.Separator + GetToggle(frame.Source.OriginalString)
                            + FragmentHelper.Separator;
                        if (id != null)
                        {
                            uri += id;
                        }
                        frame.Source = new Uri(uri, UriKind.Relative);
                        break;
                    }
                }
            }
        }

        private bool GetToggle(string fragment)
        {
            bool toggle = false;

            string[] subFragments = FragmentHelper.GetFragments(fragment);
            if (subFragments.Length > 0
                && !string.IsNullOrWhiteSpace(subFragments[0]))
            {
                toggle = !bool.Parse(subFragments[0]);
            }

            return toggle;
        }
        #endregion
    }
}
