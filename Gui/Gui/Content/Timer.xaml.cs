﻿using System;
using System.IO;
using System.Media;
using System.Windows.Threading;
using System.Windows;
using System.Windows.Controls;


namespace Gui
{
    /// <summary>
    /// Interaction logic for Timer.xaml
    /// </summary>
    public partial class Timer : UserControl
    {
        public Timer()
        {
            InitializeComponent();

            _timer = new DispatcherTimer();
            _timer.Interval = TimeSpan.FromSeconds(0.01);
            _timer.Tick += Tick;
        }

        /// <summary>
        /// Timer object
        /// </summary>
        DispatcherTimer _timer = null;

        /// <summary>
        /// Time to start timing from
        /// </summary>
        public DateTime startTime;
        public double timerLimit = double.MaxValue;

        public double GetTime()
        {
            double time = (DateTime.Now - startTime).TotalSeconds;
            if(time > timerLimit)
            {
                time = timerLimit;
            }

            return time;
        }

        /// <summary>
        /// Start timer
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="args">event arguments</param>
        private void OnVisibleChanged(object sender, DependencyPropertyChangedEventArgs args)
        {
            if (Visibility == Visibility.Visible)
            {
                _timer.Start();
            }
            else
            {
                _timer.Stop();
            }
        }

        /// <summary>
        /// Update handler
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Tick(object sender, EventArgs e)
        {
            TimeDisplay.Text = GetTime().ToString("N2");
        }
    }
}
