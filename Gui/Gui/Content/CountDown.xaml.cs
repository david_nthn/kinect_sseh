﻿using System;
using System.IO;
using System.Media;
using System.Threading;
using System.Windows;
using System.Windows.Controls;


namespace Gui
{
    /// <summary>
    /// Interaction logic for CountDown.xaml
    /// </summary>
    public partial class CountDown : UserControl
    {
        /// <summary>
        /// Sound to play to signify count still going
        /// </summary>
        static Stream CountBeep = Application.GetResourceStream(new Uri("pack://application:,,,/Gui;component/Resources/CountBeep.wav")).Stream;

        /// <summary>
        /// Sound to play to signify count has reached the end
        /// </summary>
        static Stream CountEnd = Application.GetResourceStream(new Uri("pack://application:,,,/Gui;component/Resources/CountEnd.wav")).Stream;
                
        public CountDown()
        {
            InitializeComponent();
            DataContext = this;
        }

        /// <summary>
        /// Thread object
        /// </summary>
        Thread thread = null;

        /// <summary>
        /// Play a sound during countdown
        /// </summary>
        SoundPlayer TickBeep = new SoundPlayer(CountBeep);
        SoundPlayer TickEnd = new SoundPlayer(CountEnd);

        /// <summary>
        /// Start countdown
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="args">event arguments</param>
        private void OnVisibleChanged(object sender, DependencyPropertyChangedEventArgs args)
        {
            if (Visibility == Visibility.Visible)
            {
                CurrentCount = CountStart;
                thread = new Thread(DoCounting);
                thread.Start(Silent);
            }
            else if(thread != null)
            {
                thread.Abort();
                thread = null;
            }
        }

        /// <summary>
        /// Actual counting logic
        /// </summary>
        private void DoCounting(object isSilent)
        {
            //initialize
            int localCount = 0;
            bool countRunning = true;

            Dispatcher.Invoke((Action)delegate
            {
                localCount = CurrentCount;
                countRunning = Visibility == Visibility.Visible;
            });

            //do count, exit when:
            //* ended
            //* stopped
            while (localCount > 0
                && countRunning)
            {
                if (!(bool)isSilent)
                {
                    TickBeep.Play();
                }

                Thread.Sleep(1000);

                Dispatcher.Invoke((Action)delegate
                {
                    CurrentCount--;
                    localCount = CurrentCount;
                    countRunning = Visibility == Visibility.Visible;
                });
            }

            //play 'count reached end' sound
            if (countRunning && !(bool)isSilent)
            {
                TickEnd.Play();
            }

            Dispatcher.Invoke((Action)delegate
            {
                Visibility = Visibility.Hidden;
                RaiseOnCountdownCompleteEvent();
            });
        }

        /// <summary>
        /// At what number should the count be started
        /// </summary>
        public int CountStart
        {
            get { return (int)GetValue(CountStartProperty); }
            set { SetValue(CountStartProperty, value); }
        }
        public static readonly DependencyProperty CountStartProperty = DependencyProperty.Register("CountStart", typeof(int), typeof(CountDown), new PropertyMetadata(null));

        /// <summary>
        /// The current value of the count
        /// </summary>
        public int CurrentCount
        {
            get { return (int)GetValue(CurrentCountProperty); }
            set { SetValue(CurrentCountProperty, value); }
        }
        public static readonly DependencyProperty CurrentCountProperty = DependencyProperty.Register("CurrentCount", typeof(int), typeof(CountDown), new PropertyMetadata(null));

        /// <summary>
        /// The current value of the count
        /// </summary>
        public bool Silent
        {
            get { return (bool)GetValue(SilentProperty); }
            set { SetValue(SilentProperty, value); }
        }
        public static readonly DependencyProperty SilentProperty = DependencyProperty.Register("Silent", typeof(bool), typeof(CountDown), new PropertyMetadata(null));

        /// <summary>
        /// Event to detect when the countdown reaches the end
        /// </summary>
        void RaiseOnCountdownCompleteEvent()
        {
            RoutedEventArgs newEventArgs = new RoutedEventArgs(OnCountdownCompleteEvent);
            RaiseEvent(newEventArgs);
        }
        public event RoutedEventHandler OnCountdownComplete
        {
            add { AddHandler(OnCountdownCompleteEvent, value); }
            remove { RemoveHandler(OnCountdownCompleteEvent, value); }
        }
        public static readonly RoutedEvent OnCountdownCompleteEvent = EventManager.RegisterRoutedEvent("OnCountdownComplete", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(CountDown));
    }
}
