﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Common;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using FirstFloor.ModernUI.Windows.Navigation;

namespace Gui
{
    /// <summary>
    /// Helper class that handles data validation of the data
    /// </summary>
    public class SchoolClassFormViewModel
        : NotifyPropertyChanged, IDataErrorInfo
    {
        #region Data
        private int? Id;

        private string schoolClass;
        public string Class
        {
            get { return schoolClass; }
            set
            {
                if (schoolClass != value)
                {
                    schoolClass = value;
                    OnPropertyChanged("Class");
                }
            }
        }
        #endregion

        #region IDataErrorInfo
        public string Error
        {
            get { return string.Empty; }
        }

        public string this[string columnName]
        {
            get
            {
                if(columnName == "Class"
                    && string.IsNullOrWhiteSpace(schoolClass))
                {
                    return "Must have a name";
                }
                return string.Empty;
            }
        }
        #endregion

        /// <summary>
        /// Reset the control
        /// </summary>
        public void ClearData()
        {
            Id = null;
            Class = null;
        }

        /// <summary>
        /// Populate the control
        /// </summary>
        /// <param name="id">Id of the row to populate the control</param>
        public void PopulateData(int id)
        {
            Id = id;
            object schoolClass = new DatabaseHelper().GetSchoolClass(id);

            Class = DatabaseHelper.GetSchoolClass_Class(schoolClass);
        }

        /// <summary>
        /// Add a subject to the database
        /// </summary>
        public int AddSchoolClass()
        {
            int id = new DatabaseHelper().AddSchoolClass((int)EditSchoolData.YearID, Class);

            ClearData();
            return id;
        }

        /// <summary>
        /// Edit a subject in the database
        /// </summary>
        public int EditSchoolClass()
        {
            new DatabaseHelper().EditSchoolClass((int)Id, Class);

            return (int)Id;
        }
    }

    /// <summary>
    /// Interaction logic for UsersForm.xaml
    /// </summary>
    public partial class SchoolClassForm : UserControl, IContent
    {
        public SchoolClassForm()
        {
            InitializeComponent();
        }

        #region IContent
        /// <summary>
        /// Handle sub-pages
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnFragmentNavigation(FragmentNavigationEventArgs e)
        {
            switch(FragmentHelper.GetFragmentName(e.Fragment))
            {
                case "add":
                default:
                    {
                        Add.Visibility = Visibility.Visible;
                        Apply.Visibility = Visibility.Collapsed;

                        SchoolClassFormViewModel vm = Form.DataContext as SchoolClassFormViewModel;
                        if (vm != null)
                        {
                            vm.ClearData();
                        }
                        EditSchoolData.ClassID = null;

                        break;
                    }
                case "edit":
                    {
                        Add.Visibility = Visibility.Collapsed;
                        Apply.Visibility = Visibility.Visible;

                        string[] subFragments = FragmentHelper.GetFragments(e.Fragment);
                        if (subFragments.Length > 0
                            && !string.IsNullOrWhiteSpace(subFragments[0]))
                        {
                            SchoolClassFormViewModel vm = Form.DataContext as SchoolClassFormViewModel;
                            if (vm != null)
                            {
                                vm.PopulateData(int.Parse(subFragments[0]));
                            }

                            EditSchoolData.ClassID = int.Parse(subFragments[0]);
                        }
                        break;
                    }
            }
        }

        /// <summary>
        /// Unload event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedFrom(NavigationEventArgs e)
        {
        }

        /// <summary>
        /// Load event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        /// <summary>
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
        }
        #endregion

        #region EventHandler
        /// <summary>
        /// Add a subject to the database
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void AddSchoolClass(object sender, RoutedEventArgs e)
        {
            SchoolClassFormViewModel vm = Form.DataContext as SchoolClassFormViewModel;
            if (vm == null)
            {
                return;
            }

            int id = vm.AddSchoolClass();

            RefreshPage(null);
        }

        /// <summary>
        /// Edit a subject to the database
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void EditSchoolClass(object sender, RoutedEventArgs e)
        {
            SchoolClassFormViewModel vm = Form.DataContext as SchoolClassFormViewModel;
            if (vm == null)
            {
                return;
            }

            int id = vm.EditSchoolClass();

            RefreshPage(id);
        }

        private void RefreshPage(int? id)
        {
            FrameworkElement window = this;

            while (window != null)
            {
                window = (FrameworkElement)window.Parent;
                if (window as ModernFrame != null)
                {
                    ModernFrame frame = window as ModernFrame;
                    if (frame.Source != null
                        && string.Compare(frame.Source.OriginalString, 0, "/Pages/SchoolClass.xaml", 0, 23, true) == 0)
                    {
                        string uri = "/Pages/SchoolClass.xaml#"
                            + FragmentHelper.Separator + GetToggle(frame.Source.OriginalString)
                            + FragmentHelper.Separator;
                        if (id != null)
                        {
                            uri += id;
                        }
                        frame.Source = new Uri(uri, UriKind.Relative);
                        break;
                    }
                }
            }
        }

        private bool GetToggle(string fragment)
        {
            bool toggle = false;

            string[] subFragments = FragmentHelper.GetFragments(fragment);
            if (subFragments.Length > 0
                && !string.IsNullOrWhiteSpace(subFragments[0]))
            {
                toggle = !bool.Parse(subFragments[0]);
            }

            return toggle;
        }
        #endregion
    }
}
