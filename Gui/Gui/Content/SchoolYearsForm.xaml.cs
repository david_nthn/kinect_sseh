﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Common;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using FirstFloor.ModernUI.Windows.Navigation;

namespace Gui
{
    /// <summary>
    /// Helper class that handles data validation of the data
    /// </summary>
    public class SchoolYearsFormViewModel
        : NotifyPropertyChanged, IDataErrorInfo
    {
        #region Data
        private int? Id;

        private string year;
        public string Year
        {
            get { return year; }
            set
            {
                if (year != value)
                {
                    year = value;
                    OnPropertyChanged("Year");
                }
            }
        }
        #endregion

        #region IDataErrorInfo
        public string Error
        {
            get { return string.Empty; }
        }

        public string this[string columnName]
        {
            get
            {
                if (columnName == "Year")
                {
                    if (string.IsNullOrWhiteSpace(year))
                    {
                        return "Must have a value";
                    }
                }
                return string.Empty;
            }
        }
        #endregion

        /// <summary>
        /// Reset the control
        /// </summary>
        public void ClearData()
        {
            Id = null;
            Year = null;
        }

        /// <summary>
        /// Populate the control
        /// </summary>
        /// <param name="id">Id of the row to populate the control</param>
        public void PopulateData(int id)
        {
            Id = id;
            object schoolYear = new DatabaseHelper().GetSchoolYear(id);

            Year = DatabaseHelper.GetSchoolYear_Year(schoolYear);
        }

        /// <summary>
        /// Add a subject to the database
        /// </summary>
        public int AddSchoolYear()
        {
            int id = new DatabaseHelper().AddSchoolYear((int)EditSchoolData.SchoolID, Year);

            ClearData();
            return id;
        }

        /// <summary>
        /// Edit a subject in the database
        /// </summary>
        public int EditSchoolYear()
        {
            new DatabaseHelper().EditSchoolYear((int)Id, Year);

            return (int)Id;
        }
    }

    /// <summary>
    /// Interaction logic for UsersForm.xaml
    /// </summary>
    public partial class SchoolYearsForm : UserControl, IContent
    {
        public SchoolYearsForm()
        {
            InitializeComponent();
        }

        #region IContent
        /// <summary>
        /// Handle sub-pages
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnFragmentNavigation(FragmentNavigationEventArgs e)
        {
            switch(FragmentHelper.GetFragmentName(e.Fragment))
            {
                case "add":
                default:
                    {
                        Add.Visibility = Visibility.Visible;
                        Apply.Visibility = Visibility.Collapsed;

                        SchoolYearsFormViewModel vm = Form.DataContext as SchoolYearsFormViewModel;
                        if (vm != null)
                        {
                            vm.ClearData();
                        }
                        ChildForm.Source = null;
                        EditSchoolData.YearID = null;
                        EditSchoolData.ClassID = null;

                        break;
                    }
                case "edit":
                    {
                        Add.Visibility = Visibility.Collapsed;
                        Apply.Visibility = Visibility.Visible;

                        string[] subFragments = FragmentHelper.GetFragments(e.Fragment);
                        if (subFragments.Length > 0
                            && !string.IsNullOrWhiteSpace(subFragments[0]))
                        {
                            SchoolYearsFormViewModel vm = Form.DataContext as SchoolYearsFormViewModel;
                            if (vm != null)
                            {
                                vm.PopulateData(int.Parse(subFragments[0]));
                            }

                            EditSchoolData.YearID = int.Parse(subFragments[0]);
                            EditSchoolData.ClassID = null;

                            string uri = "/Pages/SchoolClass.xaml#"
                                + FragmentHelper.Separator + GetToggle(ChildForm.Source == null ? null : ChildForm.Source.OriginalString)
                                        + FragmentHelper.Separator;
                            ChildForm.Source = new Uri(uri, UriKind.Relative);
                        }
                        else
                        {
                            ChildForm.Source = null;
                        }
                        break;
                    }
            }
        }

        /// <summary>
        /// Unload event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedFrom(NavigationEventArgs e)
        {
        }

        /// <summary>
        /// Load event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        /// <summary>
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
        }
        #endregion

        #region EventHandler
        /// <summary>
        /// Add a subject to the database
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void AddSchoolYear(object sender, RoutedEventArgs e)
        {
            SchoolYearsFormViewModel vm = Form.DataContext as SchoolYearsFormViewModel;
            if (vm == null)
            {
                return;
            }

            int id = vm.AddSchoolYear();

            RefreshPage(id);
        }

        /// <summary>
        /// Edit a subject to the database
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void EditSchoolYear(object sender, RoutedEventArgs e)
        {
            SchoolYearsFormViewModel vm = Form.DataContext as SchoolYearsFormViewModel;
            if (vm == null)
            {
                return;
            }

            int id = vm.EditSchoolYear();

            RefreshPage(id);
        }

        private void RefreshPage(int? id)
        {
            FrameworkElement window = this;

            while (window != null)
            {
                window = (FrameworkElement)window.Parent;
                if (window as ModernFrame != null)
                {
                    ModernFrame frame = window as ModernFrame;
                    if (frame.Source != null
                        && string.Compare(frame.Source.OriginalString, 0, "/Pages/SchoolYears.xaml", 0, 23, true) == 0)
                    {
                        string uri = "/Pages/SchoolYears.xaml#"
                            + FragmentHelper.Separator + GetToggle(frame.Source.OriginalString)
                            + FragmentHelper.Separator;
                        if(id != null)
                        {
                            uri += id;
                        }
                        frame.Source = new Uri(uri, UriKind.Relative);
                        break;
                    }
                }
            }
        }

        private bool GetToggle(string fragment)
        {
            bool toggle = false;

            string[] subFragments = FragmentHelper.GetFragments(fragment);
            if (subFragments.Length > 0
                && !string.IsNullOrWhiteSpace(subFragments[0]))
            {
                toggle = !bool.Parse(subFragments[0]);
            }

            return toggle;
        }
        #endregion
    }
}
