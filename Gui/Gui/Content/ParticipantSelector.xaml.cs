﻿using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Common;

namespace Gui
{
    /// <summary>
    /// Interaction logic for ParticipantSelector.xaml
    /// </summary>
    public partial class ParticipantSelector : ComboBox
    {
        public ParticipantSelector()
        {
            InitializeComponent();

            Loaded += OnLoaded;
        }

        /// <summary>
        /// Update the contents
        /// </summary>
        public void RefreshDataContext()
        {
            DataContext = new DatabaseHelper().GetSubjectTable();

            ItemsSource = (DataView)DataContext;

            if (lv != null)
            {
                lv.ItemsSource = ItemsSource;

                lv.SelectedIndex = -1;
            }
        }

        /// <summary>
        /// Sanitize user input for use in RowFilter
        /// </summary>
        private string EscapeLikeValue(string value)
        {
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < value.Length; i++)
            {
                char c = value[i];
                if(c == '*' || c == '%' || c == '[' || c == ']')
                {
                    sb.Append("[").Append(c).Append("]");
                }
                else if(c == '\'')
                {
                    sb.Append("''");
                }
                else
                {
                    sb.Append(c);
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// Update the contents of the ListView, based on the contents of the TextBox
        /// </summary>
        private void tb_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(lv == null)
            {
                return;
            }

            DataView view = (DataView)DataContext;

            string text = EscapeLikeValue(((TextBox)e.Source).Text.Trim());
            if(string.IsNullOrWhiteSpace(text))
            {
                view.RowFilter = string.Empty;
            }
            else
            {
                view.RowFilter = string.Format("FirstName + ' ' + LastName Like '*{0}*'", text);
            }
        }

        /// <summary>
        /// ListView contained within the ComboBox
        /// </summary>
        private ExtendedListView lv = null;

        /// <summary>
        /// TextBox contained within the ComboBox
        /// </summary>
        private TextBox tb = null;
        
        /// <summary>
        /// Setup the contents of the ComboBoxs dropdown
        /// </summary>
        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            FrameworkElement itemPresenter = (FrameworkElement)Template.FindName("ItemsPresenter", this);
            if (itemPresenter == null)
            {
                return;
            }

            Grid parent = itemPresenter.Parent as Grid;
            if (parent == null)
            {
                return;
            }

            Grid grid = new Grid();

            RowDefinition rd = new RowDefinition();
            rd.Height = GridLength.Auto;
            grid.RowDefinitions.Add(rd);
            rd = new RowDefinition();
            rd.Height = GridLength.Auto;
            grid.RowDefinitions.Add(rd);

            //Replace the ComboBox ItemPresenter with our own controls
            parent.Children.Remove(itemPresenter);
            parent.Children.Add(grid);

            //Add filter TextBox
            tb = new TextBox();
            Grid.SetRow(tb, 0);
            grid.Children.Add(tb);

            tb.Margin = new Thickness(5);
            tb.TextChanged += tb_TextChanged;

            //Add ListView to present data
            lv = new ExtendedListView();
            Grid.SetRow(lv, 1);
            grid.Children.Add(lv);

            lv.SelectionMode = SelectionMode.Single;
            lv.HorizontalAlignment = HorizontalAlignment.Stretch;
            lv.Height = 100;
            lv.Name = "DropDownListView";
            lv.SelectionChanged += OnSelectionChanged_Participants;

            RefreshDataContext();

            GridView gv = new GridView();

            {
                GridViewColumn gvc = new GridViewColumn();
                gvc.DisplayMemberBinding = new Binding("FirstName");
                GridViewColumnHeader gvch = new GridViewColumnHeader();
                gvch.Content = "First Name";
                gvc.Header = gvch;
                gv.Columns.Add(gvc);
            }

            {
                GridViewColumn gvc = new GridViewColumn();
                gvc.DisplayMemberBinding = new Binding("LastName");
                GridViewColumnHeader gvch = new GridViewColumnHeader();
                gvch.Content = "Last Name";
                gvc.Header = gvch;
                gv.Columns.Add(gvc);
            }

            lv.View = gv;

            lv.SelectedIndex = -1;
        }
        
        #region EventHandlers
        /// <summary>
        /// Set the selected item of the combobox based on the selected item of the contained listview
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void OnSelectionChanged_Participants(object sender, RoutedEventArgs e)
        {
            SelectedItem = (e.OriginalSource as ListView).SelectedItem;
            if (SelectedItem != null)
            {
                IsDropDownOpen = false;
            }
        }
        #endregion
    }
}
