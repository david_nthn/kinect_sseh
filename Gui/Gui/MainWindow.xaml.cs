﻿using System.Windows;
using System.Windows.Controls;
using FirstFloor.ModernUI.Windows.Controls;

namespace Gui
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : ModernWindow
    {
        /// <summary>
        /// Cache for menu object
        /// </summary>
        public ModernMenu Menu;

        public MainWindow()
        {
            InitializeComponent();
            
            SettingsAppearanceViewModel settings = new SettingsAppearanceViewModel();
            settings.SetThemeAndColor(ApplicationSettings.Default.SelectedThemeDisplayName,
                  ApplicationSettings.Default.SelectedThemeSource,
                  ApplicationSettings.Default.SelectedAccentColor,
                  ApplicationSettings.Default.SelectedFontSize);

            Loaded += OnLoaded;
        }

        /// <summary>
        /// Load event handling
        /// </summary>
        public void OnLoaded(object sender, RoutedEventArgs e)
        {
            //find menu and back button
            Grid grid = (Grid)Template.FindName("LayoutRoot", this);
            foreach(UIElement item in grid.Children)
            {
                if(item as ModernMenu != null)
                {
                    Menu = item as ModernMenu;
                }
                if (item as ModernButton != null)
                {
                    ModernButton back = item as ModernButton;
                    back.Visibility = Visibility.Collapsed;
                }
            }
        }
    }
}
