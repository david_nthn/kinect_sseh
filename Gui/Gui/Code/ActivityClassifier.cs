﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Accord.MachineLearning.VectorMachines;
using Microsoft.Kinect;
using SkeletonReaderWriter;

namespace Gui
{

    public enum DimensionType { X = 0, Y = 1, Z = 2 } 

    public class ActivityClassifier
    {
        /// <summary>
        /// Pass skeleton data through the classifier engine
        /// </summary>
        /// <param name="SkeletonStream">Skeletal data to use in classification</param>
        /// <param name="Activity">Activity that is being classified</param>
        /// <returns>Rating of how close the 'SkeletonStream' resembles 'Activity'</returns>
        public static int Classify(byte[] SkeletonStream, string Activity)
        {
            int result = 0;

            SkeletonReader streamReader = new SkeletonReader(SkeletonStream);
            
            if(streamReader.NumberOfSavedFrames() == 0)
            {
                //not enough frames to make the evaluation
                return result;
            }

            double[] featurevector = extractFMSFeature(SkelReader2ListFrame(streamReader));

            string fileName = "./Data/"+ Activity + "SVMMachine.dat";
            System.IO.Stream fileStream = new FileStream(fileName, FileMode.Open);
            KernelSupportVectorMachine machine = KernelSupportVectorMachine.Load(fileStream);
            result = (int)Math.Floor(machine.Compute(featurevector));
            fileStream.Close();  
            return result;                       
        }

        private static List<Frame> SkelReader2ListFrame(SkeletonReader streamReader)
        {
            uint framecount = streamReader.NumberOfSavedFrames();
            List<Frame> frameList = new List<Frame>();

            for (uint frame = 0; frame < framecount; frame++)
            {
                Frame recordingFrame = new Frame();
                recordingFrame.features = new Dictionary<JointType, double[]>();
                recordingFrame.timestamp = streamReader.GetFrame(frame).timestamp;

                foreach (JointType jntType in Enum.GetValues(typeof(JointType)))
                {
                    SkeletonReader.SkeletonFrameJoint skelJoint = Array.Find(streamReader.GetFrame(frame).joints, j => j.type == jntType);
                    double[] point = new double[] { (double)skelJoint.X, (double)skelJoint.Y, (double)skelJoint.Z };
                    recordingFrame.features.Add(jntType, point);
                }

                frameList.Add(recordingFrame);
            }

            return frameList;
        }

        private static double[] extractFMSFeature(List<Frame> recording)
        {
            List<double> features = new List<double>();

            foreach (JointType jntType in Enum.GetValues(typeof(JointType)))
            {
                // Triplet for Standard Deviation of each Dimension
                double[] jntFeatures = new double[3];

                // IEnumerabl of double[] consisting of X,Y,Z position of jntType
                IEnumerable<double[]> trajectory = recording.Select(x => x.features[jntType]);

                foreach (DimensionType dimType in Enum.GetValues(typeof(DimensionType)))
                {
                    IEnumerable<double> dimarray = trajectory.Select(x => x[(int)dimType]);
                    double sigma = GetSigma(dimarray);

                    jntFeatures[(int)dimType] = sigma;
                }
                features.AddRange(jntFeatures);
            }
            return features.ToArray();
        }

        private static double GetSigma(IEnumerable<double> numarray)
        {
            // Average
            double mu = numarray.Average();
            // Sum of Squared Differences
            double SSD = numarray.Select(x => (x - mu) * (x - mu)).Sum();
            // Standard Deviation
            double sigma = Math.Sqrt(SSD / numarray.Count());

            return sigma;
        }

        public class Frame
        {
            public long timestamp { get; set; }
            public Dictionary<JointType, double[]> features { get; set; }
        }
    }
}
