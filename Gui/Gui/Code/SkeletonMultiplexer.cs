﻿using System;
using System.Collections.Generic;
using Accord.Math;
using Accord.Math.Decompositions;
using Microsoft.Kinect;
using SkeletonReaderWriter;

namespace Gui
{
    public static class SkeletonMultiplexer
    {


        static public byte[] Multiplex(byte[] LeftCalibration, byte[] LeftStream, byte[] RightCalibration, byte[] RightStream)
        {
            SkeletonReader streamReaderLeft  = new SkeletonReader(LeftStream);
            SkeletonReader streamReaderRight = new SkeletonReader(RightStream);
          
            uint leftCount  = streamReaderLeft.NumberOfSavedFrames();
            uint rightCount = streamReaderRight.NumberOfSavedFrames();

            List<double[,]> leftJointStream = new List<double[,]>();
            List<double[,]> rightJointStream = new List<double[,]>();

            List<float[,]> leftOrientStream = new List<float[,]>();
            List<float[,]> rightOrientStream = new List<float[,]>();

            List<long> timestampLeft = new List<long>();
            List<long> timestampRight = new List<long>();

            List<byte[]> jntStartEnd = new List<byte[]>();

            for (uint frame = 0; frame < leftCount; frame++)
            {
                SkeletonReader.SkeletonFrameData skelframe = streamReaderLeft.GetFrame(frame);
                timestampLeft.Add(skelframe.timestamp);
                double[,] positions = new double[skelframe.joints.Length, 3];
                float[,] orientations = new float[skelframe.orientations.Length, 4];

                foreach(JointType jntType in Enum.GetValues(typeof(JointType)))
                {
                    SkeletonReader.SkeletonFrameJoint skelJoint = Array.Find(skelframe.joints, j => j.type == jntType);
                    SkeletonReader.SkeletonFrameOrientation skelOrient = Array.Find(skelframe.orientations, j => j.endJoint == jntType);
                    positions[(int)jntType, 0] = skelJoint.X;
                    positions[(int)jntType, 1] = skelJoint.Y;
                    positions[(int)jntType, 2] = skelJoint.Z;
                    orientations[(int)jntType, 0] = skelOrient.X;
                    orientations[(int)jntType, 1] = skelOrient.Y;
                    orientations[(int)jntType, 2] = skelOrient.Z;
                    orientations[(int)jntType, 3] = skelOrient.W;

                    byte[] strtend = new byte[] {(byte)skelOrient.startJoint,(byte)skelOrient.endJoint };
                    jntStartEnd.Add(strtend);
                }

                leftJointStream.Add(positions);
                leftOrientStream.Add(orientations);
            }

            for (uint frame = 0; frame < rightCount; frame++)
            {
                SkeletonReader.SkeletonFrameData skelframe = streamReaderRight.GetFrame(frame);
                double[,] positions = new double[skelframe.joints.Length, 3];
                float[,] orientations = new float[skelframe.orientations.Length, 4];
                timestampRight.Add(skelframe.timestamp);

                foreach (JointType jntType in Enum.GetValues(typeof(JointType)))
                {
                    SkeletonReader.SkeletonFrameJoint skelJoint = Array.Find(skelframe.joints, j => j.type == jntType);
                    SkeletonReader.SkeletonFrameOrientation skelOrient = Array.Find(skelframe.orientations, j => j.endJoint == jntType);
                    positions[(int)jntType, 0] = skelJoint.X;
                    positions[(int)jntType, 1] = skelJoint.Y;
                    positions[(int)jntType, 2] = skelJoint.Z;
                    orientations[(int)jntType, 0] = skelOrient.X;
                    orientations[(int)jntType, 1] = skelOrient.Y;
                    orientations[(int)jntType, 2] = skelOrient.Z;
                    orientations[(int)jntType, 3] = skelOrient.W;
                }

                rightJointStream.Add(positions);
                rightOrientStream.Add(orientations);
            }

            List<double[,]> CombinedStream = CombineStream(leftJointStream, rightJointStream);
            List<float[,]> orientStream = (leftJointStream.Count > rightJointStream.Count) ? leftOrientStream: rightOrientStream;
            List<long> timestamp = (leftJointStream.Count > rightJointStream.Count) ? timestampLeft : timestampRight;

            //TODO: Need to work out how to use version number to Write to byte stream
            // Use V1 Form

            List<byte> bytes = new List<byte>();
            //Header Frame
            bytes.Add(1);
            bytes.Add(20);
            bytes.Add(20);

            // Skeleton Frames
            for (int frame = 0; frame < CombinedStream.Count; frame++)
            {
                bytes.AddRange(BitConverter.GetBytes(timestamp[frame]));
                for (byte j = 0; j < 20; j++)
                {
                    bytes.Add(j);
                   
                    double[,] position = CombinedStream[frame];
                    bytes.AddRange(BitConverter.GetBytes(Convert.ToSingle(position[j, 0])));
                    bytes.AddRange(BitConverter.GetBytes(Convert.ToSingle(position[j, 1])));
                    bytes.AddRange(BitConverter.GetBytes(Convert.ToSingle(position[j, 2])));
                    bytes.Add(0);
                }

                for (byte o = 0; o < 20; o++)
                {
                    byte[] strtend = jntStartEnd[o];
                    bytes.Add(strtend[0]);
                    bytes.Add(strtend[1]);

                    float[,] orientations = orientStream[frame];
                    bytes.AddRange(BitConverter.GetBytes(orientations[o, 0]));
                    bytes.AddRange(BitConverter.GetBytes(orientations[o, 1]));
                    bytes.AddRange(BitConverter.GetBytes(orientations[o, 2]));
                    bytes.AddRange(BitConverter.GetBytes(orientations[o, 3]));
                }
            }

            return bytes.ToArray();
        }

        static private List<double[,]> CombineStream(List<double[,]> JointStreamA, List<double[,]> JointStreamB)
        {
            //TODO: align two streams according to timestamp

            // Take the first n frames of each stream where n is the length of the shorter of the two streams.
            int length = (JointStreamA.Count < JointStreamB.Count) ? JointStreamA.Count : JointStreamB.Count;

            List<double[,]> combinedStream = new List<double[,]>(length);

            for (int frame = 0; frame < length; frame++)
            {
                double[,] R = KabschAlgorithm.GetRotationMatrix(JointStreamA[frame], JointStreamB[frame]);
                double[] t = KabschAlgorithm.GetTranslationVector(JointStreamA[frame], JointStreamB[frame], R);

                // 20x3 matrix
                double[,] Qdash = Matrix.Multiply(JointStreamB[frame], R);
                // 20x3 matrix. Stack translation vector to subtract from each point. 
                double[,] stackT = MatrixExtra.repmat(t, 20);
                // Takes the average of the two streams. 
                combinedStream.Add(Matrix.Divide(Matrix.Add(JointStreamA[frame],Matrix.Subtract(Qdash, stackT)),2));
            }

            return combinedStream;
        }
       
    }

    /// <summary>
    /// Implementation of the Kabsch Algorithm to find the optimal rigid rotatation matrix R 
    /// and translation vector t for between coordinate system A and coordinate system B.
    /// This is achieved by minimizing the root mean squared standard deviation (RMSD)
    /// between the set of points, which is solved by taking the SVD of the covariance matrix.
    /// 
    /// [see: http://en.wikipedia.org/wiki/Kabsch_algorithm]
    /// 
    /// Requires three steps
    ///     1. Translation
    ///     2. Covariance Matrix computation
    ///     3. Optimal Rotation Matrix computation
    ///     
    /// </summary>
    public class KabschAlgorithm
    {
        private static readonly int DIMENSION_SIZE = 3;
        private static readonly int NUMBER_OF_POINTS = 20;


        public static double[,] GetRotationMatrix(double[,] matrixP, double[,] matrixQ)
        {
            double[,] rotation = new double[DIMENSION_SIZE, DIMENSION_SIZE];

            //Step 1.
            double[] centroidP = GetCentroid(matrixP);
            double[] centroidQ = GetCentroid(matrixQ);

            double[,] Pdash = GetTranslation(matrixP, centroidP);
            double[,] Qdash = GetTranslation(matrixQ, centroidQ);

            // Step 2.
            double[,] covariance = GetCovarianceMatrix(Pdash, Qdash);

            //Step 3. 
            // covariance = V S W^T
            SingularValueDecomposition svd = new SingularValueDecomposition(covariance);

            double[,] V = svd.LeftSingularVectors;
            double[,] W = svd.RightSingularVectors;

            double[,] I = Matrix.Identity(DIMENSION_SIZE);

            if (Matrix.Determinant(Matrix.MultiplyByTranspose(W, V)) < 0)
            {
                I[DIMENSION_SIZE-1, DIMENSION_SIZE-1] = -1;
            }

            rotation = Matrix.MultiplyByTranspose(Matrix.Multiply(W, I), V);

            return rotation;
        }


        public static double[] GetTranslationVector(double[,] matrixP, double[,] matrixQ, double[,] rotation)
        {
            double[] translation = new double[DIMENSION_SIZE];

            double[] centroidP = GetCentroid(matrixP);
            double[] centroidQ = GetCentroid(matrixQ);

            translation = Matrix.Subtract(centroidQ, Matrix.Multiply(rotation, centroidP));

            return translation;
        }

        /// <summary>
        /// Step 1.Need to ensure that origins of each coordinate system cooincides. 
        /// This is done by subtracting from the point coordinates the coordinates 
        /// of the respective centroid.
        /// </summary>
        /// <param name="coordinates"> A NxD matrix of points </param>
        /// <returns></returns>
        private static double[,] GetTranslation(double[,] coordinates, double[] centroid)
        {
            double[,] translated = new double[NUMBER_OF_POINTS, DIMENSION_SIZE];

            double xbar = centroid[0];
            double ybar = centroid[1];
            double zbar = centroid[2];

            for (int point = 0; point < NUMBER_OF_POINTS; point++)
            {
                translated[point, 0] = coordinates[point, 0] - xbar;
                translated[point, 1] = coordinates[point, 1] - ybar;
                translated[point, 2] = coordinates[point, 2] - zbar;
            }

            return translated;
        }

        private static double[] GetCentroid(double[,] coordinates)
        {
            double xsum = 0, ysum = 0, zsum = 0;
            for (int point = 0; point < NUMBER_OF_POINTS; point++)
            {
                xsum = xsum + coordinates[point, 0];
                ysum = ysum + coordinates[point, 1];
                zsum = zsum + coordinates[point, 2];
            }

            double xbar = xsum / NUMBER_OF_POINTS;
            double ybar = ysum / NUMBER_OF_POINTS;
            double zbar = zsum / NUMBER_OF_POINTS;

            double[] centroid = new double[] { xbar, ybar, zbar };
            return centroid;
        }


        private static double[,] GetCovarianceMatrix(double[,] translatedA, double[,] translatedB)
        {
            double[,] covariance = new double[DIMENSION_SIZE, DIMENSION_SIZE];

            // covariance = A'*B
            covariance = Matrix.TransposeAndMultiply(translatedA, translatedB);

            return covariance;
        }

    }

    public static class MatrixExtra
    {
        // Stack row vector n times
        public static double[,] repmat(double[] v, int n)
        {
            int col = v.Length;

            double[,] repeatedmat = new double[n, col];

            for (int i = 0; i < col; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    repeatedmat[j, i] = v[i];
                }
            }

            return repeatedmat;
        }
    }
}
