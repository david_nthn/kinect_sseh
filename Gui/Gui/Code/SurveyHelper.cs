﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml.Serialization;

namespace Gui
{
    /// <summary>
    /// Survey entry for XML
    /// </summary>
    [XmlType("Survey")]
    public class Survey
    {
        [XmlAttribute("name", DataType = "string")]
        public string Name { get; set; }

        [XmlAttribute("url", DataType = "string")]
        public string Url { get; set; }

        public Survey() { }

        public Survey(string name, string url)
        {
            Name = name;
            Url = url;
        }
    }

    /// <summary>
    /// Root class for XML
    /// </summary>
    [XmlRoot("Root")]
    [XmlInclude(typeof(Survey))] // include type class Survey
    public class SurveysList
    {
        [XmlArray("Surveys")]
        [XmlArrayItem("Survey")]
        public ObservableCollection<Survey> Surveys = new ObservableCollection<Survey>();

        public SurveysList() { }
    }

    /// <summary>
    /// Helper class to handle reading from / writing to XML
    /// </summary>
    public class SurveyHelper
    {
        /// <summary>
        /// Cached data
        /// </summary>
        public SurveysList data;

        public SurveyHelper()
        {
            Refresh();
        }

        /// <summary>
        /// Read from XML file, overwriting any changes not yet committed
        /// </summary>
        public void Refresh()
        {
            Type[] personTypes = { typeof(Survey) };
            XmlSerializer serializer = new XmlSerializer(typeof(SurveysList), personTypes);

            FileStream fs = new FileStream("Data/Surveys.xml", FileMode.Open);
            data = (SurveysList)serializer.Deserialize(fs);
            fs.Close();
        }

        /// <summary>
        /// Save changes back to XML file
        /// </summary>
        public void Commit()
        {
            Type[] personTypes = { typeof(Survey) };
            XmlSerializer serializer = new XmlSerializer(typeof(SurveysList), personTypes);

            FileStream fs = new FileStream("Data/Surveys.xml", FileMode.Create);
            serializer.Serialize(fs, data);
            fs.Close();
        }
    }
}
