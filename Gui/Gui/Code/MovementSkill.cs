﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gui
{
    /// <summary>
    /// Small helper class to ensure strings are consistent
    /// </summary>
    public static class MovementSkill
    {
        private struct Category
        {
            public readonly string ID;
            public readonly string DisplayName;

            public Category(string ID, string DisplayName)
            {
                this.ID = ID;
                this.DisplayName = DisplayName;
            }
        };
        private static readonly List<Category> Categories = new List<Category>()
        {
            new Category("FMS", "FMS"),
            new Category("UNI_ACTIVE", "UNI ACTIVE")
        };

        public enum Index
        {
            FMS = 0,
            UniActive = 1
        };
        public static Index Current = Index.FMS;

        public static String DisplayName
        {
            get
            {
                return Categories[(int)Current].DisplayName;
            }
        }

        public static String ID
        {
            get
            {
                return Categories[(int)Current].ID;
            }
        }
    }
}
