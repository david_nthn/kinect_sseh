﻿using System;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Common;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Navigation;

namespace FMSPlayBack
{
    /// <summary>
    /// Helper that inversly converts from int to bool
    /// ie. 0 -> true, other -> false
    /// </summary>
    public class BoolConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            return (int)value == 0;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        #endregion
    }

    namespace Content
    {
        /// <summary>
        /// Helper that holds the context for the SessionsForm class
        /// </summary>
        public class SessionContext : INotifyPropertyChanged
        {
            private DataRowView activity;
            public DataRowView Activity
            {
                get { return activity; }
                set
                {
                    activity = value;
                    OnPropertyChanged("Activity");
                }
            }

            private DataRowView subject;
            public DataRowView Subject
            {
                get { return subject; }
                set
                {
                    subject = value;
                    OnPropertyChanged("Subject");
                }
            }

            // Declare the event 
            public event PropertyChangedEventHandler PropertyChanged;
            private void OnPropertyChanged(string prop)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(prop));
                }
            }
        }

        /// <summary>
        /// Interaction logic for SessionsForm.xaml
        /// </summary>
        public partial class SessionsForm : UserControl, IContent
        {
            public SessionsForm()
            {
                InitializeComponent();

                if (File.Exists(DatabaseHelper.DefaultDBPath))
                {
                    File_TextBox.Text = new FileInfo(DatabaseHelper.DefaultDBPath).FullName;
                }
            }

            #region IContent
            /// <summary>
            /// Handle sub-pages
            /// </summary>
            /// <param name="e">event arguments</param>
            public void OnFragmentNavigation(FragmentNavigationEventArgs e)
            {
                switch (FragmentHelper.GetFragmentName(e.Fragment))
                {
                    case "session":
                        {
                            Browse.Visibility = Visibility.Visible;
                            BrowseFile.Visibility = Visibility.Collapsed;
                            Details.Visibility = Visibility.Collapsed;
                            DataContext = null;

                            break;
                        }
                    case "database":
                        {
                            Browse.Visibility = Visibility.Collapsed;
                            BrowseFile.Visibility = Visibility.Visible;
                            Details.Visibility = Visibility.Collapsed;
                            DataContext = null;

                            break;
                        }
                    case "activity":
                        {
                            Browse.Visibility = Visibility.Collapsed;
                            BrowseFile.Visibility = Visibility.Collapsed;
                            Details.Visibility = Visibility.Visible;

                            string[] subFragments = FragmentHelper.GetFragments(e.Fragment);
                            if (subFragments.Length > 0
                                && !string.IsNullOrWhiteSpace(subFragments[0]))
                            {
                                SessionContext c = new SessionContext();
                                if (!string.IsNullOrWhiteSpace(File_TextBox.Text))
                                {
                                    c.Activity = new DatabaseHelper(File_TextBox.Text).GetActivity(int.Parse(subFragments[0]));
                                    c.Subject = new DatabaseHelper(File_TextBox.Text).GetSubject((int)DatabaseHelper.GetField<int>(c.Activity, "SubjectID"));
                                }
                                DataContext = c;
                            }
                            break;
                        }
                }
            }

            /// <summary>
            /// Unload event handling
            /// </summary>
            /// <param name="e">event arguments</param>
            public void OnNavigatedFrom(NavigationEventArgs e)
            {
            }

            /// <summary>
            /// Load event handling
            /// </summary>
            /// <param name="e">event arguments</param>
            public void OnNavigatedTo(NavigationEventArgs e)
            {
            }

            /// <summary>
            /// </summary>
            /// <param name="e">event arguments</param>
            public void OnNavigatingFrom(NavigatingCancelEventArgs e)
            {
            }
            #endregion

            #region EventHandlers
            /// <summary>
            /// Helper that requests a folder location from the user
            /// </summary>
            /// <param name="sender">object sending the event</param>
            /// <param name="e">event arguments</param>
            private void BrowseFolder(object sender, RoutedEventArgs e)
            {
                var dialog = new System.Windows.Forms.FolderBrowserDialog();
                dialog.SelectedPath = Folder_TextBox.Text;

                System.Windows.Forms.DialogResult result = dialog.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    Folder_TextBox.Text = dialog.SelectedPath;
                }
            }

            /// <summary>
            /// Helper that requests a db location from the user
            /// </summary>
            /// <param name="sender">object sending the event</param>
            /// <param name="e">event arguments</param>
            private void BrowsePath(object sender, RoutedEventArgs e)
            {
                // Configure open file dialog box
                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
                dlg.Multiselect = false;
                dlg.CheckFileExists = true;
                if (string.IsNullOrWhiteSpace(File_TextBox.Text))
                {
                    dlg.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;
                    dlg.FileName = ""; // Default file name
                }
                else
                {
                    FileInfo info = new FileInfo(File_TextBox.Text);
                    dlg.InitialDirectory = info.DirectoryName;
                    dlg.FileName = info.Name; // Default file name
                }
                dlg.DefaultExt = ".mdb"; // Default file extension
                dlg.Filter = "Database (.mdb)|*.mdb"; // Filter files by extension 

                // Show open file dialog box
                Nullable<bool> result = dlg.ShowDialog();

                // Process open file dialog box results 
                if (result == true)
                {
                    // Open document 
                    File_TextBox.Text = dlg.FileName;
                    
                    MainWindow window = Application.Current.MainWindow as MainWindow;
                    if (window != null)
                    {
                        window.ContentSource = new Uri("/Pages/Sessions.xaml#" + File_TextBox.Text, UriKind.Relative);
                    }
                }
            }

            /// <summary>
            /// Helper that updates the 'playback' link to use the text of the textbox
            /// </summary>
            /// <param name="sender">object sending the event</param>
            /// <param name="e">event arguments</param>
            private void Folder_TextBox_TextChanged(object sender, TextChangedEventArgs e)
            {
                MainWindow window = Application.Current.MainWindow as MainWindow;
                if (window != null)
                {
                    LinkGroup group = window.Menu.LinkGroups[0];

                    foreach (Link link in group.Links)
                    {
                        if (link.DisplayName == "playback")
                        {
                            link.Source = new Uri("/Pages/Playback.xaml#" + Folder_TextBox.Text, UriKind.Relative);
                        }
                    }
                }
            }
            #endregion
        }
    }
}
