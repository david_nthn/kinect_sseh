﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FMSPlayBack
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : ModernWindow
    {
        /// <summary>
        /// Cache for menu object
        /// </summary>
        public ModernMenu Menu;

        public MainWindow()
        {
            InitializeComponent();

            Loaded += OnLoaded;
        }

        /// <summary>
        /// Load event handling
        /// </summary>
        public void OnLoaded(object sender, RoutedEventArgs e)
        {
            //find menu and back button
            Grid grid = (Grid)Template.FindName("LayoutRoot", this);
            foreach (UIElement item in grid.Children)
            {
                if (item as ModernMenu != null)
                {
                    Menu = item as ModernMenu;
                }
                if (item as ModernButton != null)
                {
                    ModernButton back = item as ModernButton;
                    back.Visibility = Visibility.Collapsed;
                }
            }
        }
    }
}
