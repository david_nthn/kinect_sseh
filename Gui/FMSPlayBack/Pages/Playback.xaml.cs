﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using Common;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Navigation;
using SkeletonReaderWriter;

namespace FMSPlayBack.Pages
{
    /// <summary>
    /// Interaction logic for Playback.xaml
    /// </summary>
    public partial class Playback : UserControl, IContent
    {
        public Playback()
        {
            InitializeComponent();
            DataContext = this;

            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(60);
            timer.Tick += new EventHandler(timer_Tick);
        }

        #region IContent
        /// <summary>
        /// Handle sub-pages
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnFragmentNavigation(FragmentNavigationEventArgs e)
        {
            Path = FragmentHelper.GetFragmentName(e.Fragment);
        }

        /// <summary>
        /// Unload event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedFrom(NavigationEventArgs e)
        {
        }

        /// <summary>
        /// Load event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        /// <summary>
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
        }
        #endregion

        /// <summary>
        /// Object handling access to the left skeleton stream
        /// </summary>
        private SkeletonReader LeftReader = new SkeletonReader(new byte[0]);

        /// <summary>
        /// Object handling access to the right skeleton stream
        /// </summary>
        private SkeletonReader RightReader = new SkeletonReader(new byte[0]);

        /// <summary>
        /// Used to reduce precision loss due to the slider ticks being stored as doubles
        /// </summary>
        private long firstTick;

        /// <summary>
        /// Callback timer to update scrubber while playing
        /// </summary>
        private DispatcherTimer timer;

        /// <summary>
        /// Helper to set up slider ticks / range
        /// </summary>
        private void InitSlider(Slider slider, SkeletonReader reader)
        {
            DoubleCollection ticks = new DoubleCollection();
            for (uint i = 0; i < reader.NumberOfSavedFrames(); i++)
            {
                ticks.Add(reader.GetFrame(i).timestamp - firstTick);
            }
            if (ticks.Count == 0)
            {
                //add some kind of non-zero tick so slider still 'works' but does nothing useful
                ticks.Add(1);
            }
            slider.Ticks = ticks;
            slider.Maximum = ticks.Last();
            slider.Value = 0;
        }

        /// <summary>
        /// Update Path
        /// </summary>
        private void UpdatePath()
        {
            if(IsPlaying)
            {
                DoPlayPause();
            }

            //Set up left skeleton view and connected controls
            LeftReader = new SkeletonReader(Path + "\\Left.bin");
            if (LeftReader.NumberOfSavedFrames() == 0)
            {
                ShowLeftSkeleton.IsChecked = false;
                ShowLeftSkeleton.IsEnabled = false;
            }
            else
            {
                ShowLeftSkeleton.IsChecked = true;
                ShowLeftSkeleton.IsEnabled = true;
            }

            //Set up right skeleton view and connected controls
            RightReader = new SkeletonReader(Path + "\\Right.bin");
            if (LeftReader.NumberOfSavedFrames() == 0)
            {
                ShowRightSkeleton.IsChecked = false;
                ShowRightSkeleton.IsEnabled = false;
            }
            else
            {
                ShowRightSkeleton.IsChecked = true;
                ShowRightSkeleton.IsEnabled = true;
            }

            //Set up video view and connected controls
            if (!string.IsNullOrWhiteSpace(Path)
                && File.Exists(Path + "\\Video.mp4"))
            {
                Video.Source = new Uri(Path + "\\Video.mp4");

                Video.Pause();

                ShowVideo.IsChecked = true;
                ShowVideo.IsEnabled = true;
            }
            else
            {
                Video.Source = null;

                ShowVideo.IsChecked = false;
                ShowVideo.IsEnabled = false;
            }

            //find value to localize ticks to, to reduce precision loss from converting to double
            firstTick = long.MaxValue;
            if (LeftReader.InitialTimestamp() != null)
            {
                firstTick = (long)LeftReader.InitialTimestamp();
            }
            else
            {
                SkeletonReader.SkeletonFrameData frame = LeftReader.GetFrame(0);
                if (frame != null)
                {
                    firstTick = frame.timestamp;
                }
            }

            if (RightReader.InitialTimestamp() != null)
            {
                long time = (long)RightReader.InitialTimestamp();
                if (time < firstTick)
                {
                    firstTick = time;
                }
            }
            else
            {
                SkeletonReader.SkeletonFrameData frame = RightReader.GetFrame(0);
                if (frame != null
                    && frame.timestamp < firstTick)
                {
                    firstTick = frame.timestamp;
                }
            }

            InitSlider(LeftScrubber, LeftReader);
            InitSlider(RightScrubber, RightReader);

            if(LeftScrubber.Maximum < RightScrubber.Maximum)
            {
                LeftScrubber.Maximum = RightScrubber.Maximum;
            }

            if (RightScrubber.Maximum < LeftScrubber.Maximum)
            {
                RightScrubber.Maximum = LeftScrubber.Maximum;
            }
        }

        /// <summary>
        /// Update views based on slider position
        /// </summary>
        private void Scrubber_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            long timestamp = firstTick + (long)(e.NewValue);

            LeftStream.ProcessSkeletonFrame(LeftReader.GetFrameByTimeStamp(timestamp, SkeletonReader.SearchTerm.Nearest));
            RightStream.ProcessSkeletonFrame(RightReader.GetFrameByTimeStamp(timestamp, SkeletonReader.SearchTerm.Nearest));

            if (!IsPlaying)
            {
                Video.Position = new TimeSpan((long)(e.NewValue));
            }
        }

        /// <summary>
        /// Play / Pause playback
        /// </summary>
        private void DoPlayPause()
        {
            IsPlaying = !IsPlaying;
            if(IsPlaying)
            {
                Video.Play();
                timer.Start();
            }
            else
            {
                Video.Pause();
                timer.Stop();
            }
        }

        /// <summary>
        /// Handler for the Play / Pause button
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void PlayPause_Click(object sender, RoutedEventArgs e)
        {
            DoPlayPause();
        }

        /// <summary>
        /// Timer Callback that will update the scrubber during playback
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        void timer_Tick(object sender, EventArgs e)
        {
            Dispatcher.Invoke(UpdateScrubber);
        }

        /// <summary>
        /// Update the position of the scrubber based on the position of the video file
        /// </summary>
        void UpdateScrubber()
        {
            if (IsPlaying)
            {
                LeftScrubber.Value = Video.Position.Ticks;
            }
        }
        
        /// <summary>
        /// Media reached end
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Video_MediaEnded(object sender, RoutedEventArgs e)
        {
            DoPlayPause();

            //reset back to beginning of stream
            LeftScrubber.Value = 0;

            //not sure why, but setting the position too low (below half a frame duration?) doesn't reset some videos (shorter than 1s?) properly
            Video.Position = new TimeSpan(10000);
        }
        
        /// <summary>
        /// Path to the directory that contains the data to be played back
        /// </summary>
        public string Path
        {
            get { return (string)GetValue(PathProperty); }
            set { SetValue(PathProperty, value); }
        }
        public static readonly DependencyProperty PathProperty = DependencyProperty.Register("Path", typeof(string), typeof(Playback), new PropertyMetadata(null, OnPathChanged));

        /// <summary>
        /// Handle changes to the Path
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="args">event arguments</param>
        private static void OnPathChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            //sender should be this class
            var home = sender as Playback;

            //if not, ignore it
            if (null == home)
            {
                return;
            }

            home.UpdatePath();
        }
        
        /// <summary>
        /// Whether session is playing
        /// </summary>
        public bool IsPlaying
        {
            get { return (bool)GetValue(IsPlayingProperty); }
            set { SetValue(IsPlayingProperty, value); }
        }
        public static readonly DependencyProperty IsPlayingProperty = DependencyProperty.Register("IsPlaying", typeof(bool), typeof(Playback), new PropertyMetadata(false));
        }
}
