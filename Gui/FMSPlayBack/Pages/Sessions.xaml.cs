﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Common;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Navigation;

namespace FMSPlayBack.Pages
{
    /// <summary>
    /// Interaction logic for Sessions.xaml
    /// </summary>
    public partial class Sessions : UserControl, IContent
    {
        public Sessions()
        {
            InitializeComponent();

            if (File.Exists(DatabaseHelper.DefaultDBPath))
            {
                DataContext = new DatabaseHelper().GetTable("Activity");
            }
            CommandsListView.SelectedIndex = 0;
        }

        #region IContent
        /// <summary>
        /// Handle sub-pages
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnFragmentNavigation(FragmentNavigationEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(FragmentHelper.GetFragmentName(e.Fragment)))
            {
                DataContext = new DatabaseHelper(FragmentHelper.GetFragmentName(e.Fragment)).GetTable("Activity");
            }
        }

        /// <summary>
        /// Unload event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedFrom(NavigationEventArgs e)
        {
        }

        /// <summary>
        /// Load event handling
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        /// <summary>
        /// </summary>
        /// <param name="e">event arguments</param>
        public void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
        }
        #endregion

        #region EventHandlers
        /// <summary>
        /// Event handler to select an item in the 'command' list
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void OnSelectionChanged_Commands(object sender, RoutedEventArgs e)
        {
            object item = (e.OriginalSource as ListView).SelectedItem;
            if (item == null)
            {
                return;
            }
            SessionsListView.SelectedItem = null;

            if ((e.OriginalSource as ListView).SelectedIndex == 0)
            {
                Form.Source = new Uri("/Content/SessionsForm.xaml#session", UriKind.Relative);
            }
            else
            {
                Form.Source = new Uri("/Content/SessionsForm.xaml#database", UriKind.Relative);
            }
        }

        /// <summary>
        /// Event handler to select an item in the 'session' list
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void OnSelectionChanged_Sessions(object sender, RoutedEventArgs e)
        {
            object item = (e.OriginalSource as ListView).SelectedItem;
            if (item == null)
            {
                return;
            }
            CommandsListView.SelectedItem = null;

            string uri = "/Content/SessionsForm.xaml#activity"
                + FragmentHelper.Separator + ((int)DatabaseHelper.GetField<int>(SessionsListView.SelectedItem, "ID")).ToString();
            Form.Source = new Uri(uri, UriKind.Relative);
        }
        #endregion

        #region ListView Sorting
        /// <summary>
        /// State to know which header to sort
        /// </summary>
        GridViewColumnHeader _lastHeaderClicked = null;

        /// <summary>
        /// State to know which direction to sort
        /// </summary>
        ListSortDirection _lastDirection = ListSortDirection.Ascending;

        /// <summary>
        /// Do the actual sorting
        /// </summary>
        /// <param name="headerClicked">Column to sort</param>
        private void DoSort(GridViewColumnHeader headerClicked)
        {
            ICollectionView dataView = CollectionViewSource.GetDefaultView(SessionsListView.ItemsSource);
            if (dataView != null
                && headerClicked != null
                && headerClicked.Role != GridViewColumnHeaderRole.Padding)
            {
                //determine direction to sort
                ListSortDirection direction;
                if (headerClicked != _lastHeaderClicked)
                {
                    direction = ListSortDirection.Ascending;
                }
                else
                {
                    if (_lastDirection == ListSortDirection.Ascending)
                    {
                        direction = ListSortDirection.Descending;
                    }
                    else
                    {
                        direction = ListSortDirection.Ascending;
                    }
                }

                //sort
                dataView.SortDescriptions.Clear();
                string sortBy = ((Binding)headerClicked.Column.DisplayMemberBinding).Path.Path;
                SortDescription sd = new SortDescription(sortBy, direction);
                dataView.SortDescriptions.Add(sd);
                dataView.Refresh();

                //add sort direction arrows
                if (direction == ListSortDirection.Ascending)
                {
                    headerClicked.Column.HeaderTemplate =
                        Resources["HeaderTemplateArrowUp"] as DataTemplate;
                }
                else
                {
                    headerClicked.Column.HeaderTemplate =
                        Resources["HeaderTemplateArrowDown"] as DataTemplate;
                }

                // Remove arrow from previously sorted header
                if (_lastHeaderClicked != null && _lastHeaderClicked != headerClicked)
                {
                    _lastHeaderClicked.Column.HeaderTemplate = null;
                }

                _lastHeaderClicked = headerClicked;
                _lastDirection = direction;
            }
        }

        /// <summary>
        /// Event to handle when to sort
        /// </summary>
        private void OnClick_GridViewColumnHeader(object sender, RoutedEventArgs e)
        {
            DoSort(e.OriginalSource as GridViewColumnHeader);
        }
        #endregion
    }
}
