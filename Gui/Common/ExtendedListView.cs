﻿using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Common
{
    public class ExtendedListView : ListView
    {
        public ExtendedListView() : base()
        {
            this.Style = new Style(typeof(ListView), (Style)FindResource(typeof(ListView)));
            SetupSortableColumns();
        }

        #region ListView Sorting
        /// <summary>
        /// Init stuff needed to support sortable columns
        /// </summary>
        void SetupSortableColumns()
        {
            ParserContext parserContext = new ParserContext();
            parserContext.XmlnsDictionary.Add("", "http://schemas.microsoft.com/winfx/2006/xaml/presentation");
            parserContext.XmlnsDictionary.Add("x", "http://schemas.microsoft.com/winfx/2006/xaml");
            {
                string xml =
                    "<DataTemplate x:Key=\"HeaderTemplateArrowUp\">" +
                        "<DockPanel LastChildFill=\"True\" Width=\"{Binding ActualWidth, RelativeSource={RelativeSource FindAncestor, AncestorType={x:Type GridViewColumnHeader}}}\">" +
                        "<Path x:Name=\"arrowUp\" StrokeThickness=\"1\" Fill=\"Gray\" Data=\"M 5,10 L 15,10 L 10,5 L 5,10\" DockPanel.Dock=\"Right\" Width=\"20\" HorizontalAlignment=\"Right\" Margin=\"5,0,5,0\" SnapsToDevicePixels=\"True\"/>" +
                        "<TextBlock Text=\"{Binding }\" />" +
                        "</DockPanel>" +
                    "</DataTemplate>";
                Stream stream = new MemoryStream(System.Text.ASCIIEncoding.ASCII.GetBytes(xml));
                DataTemplate template = (DataTemplate)XamlReader.Load(stream, parserContext);
                Resources.Add("HeaderTemplateArrowUp", template);
            }
            {
                var xml =
                    "<DataTemplate x:Key=\"HeaderTemplateArrowDown\">" +
                        "<DockPanel LastChildFill=\"True\" Width=\"{Binding ActualWidth, RelativeSource={RelativeSource FindAncestor, AncestorType={x:Type GridViewColumnHeader}}}\">" +
                        "<Path x:Name=\"arrowDown\" StrokeThickness=\"1\" Fill=\"Gray\" Data=\"M 5,5 L 10,10 L 15,5 L 5,5\" DockPanel.Dock=\"Right\" Width=\"20\" HorizontalAlignment=\"Right\" Margin=\"5,0,5,0\" SnapsToDevicePixels=\"True\"/>" +
                        "<TextBlock Text=\"{Binding }\" />" +
                        "</DockPanel>" +
                    "</DataTemplate>";
                Stream stream = new MemoryStream(System.Text.ASCIIEncoding.ASCII.GetBytes(xml));
                DataTemplate template = (DataTemplate)XamlReader.Load(stream, parserContext);
                Resources.Add("HeaderTemplateArrowDown", template);
            }

            AddHandler(GridViewColumnHeader.ClickEvent, new RoutedEventHandler(OnClick_GridViewColumnHeader));
        }

        /// <summary>
        /// State to know which header to sort
        /// </summary>
        GridViewColumnHeader _lastHeaderClicked = null;

        /// <summary>
        /// State to know which direction to sort
        /// </summary>
        ListSortDirection _lastDirection = ListSortDirection.Ascending;

        /// <summary>
        /// Resort if needed
        /// </summary>
        public void DoSort()
        {
            if (_lastHeaderClicked != null)
            {
                if (_lastDirection == ListSortDirection.Ascending)
                {
                    _lastDirection = ListSortDirection.Descending;
                }
                else
                {
                    _lastDirection = ListSortDirection.Ascending;
                }
                DoSort(_lastHeaderClicked);
            }
        }

        /// <summary>
        /// Refresh the listview and ensure that the columns are wide enough to read the contents
        /// </summary>
        public void RefreshView()
        {
            Items.Refresh();

            foreach (GridViewColumn column in ((GridView)View).Columns)
            {
                if (double.IsNaN(column.Width))
                {
                    column.Width = column.ActualWidth;
                }

                column.Width = double.NaN;
            }
        }

        /// <summary>
        /// Do the actual sorting
        /// </summary>
        /// <param name="headerClicked">Column to sort</param>
        private void DoSort(GridViewColumnHeader headerClicked)
        {
            ICollectionView dataView = CollectionViewSource.GetDefaultView(ItemsSource);
            if (dataView != null
                && headerClicked != null
                && headerClicked.Role != GridViewColumnHeaderRole.Padding)
            {
                //determine direction to sort
                ListSortDirection direction;
                if (headerClicked != _lastHeaderClicked)
                {
                    direction = ListSortDirection.Ascending;
                }
                else
                {
                    if (_lastDirection == ListSortDirection.Ascending)
                    {
                        direction = ListSortDirection.Descending;
                    }
                    else
                    {
                        direction = ListSortDirection.Ascending;
                    }
                }

                //sort
                dataView.SortDescriptions.Clear();
                string sortBy = ((Binding)headerClicked.Column.DisplayMemberBinding).Path.Path;
                SortDescription sd = new SortDescription(sortBy, direction);
                dataView.SortDescriptions.Add(sd);
                dataView.Refresh();

                //add sort direction arrows
                if (direction == ListSortDirection.Ascending)
                {
                    headerClicked.Column.HeaderTemplate =
                        Resources["HeaderTemplateArrowUp"] as DataTemplate;
                }
                else
                {
                    headerClicked.Column.HeaderTemplate =
                        Resources["HeaderTemplateArrowDown"] as DataTemplate;
                }

                // Remove arrow from previously sorted header
                if (_lastHeaderClicked != null && _lastHeaderClicked != headerClicked)
                {
                    _lastHeaderClicked.Column.HeaderTemplate = null;
                }

                _lastHeaderClicked = headerClicked;
                _lastDirection = direction;
            }
        }

        /// <summary>
        /// Event to handle when to sort
        /// </summary>
        public void OnClick_GridViewColumnHeader(object sender, RoutedEventArgs e)
        {
            DoSort(e.OriginalSource as GridViewColumnHeader);
        }
        #endregion
    }
}
