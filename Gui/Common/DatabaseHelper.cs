﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Xml;

namespace Common
{
    /// <summary>
    /// Helper class that deals with Database functionality
    /// </summary>
    public class DatabaseHelper
    {
        private static bool upgraded = false;

        public DatabaseHelper(string path = DefaultDBPath, bool overwrite = false, bool forceUpgrade = false)
        {
            DbFile = path;
            Connection = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + DbFile;

            //handle existing database file
            if (File.Exists(DbFile))
            {
                if (overwrite)
                {
                    //delete existing file
                    File.Delete(DbFile);
                }
                else
                {
                    //don't try to upgrade every single time
                    if (!upgraded || forceUpgrade)
                    {
                        UpgradeDB();
                        upgraded = true;
                    }
                    return;
                }
            }

            CreateDB();
        }

        #region Subject
        /// <summary>
        /// Get the Subject table
        /// </summary>
        /// <returns>In-memory cache of the requested table</returns>
        public DataView GetSubjectTable()
        {
            return GetTable("Subject");
        }

        /// <summary>
        /// Get row from the Subject table
        /// </summary>
        /// <param name="rowId">Id of the row being requested</param>
        /// <returns>In-memory cache of the requested row</returns>
        public DataRowView GetSubject(int rowId)
        {
            return GetRow("Subject", rowId);
        }

        #region Fields
        /// <summary>
        /// Get ID from row
        /// </summary>
        /// <param name="row">row to get the ID from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static int GetSubject_ID(object row)
        {
            return (int)GetField<int>(row, "ID");
        }

        /// <summary>
        /// Get FirstName from row
        /// </summary>
        /// <param name="row">row to get the FirstName from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static string GetSubject_FirstName(object row)
        {
            return (string)GetField<string>(row, "FirstName");
        }

        /// <summary>
        /// Get LastName from row
        /// </summary>
        /// <param name="row">row to get the LastName from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static string GetSubject_LastName(object row)
        {
            return (string)GetField<string>(row, "LastName");
        }

        /// <summary>
        /// Get Birthday from row
        /// </summary>
        /// <param name="row">row to get the Birthday from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static DateTime? GetSubject_Birthday(object row)
        {
            return (DateTime?)GetField<DateTime>(row, "Birthday");
        }

        /// <summary>
        /// Get isMale from row
        /// </summary>
        /// <param name="row">row to get the isMale from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static bool GetSubject_isMale(object row)
        {
            return (int?)GetField<int>(row, "Gender") != 0;
        }

        /// <summary>
        /// Get isFemale from row
        /// </summary>
        /// <param name="row">row to get the isFemale from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static bool GetSubject_isFemale(object row)
        {
            return (int?)GetField<int>(row, "Gender") == 0;
        }

        /// <summary>
        /// Get CurrentSchoolClassID from row
        /// </summary>
        /// <param name="row">row to get the CurrentSchoolClassID from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static int? GetSubject_CurrentSchoolClassID(object row)
        {
            return (int?)GetField<int>(row, "CurrentSchoolClassID");
        }

        /// <summary>
        /// Get HomeAddress from row
        /// </summary>
        /// <param name="row">row to get the HomeAddress from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static string GetSubject_HomeAddress(object row)
        {
            return (string)GetField<string>(row, "HomeAddress");
        }

        /// <summary>
        /// Get Postcode from row
        /// </summary>
        /// <param name="row">row to get the Postcode from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static string GetSubject_Postcode(object row)
        {
            return (string)GetField<string>(row, "Postcode");
        }

        /// <summary>
        /// Get Phone from row
        /// </summary>
        /// <param name="row">row to get the Phone from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static string GetSubject_Phone(object row)
        {
            return (string)GetField<string>(row, "Phone");
        }

        /// <summary>
        /// Get ParentDetails from row
        /// </summary>
        /// <param name="row">row to get the ParentDetails from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static string GetSubject_ParentDetails(object row)
        {
            return (string)GetField<string>(row, "ParentDetails");
        }

        /// <summary>
        /// Get ParentContact from row
        /// </summary>
        /// <param name="row">row to get the ParentContact from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static string GetSubject_ParentContact(object row)
        {
            return (string)GetField<string>(row, "ParentContact");
        }

        /// <summary>
        /// Get ParentEmail from row
        /// </summary>
        /// <param name="row">row to get the ParentEmail from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static string GetSubject_ParentEmail(object row)
        {
            return (string)GetField<string>(row, "ParentEmail");
        }

        /// <summary>
        /// Get UUID from row
        /// </summary>
        /// <param name="row">row to get the UUID from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static string GetSubject_UUID(object row)
        {
            return (string)GetField<string>(row, "UUID");
        }
        #endregion

        /// <summary>
        /// Add an entry to the 'Subject' table
        /// </summary>
        /// <returns>ID of newly added row</returns>
        public int AddSubject(string firstName, string lastName, DateTime? birthday, bool gender,
            int? schoolClassId,
            string homeAddress, string postcode, string phone,
            string parentDetails, string parentContact, string parentEmail)
        {
            int retval = -1;

            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                OleDbCommand cmd = new OleDbCommand("INSERT INTO Subject "
                    + "([FirstName], [LastName], [Birthday], [Gender], [CurrentSchoolClassID], "
                    + "[HomeAddress], [Postcode], [Phone], [ParentDetails], [ParentContact], [ParentEmail], [UUID]) "
                    + "VALUES (@FirstName, @LastName, @Birthday, @Gender, @CurrentSchoolClassID, "
                    + "@HomeAddress, @Postcode, @Phone, @ParentDetails, @ParentContact, @ParentEmail, @UUID)", con);

                //populate parameters
                //this should escapify strings appropriately
                cmd.Parameters.AddWithValue("@FirstName", DBNullifyString(firstName));
                cmd.Parameters.AddWithValue("@LastName", DBNullifyString(lastName));
                cmd.Parameters.AddWithValue("@Birthday", DBNullifyString(birthday.ToString()));
                cmd.Parameters.AddWithValue("@Gender", (gender == true) ? 1 : 0);
                cmd.Parameters.AddWithValue("@CurrentSchoolClassID", schoolClassId == null ? (object)DBNull.Value : (int)schoolClassId);
                cmd.Parameters.AddWithValue("@HomeAddress", DBNullifyString(homeAddress));
                cmd.Parameters.AddWithValue("@Postcode", DBNullifyString(postcode));
                cmd.Parameters.AddWithValue("@Phone", DBNullifyString(phone));
                cmd.Parameters.AddWithValue("@ParentDetails", DBNullifyString(parentDetails));
                cmd.Parameters.AddWithValue("@ParentContact", DBNullifyString(parentContact));
                cmd.Parameters.AddWithValue("@ParentEmail", DBNullifyString(parentEmail));
                cmd.Parameters.AddWithValue("@UUID", DBNullifyString(DateTimeToStr(birthday) + lastName + firstName));

                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                //get id of row (AutoId) so that extra operations can be done on this
                cmd.CommandText = "SELECT @@IDENTITY";
                retval = (int)cmd.ExecuteScalar();
            }

            return retval;
        }

        /// <summary>
        /// Edit an entry to the 'Subject' table
        /// </summary>
        public void EditSubject(int ID, string firstName, string lastName, DateTime? birthday, bool gender,
            int? schoolClassId,
            string homeAddress, string postcode, string phone,
            string parentDetails, string parentContact, string parentEmail)
        {
            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                OleDbCommand cmd = new OleDbCommand("UPDATE Subject SET "
                    + "FirstName = @FirstName, "
                    + "LastName = @LastName, "
                    + "Birthday = @Birthday, "
                    + "Gender = @Gender, "
                    + "CurrentSchoolClassID = @CurrentSchoolClassID, "
                    + "HomeAddress = @HomeAddress, "
                    + "Postcode = @Postcode, "
                    + "Phone = @Phone, "
                    + "ParentDetails = @ParentDetails, "
                    + "ParentContact = @ParentContact, "
                    + "ParentEmail = @ParentEmail, "
                    + "UUID = @UUID "
                    + "WHERE ID = @ID", con);

                //populate parameters
                //this should escapify strings appropriately
                cmd.Parameters.AddWithValue("@FirstName", DBNullifyString(firstName));
                cmd.Parameters.AddWithValue("@LastName", DBNullifyString(lastName));
                cmd.Parameters.AddWithValue("@Birthday", DBNullifyString(birthday.ToString()));
                cmd.Parameters.AddWithValue("@Gender", (gender == true) ? 1 : 0);
                cmd.Parameters.AddWithValue("@CurrentSchoolClassID", schoolClassId == null ? (object)DBNull.Value : (int)schoolClassId);
                cmd.Parameters.AddWithValue("@HomeAddress", DBNullifyString(homeAddress));
                cmd.Parameters.AddWithValue("@Postcode", DBNullifyString(postcode));
                cmd.Parameters.AddWithValue("@Phone", DBNullifyString(phone));
                cmd.Parameters.AddWithValue("@ParentDetails", DBNullifyString(parentDetails));
                cmd.Parameters.AddWithValue("@ParentContact", DBNullifyString(parentContact));
                cmd.Parameters.AddWithValue("@ParentEmail", DBNullifyString(parentEmail));
                cmd.Parameters.AddWithValue("@UUID", DBNullifyString(DateTimeToStr(birthday) + lastName + firstName));
                cmd.Parameters.AddWithValue("@ID", ID);

                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region Session
        /// <summary>
        /// Get row from the SessionInfo table
        /// </summary>
        /// <param name="rowId">Id of the row being requested</param>
        /// <returns>In-memory cache of the requested row</returns>
        public DataRowView GetSession(int rowId)
        {
            return GetRow("SessionInfo", rowId);
        }

        #region Fields
        /// <summary>
        /// Get ID from row
        /// </summary>
        /// <param name="row">row to get the ID from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static int GetSession_ID(object row)
        {
            return (int)GetField<int>(row, "ID");
        }

        /// <summary>
        /// Get SubjectID from row
        /// </summary>
        /// <param name="row">row to get the SubjectID from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static int GetSession_SubjectID(object row)
        {
            return (int)GetField<int>(row, "SubjectID");
        }

        /// <summary>
        /// Get SessionDate from row
        /// </summary>
        /// <param name="row">row to get the SessionDate from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static DateTime? GetSession_SessionDate(object row)
        {
            return (DateTime?)GetField<DateTime>(row, "SessionDate");
        }

        /// <summary>
        /// Get SessionType from row
        /// </summary>
        /// <param name="row">row to get the SessionType from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static string GetSession_SessionType(object row)
        {
            return (string)GetField<string>(row, "SessionType");
        }

        /// <summary>
        /// Get SchoolClassID from row
        /// </summary>
        /// <param name="row">row to get the SchoolClassID from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static int? GetSession_SchoolClassID(object row)
        {
            return (int?)GetField<int>(row, "SchoolClassID");
        }

        /// <summary>
        /// Get Completed from row
        /// </summary>
        /// <param name="row">row to get the Completed from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static bool GetSession_Completed(object row)
        {
            return (int)GetField<int>(row, "Completed") == 1;
        }

        /// <summary>
        /// Get Invalid from row
        /// </summary>
        /// <param name="row">row to get the Invalid from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static bool GetSession_Invalid(object row)
        {
            return (int)GetField<int>(row, "Invalid") == 1;
        }
        #endregion

        /// <summary>
        /// Get valid sessions
        /// </summary>
        /// <param name="sessionType">Type of session to get</param>
        /// <returns>In-memory cache of the requested rows</returns>
        public DataView GetValidSessions(string sessionType)
        {
            DataView retval = null;

            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                OleDbDataAdapter adpt = new OleDbDataAdapter("SELECT SessionInfo.ID as ID, * FROM SessionInfo INNER JOIN Subject ON SessionInfo.SubjectID = Subject.ID WHERE SessionInfo.Invalid = 0", con);
                if (!string.IsNullOrWhiteSpace(sessionType))
                {
                    adpt.SelectCommand.CommandText += " AND SessionInfo.SessionType = @SessionType";
                    adpt.SelectCommand.Parameters.AddWithValue("@SessionType", sessionType);
                }

                DataSet dtst = new DataSet();

                adpt.Fill(dtst);
                retval = dtst.Tables[0].DefaultView;

                if (retval.Count == 0)
                {
                    retval = null;
                }
            }

            return retval;
        }

        /// <summary>
        /// Get the Subject table
        /// </summary>
        /// <param name="classId">Id of the row being requested</param>
        /// <returns>In-memory cache of the requested table</returns>
        public DataView GetValidSessionsByClass(string sessionType, int classId)
        {
            DataView retval = null;

            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                OleDbDataAdapter adpt = new OleDbDataAdapter("SELECT * FROM SessionInfo WHERE Invalid = 0 AND SchoolClassID = " + classId, con);
                if (!string.IsNullOrWhiteSpace(sessionType))
                {
                    adpt.SelectCommand.CommandText += " AND SessionType = @SessionType";
                    adpt.SelectCommand.Parameters.AddWithValue("@SessionType", sessionType);
                }

                DataSet dtst = new DataSet();

                adpt.Fill(dtst);
                retval = dtst.Tables[0].DefaultView;

                if (retval.Count == 0)
                {
                    retval = null;
                }
            }

            return retval;
        }

        /// <summary>
        /// Get the Subject table
        /// </summary>
        /// <param name="classId">Id of the row being requested</param>
        /// <returns>In-memory cache of the requested table</returns>
        public DataView GetValidSessionsByYear(string sessionType, int schoolYearId)
        {
            DataView retval = null;

            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                OleDbDataAdapter adpt = new OleDbDataAdapter("SELECT SessionInfo.ID as ID, * FROM SessionInfo INNER JOIN SchoolClass ON SessionInfo.SchoolClassID = SchoolClass.ID WHERE Invalid = 0 AND SchoolClass.SchoolYearID = " + schoolYearId, con);
                if (!string.IsNullOrWhiteSpace(sessionType))
                {
                    adpt.SelectCommand.CommandText += " AND SessionType = @SessionType";
                    adpt.SelectCommand.Parameters.AddWithValue("@SessionType", sessionType);
                }

                DataSet dtst = new DataSet();

                adpt.Fill(dtst);
                retval = dtst.Tables[0].DefaultView;

                if (retval.Count == 0)
                {
                    retval = null;
                }
            }

            return retval;
        }

        /// <summary>
        /// Add an entry to the 'SessionInfo' table
        /// </summary>
        /// <returns>ID of newly added row</returns>
        public int AddSession(DataRowView subject, DateTime timestamp, string sessionType)
        {
            int retval = -1;

            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                OleDbCommand cmd = new OleDbCommand("INSERT INTO SessionInfo "
                    + "([SubjectID], [SessionDate], [SessionType], [SchoolClassID], [Completed], [Invalid]) "
                    + "VALUES (@SubjectID, @SessionDate, @SessionType, @SchoolClassID, @Completed, @Invalid)", con);

                //populate parameters
                //this should escapify strings appropriately
                cmd.Parameters.AddWithValue("@SubjectID", GetFieldDBNull<int>(subject, "ID"));
                cmd.Parameters.AddWithValue("@SessionDate", timestamp.ToString());
                cmd.Parameters.AddWithValue("@SessionType", sessionType);
                cmd.Parameters.AddWithValue("@SchoolClassID", GetFieldDBNull<int>(subject, "CurrentSchoolClassID"));
                cmd.Parameters.AddWithValue("@Completed", 0);
                cmd.Parameters.AddWithValue("@Invalid", 0);

                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                //get id of row (AutoId) so that extra operations can be done on this
                cmd.CommandText = "SELECT @@IDENTITY";
                retval = (int)cmd.ExecuteScalar();
            }

            return retval;
        }

        /// <summary>
        /// Find the ID of the active session for a subject, if one exists
        /// </summary>
        /// <returns>ID of active session, null if none found</returns>
        public int? GetActiveSessionID(DataRowView subject)
        {
            int? retval = null;

            if(subject == null)
            {
                return retval;
            }

            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                OleDbCommand cmd = new OleDbCommand("SELECT ID FROM SessionInfo WHERE Completed = 0 AND Invalid = 0 AND SubjectID = " + GetFieldDBNull<int>(subject, "ID"), con);
                OleDbDataReader reader = cmd.ExecuteReader();
                if(reader.HasRows)
                {
                    reader.Read();
                    retval = (int)reader[0];
                }
            }

            return retval;
        }

        /// <summary>
        /// Mark an entry in the 'SessionInfo' table as completed
        /// </summary>
        public void MarkSessionComplete(DataRowView session)
        {
            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                OleDbCommand cmd = new OleDbCommand("UPDATE SessionInfo SET Completed = 1 WHERE ID = " + GetFieldDBNull<int>(session, "ID"), con);
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Mark an entry in the 'SessionInfo' table as completed
        /// </summary>
        public void MarkSessionInvalid(int sessionID)
        {
            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                OleDbCommand cmd = new OleDbCommand("UPDATE SessionInfo SET Invalid = 1 WHERE ID = " + sessionID, con);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region Activity
        /// <summary>
        /// Get row from the Activity table
        /// </summary>
        /// <param name="rowId">Id of the row being requested</param>
        /// <returns>In-memory cache of the requested row</returns>
        public DataRowView GetActivity(int rowId)
        {
            return GetRow("Activity", rowId);
        }

        #region ActivityFields
        /// <summary>
        /// Get ID from row
        /// </summary>
        /// <param name="row">row to get the ID from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static int GetActivity_ID(object row)
        {
            return (int)GetField<int>(row, "ID");
        }

        /// <summary>
        /// Get SessionID from row
        /// </summary>
        /// <param name="row">row to get the SessionID from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static int GetActivity_SessionID(object row)
        {
            return (int)GetField<int>(row, "SessionID");
        }

        /// <summary>
        /// Get ActivityDate from row
        /// </summary>
        /// <param name="row">row to get the ActivityDate from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static DateTime? GetActivity_ActivityDate(object row)
        {
            return (DateTime?)GetField<DateTime>(row, "ActivityDate");
        }

        /// <summary>
        /// Get ActivityType from row
        /// </summary>
        /// <param name="row">row to get the ActivityType from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static string GetActivity_ActivityType(object row)
        {
            return (string)GetField<string>(row, "ActivityType");
        }

        /// <summary>
        /// Get Notes from row
        /// </summary>
        /// <param name="row">row to get the Notes from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static string GetActivity_Notes(object row)
        {
            return (string)GetField<string>(row, "Notes");
        }

        /// <summary>
        /// Get IsValid from row
        /// </summary>
        /// <param name="row">row to get the IsValid from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static bool GetActivity_IsValid(object row)
        {
            return (int)GetField<int>(row, "IsValid") == 1;
        }

        /// <summary>
        /// Get Directory from row
        /// </summary>
        /// <param name="row">row to get the Directory from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static string GetActivity_Directory(object row)
        {
            return (string)GetField<string>(row, "Directory");
        }
        #endregion

        #region ActivityDataFields
        /// <summary>
        /// Get ID from row
        /// </summary>
        /// <param name="row">row to get the ID from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static int GetActivityData_ID(object row)
        {
            return (int)GetField<int>(row, "ID");
        }

        /// <summary>
        /// Get Name from row
        /// </summary>
        /// <param name="row">row to get the Name from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static string GetActivityData_Name(object row)
        {
            return (string)GetField<string>(row, "Name");
        }

        /// <summary>
        /// Get FloatData from row
        /// </summary>
        /// <param name="row">row to get the FloatData from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static float? GetActivityData_FloatData(object row)
        {
            return (float?)(double?)GetField<double>(row, "FloatData");
        }

        /// <summary>
        /// Get BoolData from row
        /// </summary>
        /// <param name="row">row to get the BoolData from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static int? GetActivityData_BoolData(object row)
        {
            return (int?)GetField<int>(row, "BoolData");
        }
        #endregion

        /// <summary>
        /// Get acivities for the provided session
        /// </summary>
        /// <param name="session">Session to get the activities for</param>
        /// <returns>In-memory cache of the requested rows</returns>
        public DataView GetSessionActivities(DataRowView session)
        {
            DataView retval = null;

            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                OleDbDataAdapter adpt = new OleDbDataAdapter("SELECT * FROM Activity WHERE SessionID = " + GetFieldDBNull<int>(session, "ID"), con);
                DataSet dtst = new DataSet();

                adpt.Fill(dtst);

                retval = dtst.Tables[0].DefaultView;

                if(retval.Count == 0)
                {
                    retval = null;
                }
            }

            return retval;
        }

        /// <summary>
        /// Get acivities float data for provided activity
        /// </summary>
        /// <param name="activity">Activity to get the data for</param>
        /// <returns>In-memory cache of the requested rows</returns>
        public DataView GetActivityData(int activity)
        {
            DataView retval = null;

            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                OleDbDataAdapter adpt = new OleDbDataAdapter("SELECT * FROM ActivityData WHERE ActivityID = " + activity, con);
                DataSet dtst = new DataSet();

                adpt.Fill(dtst);

                retval = dtst.Tables[0].DefaultView;

                if (retval.Count == 0)
                {
                    retval = null;
                }
            }

            return retval;
        }

        /// <summary>
        /// Add an entry to the 'Activity' table
        /// </summary>
        /// <returns>ID of newly added row</returns>
        public int AddActivity(DataRowView session, DateTime timestamp, string dir, string activityType, string notes, bool isValid)
        {
            int retval = -1;

            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                OleDbCommand cmd = new OleDbCommand("INSERT INTO Activity "
                    + "([SessionID], [ActivityDate], [ActivityType], [Notes], [IsValid], [Directory]) "
                    + "VALUES (@SessionID, @ActivityDate, @ActivityType, @Notes, @IsValid, @Directory)", con);

                //populate parameters
                //this should escapify strings appropriately
                cmd.Parameters.AddWithValue("@SessionID", GetFieldDBNull<int>(session, "ID"));
                cmd.Parameters.AddWithValue("@ActivityDate", timestamp.ToString());
                cmd.Parameters.AddWithValue("@ActivityType", activityType);
                cmd.Parameters.AddWithValue("@Notes", notes);
                cmd.Parameters.AddWithValue("@IsValid", (isValid == true) ? 1 : 0);
                cmd.Parameters.AddWithValue("@Directory", dir);

                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                //get id of row (AutoId) so that extra operations can be done on this
                cmd.CommandText = "SELECT @@IDENTITY";
                retval = (int)cmd.ExecuteScalar();
            }

            return retval;
        }

        /// <summary>
        /// Add an entry to the 'ActivityDataFloat' table
        /// </summary>
        public void AddActivityDataFloat(int activityID, string name, string data)
        {
            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                OleDbCommand cmd = new OleDbCommand("INSERT INTO ActivityData "
                    + "([ActivityID], [Name], [FloatData]) "
                    + "VALUES (@ActivityID, @Name, @FloatData)", con);

                //populate parameters
                //this should escapify strings appropriately
                cmd.Parameters.AddWithValue("@ActivityID", activityID);
                cmd.Parameters.AddWithValue("@Name", name);
                cmd.Parameters.AddWithValue("@FloatData", StrToSingle(data));

                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
            }
        }

        /// <summary>
        /// Add an entry to the 'ActivityDataBool' table
        /// </summary>
        public void AddActivityDataBool(int activityID, string name, bool data)
        {
            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                OleDbCommand cmd = new OleDbCommand("INSERT INTO ActivityData "
                    + "([ActivityID], [Name], [BoolData]) "
                    + "VALUES (@ActivityID, @Name, @BoolData)", con);

                //populate parameters
                //this should escapify strings appropriately
                cmd.Parameters.AddWithValue("@ActivityID", activityID);
                cmd.Parameters.AddWithValue("@Name", name);
                cmd.Parameters.AddWithValue("@BoolData", data ? 1 : 0);

                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
            }
        }
        #endregion

        #region School
        /// <summary>
        /// Get the School table
        /// </summary>
        /// <returns>In-memory cache of the requested table</returns>
        public DataView GetSchoolTable()
        {
            return GetTable("School");
        }

        /// <summary>
        /// Get row from the School table
        /// </summary>
        /// <param name="rowId">Id of the row being requested</param>
        /// <returns>In-memory cache of the requested row</returns>
        public DataRowView GetSchool(int rowId)
        {
            return GetRow("School", rowId);
        }

        #region SchoolFields
        /// <summary>
        /// Get ID from row
        /// </summary>
        /// <param name="row">row to get the ID from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static int GetSchool_ID(object row)
        {
            return (int)GetField<int>(row, "ID");
        }

        /// <summary>
        /// Get Name from row
        /// </summary>
        /// <param name="row">row to get the Name from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static string GetSchool_Name(object row)
        {
            return (string)GetField<string>(row, "Name");
        }
        #endregion

        /// <summary>
        /// Add an entry to the 'School' table
        /// </summary>
        /// <returns>ID of newly added row</returns>
        public int AddSchool(string name)
        {
            int retval = -1;

            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                OleDbCommand cmd = new OleDbCommand("INSERT INTO School "
                    + "([Name]) "
                    + "VALUES (@Name)", con);

                //populate parameters
                //this should escapify strings appropriately
                cmd.Parameters.AddWithValue("@Name", DBNullifyString(name));

                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                //get id of row (AutoId) so that extra operations can be done on this
                cmd.CommandText = "SELECT @@IDENTITY";
                retval = (int)cmd.ExecuteScalar();
            }

            return retval;
        }

        /// <summary>
        /// Edit an entry to the 'School' table
        /// </summary>
        public void EditSchool(int ID, string name)
        {
            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                OleDbCommand cmd = new OleDbCommand("UPDATE School SET "
                    + "Name = @Name "
                    + "WHERE ID = @ID", con);

                //populate parameters
                //this should escapify strings appropriately
                cmd.Parameters.AddWithValue("@Name", DBNullifyString(name));
                cmd.Parameters.AddWithValue("@ID", ID);

                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region SchoolYear
        /// <summary>
        /// Get the SchoolYear table
        /// </summary>
        /// <returns>In-memory cache of the requested table</returns>
        public DataView GetSchoolYearTable(int? schoolID)
        {
            DataView retval = null;

            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                OleDbDataAdapter adpt = new OleDbDataAdapter("SELECT * FROM SchoolYear", con);
                if (schoolID != null)
                {
                    adpt.SelectCommand.CommandText += " WHERE SchoolID = @ID";
                    adpt.SelectCommand.Parameters.AddWithValue("@ID", schoolID);
                }

                DataSet dtst = new DataSet();

                adpt.Fill(dtst);
                retval = dtst.Tables[0].DefaultView;

                if (retval.Count == 0)
                {
                    retval = null;
                }
            }

            return retval;
        }

        /// <summary>
        /// Get row from the SchoolYear table
        /// </summary>
        /// <param name="rowId">Id of the row being requested</param>
        /// <returns>In-memory cache of the requested row</returns>
        public DataRowView GetSchoolYear(int rowId)
        {
            return GetRow("SchoolYear", rowId);
        }

        #region SchoolYearFields
        /// <summary>
        /// Get ID from row
        /// </summary>
        /// <param name="row">row to get the ID from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static int GetSchoolYear_ID(object row)
        {
            return (int)GetField<int>(row, "ID");
        }

        /// <summary>
        /// Get SchoolID from row
        /// </summary>
        /// <param name="row">row to get the SchoolID from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static int GetSchoolYear_SchoolID(object row)
        {
            return (int)GetField<int>(row, "SchoolID");
        }

        /// <summary>
        /// Get SchoolYear from row
        /// </summary>
        /// <param name="row">row to get the SchoolYear from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static string GetSchoolYear_Year(object row)
        {
            return (string)GetField<string>(row, "SchoolYear");
        }
        #endregion

        /// <summary>
        /// Add an entry to the 'SchoolYear' table
        /// </summary>
        /// <returns>ID of newly added row</returns>
        public int AddSchoolYear(int schoolID, string year)
        {
            int retval = -1;

            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                OleDbCommand cmd = new OleDbCommand("INSERT INTO SchoolYear "
                    + "([SchoolID], [SchoolYear]) "
                    + "VALUES (@SchoolID, @SchoolYear)", con);

                //populate parameters
                //this should escapify strings appropriately
                cmd.Parameters.AddWithValue("@SchoolID", schoolID);
                cmd.Parameters.AddWithValue("@SchoolYear", DBNullifyString(year));

                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                //get id of row (AutoId) so that extra operations can be done on this
                cmd.CommandText = "SELECT @@IDENTITY";
                retval = (int)cmd.ExecuteScalar();
            }

            return retval;
        }

        /// <summary>
        /// Edit an entry to the 'SchoolYear' table
        /// </summary>
        public void EditSchoolYear(int ID, string year)
        {
            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                OleDbCommand cmd = new OleDbCommand("UPDATE SchoolYear SET "
                    + "SchoolYear = @SchoolYear "
                    + "WHERE ID = @ID", con);

                //populate parameters
                //this should escapify strings appropriately
                cmd.Parameters.AddWithValue("@SchoolYear", DBNullifyString(year));
                cmd.Parameters.AddWithValue("@ID", ID);

                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region SchoolClass
        /// <summary>
        /// Get the SchoolClass table
        /// </summary>
        /// <returns>In-memory cache of the requested table</returns>
        public DataView GetSchoolClassTable(int? schoolYearID)
        {
            DataView retval = null;

            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                OleDbDataAdapter adpt = new OleDbDataAdapter("SELECT * FROM SchoolClass", con);
                if (schoolYearID != null)
                {
                    adpt.SelectCommand.CommandText += " WHERE SchoolYearID = @ID";
                    adpt.SelectCommand.Parameters.AddWithValue("@ID", schoolYearID);
                }

                DataSet dtst = new DataSet();

                adpt.Fill(dtst);
                retval = dtst.Tables[0].DefaultView;

                if (retval.Count == 0)
                {
                    retval = null;
                }
            }

            return retval;
        }

        /// <summary>
        /// Get row from the SchoolClass table
        /// </summary>
        /// <param name="rowId">Id of the row being requested</param>
        /// <returns>In-memory cache of the requested row</returns>
        public DataRowView GetSchoolClass(int rowId)
        {
            return GetRow("SchoolClass", rowId);
        }

        #region SchoolClassFields
        /// <summary>
        /// Get ID from row
        /// </summary>
        /// <param name="row">row to get the ID from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static int GetSchoolClass_ID(object row)
        {
            return (int)GetField<int>(row, "ID");
        }

        /// <summary>
        /// Get SchoolYearID from row
        /// </summary>
        /// <param name="row">row to get the SchoolYearID from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static int GetSchoolClass_SchoolYearID(object row)
        {
            return (int)GetField<int>(row, "SchoolYearID");
        }

        /// <summary>
        /// Get SchoolClass from row
        /// </summary>
        /// <param name="row">row to get the SchoolClass from</param>
        /// <returns>In-memory cache of the requested row</returns>
        public static string GetSchoolClass_Class(object row)
        {
            return (string)GetField<string>(row, "SchoolClass");
        }
        #endregion

        /// <summary>
        /// Add an entry to the 'SchoolClass' table
        /// </summary>
        /// <returns>ID of newly added row</returns>
        public int AddSchoolClass(int schoolYearID, string schoolClass)
        {
            int retval = -1;

            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                OleDbCommand cmd = new OleDbCommand("INSERT INTO SchoolClass "
                    + "([SchoolYearID], [SchoolClass]) "
                    + "VALUES (@SchoolYearID, @SchoolClass)", con);

                //populate parameters
                //this should escapify strings appropriately
                cmd.Parameters.AddWithValue("@SchoolYearID", schoolYearID);
                cmd.Parameters.AddWithValue("@SchoolClass", DBNullifyString(schoolClass));

                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                //get id of row (AutoId) so that extra operations can be done on this
                cmd.CommandText = "SELECT @@IDENTITY";
                retval = (int)cmd.ExecuteScalar();
            }

            return retval;
        }

        /// <summary>
        /// Edit an entry to the 'SchoolClass' table
        /// </summary>
        public void EditSchoolClass(int ID, string schoolClass)
        {
            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                OleDbCommand cmd = new OleDbCommand("UPDATE SchoolClass SET "
                    + "SchoolClass = @SchoolClass "
                    + "WHERE ID = @ID", con);

                //populate parameters
                //this should escapify strings appropriately
                cmd.Parameters.AddWithValue("@SchoolClass", DBNullifyString(schoolClass));
                cmd.Parameters.AddWithValue("@ID", ID);

                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region Import Export
        /// <summary>
        /// Export subjects
        /// </summary>
        /// <param name="file">file to save exported subjects to</param>
        /// <returns>In-memory cache of the requested rows</returns>
        public void ExportSubjects(string file)
        {
            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                DataSet dtst = new DataSet("Data");

                OleDbDataAdapter dbData = new OleDbDataAdapter("SELECT * FROM DBData", con);
                OleDbDataAdapter school = new OleDbDataAdapter("SELECT * FROM School", con);
                OleDbDataAdapter schoolYear = new OleDbDataAdapter("SELECT * FROM SchoolYear", con);
                OleDbDataAdapter schoolClass = new OleDbDataAdapter("SELECT * FROM SchoolClass", con);
                OleDbDataAdapter subject = new OleDbDataAdapter("SELECT * FROM Subject", con);

                dbData.Fill(dtst, "DBData");
                school.Fill(dtst, "School");
                schoolYear.Fill(dtst, "SchoolYear");
                schoolClass.Fill(dtst, "SchoolClass");
                subject.Fill(dtst, "Subject");

                foreach (DataTable table in dtst.Tables)
                {
                    foreach(DataColumn column in table.Columns)
                    {
                        switch(column.ColumnName)
                        {
                            default:
                                {
                                    column.ColumnMapping = MappingType.Attribute;
                                    break;
                                }
                            case "ID":
                            case "SchoolID":
                            case "SchoolYearID":
                            case "CurrentSchoolClassID":
                                {
                                    column.ColumnMapping = MappingType.Hidden;
                                    break;
                                }
                        }
                    }
                }

                DataRelation SchoolSchoolYear = dtst.Relations.Add("SchoolSchoolYear", dtst.Tables["School"].Columns["ID"], dtst.Tables["SchoolYear"].Columns["SchoolID"]);
                SchoolSchoolYear.Nested = true;
                DataRelation SchoolYearSchoolClass = dtst.Relations.Add("SchoolYearSchoolClass", dtst.Tables["SchoolYear"].Columns["ID"], dtst.Tables["SchoolClass"].Columns["SchoolYearID"]);
                SchoolYearSchoolClass.Nested = true;
                DataRelation SchoolClassSubject = dtst.Relations.Add("SchoolClassSubject", dtst.Tables["SchoolClass"].Columns["ID"], dtst.Tables["Subject"].Columns["CurrentSchoolClassID"]);
                SchoolClassSubject.Nested = true;

                dtst.WriteXml(file);
            }
        }
        
        /// <summary>
        /// Import subjects
        /// </summary>
        /// <param name="file">file to save exported subjects from</param>
        /// <returns>In-memory cache of the requested rows</returns>
        private string XmlGetAttributeOrNull(XmlNode node, string attribute)
        {
            string retval = null;
            if (node.Attributes[attribute] != null)
            {
                retval = node.Attributes[attribute].Value;
                if(string.IsNullOrEmpty(retval))
                {
                    retval = null;
                }
            }
            return retval;
        }
        public void ImportSubjects(string file, bool overwriteExisting)
        {
            XmlDocument data = new XmlDocument();
            data.Load(file);

            XmlNode dbData = data.SelectSingleNode("Data/DBData");
            if(dbData == null
                || XmlGetAttributeOrNull(dbData, "Version") != currentDBVersion.ToString())
            {
                //incorrect version
                return;
            }

            XmlNodeList schoolList = data.SelectNodes("Data/School");
            if(schoolList == null)
            {
                //no data
                return;
            }
            
            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                foreach (XmlNode school in schoolList)
                {
                    int schoolId = GetOrAddSchool(con, XmlGetAttributeOrNull(school, "Name"));

                    foreach(XmlNode schoolYear in school.ChildNodes)
                    {
                        int schoolYearId = GetOrAddSchoolYear(con, schoolId, XmlGetAttributeOrNull(schoolYear, "SchoolYear"));

                        foreach (XmlNode schoolClass in schoolYear.ChildNodes)
                        {
                            int schoolClassId = GetOrAddSchoolClass(con, schoolYearId, XmlGetAttributeOrNull(schoolClass, "SchoolClass"));

                            foreach (XmlNode subject in schoolClass.ChildNodes)
                            {
                                string UUID = XmlGetAttributeOrNull(subject, "UUID");

                                int? subjectId = null;
                                OleDbCommand cmd = new OleDbCommand("SELECT ID FROM Subject WHERE UUID ", con);
                                if (UUID == null)
                                {
                                    cmd.CommandText += "IS NULL";
                                }
                                else
                                {
                                    cmd.CommandText += "= @UUID";
                                    cmd.Parameters.AddWithValue("@UUID", UUID);
                                }

                                OleDbDataReader reader = cmd.ExecuteReader();
                                if (reader.HasRows)
                                {
                                    reader.Read();
                                    subjectId = (int)reader[0];
                                }
                                reader.Close();
                                cmd.Parameters.Clear();

                                if(subjectId != null
                                    && !overwriteExisting)
                                {
                                    continue;
                                }

                                string FirstName = XmlGetAttributeOrNull(subject, "FirstName");
                                string LastName = XmlGetAttributeOrNull(subject, "LastName");
                                string strBirthday = XmlGetAttributeOrNull(subject, "Birthday");
                                DateTime? Birthday = strBirthday == null ? (DateTime?) null : Convert.ToDateTime(strBirthday);
                                bool Gender = XmlGetAttributeOrNull(subject, "Gender") != "0";
                                string HomeAddress = XmlGetAttributeOrNull(subject, "HomeAddress");
                                string Postcode = XmlGetAttributeOrNull(subject, "Postcode");
                                string Phone = XmlGetAttributeOrNull(subject, "Phone");
                                string ParentDetails = XmlGetAttributeOrNull(subject, "ParentDetails");
                                string ParentContact = XmlGetAttributeOrNull(subject, "ParentContact");
                                string ParentEmail = XmlGetAttributeOrNull(subject, "ParentEmail");

                                if (subjectId == null)
                                {
                                    AddSubject(FirstName, LastName, Birthday, Gender, schoolClassId, HomeAddress, Postcode, Phone, ParentDetails, ParentContact, ParentEmail);
                                }
                                else
                                {
                                    EditSubject((int)subjectId, FirstName, LastName, Birthday, Gender, schoolClassId, HomeAddress, Postcode, Phone, ParentDetails, ParentContact, ParentEmail);
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion

        /// <summary>
        /// default path to the database file
        /// </summary>
        public const string DefaultDBPath = "Data/Database.mdb";

        #region internal
        //interal helpers

        /// <summary>
        /// path to the database file
        /// </summary>
        private string DbFile;

        /// <summary>
        /// connection string describing database
        /// </summary>
        private string Connection;

        /// <summary>
        /// Create the database file
        /// </summary>
        private void CreateDB()
        {
            // Use a late bound COM object to create a new catalog. This is so we avoid an interop assembly. 
            var catType = Type.GetTypeFromProgID("ADOX.Catalog");
            object o = Activator.CreateInstance(catType);
            catType.InvokeMember("Create", System.Reflection.BindingFlags.InvokeMethod, null, o, new object[] { Connection });

            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                //Subject table
                OleDbCommand Subject = new OleDbCommand("CREATE TABLE Subject ("
                                                        + "ID AUTOINCREMENT NOT NULL PRIMARY KEY,"
                                                        + "FirstName varchar(255),"
                                                        + "LastName varchar(255),"
                                                        + "Birthday DATE,"
                                                        + "Gender INTEGER,"
                                                        + "School varchar(255),"
                                                        + "SchoolYear INTEGER,"
                                                        + "SchoolClass varchar(255),"
                                                        + "HomeAddress varchar(255),"
                                                        + "Postcode varchar(255),"
                                                        + "Phone varchar(255),"
                                                        + "ParentDetails varchar(255),"
                                                        + "ParentContact varchar(255),"
                                                        + "ParentEmail varchar(255),"
                                                        + "UUID varchar(255)"
                                                        + ")", con);
                Subject.ExecuteNonQuery();

                //Activity table
                OleDbCommand Activity = new OleDbCommand("CREATE TABLE Activity ("
                                                        + "ID AUTOINCREMENT NOT NULL PRIMARY KEY,"
                                                        + "SubjectID INTEGER,"
                                                        + "ActivityDate DATETIME,"
                                                        + "ActivityType varchar(255),"
                                                        + "ClassifierRating FLOAT,"
                                                        + "HumanRating FLOAT,"
                                                        + "Notes LONGTEXT,"
                                                        + "IsValid INTEGER,"
                                                        + "Directory LONGTEXT,"
                                                        + "School varchar(255),"
                                                        + "SchoolYear INTEGER,"
                                                        + "FOREIGN KEY (SubjectID) REFERENCES Subject(ID)"
                                                        + ")", con);
                Activity.ExecuteNonQuery();
            }

            //Would be more efficient to create DB in final form
            //but this reduces the risk of differences between created and upgraded DBs
            UpgradeDB();
        }

        #region Upgrade
        private const int currentDBVersion = 3;
        /// <summary>
        /// Upgrade the database file
        /// </summary>
        private void UpgradeDB()
        {
            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                UpgradeDB_00(con);

                //check DB version
                OleDbCommand Version = new OleDbCommand("SELECT Version FROM DBData", con);
                int version = (int)Version.ExecuteScalar();

                if (version < 1)
                {
                    UpgradeDB_01(con);
                }

                if (version < 2)
                {
                    UpgradeDB_02(con);
                }

                if (version < 3)
                {
                    UpgradeDB_03(con);
                }

                //lastly, update version number
                OleDbCommand cmd = new OleDbCommand("UPDATE DBData SET Version = " + currentDBVersion, con);
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Upgrade the database file to support versioning
        /// </summary>
        private void UpgradeDB_00(OleDbConnection con)
        {
            //check for existance of Version table
            string[] restrictions = new string[4];
            restrictions[2] = "DBData";
            DataTable DBDataExists = con.GetSchema("Tables", restrictions);
            if (DBDataExists.Rows.Count == 0)
            {
                //doesn't exist
                OleDbCommand cmd = new OleDbCommand("CREATE TABLE DBData ("
                                                        + "Version INTEGER"
                                                        + ")", con);
                cmd.ExecuteNonQuery();

                cmd.CommandText = "INSERT INTO DBData VALUES (0)";
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Upgrade the database file to version 1
        /// </summary>
        private void UpgradeDB_01(OleDbConnection con)
        {
            //Session table
            {
                OleDbCommand cmd = new OleDbCommand("CREATE TABLE SessionInfo ("
                                                        + "ID AUTOINCREMENT NOT NULL PRIMARY KEY,"
                                                        + "SubjectID INTEGER,"
                                                        + "SessionDate DATETIME,"
                                                        + "SessionType varchar(255),"
                                                        + "School varchar(255),"
                                                        + "SchoolYear INTEGER,"
                                                        + "Completed INTEGER,"
                                                        + "Invalid INTEGER,"
                                                        + "CONSTRAINT FK_SessionInfo FOREIGN KEY (SubjectID) REFERENCES Subject(ID)"
                                                        + ")", con);
                cmd.ExecuteNonQuery();

                //Add SessionID field to Activity
                cmd.CommandText = "ALTER TABLE Activity ADD COLUMN SessionID INTEGER";
                cmd.ExecuteNonQuery();

                //Generate initial Sessions based on pre-existing Activities (if any)
                //Probably a way to do this with pure SQL
                DataSet dtst = new DataSet();
                cmd.CommandText = "SELECT * FROM Activity";
                OleDbDataAdapter adpt = new OleDbDataAdapter();
                adpt.SelectCommand = cmd;
                adpt.Fill(dtst);
                if (dtst.Tables[0].DefaultView.Count > 0)
                {
                    int SessionID = -1;

                    int prevSubjectID = -1;
                    DateTime prevActivityDate = DateTime.Today.AddYears(1);
                    string prevSchool = null;
                    int? prevSchoolYear = -1;

                    foreach (DataRowView row in dtst.Tables[0].DefaultView)
                    {
                        int SubjectID = (int)GetField<int>(row, "SubjectID");
                        DateTime ActivityDate = (DateTime)GetField<DateTime>(row, "ActivityDate");
                        string School = (string)GetField<string>(row, "School");
                        int? SchoolYear = (int?)GetField<int>(row, "SchoolYear");

                        if (SubjectID != prevSubjectID
                            || School != prevSchool
                            || SchoolYear != prevSchoolYear
                            || ActivityDate.Date != prevActivityDate.Date
                            )
                        {
                            cmd.CommandText = "INSERT INTO SessionInfo "
                                            + "([SubjectID], [SessionDate], [SessionType], [School], [SchoolYear], [Completed], [Invalid]) "
                                            + "VALUES (@SubjectID, @SessionDate, @SessionType, @School, @SchoolYear, @Completed, @Invalid)";

                            //populate parameters
                            //this should escapify strings appropriately
                            cmd.Parameters.AddWithValue("@SubjectID", SubjectID);
                            cmd.Parameters.AddWithValue("@SessionDate", DBNullifyString(ActivityDate.ToString()));
                            cmd.Parameters.AddWithValue("@SessionType", DBNullifyString("FMS"));
                            cmd.Parameters.AddWithValue("@School", DBNullifyString(School));
                            cmd.Parameters.AddWithValue("@SchoolYear", SchoolYear == null ? (object)DBNull.Value : (int)SchoolYear);
                            cmd.Parameters.AddWithValue("@Completed", 1);
                            cmd.Parameters.AddWithValue("@Invalid", 0);

                            cmd.ExecuteNonQuery();
                            cmd.Parameters.Clear();

                            //get id of row (AutoId) so that extra operations can be done on this
                            cmd.CommandText = "SELECT @@IDENTITY";
                            SessionID = (int)cmd.ExecuteScalar();

                            prevSubjectID = SubjectID;
                            prevActivityDate = ActivityDate;
                            prevSchool = School;
                            prevSchoolYear = SchoolYear;
                        }

                        cmd.CommandText = "UPDATE Activity SET SessionID = " + SessionID + " WHERE ID = " + (int)GetField<int>(row, "ID");
                        cmd.ExecuteNonQuery();
                    }
                }

                //remove foreign key constraint
                String[] restrictions = new string[] { null };
                DataTable schema = con.GetOleDbSchemaTable(OleDbSchemaGuid.Foreign_Keys, restrictions);
                foreach (DataRow row in schema.Rows)
                {
                    if (row["FK_TABLE_NAME"].ToString() == "Activity")
                    {
                        cmd.CommandText = "ALTER TABLE Activity DROP CONSTRAINT " + row["FK_NAME"].ToString();
                        cmd.ExecuteNonQuery();
                    }
                }

                //remove SubjectID field
                cmd.CommandText = "ALTER TABLE Activity DROP COLUMN SubjectID";
                cmd.ExecuteNonQuery();

                //remove School field
                cmd.CommandText = "ALTER TABLE Activity DROP COLUMN School";
                cmd.ExecuteNonQuery();

                //remove SchoolYear field
                cmd.CommandText = "ALTER TABLE Activity DROP COLUMN SchoolYear";
                cmd.ExecuteNonQuery();

                //add foreign key constraint
                cmd.CommandText = "ALTER TABLE Activity ADD CONSTRAINT FK_Activity FOREIGN KEY (SessionID) REFERENCES SessionInfo(ID)";
                cmd.ExecuteNonQuery();
            }

            //ActivityData[Float|Bool]
            {
                OleDbCommand cmd = new OleDbCommand("CREATE TABLE ActivityDataFloat ("
                                                        + "ID AUTOINCREMENT NOT NULL PRIMARY KEY,"
                                                        + "ActivityID INTEGER,"
                                                        + "Name varchar(255),"
                                                        + "Data FLOAT,"
                                                        + "CONSTRAINT FK_ActivityDataFloat FOREIGN KEY (ActivityID) REFERENCES Activity(ID)"
                                                        + ")", con);
                cmd.ExecuteNonQuery();

                cmd.CommandText = "CREATE TABLE ActivityDataBool ("
                                    + "ID AUTOINCREMENT NOT NULL PRIMARY KEY,"
                                    + "ActivityID INTEGER,"
                                    + "Name varchar(255),"
                                    + "Data INTEGER,"
                                    + "CONSTRAINT FK_ActivityDataBool FOREIGN KEY (ActivityID) REFERENCES Activity(ID)"
                                    + ")";
                cmd.ExecuteNonQuery();

                //Generate initial data based on pre-existing Activities (if any)
                //Probably a way to do this with pure SQL
                DataSet dtst = new DataSet();
                cmd.CommandText = "SELECT * FROM Activity";
                OleDbDataAdapter adpt = new OleDbDataAdapter();
                adpt.SelectCommand = cmd;
                adpt.Fill(dtst);
                if (dtst.Tables[0].DefaultView.Count > 0)
                {
                    foreach (DataRowView row in dtst.Tables[0].DefaultView)
                    {
                        int ActivityID = (int)GetField<int>(row, "ID");
                        var HumanRating = GetFieldDBNull<object>(row, "HumanRating");
                        var ClassifierRating = GetFieldDBNull<object>(row, "ClassifierRating");

                        cmd.CommandText = "INSERT INTO ActivityDataFloat "
                                        + "([ActivityID], [Name], [Data]) "
                                        + "VALUES (@ActivityID, @Name, @Data)";

                        //populate parameters
                        //this should escapify strings appropriately
                        cmd.Parameters.AddWithValue("@ActivityID", ActivityID);
                        cmd.Parameters.AddWithValue("@Name", "Classifier Rating");
                        cmd.Parameters.AddWithValue("@Data", ClassifierRating);

                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();

                        cmd.CommandText = "INSERT INTO ActivityDataFloat "
                                        + "([ActivityID], [Name], [Data]) "
                                        + "VALUES (@ActivityID, @Name, @Data)";

                        //populate parameters
                        //this should escapify strings appropriately
                        cmd.Parameters.AddWithValue("@ActivityID", ActivityID);
                        cmd.Parameters.AddWithValue("@Name", "Human Rating");
                        cmd.Parameters.AddWithValue("@Data", HumanRating);

                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }
                }

                //remove ClassifierRating field
                cmd.CommandText = "ALTER TABLE Activity DROP COLUMN ClassifierRating";
                cmd.ExecuteNonQuery();

                //remove HumanRating field
                cmd.CommandText = "ALTER TABLE Activity DROP COLUMN HumanRating";
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Upgrade the database file to version 2
        /// </summary>
        private int GetOrAddSchool(OleDbConnection con, string Name)
        {
            int? SchoolID = null;

            OleDbCommand cmd = new OleDbCommand("SELECT ID FROM School WHERE Name ", con);
            if (string.IsNullOrEmpty(Name))
            {
                cmd.CommandText += "IS NULL";
                Name = null;
            }
            else
            {
                cmd.CommandText += "= @Name";
                cmd.Parameters.AddWithValue("@Name", Name);
            }

            OleDbDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                SchoolID = (int)reader[0];
            }
            reader.Close();
            cmd.Parameters.Clear();

            if (SchoolID == null)
            {
                cmd.CommandText = "INSERT INTO School "
                                + "([Name]) "
                                + "VALUES (@Name)";

                //populate parameters
                //this should escapify strings appropriately
                cmd.Parameters.AddWithValue("@Name", DBNullifyString(Name));

                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                //get id of row (AutoId) so that extra operations can be done on this
                cmd.CommandText = "SELECT @@IDENTITY";
                SchoolID = (int)cmd.ExecuteScalar();
            }

            return (int)SchoolID;
        }
        private int GetOrAddSchoolYear(OleDbConnection con, int SchoolID, string SchoolYear)
        {
            int? SchoolYearID = null;

            OleDbCommand cmd = new OleDbCommand("SELECT ID FROM SchoolYear WHERE SchoolID = @SchoolID AND SchoolYear ", con);
            cmd.Parameters.AddWithValue("@SchoolID", SchoolID);
            if (string.IsNullOrEmpty(SchoolYear))
            {
                cmd.CommandText += "IS NULL";
                SchoolYear = null;
            }
            else
            {
                cmd.CommandText += "= @SchoolYear";
                cmd.Parameters.AddWithValue("@SchoolYear", SchoolYear);
            }

            OleDbDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                SchoolYearID = (int)reader[0];
            }
            reader.Close();
            cmd.Parameters.Clear();

            if (SchoolYearID == null)
            {
                cmd.CommandText = "INSERT INTO SchoolYear "
                                + "([SchoolID], [SchoolYear]) "
                                + "VALUES (@SchoolID, @SchoolYear)";

                //populate parameters
                //this should escapify strings appropriately
                cmd.Parameters.AddWithValue("@SchoolID", SchoolID);
                cmd.Parameters.AddWithValue("@SchoolYear", DBNullifyString(SchoolYear));

                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                //get id of row (AutoId) so that extra operations can be done on this
                cmd.CommandText = "SELECT @@IDENTITY";
                SchoolYearID = (int)cmd.ExecuteScalar();
            }

            return (int)SchoolYearID;
        }
        private int GetOrAddSchoolClass(OleDbConnection con, int SchoolYearID, string SchoolClass)
        {
            int? SchoolClassID = null;

            OleDbCommand cmd = new OleDbCommand("SELECT ID FROM SchoolCLass WHERE SchoolYearID = @SchoolYearID AND SchoolClass ", con);
            cmd.Parameters.AddWithValue("@SchoolYearID", SchoolYearID);
            if (string.IsNullOrEmpty(SchoolClass))
            {
                cmd.CommandText += "IS NULL";
                SchoolClass = null;
            }
            else
            {
                cmd.CommandText += "= @SchoolClass";
                cmd.Parameters.AddWithValue("@SchoolClass", SchoolClass);
            }

            OleDbDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                SchoolClassID = (int)reader[0];
            }
            reader.Close();
            cmd.Parameters.Clear();

            if (SchoolClassID == null)
            {
                cmd.CommandText = "INSERT INTO SchoolClass "
                                + "([SchoolYearID], [SchoolClass]) "
                                + "VALUES (@SchoolYearID, @SchoolClass)";

                //populate parameters
                //this should escapify strings appropriately
                cmd.Parameters.AddWithValue("@SchoolYearID", SchoolYearID);
                cmd.Parameters.AddWithValue("@SchoolClass", DBNullifyString(SchoolClass));

                cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();

                //get id of row (AutoId) so that extra operations can be done on this
                cmd.CommandText = "SELECT @@IDENTITY";
                SchoolClassID = (int)cmd.ExecuteScalar();
            }

            return (int)SchoolClassID;
        }
        private void UpgradeDB_02(OleDbConnection con)
        {
            //Merge ActivityData* tables
            {
                OleDbCommand cmd = new OleDbCommand("SELECT * INTO ActivityData FROM ActivityDataFloat", con);
                cmd.ExecuteNonQuery();
                //constraints aren't auto created
                cmd.CommandText = "ALTER TABLE ActivityData ADD CONSTRAINT FK_ActivityData FOREIGN KEY (ActivityID) REFERENCES Activity(ID)";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "DROP TABLE ActivityDataFloat";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "ALTER TABLE ActivityData ADD COLUMN FloatData FLOAT";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "UPDATE ActivityData SET FloatData = Data";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "ALTER TABLE ActivityData DROP COLUMN Data";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "ALTER TABLE ActivityData ADD COLUMN BoolData INTEGER";
                cmd.ExecuteNonQuery();

                //Generate initial data based on pre-existing Activities (if any)
                //Probably a way to do this with pure SQL
                DataSet dtst = new DataSet();
                cmd.CommandText = "SELECT * FROM ActivityDataBool";
                OleDbDataAdapter adpt = new OleDbDataAdapter();
                adpt.SelectCommand = cmd;
                adpt.Fill(dtst);
                if (dtst.Tables[0].DefaultView.Count > 0)
                {
                    foreach (DataRowView row in dtst.Tables[0].DefaultView)
                    {
                        int ActivityID = (int)GetField<int>(row, "ActivityID");
                        var Name = GetFieldDBNull<object>(row, "Name");
                        var Data = GetFieldDBNull<object>(row, "Data");

                        cmd.CommandText = "INSERT INTO ActivityData "
                                        + "([ActivityID], [Name], [BoolData]) "
                                        + "VALUES (@ActivityID, @Name, @BoolData)";

                        //populate parameters
                        //this should escapify strings appropriately
                        cmd.Parameters.AddWithValue("@ActivityID", ActivityID);
                        cmd.Parameters.AddWithValue("@Name", Name);
                        cmd.Parameters.AddWithValue("@BoolData", Data);

                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }
                }

                cmd.CommandText = "DROP TABLE ActivityDataBool";
                cmd.ExecuteNonQuery();
            }

            //split out School info
            {
                OleDbCommand cmd = new OleDbCommand("CREATE TABLE School ("
                                                        + "ID AUTOINCREMENT NOT NULL PRIMARY KEY,"
                                                        + "Name varchar(255)"
                                                        + ")", con);
                cmd.ExecuteNonQuery();

                cmd.CommandText = "CREATE TABLE SchoolYear ("
                                + "ID AUTOINCREMENT NOT NULL PRIMARY KEY,"
                                + "SchoolID INTEGER,"
                                + "SchoolYear INTEGER,"
                                + "CONSTRAINT FK_SchoolYear FOREIGN KEY (SchoolID) REFERENCES School(ID)"
                                + ")";
                cmd.ExecuteNonQuery();

                cmd.CommandText = "CREATE TABLE SchoolClass ("
                                + "ID AUTOINCREMENT NOT NULL PRIMARY KEY,"
                                + "SchoolYearID INTEGER,"
                                + "SchoolClass varchar(255),"
                                + "CONSTRAINT FK_SchoolClass FOREIGN KEY (SchoolYearID) REFERENCES SchoolYear(ID)"
                                + ")";
                cmd.ExecuteNonQuery();

                //Add SchoolClassID field to Subject
                cmd.CommandText = "ALTER TABLE Subject ADD COLUMN CurrentSchoolClassID INTEGER";
                cmd.ExecuteNonQuery();
                //add foreign key constraint
                cmd.CommandText = "ALTER TABLE Subject ADD CONSTRAINT FK_Subject FOREIGN KEY (CurrentSchoolClassID) REFERENCES SchoolClass(ID)";
                cmd.ExecuteNonQuery();

                //Add SchoolClassID field to Session
                cmd.CommandText = "ALTER TABLE SessionInfo ADD COLUMN SchoolClassID INTEGER";
                cmd.ExecuteNonQuery();
                //add foreign key constraint
                cmd.CommandText = "ALTER TABLE SessionInfo ADD CONSTRAINT FK_SessionInfo2 FOREIGN KEY (SchoolClassID) REFERENCES SchoolClass(ID)";
                cmd.ExecuteNonQuery();

                //Generate initial data based on pre-existing data (if any)
                //Probably a way to do this with pure SQL
                DataSet dtst = new DataSet();
                cmd.CommandText = "SELECT * FROM Subject";
                OleDbDataAdapter adpt = new OleDbDataAdapter();
                adpt.SelectCommand = cmd;
                adpt.Fill(dtst);
                foreach (DataRowView row in dtst.Tables[0].DefaultView)
                {
                    int ID = (int)GetField<int>(row, "ID");
                    string Name = (string)GetField<string>(row, "School");
                    string SchoolYear = ((int?)GetField<int>(row, "SchoolYear")).ToString();
                    string SchoolClass = (string)GetField<string>(row, "SchoolClass");

                    //add school if not already present
                    int SchoolID = GetOrAddSchool(con, Name);
                    //add year if not already present
                    int SchoolYearID = GetOrAddSchoolYear(con, SchoolID, SchoolYear);
                    //add class if not already present
                    int SchoolClassID = GetOrAddSchoolClass(con, SchoolYearID, SchoolClass);

                    cmd.CommandText = "UPDATE Subject SET CurrentSchoolClassID = @SchoolClassID WHERE ID = @ID";
                    cmd.Parameters.AddWithValue("@SchoolClassID", SchoolClassID);
                    cmd.Parameters.AddWithValue("@ID", ID);
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();

                    DataSet dtst2 = new DataSet();
                    cmd.CommandText = "SELECT * FROM SessionInfo WHERE SubjectID = @ID";
                    cmd.Parameters.AddWithValue("@ID", ID);
                    OleDbDataAdapter adpt2 = new OleDbDataAdapter();
                    adpt2.SelectCommand = cmd;
                    adpt2.Fill(dtst2);
                    cmd.Parameters.Clear();
                    foreach (DataRowView session in dtst2.Tables[0].DefaultView)
                    {
                        int sessionID = (int)GetField<int>(session, "ID");
                        string sessionName = (string)GetField<string>(session, "School");
                        string sessionSchoolYear = ((int?)GetField<int>(session, "SchoolYear")).ToString();

                        //add school if not already present
                        int sessionSchoolID = GetOrAddSchool(con, sessionName);
                        //add year if not already present
                        int sessionSchoolYearID = GetOrAddSchoolYear(con, sessionSchoolID, sessionSchoolYear);
                        //if session SchoolYearID matches that of the subject, assume same class
                        //otherwise add class if not already present
                        int sessionSchoolClassID = SchoolClassID;
                        if (sessionSchoolYearID != SchoolYearID)
                        {
                            sessionSchoolClassID = GetOrAddSchoolClass(con, sessionSchoolYearID, null);
                        }

                        cmd.CommandText = "UPDATE SessionInfo SET SchoolClassID = @SchoolClassID WHERE ID = @ID";
                        cmd.Parameters.AddWithValue("@SchoolClassID", sessionSchoolClassID);
                        cmd.Parameters.AddWithValue("@ID", sessionID);
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }
                }
                
                //Drop unneeded columns
                cmd.CommandText = "ALTER TABLE Subject DROP COLUMN School";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "ALTER TABLE Subject DROP COLUMN SchoolYear";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "ALTER TABLE Subject DROP COLUMN SchoolClass";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "ALTER TABLE SessionInfo DROP COLUMN School";
                cmd.ExecuteNonQuery();
                cmd.CommandText = "ALTER TABLE SessionInfo DROP COLUMN SchoolYear";
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Upgrade the database file to version 3
        /// </summary>
        private void UpgradeDB_03(OleDbConnection con)
        {
            //Convert SchoolYear:SchoolYear to a string
            {
                OleDbCommand cmd = new OleDbCommand("ALTER TABLE SchoolYear ALTER COLUMN SchoolYear varchar(255)", con);
                cmd.ExecuteNonQuery();
            }
        }
        #endregion

        #region Accessors
        /// <summary>
        /// Add an entry to the 'Activity' table
        /// </summary>
        /// <param name="tableName">Table to fill returned cache</param>
        /// <returns>In-memory cache of 'tableName' in database</returns>
        private DataView GetTable(string tableName)
        {
            DataView retval = null;

            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();
                
                OleDbDataAdapter adpt = new OleDbDataAdapter("SELECT * FROM " + tableName, con);

                DataSet dtst = new DataSet();
                adpt.Fill(dtst);

                retval = dtst.Tables[0].DefaultView;

                if(retval.Count == 0)
                {
                    retval = null;
                }
            }

            return retval;
        }

        /// <summary>
        /// Get row from a table
        /// </summary>
        /// <param name="tableName">tablename of row being requested</param>
        /// <param name="rowId">Id of the row being requested</param>
        /// <returns>In-memory cache of the requested row</returns>
        private DataRowView GetRow(string tableName, int rowId)
        {
            DataRowView retval = null;

            //close/dispose of connection automatically on failure
            using (OleDbConnection con = new OleDbConnection(Connection))
            {
                con.Open();

                OleDbDataAdapter adpt = new OleDbDataAdapter("SELECT * FROM " + tableName + " WHERE ID=" + rowId, con);
                DataSet dtst = new DataSet();

                adpt.Fill(dtst);

                retval = dtst.Tables[0].DefaultView[0];
            }

            return retval;
        }

        /// <summary>
        /// Get a field by name and type
        /// </summary>
        /// <param name="row">Row to get field value from, recieved as object to decouple caller</param>
        /// <param name="columnName">Name of column to get value from</param>
        /// <returns>Nullable object of the value of the requested field</returns>
        private static object GetField<T>(object row, string columnName)
        {
            object retval = GetFieldDBNull<T>(row, columnName);

            //convert to null as appropriate
            if (retval == DBNull.Value)
            {
                return null;
            }
            return retval;
        }

        /// <summary>
        /// Helper to get a field by name and type, accounting for nullabale fields
        /// </summary>
        /// <param name="row">Row to get field value from, recieved as object to decouple caller</param>
        /// <param name="columnName">Name of column to get value from</param>
        /// <returns>Object of the value of the requested field, DBNull if field is null</returns>
        private static object GetFieldDBNull<T>(object row, string columnName)
        {
            DataRowView r = (DataRowView)row;
            if (r.Row.IsNull(columnName))
            {
                return DBNull.Value;
            }
            return r.Row.Field<T>(columnName);
        }
        #endregion

        #region Conversion
        /// <summary>
        /// Helper to convert from DateTime to string, accounting for null
        /// </summary>
        /// <param name="date">Nullable DateTime to convert</param>
        /// <returns>Sortable string of date</returns>
        private static string DateTimeToStr(DateTime? date)
        {
            if(date == null)
            {
                return null;
            }
            else
            {
                //Year Month Day order for sortability
                return ((DateTime)date).ToString("yyyyMMdd");
            }
        }

        /// <summary>
        /// Helper to convert a string to DBNull as appropriate (ie. null or whitespace)
        /// </summary>
        /// <param name="text">String to convert</param>
        /// <returns>Converted string</returns>
        private static object DBNullifyString(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return DBNull.Value;
            }
            return text;
        }

        /// <summary>
        /// Helper to convert a string to Int32
        /// </summary>
        /// <param name="text">String to convert</param>
        /// <returns>Converted string, DBNull if invalid conversion</returns>
        private static object StrToInt(string text)
        {
            if (DBNullifyString(text) == DBNull.Value)
            {
                return DBNull.Value;
            }
            return Convert.ToInt32(text);
        }

        /// <summary>
        /// Helper to convert a string to Single
        /// </summary>
        /// <param name="text">String to convert</param>
        /// <returns>Converted string, DBNull if invalid conversion</returns>
        private static object StrToSingle(string text)
        {
            if (DBNullifyString(text) == DBNull.Value)
            {
                return DBNull.Value;
            }
            return Convert.ToSingle(text);
        }

        /// <summary>
        /// Helper to convert a bool to database format
        /// </summary>
        /// <param name="check">bool to convert</param>
        /// <returns>Converted bool, DBNull if null</returns>
        private static object BoolToDB(bool? check)
        {
            if(check == null)
            {
                return DBNull.Value;
            }
            return (bool)check ? 1 : 0;
        }
        #endregion
        #endregion
    }
}
