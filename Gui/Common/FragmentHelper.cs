﻿using System;
using System.Linq;

namespace Common
{
    static public class FragmentHelper
    {
        static public char Separator = '¿';
        static public string GetFragmentName(string fragment)
        {
            if (fragment == null)
            {
                return null;
            }

            string[] subFragments = fragment.Split(new char[] { Separator }, StringSplitOptions.None);

            if (subFragments.Length == 0)
            {
                return fragment;
            }
            else
            {
                return subFragments[0];
            }
        }

        static public string[] GetFragments(string fragment)
        {
            string[] retval = new string[0];

            if(fragment == null)
            {
                return retval;
            }

            string[] subFragments = fragment.Split(new char[] { Separator }, StringSplitOptions.None);

            if (subFragments.Length < 2)
            {
                return retval;
            }
            else
            {
                return subFragments.Skip(1).ToArray();
            }
        }
    }
}
