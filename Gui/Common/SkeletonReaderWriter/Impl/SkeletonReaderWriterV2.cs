﻿using System;
using System.IO;
using Microsoft.Kinect;

namespace SkeletonReaderWriter.Impl
{
    // Implementation of skeleton reader and writer for V2 of the save format
    //
    // Changes from V1:
    // * Added start timestamp to header
    // * Added ColorImage mapping for each joint
    //
    // V2 format:
    //
    // Header
    // Skeleton Frame
    // Skeleton Frame
    // ...
    // :EoF:
    //
    // ~~~~~~~~~~~~~~~~~~~~~~
    // Header (11 bytes)
    // ~~~~~~~~~~~~~~~~~~~~~~
    // byte: Version Number (always 0x02)
    // byte: Number of Joints per skeleton frame (KinectV1: always 20)
    // byte: Number of Orientations per skeleton frame (KinectV1: always 20)
    // long: Number of 100-nanosecond intervals that have elapsed since January 1, 0001 at 00:00:00.000 in the Gregorian calendar
    //
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Skeleton Frame (KinectV1: 648 bytes)
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // long: Number of 100-nanosecond intervals that have elapsed since January 1, 0001 at 00:00:00.000 in the Gregorian calendar
    // : Joints (22 bytes each, KinectV1 total: 440 bytes)
    //     byte : Joint id (see Microsoft.Kinect.JointType)
    //     float: X position
    //     float: Y position
    //     float: Z position
    //     byte : Tracking state of joint
    //     int  : X position of joint on color playback
    //     int  : Y position of joint on color playback
    // : Orientations (18 bytes each, KinectV1 total: 360 bytes)
    //     byte : Starting joint id (see Microsoft.Kinect.JointType)
    //     byte : Ending joint id (see Microsoft.Kinect.JointType)
    //     float: X component of Quaternion
    //     float: Y component of Quaternion
    //     float: Z component of Quaternion
    //     float: W component of Quaternion
    //

    
    /// <summary>
    /// Implementation of skeleton reader for V2 of the save format
    /// </summary>
    public class SkeletonReaderV2 : ISkeletonReaderImpl
    {
        public SkeletonReaderV2(byte[] data)
        {
            if(!IsSupported(data))
            {
                throw new InvalidDataException();
            }

            bReader = new BinaryReader(new MemoryStream(data));

            //cache values stored in the header
            bReader.BaseStream.Seek(0, SeekOrigin.Begin);

            bReader.ReadByte();//version
            NumJoints = bReader.ReadByte();
            NumOrientations = bReader.ReadByte();
            InitialTimeStamp = bReader.ReadInt64();
        }

        /// <summary>
        /// Whether this Reader is able to process the data stream
        /// </summary>
        /// <param name="data">skeleton data to read</param>
        public static bool IsSupported(byte[] data)
        {
            return data != null                     //has data
                    && data.Length >= HeaderSize    //has at least the header
                    && data[0] == 2;                //is V2
        }

        /// <summary>
        /// Get the initial timestamp of the data
        /// </summary>
        public override long? InitialTimestamp()
        {
            return InitialTimeStamp;
        }

        /// <summary>
        /// Number of frames of data stored in the data stream
        /// </summary>
        public override uint NumberOfSavedFrames()
        {
            return ((uint)bReader.BaseStream.Length - HeaderSize) / FrameSize;
        }

        /// <summary>
        /// Get a specific frame from the data stream
        /// </summary>
        /// <param name="index">index of frame to get, 0 being the first frame</param>
        /// <returns>requested frame if valid, null otherwise</returns>
        public override SkeletonReader.SkeletonFrameData GetFrame(uint index)
        {
            if (index >= NumberOfSavedFrames())
            {
                return null;
            }

            SkeletonReader.SkeletonFrameData retval = new SkeletonReader.SkeletonFrameData();

            //make sure we are reading from the correct location
            bReader.BaseStream.Seek(HeaderSize + (index * FrameSize), SeekOrigin.Begin);

            //timestamp
            retval.timestamp = bReader.ReadInt64();

            //joints
            retval.joints = new SkeletonReader.SkeletonFrameJoint[NumJoints];
            for (int i = 0; i < NumJoints; i++ )
            {
                retval.joints[i] = new SkeletonReader.SkeletonFrameJoint();

                //type
                retval.joints[i].type = (JointType) bReader.ReadByte();

                //position
                retval.joints[i].X = bReader.ReadSingle();
                retval.joints[i].Y = bReader.ReadSingle();
                retval.joints[i].Z = bReader.ReadSingle();

                //tracking state
                retval.joints[i].trackingState = (JointTrackingState) bReader.ReadByte();

                //mapped points
                retval.joints[i].ColorPosSupported = true;
                retval.joints[i].ColourX = bReader.ReadInt32();
                retval.joints[i].ColourY = bReader.ReadInt32();
            }

            //orientations
            retval.orientations = new SkeletonReader.SkeletonFrameOrientation[NumOrientations];
            for (int i = 0; i < NumOrientations; i++)
            {
                retval.orientations[i] = new SkeletonReader.SkeletonFrameOrientation();

                //start / end joint
                retval.orientations[i].startJoint = (JointType)bReader.ReadByte();
                retval.orientations[i].endJoint = (JointType)bReader.ReadByte();

                //Quaternion
                retval.orientations[i].X = bReader.ReadSingle();
                retval.orientations[i].Y = bReader.ReadSingle();
                retval.orientations[i].Z = bReader.ReadSingle();
                retval.orientations[i].W = bReader.ReadSingle();
            }
            
            return retval;
        }

        #region internal
        /// <summary>
        /// Size of the header, in bytes
        /// </summary>
        private const uint HeaderSize = 11;

        /// <summary>
        /// Size of each joint, in bytes
        /// </summary>
        private const uint JointFrameSize = (5 * sizeof(float)) + 2;

        /// <summary>
        /// Size of each orientation, in bytes
        /// </summary>
        private const uint OrientationFrameSize = (4 * sizeof(float)) + 2;

        /// <summary>
        /// Number of joints per frame
        /// </summary>
        private uint NumJoints;

        /// <summary>
        /// Number of orientations per frame
        /// </summary>
        private uint NumOrientations;

        /// <summary>
        /// Initial time stamp to help align streams (when first couple of skeleton frames are dropped for not existing)
        /// </summary>
        private long InitialTimeStamp;

        /// <summary>
        /// Size of each frame, in bytes
        /// </summary>
        private uint FrameSize
        {
            get { return sizeof(long) + (NumJoints * JointFrameSize) + (NumOrientations * OrientationFrameSize); }
        }

        /// <summary>
        /// Helper class to read the data stream
        /// </summary>
        private BinaryReader bReader = null;
        #endregion
    }

    /// <summary>
    /// Implementation of skeleton writer for V1 of the save format
    /// </summary>
    public class SkeletonWriterV2 : ISkeletonWriterImpl
    {
        /// <summary>
        /// Helper class to write the data into the cache
        /// </summary>
        private BinaryWriter bWriter = null;

        public SkeletonWriterV2(Stream stream)
        {
            bWriter = new BinaryWriter(stream);
        }

        /// <summary>
        /// Write save format header
        /// </summary>
        public override void WriteHeader()
        {
            //Save format version number
            bWriter.Write((byte)2);

            //Number of joints
            //allows adding / removing without changing version number
            //Hard coded for now, but should always match the number of joints actually written
            bWriter.Write((byte)20);

            //Number of orientations
            //allows adding / removing without changing version number
            //Hard coded for now, but should always match the number of orientations actually written
            bWriter.Write((byte)20);

            //time stamp
            bWriter.Write(DateTime.Now.Ticks);

            //make sure that data gets written
            //important for when file based write caches delay writing changes
            bWriter.Flush();
        }
        
        /// <summary>
        /// Write skeleton information
        /// </summary>
        /// <param name="skeleton">skeleton data to record</param>
        public override void WriteFrame(KinectSensor sensor, Skeleton skeleton)
        {
            //time stamp
            bWriter.Write(DateTime.Now.Ticks);

            //joints
            foreach (Joint i in skeleton.Joints)
            {
                //store the joint type, to allow adding / removing / reordering joints without changing the version number
                bWriter.Write((byte)i.JointType);

                //joint position
                bWriter.Write(i.Position.X);
                bWriter.Write(i.Position.Y);
                bWriter.Write(i.Position.Z);

                //joint tracking state
                bWriter.Write((byte)i.TrackingState);

                ColorImagePoint color = sensor.CoordinateMapper.MapSkeletonPointToColorPoint(i.Position, ColorImageFormat.RgbResolution640x480Fps30);
                bWriter.Write(color.X);
                bWriter.Write(color.Y);
            }

            //relative orientations
            foreach (BoneOrientation orientation in skeleton.BoneOrientations)
            {
                //store the joint types, to allow adding / removing / reordering joints without changing the version number
                bWriter.Write((byte)orientation.StartJoint);
                bWriter.Write((byte)orientation.EndJoint);

                //orientation as quaternion
                bWriter.Write(orientation.HierarchicalRotation.Quaternion.X);
                bWriter.Write(orientation.HierarchicalRotation.Quaternion.Y);
                bWriter.Write(orientation.HierarchicalRotation.Quaternion.Z);
                bWriter.Write(orientation.HierarchicalRotation.Quaternion.W);
            }

            //make sure that data gets written
            //important for when file based write caches delay writing changes
            bWriter.Flush();
        }
    }
}
