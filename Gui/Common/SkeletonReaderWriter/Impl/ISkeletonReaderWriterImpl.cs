﻿using Microsoft.Kinect;

namespace SkeletonReaderWriter.Impl
{
    /// <summary>
    /// Common interface to implementations of the skeleton reader
    /// </summary>
    public abstract class ISkeletonReaderImpl
    {
        /// <summary>
        /// Get the initial timestamp of the data
        /// </summary>
        public abstract long? InitialTimestamp();

        /// <summary>
        /// Get number of saved frames
        /// </summary>
        public abstract uint NumberOfSavedFrames();

        /// <summary>
        /// Get skeleton data for a specific saved frame
        /// </summary>
        public abstract SkeletonReader.SkeletonFrameData GetFrame(uint index);
    }

    /// <summary>
    /// Common interface to implementations of the skeleton writer
    /// </summary>
    public abstract class ISkeletonWriterImpl
    {
        /// <summary>
        /// Write header information about the save format
        /// Should be able to identify save format using this
        /// </summary>
        public abstract void WriteHeader();

        /// <summary>
        /// Write skeleton frame
        /// </summary>
        /// <param name="sensor">sensor the skeleton came from</param>
        /// <param name="skeleton">skeleton data to write</param>
        public abstract void WriteFrame(KinectSensor sensor, Skeleton skeleton);
    }
}
