﻿
namespace SkeletonReaderWriter.Impl
{
    /// <summary>
    /// Implementation of a dummy skeleton reader
    /// Should appear no different to the output of a valid file that has no frame data
    /// </summary>
    public class SkeletonReaderDummy : ISkeletonReaderImpl
    {
        public SkeletonReaderDummy(byte[] data)
        {
        }

        /// <summary>
        /// Whether this Reader is able to process the data stream
        /// </summary>
        /// <param name="data">skeleton data to read</param>
        /// <returns>always true</returns>
        public static bool IsSupported(byte[] data)
        {
            return true;
        }

        /// <summary>
        /// Get the initial timestamp of the data
        /// </summary>
        public override long? InitialTimestamp()
        {
            return null;
        }

        /// <summary>
        /// Number of frames of data stored in the data stream
        /// </summary>
        /// <returns>always 0</returns>
        public override uint NumberOfSavedFrames()
        {
            return 0;
        }

        /// <summary>
        /// Get a specific frame from the data stream
        /// </summary>
        /// <param name="index">index of frame to get, 0 being the first frame</param>
        /// <returns>always null</returns>
        public override SkeletonReader.SkeletonFrameData GetFrame(uint index)
        {
            return null;
        }
    }
}
