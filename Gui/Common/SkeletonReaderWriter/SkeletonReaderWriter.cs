﻿using System.IO;
using Microsoft.Kinect;

namespace SkeletonReaderWriter
{
    /// <summary>
    /// Wrapper class to abstract away which save format was actually used
    /// </summary>
    public class SkeletonReader
    {
        public class SkeletonFrameData
        {
            public long timestamp;
            public SkeletonFrameJoint[] joints;
            public SkeletonFrameOrientation[] orientations;
        }
        public class SkeletonFrameJoint
        {
            public JointType type;
            public float X;
            public float Y;
            public float Z;
            public JointTrackingState trackingState;

            public bool ColorPosSupported = false;
            public int ColourX = int.MinValue;
            public int ColourY = int.MinValue;
        }
        public class SkeletonFrameOrientation
        {
            public JointType startJoint;
            public JointType endJoint;
            public float X;
            public float Y;
            public float Z;
            public float W;
        }

        /// <summary>
        /// Reader object being used
        /// </summary>
        private Impl.ISkeletonReaderImpl reader = null;

        /// <summary>
        /// common constructor
        /// </summary>
        private void construct(byte[] data)
        {
            //Determine which reader implementation to use
            if (Impl.SkeletonReaderV2.IsSupported(data))
            {
                reader = new Impl.SkeletonReaderV2(data);
            }

            else if (Impl.SkeletonReaderV1.IsSupported(data))
            {
                reader = new Impl.SkeletonReaderV1(data);
            }

            //none found, so create a dummy reader
            else
            {
                reader = new Impl.SkeletonReaderDummy(data);
            }
        }

        /// <summary>
        /// construct from file
        /// </summary>
        public SkeletonReader(string path)
        {
            byte[] fileContents = null;

            if (File.Exists(path))
            {
                using (FileStream file = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    fileContents = new byte[file.Length];
                    file.Read(fileContents, 0, fileContents.Length);
                }
            }

            construct(fileContents);
        }

        /// <summary>
        /// construct from byte array
        /// </summary>
        public SkeletonReader(byte[] data)
        {
            construct(data);
        }

        /// <summary>
        /// Get the initial timestamp of the data
        /// </summary>
        public long? InitialTimestamp()
        {
            return reader.InitialTimestamp();
        }

        /// <summary>
        /// Get number of saved frames
        /// </summary>
        public uint NumberOfSavedFrames()
        {
            return reader.NumberOfSavedFrames();
        }

        /// <summary>
        /// Get skeleton data for a specific saved frame
        /// </summary>
        /// <param name="index">index of frame to get, 0 being the first frame</param>
        public SkeletonFrameData GetFrame(uint index)
        {
            return reader.GetFrame(index);
        }

        /// <summary>
        /// Options for what to return from GetFrameBy* if an exact match is not found
        /// </summary>
        public enum SearchTerm
        {
            Nearest,
            FirstAfter,
            LastBefore
        }

        /// <summary>
        /// Get skeleton data using a timestamp
        /// </summary>
        /// <param name="timestamp">timestamp of frame to get</param>
        /// <param name="term">what to return if exact match not found</param>
        public SkeletonFrameData GetFrameByTimeStamp(long timestamp, SearchTerm term)
        {
            //no elements to search
            if(NumberOfSavedFrames() == 0)
            {
                return null;
            }

            uint first = 0;
            uint last = NumberOfSavedFrames() - 1;

            uint mid = first;
            SkeletonFrameData frame = null;

            //Use a binary search to find the frame by timestamp
            while (first <= last)
            {
                //get mid point
                mid = ((last - first) / 2) + first;

                frame = GetFrame(mid);

                if (timestamp < frame.timestamp)
                {
                    //avoid underflow
                    if (mid == uint.MinValue)
                    {
                        break;
                    }
                    last = mid - 1;
                }
                else if (timestamp > frame.timestamp)
                {
                    //avoid overflow
                    if (mid == uint.MaxValue)
                    {
                        break;
                    }
                    first = mid + 1;
                }
                else
                {
                    //exact match found
                    return frame;
                }
            }

            //refine search based on SearchTerm
            SkeletonFrameData before = frame;
            SkeletonFrameData after = before;

            if(after.timestamp < timestamp)
            {
                after = GetFrame(mid + 1);
            }
            else //before.timestamp > timestamp
            {
                before = GetFrame(mid - 1);
                mid = mid - 1;
            }

            //account for first / last frame
            if(before == null
                || after == null)
            {
                return null;
            }

            if(term == SearchTerm.FirstAfter)
            {
                return after;
            }

            if(term == SearchTerm.LastBefore)
            {
                return before;
            }

            //Default to Nearest
            long beforeDiff = before.timestamp - timestamp;
            long afterDiff = timestamp - after.timestamp;

            return (beforeDiff < afterDiff) ? before : after;
        }
    }

    /// <summary>
    /// Wrapper class to abstract away selection of writer being used
    /// </summary>
    public class SkeletonWriter
    {
        /// <summary>
        /// Writer object being used
        /// </summary>
        private Impl.ISkeletonWriterImpl writer = null;

        public SkeletonWriter(Stream stream)
        {
            writer = new Impl.SkeletonWriterV2(stream);
        }

        /// <summary>
        /// Write save format header
        /// </summary>
        public void WriteHeader()
        {
            writer.WriteHeader();
        }

        /// <summary>
        /// Write skeleton information
        /// </summary>
        /// <param name="sensor">sensor the skeleton came from</param>
        /// <param name="skeleton">skeleton data to record</param>
        public void WriteFrame(KinectSensor sensor, Skeleton skeleton)
        {
            writer.WriteFrame(sensor, skeleton);
        }
    }
}
