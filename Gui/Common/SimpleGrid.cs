﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Common
{
    /// <summary>
    /// Collection to contain the grid lengths
    /// </summary>
    [TypeConverter(typeof(GridLengthCollectionConverter))]
    public class GridLengthCollection : ReadOnlyCollection<GridLength>
    {
        public GridLengthCollection(IList<GridLength> lengths)
            : base(lengths)
        {
        }
    }

    /// <summary>
    /// Converter helper to allow specifying grid length collection as a string of comma separated values
    /// </summary>
    public class GridLengthCollectionConverter : TypeConverter
    {
        /// <summary>
        /// Whether we can convert from a string to a grid length collection
        /// </summary>
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
                return true;
            return base.CanConvertFrom(context, sourceType);
        }

        /// <summary>
        /// Whether we can convert from a grid length collection to a string
        /// </summary>
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(string))
                return true;
            return base.CanConvertTo(context, destinationType);
        }

        /// <summary>
        /// Convert from a string to a grid length collection
        /// </summary>
        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            string s = value as string;
            if (s != null)
                return ParseString(s, culture);
            return base.ConvertFrom(context, culture, value);
        }

        /// <summary>
        /// Convert from a grid length collection to a string
        /// </summary>
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string) && value is GridLengthCollection)
                return ToString((GridLengthCollection)value, culture);
            return base.ConvertTo(context, culture, value, destinationType);
        }

        /// <summary>
        /// Do actual convertion from a grid length collection to a string
        /// </summary>
        private string ToString(GridLengthCollection value, CultureInfo culture)
        {
            var converter = new GridLengthConverter();
            return string.Join(",", value.Select(v => converter.ConvertToString(v)));
        }

        /// <summary>
        /// Do actual convertion from a string to a grid length collection
        /// </summary>
        private GridLengthCollection ParseString(string s, CultureInfo culture)
        {
            var converter = new GridLengthConverter();
            var lengths = s.Split(',').Select(p => (GridLength)converter.ConvertFromString(p.Trim()));
            return new GridLengthCollection(lengths.ToArray());
        }
    }

    /// <summary>
    /// Custom control that allows row and column definitions to be inlined with a comma separated string
    /// </summary>
    public class SimpleGrid : Grid
    {
        /// <summary>
        /// Row definitions
        /// </summary>
        public GridLengthCollection Rows
        {
            get { return (GridLengthCollection)GetValue(RowsProperty); }
            set { SetValue(RowsProperty, value); }
        }
        public static readonly DependencyProperty RowsProperty = DependencyProperty.Register("Rows", typeof(GridLengthCollection), typeof(SimpleGrid), new PropertyMetadata(null, OnRowsChanged));

        private static void OnRowsChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            //sender should be this class
            var grid = sender as SimpleGrid;

            //if not, ignore it
            if (null == grid)
            {
                return;
            }

            grid.RowDefinitions.Clear();
            GridLengthCollection _rows = args.NewValue as GridLengthCollection;
            if (_rows == null)
                return;
            foreach (var length in _rows)
            {
                grid.RowDefinitions.Add(new RowDefinition { Height = length });
            }
        }
    

        /// <summary>
        /// Column definitions
        /// </summary>
        public GridLengthCollection Columns
        {
            get { return (GridLengthCollection)GetValue(ColumnsProperty); }
            set { SetValue(ColumnsProperty, value); }
        }
        public static readonly DependencyProperty ColumnsProperty = DependencyProperty.Register("Columns", typeof(GridLengthCollection), typeof(SimpleGrid), new PropertyMetadata(null, OnColumnsChanged));

        private static void OnColumnsChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            //sender should be this class
            var grid = sender as SimpleGrid;

            //if not, ignore it
            if (null == grid)
            {
                return;
            }

            grid.ColumnDefinitions.Clear();
            GridLengthCollection _columns = args.NewValue as GridLengthCollection;
            if (_columns == null)
                return;
            foreach (var length in _columns)
            {
                grid.ColumnDefinitions.Add(new ColumnDefinition { Width = length });
            }
        }
    }
}
