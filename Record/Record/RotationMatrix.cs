﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Kinect;
using  System.Windows.Media.Media3D;

namespace WpfApplication1
{
    class RotationMatrix
    {
        
        private Matrix4 aMatrix;

        public RotationMatrix(Matrix4 rot_)
        {
            this.aMatrix = rot_;
        }


        public Vector4 ToEulerAngle(string order) 
        {


            if (order == "XYZ")
            {

                return XYZ(this.aMatrix);

            } 
            else  if(order=="YXZ")
            {
                return YXZ(this.aMatrix);
                
            }
            else if (order == "ZXY")
            {
                return ZXY(this.aMatrix);

            }
            else if (order == "ZYX")
            {
                return ZYX(this.aMatrix);

            }
            else
            {
                throw (new Exception("Invalid order " +order));
            }




        }//ToEulerAngle



        private  Vector4 XYZ(Matrix4 M)
        {
             double thetaX;
             double thetaY;
             double thetaZ;
            Vector4 v=new Vector4();
            thetaY = Math.Asin(M.M13);
            if (thetaY < (Math.PI / 2))
            {
                if (thetaY > (-Math.PI / 2))
                {
                    thetaX = Math.Atan2(-M.M23, M.M33);
                    thetaZ = Math.Atan2(-M.M12, M.M11);
                }
                else
                {
                    //warning('Not unique solution.');
                    thetaX = -Math.Atan2(M.M21, M.M22);
                    thetaZ = 0;
                }
            }
            else{
                //warning('Not unique solution.');
                thetaX = Math.Atan2(M.M21, M.M22);
                thetaZ = 0;
             }
            
            v.X = (float)thetaX;
            v.Y = (float)thetaY;
            v.Z = (float)thetaZ;
            return v;

         }


        private Vector4 YXZ(Matrix4 M)
        {
         double thetaX;
         double thetaY;
         double thetaZ;
         Vector4 v = new Vector4();


         thetaX = Math.Asin(-M.M23);
         if (thetaX < (Math.PI / 2))
         {
             if (thetaX > (-Math.PI / 2))
             {
                 thetaY = Math.Atan2(M.M13, M.M33);
                 thetaZ = Math.Atan2(M.M21, M.M22);

             }
             else
             {
                 //warning('Not unique solution.');
                 thetaY = -Math.Atan2(-M.M12, M.M11);
                 thetaZ = 0;
             }
         }
         else
         {
             //warning('Not unique solution.');
             thetaY = Math.Atan2(-M.M12, M.M11);
             thetaZ = 0;
         }

         v.X = (float)thetaX;
         v.Y = (float)thetaY;
         v.Z = (float)thetaZ;
         return v;

     }





        private Vector4 ZXY(Matrix4 M)
        {
            double thetaX;
            double thetaY;
            double thetaZ;
            Vector4 v = new Vector4();


            thetaX = Math.Asin(-M.M32);
            if (thetaX < (Math.PI / 2))
            {
                if (thetaX > (-Math.PI / 2))
                {
                    thetaZ = Math.Atan2(-M.M12, M.M22);
                    thetaY = Math.Atan2(-M.M31, M.M33);

                }
                else
                {
                    //warning('Not unique solution.');
                    thetaZ = -Math.Atan2(M.M13, M.M11);
                    thetaY = 0;
                }
            }
            else
            {
                //warning('Not unique solution.');
                thetaZ = Math.Atan2(M.M13, M.M11);
                thetaY = 0;
            }

            v.X = (float)thetaX;
            v.Y = (float)thetaY;
            v.Z = (float)thetaZ;
            return v;

        }


        private Vector4 ZYX(Matrix4 M)
        {
            double thetaX;
            double thetaY;
            double thetaZ;
            Vector4 v = new Vector4();


            thetaY = Math.Asin(-M.M31);
            if (thetaY < (Math.PI / 2))
            {
                if (thetaY > (-Math.PI / 2))
                {
                    thetaZ = Math.Atan2(M.M21, M.M11);
                    thetaX = Math.Atan2(M.M32, M.M33);

                }
                else
                {
                    //warning('Not unique solution.');
                    thetaZ = -Math.Atan2(-M.M12, M.M13);
                    thetaX = 0;
                }
            }
            else
            {
                //warning('Not unique solution.');
                thetaZ = Math.Atan2(-M.M12, M.M13);
                thetaX = 0;
            }

            v.X = (float)thetaX;
            v.Y = (float)thetaY;
            v.Z = (float)thetaZ;
            return v;

        }


        public Matrix4 Multiply(Matrix4 m)
        {
            Matrix4 result=new Matrix4();
            result.M11=dot_product(m, 1, 1);
            result.M12 = dot_product(m, 1, 2);
            result.M13 = dot_product(m, 1, 3);
            result.M14 = dot_product(m, 1, 4);

            result.M21 = dot_product(m, 2, 1);
            result.M22 = dot_product(m, 2, 2);
            result.M23 = dot_product(m, 2, 3);
            result.M24 = dot_product(m, 2, 4);

            result.M31 = dot_product(m, 3, 1);
            result.M32 = dot_product(m, 3, 2);
            result.M33 = dot_product(m, 3, 3);
            result.M34 = dot_product(m, 3, 4);

            result.M41 = dot_product(m, 4, 1);
            result.M42 = dot_product(m, 4, 2);
            result.M43 = dot_product(m, 4, 3);
            result.M44 = dot_product(m, 4, 4);

            return result;
            
            
        }


        private float dot_product(Matrix4 m, int row, int col)
        {
              float[] a=new float[4];
              float[] b=new float[4];
            
           
            if (row == 1)
                 a[0] = this.aMatrix.M11; a[1] = this.aMatrix.M12; a[2] = this.aMatrix.M13; a[3] = this.aMatrix.M14;
            if (row == 2)
                a[0] = this.aMatrix.M21; a[1] = this.aMatrix.M22; a[2] = this.aMatrix.M23; a[3] = this.aMatrix.M24;
            if (row == 3)
                a[0] = this.aMatrix.M31; a[1] = this.aMatrix.M32; a[2] = this.aMatrix.M33; a[3] = this.aMatrix.M34;
            if (row == 4)
                a[0] = this.aMatrix.M41; a[1] = this.aMatrix.M42; a[2] = this.aMatrix.M43; a[3] = this.aMatrix.M44;

            if (col == 1)
                b[0] = m.M11; b[1] = m.M21; b[2] = m.M31; b[3] =m.M41;
            if (col == 2)
                b[0] = m.M12; b[1] = m.M22; b[2] = m.M32; b[3] = m.M42;
            if (col == 3)
                b[0] = m.M13; b[1] = m.M23; b[2] = m.M33; b[3] = m.M43;
            if (col == 4)
                b[0] = m.M14; b[1] = m.M24; b[2] = m.M34; b[3] = m.M44;


            return a[0] * b[0] + a[1] * b[1] + a[2] * b[2] + a[3] * b[3];

            
            
                    
           
            
          }

        /*public Matrix3D  AnglesToMatrix(float phi , float theta, psy)
        {System.Windows.Media.Media3D.Matrix3D aaa;
            return ;
        }
         * */



    }//RotationMatrix
}//WpfApp
