HipCenter:HipCenter
HipCenter:Spine
Spine:ShoulderCenter
ShoulderCenter:Head
ShoulderCenter:ShoulderLeft
ShoulderLeft:ElbowLeft
ElbowLeft:WristLeft
WristLeft:HandLeft
ShoulderCenter:ShoulderRight
ShoulderRight:ElbowRight
ElbowRight:WristRight
WristRight:HandRight
HipCenter:HipLeft
HipLeft:KneeLeft
KneeLeft:AnkleLeft
AnkleLeft:FootLeft
HipCenter:HipRight
HipRight:KneeRight
KneeRight:AnkleRight
AnkleRight:FootRight


The format is according to StartJoint:EndJoint
Here the End joint might be the child joint
and StartJoint might be the parent joint
For example, the orientation of lower legs  with respect to upper legs are stored at ankles so the left knee joint angles can be obtained by

boneOrientations[JointType.AnkleLeft].HierarchicalRotation



