﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using Brush = System.Windows.Media.Brush;
using Brushes = System.Windows.Media.Brushes;
using Color = System.Windows.Media.Color;
using Pen = System.Windows.Media.Pen;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Microsoft.Kinect;
using Microsoft.Win32;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.Util;
using System.Drawing;

namespace Kinect2Record
{
   
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        #region Kinect Body variables

        /// <summary>
        /// Thickness of drawn joint lines
        /// </summary>
        private const double JointThickness = 3;

        /// <summary>
        /// Thickness of clip edge rectangles
        /// </summary>
        private const double ClipBoundsThickness = 10;

        /// <summary>
        /// Brush used for drawing joints that are currently tracked
        /// </summary>
        private readonly Brush trackedJointBrush = new SolidColorBrush(Color.FromArgb(255, 68, 192, 68));

        /// <summary>
        /// Brush used for drawing joints that are currently inferred
        /// </summary>        
        private readonly Brush inferredJointBrush = Brushes.Yellow;

        /// <summary>
        /// Pen used for drawing bones that are currently tracked
        /// </summary>
        private readonly Pen trackedBonePen = new Pen(Brushes.Green, 6);

        /// <summary>
        /// Pen used for drawing bones that are currently inferred
        /// </summary>        
        private readonly Pen inferredBonePen = new Pen(Brushes.Gray, 1);

        /// <summary>
        /// Drawing group for body rendering output
        /// </summary>
        private DrawingGroup drawingGroup;

        /// <summary>
        /// Drawing image that we will display
        /// </summary>
        private DrawingImage imageSource;

        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        private KinectSensor kinectSensor = null;

        /// <summary>
        /// Coordinate mapper to map one type of point to another
        /// </summary>
        private CoordinateMapper coordinateMapper = null;

        /// <summary>
        /// Reader for body frames
        /// </summary>
        private MultiSourceFrameReader reader = null;

        /// <summary>
        /// Array for the bodies
        /// </summary>
        private Body[] bodies = null;

        /// <summary>
        /// Width of display (depth space)
        /// </summary>
        private int displayWidth;

        /// <summary>
        /// Height of display (depth space)
        /// </summary>
        private int displayHeight;

        /// <summary>
        /// The time of the first frame received
        /// </summary>
        private long startTime = 0;

        /// <summary>
        /// Current status text to display
        /// </summary>
        private string statusText = null;

        /// <summary>
        /// Next time to update FPS/frame time status
        /// </summary>
        private DateTime nextStatusUpdate = DateTime.MinValue;

        /// <summary>
        /// Number of frames since last FPS/frame time status
        /// </summary>
        private uint framesSinceUpdate = 0;

        /// <summary>
        /// Timer for FPS calculation
        /// </summary>
        private Stopwatch stopwatch = null;

        #endregion

        #region Colour Stream Variables

        private WriteableBitmap colorBitmap = null;

        /// <summary>
        /// Size of the RGB pixel in the bitmap
        /// </summary>
        private readonly int bytesPerPixel = (PixelFormats.Bgr32.BitsPerPixel + 7) / 8;

        private byte[] rgbColorPixels = null;



        #endregion

        #region FilePath variables
        private string filePath = null;
        private ICommand fileBrowseCommand;
        #endregion

        #region Recording Variables

        private const int MAXNUMFRAMES = 18000; //Equivalent to 10mins
        /// <summary>
        /// Variable that holds the data that is to be written to disk
        /// </summary>
        private BodyRecordList recordedBodyFrames = null;
        private Boolean isRecord = false;
        private const string joint20 = "20 Joints";
        private const string joint25 = "25 Joints";
        private VideoWriter videoFileWriter;

        private enum JointSet {None, JointSet20, JointSet25 };

        private JointSet jointSet = JointSet.None;
        #endregion

        public MainWindow()
        {

            // create a stopwatch for FPS calculation
            this.stopwatch = new Stopwatch();
            
            // for Alpha, one sensor is supported
            this.kinectSensor = KinectSensor.Default;

            if (this.kinectSensor != null)
            {
                // get the coordinate mapper
                this.coordinateMapper = this.kinectSensor.CoordinateMapper;

                // open the sensor
                this.kinectSensor.Open();

                // get the depth (display) extents
                FrameDescription frameDescription = this.kinectSensor.DepthFrameSource.FrameDescription;
                FrameDescription colorFrameDescription = this.kinectSensor.ColorFrameSource.FrameDescription;

                this.displayWidth = frameDescription.Width;
                this.displayHeight = frameDescription.Width;

                this.bodies = new Body[this.kinectSensor.BodyFrameSource.BodyCount];

                // open the reader for the body frames
                this.reader = this.kinectSensor.OpenMultiSourceFrameReader(FrameSourceTypes.Body| FrameSourceTypes.Color);
                this.rgbColorPixels = new byte[colorFrameDescription.Width * colorFrameDescription.Height * this.bytesPerPixel];
                this.colorBitmap = new WriteableBitmap(colorFrameDescription.Width, colorFrameDescription.Height, 96.0, 96.0, PixelFormats.Bgr32, null);


                // set the status text
                //this.StatusText = Properties.Resources.InitializingStatusTextFormat;
            }
            else
            {
                // on failure, set the status text
                //this.StatusText = Properties.Resources.NoSensorStatusText;
            }

            // Create the drawing group we'll use for drawing
            this.drawingGroup = new DrawingGroup();

            // Create an image source that we can use in our image control
            this.imageSource = new DrawingImage(this.drawingGroup);

            this.DataContext = this;
            this.InitializeComponent();
            this.MouseDown += MainWindow_MouseDown;
            this.colorImg.Source = this.colorBitmap;
            recordedBodyFrames = new BodyRecordList(MAXNUMFRAMES);
                               
        }

        void MainWindow_MouseDown(object sender, MouseButtonEventArgs e)
        {            
            this.DragMove();       
        }

        /// <summary>
        /// Execute start up tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.reader != null)
            {
                this.reader.MultiSourceFrameArrived += reader_MultiSourceFrameArrived;
            }
                       
        }

        void reader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            MultiSourceFrameReference multiReference = e.FrameReference;

            if (this.startTime == 0)
            {
                this.startTime = multiReference.AcquireFrame().BodyFrameReference.RelativeTime;
            }

            try
            {
                MultiSourceFrame multiFrame = multiReference.AcquireFrame();

                if (multiFrame != null)
                {
                    // BodyFrame is IDisposable
                    using (multiFrame)
                    {

                        ColorFrame colorFrame = multiFrame.ColorFrameReference.AcquireFrame();
                        BodyFrame bodyFrame = multiFrame.BodyFrameReference.AcquireFrame();                        

                        if (bodyFrame != null)
                        {
                            using (bodyFrame)
                            {
                                #region FPS
                                this.framesSinceUpdate++;

                                // update status unless last message is sticky for a while
                                if (DateTime.Now >= this.nextStatusUpdate)
                                {
                                    // calcuate fps based on last frame received
                                    double fps = 0.0;

                                    if (this.stopwatch.IsRunning)
                                    {
                                        this.stopwatch.Stop();
                                        fps = this.framesSinceUpdate / this.stopwatch.Elapsed.TotalSeconds;
                                        this.stopwatch.Reset();
                                    }

                                    this.nextStatusUpdate = DateTime.Now + TimeSpan.FromSeconds(1);
                                    if (isRecord)
                                    {
                                        this.statusBar.Content = string.Format(Properties.Resources.RecordStatusTextFormat, recordedBodyFrames.getCount());
                                    }
                                    else
                                    {
                                        this.statusBar.Content = string.Format(Properties.Resources.StandardStatusTextFormat, fps, multiFrame.BodyFrameReference.RelativeTime - this.startTime);
                                    }
                                    //this.statusBar.Content = this.StatusText;
                                }

                                if (!this.stopwatch.IsRunning)
                                {
                                    this.framesSinceUpdate = 0;
                                    this.stopwatch.Start();
                                }
                                #endregion                                

                                using (DrawingContext dc = this.drawingGroup.Open())
                                {
                                    // Draw a transparent background to set the render size
                                    dc.DrawRectangle(System.Windows.Media.Brushes.Black, null, new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));

                                    // The first time GetAndRefreshBodyData is called, Kinect will allocate each Body in the array.
                                    // As long as those body objects are not disposed and not set to null in the array,
                                    // those body objects will be re-used.
                                    bodyFrame.GetAndRefreshBodyData(this.bodies);

                                    foreach (Body body in this.bodies)
                                    {
                                        if (body.IsTracked)
                                        {
                                            this.DrawClippedEdges(body, dc);

                                            IReadOnlyDictionary<JointType, Joint> joints = body.Joints;
                                            if (isRecord)
                                            {
                                                recordedBodyFrames.addRecord(new BodyRecord(body.Joints, body.JointOrientations));
                                            }
                                            // convert the joint points to depth (display) space
                                            Dictionary<JointType, System.Windows.Point> jointPoints = new Dictionary<JointType,  System.Windows.Point>();
                                            foreach (JointType jointType in joints.Keys)
                                            {
                                                DepthSpacePoint depthSpacePoint = this.coordinateMapper.MapCameraPointToDepthSpace(joints[jointType].Position);
                                                jointPoints[jointType] = new System.Windows.Point(depthSpacePoint.X, depthSpacePoint.Y);
                                            }

                                            this.DrawBody(joints, jointPoints, dc);

                                        }
                                    }

                                    // prevent drawing outside of our render area
                                    this.drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, this.displayWidth, this.displayHeight));
                                }
                            }
                        }

                        // Convert Color frame to bitmap
                        if (colorFrame != null)
                        {
                            using (colorFrame)
                            {
                                if (colorFrame.RawColorImageFormat == ColorImageFormat.Bgra)
                                {
                                    colorFrame.CopyRawFrameDataToArray(this.rgbColorPixels);
                                }
                                else
                                {
                                    colorFrame.CopyConvertedFrameDataToArray(this.rgbColorPixels, ColorImageFormat.Bgra);
                                }

                                this.colorBitmap.WritePixels(
                                    new Int32Rect(0, 0, this.colorBitmap.PixelWidth, this.colorBitmap.PixelHeight),
                                    this.rgbColorPixels,
                                    this.colorBitmap.PixelWidth * sizeof(int),
                                    0);

                                if (isRecord)
                                {
                                    Bitmap bmp;
                                    using (MemoryStream outStream = new MemoryStream())
                                    {
                                        BitmapEncoder enc = new BmpBitmapEncoder();
                                        enc.Frames.Add(BitmapFrame.Create((BitmapSource)this.colorBitmap));
                                        enc.Save(outStream);
                                        bmp = new System.Drawing.Bitmap(outStream);
                                    }

                                    Image<Bgr, Byte> openCVImg = new Image<Bgr, byte>(bmp);
                                    videoFileWriter.WriteFrame(openCVImg);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                // ignore if the frame is no longer available
            }
        }

        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (this.reader != null)
            {
                // BodyFrameReder is IDisposable
                this.reader.Dispose();
                this.reader = null;
            }

            // Body is IDisposable
            if (this.bodies != null)
            {
                foreach (Body body in this.bodies)
                {
                    if (body != null)
                    {
                        body.Dispose();
                    }
                }
            }

            if (this.kinectSensor != null)
            {
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }

        }

        private void CloseBtnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        private void JointSet_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton li = (sender as RadioButton);
            jointSet = String.Equals(li.Content.ToString(), joint20) ? JointSet.JointSet20 : JointSet.JointSet25;

        }

        /// <summary>
        /// Gets the bitmap to display
        /// </summary>
        public ImageSource ImageSource
        {
            get
            {
                return this.imageSource;
            }
        }

        /// <summary>
        /// Gets or sets the current status text to display
        /// </summary>
        public string StatusText
        {
            get
            {
                return this.statusText;
            }

            set
            {
                if (this.statusText != value)
                {
                    this.statusText = value;

                    // notify any bound elements that the text has changed
                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("StatusText"));
                    }

                    //OnPropertyChanged("StatusText");
                }
            }
        }

        #region Drawing Methods
        /// <summary>
        /// Draws a body
        /// </summary>
        /// <param name="joints">joints to draw</param>
        /// <param name="jointPoints">translated positions of joints to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawBody(IReadOnlyDictionary<JointType, Joint> joints, IDictionary<JointType, System.Windows.Point> jointPoints, DrawingContext drawingContext)
        {
            // Draw the bones

            // Torso
            this.DrawBone(joints, jointPoints, JointType.Head, JointType.Neck, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.Neck, JointType.SpineShoulder, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineShoulder, JointType.SpineMid, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineMid, JointType.SpineBase, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineShoulder, JointType.ShoulderRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineShoulder, JointType.ShoulderLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineBase, JointType.HipRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.SpineBase, JointType.HipLeft, drawingContext);

            // Right Arm    
            this.DrawBone(joints, jointPoints, JointType.ShoulderRight, JointType.ElbowRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.ElbowRight, JointType.WristRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristRight, JointType.HandRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.HandRight, JointType.HandTipRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristRight, JointType.ThumbRight, drawingContext);

            // Left Arm
            this.DrawBone(joints, jointPoints, JointType.ShoulderLeft, JointType.ElbowLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.ElbowLeft, JointType.WristLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristLeft, JointType.HandLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.HandLeft, JointType.HandTipLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.WristLeft, JointType.ThumbLeft, drawingContext);

            // Right Leg
            this.DrawBone(joints, jointPoints, JointType.HipRight, JointType.KneeRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.KneeRight, JointType.AnkleRight, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.AnkleRight, JointType.FootRight, drawingContext);

            // Left Leg
            this.DrawBone(joints, jointPoints, JointType.HipLeft, JointType.KneeLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.KneeLeft, JointType.AnkleLeft, drawingContext);
            this.DrawBone(joints, jointPoints, JointType.AnkleLeft, JointType.FootLeft, drawingContext);

            // Draw the joints
            foreach (JointType jointType in joints.Keys)
            {
                Brush drawBrush = null;

                TrackingState trackingState = joints[jointType].TrackingState;

                if (trackingState == TrackingState.Tracked)
                {
                    drawBrush = this.trackedJointBrush;
                }
                else if (trackingState == TrackingState.Inferred)
                {
                    drawBrush = this.inferredJointBrush;
                }

                if (drawBrush != null)
                {
                    drawingContext.DrawEllipse(drawBrush, null, jointPoints[jointType], JointThickness, JointThickness);
                }
            }
        }

        /// <summary>
        /// Draws one bone of a body (joint to joint)
        /// </summary>
        /// <param name="joints">joints to draw</param>
        /// <param name="jointPoints">translated positions of joints to draw</param>
        /// <param name="jointType0">first joint of bone to draw</param>
        /// <param name="jointType1">second joint of bone to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawBone(IReadOnlyDictionary<JointType, Joint> joints, IDictionary<JointType, System.Windows.Point> jointPoints, JointType jointType0, JointType jointType1, DrawingContext drawingContext)
        {
            Joint joint0 = joints[jointType0];
            Joint joint1 = joints[jointType1];

            // If we can't find either of these joints, exit
            if (joint0.TrackingState == TrackingState.NotTracked ||
                joint1.TrackingState == TrackingState.NotTracked)
            {
                return;
            }

            // Don't draw if both points are inferred
            if (joint0.TrackingState == TrackingState.Inferred &&
                joint1.TrackingState == TrackingState.Inferred)
            {
                return;
            }

            // We assume all drawn bones are inferred unless BOTH joints are tracked
            Pen drawPen = this.inferredBonePen;
            if ((joint0.TrackingState == TrackingState.Tracked) && (joint1.TrackingState == TrackingState.Tracked))
            {
                drawPen = this.trackedBonePen;
            }

            drawingContext.DrawLine(drawPen, jointPoints[jointType0], jointPoints[jointType1]);
        }

        /// <summary>
        /// Draws indicators to show which edges are clipping body data
        /// </summary>
        /// <param name="body">body to draw clipping information for</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawClippedEdges(Body body, DrawingContext drawingContext)
        {
            FrameEdges clippedEdges = body.ClippedEdges;

            if (clippedEdges.HasFlag(FrameEdges.Bottom))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, this.displayHeight - ClipBoundsThickness, this.displayWidth, ClipBoundsThickness));
            }

            if (clippedEdges.HasFlag(FrameEdges.Top))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, this.displayWidth, ClipBoundsThickness));
            }

            if (clippedEdges.HasFlag(FrameEdges.Left))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(0, 0, ClipBoundsThickness, this.displayHeight));
            }

            if (clippedEdges.HasFlag(FrameEdges.Right))
            {
                drawingContext.DrawRectangle(
                    Brushes.Red,
                    null,
                    new Rect(this.displayWidth - ClipBoundsThickness, 0, ClipBoundsThickness, this.displayHeight));
            }
        }

        #endregion
        #region FilePath Bindings and Validation
        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises the <see cref="E:PropertyChanged"> event.
        /// </see></summary>
        /// <param name="propertyName">The name of the property that changed.
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Gets and sets the file path.
        /// </summary>
        public string FilePath
        {
            get { return filePath; }
            set
            {
                if (!string.Equals(value, filePath, StringComparison.InvariantCultureIgnoreCase))
                {
                    filePath = value;
                    OnPropertyChanged("FilePath");
                }
            }
        }

        private void OpenFileBrowseDialog(object context)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            var retVal = dlg.ShowDialog();
            if (retVal.HasValue && retVal.Value)
            {
                FilePath = dlg.FileName;
            }
        }


        /// <summary>
        /// Gets the command used to browse for a file.
        /// </summary>
        public ICommand FileBrowseCommand
        {
            get
            {
                if (fileBrowseCommand == null)
                    fileBrowseCommand = new RelayCommand(OpenFileBrowseDialog);
                return fileBrowseCommand;
            }
        }
        #endregion



        private void start_Click(object sender, RoutedEventArgs e)
        {
            isRecord = true;
            videoFileWriter = new VideoWriter("C:\\kinectdata\\test.mp4",30, 1280, 720,true);
            //videoFileWriter.Open("C:\\kinectdata\\test.mp4", 1280, 720, 30, VideoCodec.MPEG4);
        }

        private void stop_Click(object sender, RoutedEventArgs e)
        {
            isRecord = false;
            // RECORD the recordedBodyFrames variable with Binary Reader
            recordedBodyFrames = new BodyRecordList(MAXNUMFRAMES);
            videoFileWriter = null;
        }


        class BodyRecordList
        {

            private List<BodyRecord> bodyrecordList = null;

            public BodyRecordList(int capacity)
            {
                bodyrecordList = new List<BodyRecord>(capacity);
            }

            public int getCount()
            {
                return bodyrecordList.Count();
            }

            public void addRecord(BodyRecord record)
            {
                bodyrecordList.Add(record);
            }

        }



        
        internal class BodyRecord
        {
            private IReadOnlyDictionary<JointType, Joint> jointRecord = null;
            private IReadOnlyDictionary<JointType, JointOrientation> jointOrientRecord = null;

            public BodyRecord(IReadOnlyDictionary<JointType, Joint> jointRecord, IReadOnlyDictionary<JointType, JointOrientation> jointOrientRecord)
            {
                this.jointRecord = jointRecord;
                this.jointOrientRecord = jointOrientRecord;
            }

        }
        

    }


    
    
    public class FilePathValidationRule : ValidationRule
    {

        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            if (value != null && value.GetType() != typeof(string))
                return new ValidationResult(false, "Input value was of the wrong type, expected a string");

            var filePath = value as string;

            if (string.IsNullOrWhiteSpace(filePath))
                return new ValidationResult(false, "The file path cannot be empty or whitespace.");

            if (!filePath.Contains(".dta"))
                return new ValidationResult(false, "The file name must have the extension .dta.");

            //check the path:
            if (Path.GetInvalidPathChars().Any(x => filePath.Contains(x)))
                return new ValidationResult(false, string.Format("The characters {0} are not permitted in a file path.", GetPrinatbleInvalidChars(Path.GetInvalidPathChars())));

            //check the filename (if one can be isolated out):
            string fileName = Path.GetFileName(filePath);
            if (Path.GetInvalidFileNameChars().Any(x => fileName.Contains(x)))
                return new ValidationResult(false, string.Format("The characters {0} are not permitted in a file name.", GetPrinatbleInvalidChars(Path.GetInvalidFileNameChars())));

            return new ValidationResult(true, null);
        }

        private string GetPrinatbleInvalidChars(char[] chars)
        {
            string invalidChars = string.Join("", chars.Where(x => !Char.IsWhiteSpace(x)));
            return invalidChars;
        }
    }

    public class RelayCommand : ICommand 
    { 
        #region Fields 
        readonly Action<object> _execute; 
        readonly Predicate<object> _canExecute; 
        
        #endregion // Fields 
        #region Constructors 
        public RelayCommand(Action<object> execute) : this(execute, null) 
        { 
        }
 
        public RelayCommand(Action<object> execute, Predicate<object> canExecute) 
        { 
            if (execute == null) throw new ArgumentNullException("execute"); 
            _execute = execute; 
            _canExecute = canExecute;
        } 
        #endregion // Constructors 
        #region ICommand Members 
        [DebuggerStepThrough] 
        public bool CanExecute(object parameter) { return _canExecute == null ? true : _canExecute(parameter); } 
        public event EventHandler CanExecuteChanged 
        { 
            add { CommandManager.RequerySuggested += value; } 
            remove { CommandManager.RequerySuggested -= value; } 
        } 
        public void Execute(object parameter) { _execute(parameter); } 
        #endregion // ICommand Members 
    }
}
