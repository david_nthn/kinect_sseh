﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Kinect;
using System.IO;
using System.Windows.Threading;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        IRTimer irTimer = new IRTimer();
        const int skeletonCount = 6;
        Skeleton[] allSkeletons = new Skeleton[skeletonCount];
        int cnt = 0;
        int sts = 0;
        System.IO.BinaryWriter bWriter = null;
        private KinectSensor sensor1;
        JointCollection jp;
        double pi = Math.PI;
        
        StreamWriter boneOrientationWriter;

        /// <summary>
        /// Bitmap that will hold color information
        /// </summary>
        private WriteableBitmap colorBitmap;

        /// <summary>
        /// Intermediate storage for the color data received from the camera
        /// </summary>
        private byte[] colorPixels;

      

        //Itermediate storage for the time tick for for checking setting file every 2 seconds
        long time_ticks=DateTime.Now.Ticks;
        bool display = false;

        string projectDir = "";

        double[] orientation_holder = new double[60];

        SkeletonRecords records=new SkeletonRecords();//to hold skeletons in memory
        
        

        public MainWindow()
        {
            InitializeComponent();
            SetWindowPosition();
            if (FindSensor1() != true)
            {
                MessageBox.Show("No Kinect Sensors Found");
            }
            else
            {
                this.irTimer.TimerTask += irTimer_TimerTask;
                //this.irTimer.Start();
            }


        }

        /// <summary>
        /// Sets the window position to the top right corner of the "Record" window
        /// </summary>
        private void SetWindowPosition()
        {
             
            string[] args=Environment.GetCommandLineArgs();
            double left=0;
            double top=0;
            if(args.Length>1)
            {
                try
                {
                    left = double.Parse(args[1]);
                    top = double.Parse(args[2]);
                    this.projectDir = args[3];
                    this.WindowStartupLocation = WindowStartupLocation.Manual;
                    this.Left = left;
                    this.Top = top;
                    textBox1.Text = "Correct" + left + ": " + top + "("+args[1] + ": " + args[2]+")"; 
                }
                catch (Exception e) { textBox1.Text =   "Incorrect"+args[1] + ": " + args[2]; }

                this.Title = "Working on " + projectDir;
                 

            }

             
            
            
        }


        private bool FindSensor1()
        {
            KinectSensor tmp = null;
            bool any = false;
            foreach (var potentialSensor in KinectSensor.KinectSensors)
                if (potentialSensor.Status == KinectStatus.Connected)
                {
                    tmp = potentialSensor;
                    any = true;
                }
            if (any == true) 
                {
                    this.sensor1 = tmp;
                    this.sensor1.SkeletonStream.Enable();
                    this.sensor1.SkeletonFrameReady += new EventHandler<SkeletonFrameReadyEventArgs>(sensor1_SkeletonFrameReady);
                    InitializeImageReceiver(); 
                    try
                    {
                        this.sensor1.Start();
                        sensor1.ElevationAngle = 0;

                    }
                    catch
                    {
                        MessageBox.Show("Cannot Start Sensor 1");
                        return false;
                    }

                    return true;
                }
            return false;
        }

        private void sensor1_SkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
        {
            canvas1.Children.Clear();
            Rectangle rect = new Rectangle { Stroke = Brushes.Black, StrokeThickness = 1 };
            Canvas.SetLeft(rect, 0);
            Canvas.SetTop(rect, 0);
            rect.Width = 400;
            rect.Height = 400;
            canvas1.Children.Add(rect);
            Line l1 = new Line { Stroke = Brushes.DarkGray, StrokeThickness = 1 };
            l1.X1 = 200;
            l1.X2 = 200;
            l1.Y1 = 0;
            l1.Y2 = 400;
            canvas1.Children.Add(l1);
            Line l2 = new Line { Stroke = Brushes.DarkGray, StrokeThickness = 1 };
            l2.Y1 = 200;
            l2.Y2 = 200;
            l2.X1 = 0;
            l2.X2 = 400;
            canvas1.Children.Add(l2);

            if (File.Exists(System.IO.Path.Combine(this.projectDir, "config.dta")))
            {
                if (sts < 10) StartFilming(); //startfilming increases sts by 1 and open file for saving if sts==10.  
            }
            else
            {
                if (sts >= 10) StopFilming(); //no config data (this sets sts to 0)
            }

            Skeleton first = GetFirstSkeleton(e);
            double timeStamp = (double)DateTime.Now.Ticks;

            if (first == null) return;
             
             


            jp = first.Joints;

            drawSeg(JointType.ShoulderCenter, JointType.HipCenter);
            drawSeg(JointType.ElbowLeft, JointType.WristLeft);
            drawSeg(JointType.ShoulderCenter, JointType.ShoulderLeft);
            drawSeg(JointType.ElbowLeft, JointType.ShoulderLeft);
            drawSeg(JointType.ElbowRight, JointType.WristRight);
            drawSeg(JointType.ShoulderCenter, JointType.ShoulderRight);
            drawSeg(JointType.ElbowRight, JointType.ShoulderRight);

            drawSeg(JointType.KneeLeft, JointType.AnkleLeft);
            drawSeg(JointType.HipCenter, JointType.HipLeft);
            drawSeg(JointType.KneeLeft, JointType.HipLeft);
            drawSeg(JointType.KneeRight, JointType.AnkleRight);
            drawSeg(JointType.HipCenter, JointType.HipRight);
            drawSeg(JointType.KneeRight, JointType.HipRight);

            drawHand(JointType.HandLeft, 0);
            drawHand(JointType.HandRight, 0);
            drawHand(JointType.FootLeft, 0);
            drawHand(JointType.FootRight, 0);
            drawHand(JointType.Head, 1);


            if (sts >= 10)
            {
                SaveSkeleton(first);
                //this.records.AddRecord(this.SkeletonToRecord(first, timeStamp)) ;
            }
           
        } 



        private void drawSeg(JointType a, JointType b)
        {
            Line l = new Line { StrokeThickness = 2 };
            if ((jp[a].TrackingState == JointTrackingState.Tracked) && (jp[b].TrackingState == JointTrackingState.Tracked))
                l.Stroke = Brushes.Blue;
            else
                l.Stroke = Brushes.Red;
            l.X1 = ScreenX(jp[a].Position.X);
            l.X2 = ScreenX(jp[b].Position.X);
            l.Y1 = ScreenY(jp[a].Position.Y);
            l.Y2 = ScreenY(jp[b].Position.Y);

            canvas1.Children.Add(l);
        }

        private void drawHand(JointType a, int sz)
        {
            Line l1 = new Line { StrokeThickness = 2 };
            Line l2 = new Line { StrokeThickness = 2 };

            if (jp[a].TrackingState == JointTrackingState.Tracked)
                l1.Stroke = l2.Stroke = Brushes.Blue;
            else
                l1.Stroke = l2.Stroke = Brushes.Red;
            l1.X1 = ScreenX(jp[a].Position.X) - 2;
            l1.X2 = l1.X1 + 4;
            l1.Y1 = ScreenY(jp[a].Position.Y) - 2;
            l1.Y2 = l1.Y1 + 4;
            l2.X1 = l1.X1;
            l2.X2 = l1.X2;
            l2.Y1 = l1.Y2;
            l2.Y2 = l1.Y1;


            canvas1.Children.Add(l1);
            canvas1.Children.Add(l2);
        }



        private int ScreenX(double x)
        {
            return 200 + (int)(x * 100.0);
        }

        private int ScreenY(double y)
        {
            return 200 - (int)(y * 100.0);
        }

        
        void SaveSkeleton(Skeleton first)
        {
 //           ++cnt;
 //           if (cnt >= 3)
 //           {
                double timestamp = (double)DateTime.Now.Ticks;
                string orientation_string = this.GetBoneOrientationsString(first, timestamp);

                JointCollection jp = first.Joints;
                double d;
                char ch;
                foreach (Joint i in jp)
                {
                    d = (double)i.Position.X;
                    bWriter.Write(d);
                    d = (double)i.Position.Y;
                    bWriter.Write(d);
                    d = (double)i.Position.Z;
                    bWriter.Write(d);
                    if (i.TrackingState == JointTrackingState.Tracked) ch = '1';
                    else ch = '0';
                    bWriter.Write(ch);
                }

                //write relative orientations too
                for (int xx = 0; xx < 60; xx++)
                    bWriter.Write(this.orientation_holder[xx]);

  
               
                bWriter.Write(timestamp);
  
            //save orientatios of the bone segments
            //@todo save in memory first and dump into file when stop button is clicked
            if (this.boneOrientationWriter != null )
            {
                this.boneOrientationWriter.WriteLine(orientation_string);
             }
        }
         
        
        /// <summary>
        /// Converts the Microsoft.Kinect.Skeleton to WpfApplication1.SkeletonRecord
        /// </summary>
        /// <param name="aSkel"> Skeleton object</param>
        /// <returns> SkeletonRecord object</returns>
        public SkeletonRecord SkeletonToRecord(Skeleton aSkel,double timeStamp)
        {
             
            JointCollection jp = aSkel.Joints;
           
            char ch;
            double[] joints = new double[60];
            char[] val = new char[20];

            int index = 0;
            foreach (Joint i in jp)
            {
                joints[index*3] = (double)i.Position.X;

                joints[index*3+1] = (double)i.Position.Y;

                joints[index*3+2] = (double)i.Position.Z;
                 
                if (i.TrackingState == JointTrackingState.Tracked) ch = '1'; else ch = '0';
                val[index] = ch;
                index++;
            }

            index = 0;
            foreach (BoneOrientation orientation in aSkel.BoneOrientations)
            {
                Matrix4 rot = orientation.HierarchicalRotation.Matrix;
                Vector4 ang = new RotationMatrix(rot).ToEulerAngle("YXZ");
                

                this.orientation_holder[index*3] = ang.X;
                this.orientation_holder[index*3+1] = ang.Y;
                this.orientation_holder[index*3+2] = ang.Z;
                index++;
            }

            return new SkeletonRecord(orientation_holder, joints, val, timeStamp);



        }

        Skeleton GetFirstSkeleton(SkeletonFrameReadyEventArgs e)
        {
            using (SkeletonFrame skeletonFrameData = e.OpenSkeletonFrame())
            {
                if (skeletonFrameData == null)
                {
                    return null;
                }


                skeletonFrameData.CopySkeletonDataTo(allSkeletons);

                //get the first tracked skeleton
                Skeleton first = (from s in allSkeletons
                                  where s.TrackingState == SkeletonTrackingState.Tracked
                                  select s).FirstOrDefault();

                return first;

            }
        }

        
        private void StopKinect(KinectSensor sensor)
        {

            if (sensor != null)
            {
                if (sensor.IsRunning)
                {
                    //stop sensor 
                    sensor.Stop();

                    //stop audio if not null
                    if (sensor.AudioSource != null)
                    {
                        sensor.AudioSource.Stop();
                    }


                }
            }
        }

        
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            
            StopKinect(this.sensor1);

        }

        
   

        private void StopFilming()
        {
                textBox1.Text = "Not Recording";
                sts = 0; 
                bWriter.Close();
                this.boneOrientationWriter.Close();
            }
         
        private void StartFilming()
        {
            if (++sts == 10)
            {
                string[] s = new String[3];
                using (StreamReader b = new StreamReader(System.IO.Path.Combine(this.projectDir, "config.dta")))
                {
                    s[0] = b.ReadLine();
                    s[1] = b.ReadLine();
                    s[2] = b.ReadLine();
                }
                textBox1.Text = "Recording: " + s[0] + ", Sample " + s[2];
                //file to save data from second kinect
                string bstring = System.IO.Path.Combine(this.projectDir, "b" + s[1].PadLeft(3, '0') + s[2].PadLeft(5, '0') + ".dta");
                bWriter = new BinaryWriter(File.Open(bstring, FileMode.Create));
                this.textBox1.Text = bstring;

                //open a file to write orientations

                 string boneOrientString = bstring.Substring(0, bstring.Length - 4) + ".orientation";
                 boneOrientationWriter = new StreamWriter(boneOrientString);
                

            }
            
        }

 
 /**
 *  Returns  1 x 60 vector of orientions of bone segments of the skeleton  in terms of Euler Angles. Each elements are separated by space.
 *  Each agles are computer in YXZ order
 *  See Kiect documentatios for the 
 */

        private string GetBoneOrientationsString(Skeleton skeleton,double timestamp)
        {
            //long ticks_milliseconds = (long)Math.Round((double)(1.0 * DateTime.Now.Ticks / TimeSpan.TicksPerSecond) * 1000, 0);
            string angles = "" + timestamp + " ";
            



            if (skeleton != null)
            {
                int i = 0;
                foreach (BoneOrientation orientation in skeleton.BoneOrientations)
                {

                    Matrix4 rot = orientation.HierarchicalRotation.Matrix;
                    Vector4 ang = new RotationMatrix(rot).ToEulerAngle("YXZ");
                    angles = angles + string.Format("{0:f2}", ang.X * 180 / pi) + " ";
                    angles = angles + string.Format("{0:f2}", ang.Y * 180 / pi) + " ";
                    angles = angles + string.Format("{0:f2}", ang.Z * 180 / pi) + " ";

                    this.orientation_holder[i++] = ang.X;
                    this.orientation_holder[i++] = ang.Y;
                    this.orientation_holder[i++] = ang.Z;

                }
            }


            return angles;

            //Note rotation axes convention  used by vicon and kinect are different
            // Note    System.Windows.Media.Media3D.Matrix3D for matrix operations
            //TimeSpan.TicksPerSecond

        }


        //Initializes all the resources required to display image

        private void InitializeImageReceiver()
        {
            

            // Turn on the color stream to receive color frames
            this.sensor1.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);

            // Allocate space to put the pixels we'll receive
            this.colorPixels = new byte[this.sensor1.ColorStream.FramePixelDataLength];

            // This is the bitmap we'll display on-screen
            this.colorBitmap =  new WriteableBitmap(this.sensor1.ColorStream.FrameWidth, this.sensor1.ColorStream.FrameHeight, 96.0, 96.0, PixelFormats.Pbgra32, null);

           
            // Set the image we display to point to the bitmap where we'll put the image data
            this.Image.Source = this.colorBitmap;

            // Add an event handler to be called whenever there is new color frame data
            this.sensor1.ColorFrameReady += this.SensorColorFrameReady;

        }



        /// <summary>
        /// Event handler for Kinect sensor's ColorFrameReady event
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void SensorColorFrameReady(object sender, ColorImageFrameReadyEventArgs e)
        {
            //check if the image should be displayed every two seconds 
           
            if ((DateTime.Now.Ticks - this.time_ticks) / TimeSpan.TicksPerSecond > 2)
            {
                this.time_ticks = DateTime.Now.Ticks;
                checkDisplaySetting();
                

            }

            if (this.display)
            {
                using (ColorImageFrame colorFrame = e.OpenColorImageFrame())
                {



                    if (colorFrame != null)
                    {
                        // Copy the pixel data from the image to a temporary array
                        colorFrame.CopyPixelDataTo(this.colorPixels);

                        //set the alpha values to 255
                        for (int i = 0; i < this.colorPixels.Length; )
                        {
                            i = i + 4;
                            colorPixels[i - 1] = 0xff;
                        }


                        // Write the pixel data into our bitmap 

                        this.colorBitmap.WritePixels(
                            new Int32Rect(0, 0, this.colorBitmap.PixelWidth, this.colorBitmap.PixelHeight),
                            this.colorPixels,
                            this.colorBitmap.PixelWidth * sizeof(int),
                            0);
                        //  this.colorBitmap=BitmapFactory.ConvertToPbgra32Format(this.colorBitmap);




                    }

                }
            }
 
                
        }
         

        // Check the  setting file. Returns true if  the main application is displaying the image
        private bool checkDisplaySetting()
        {

            string setting_file = System.IO.Path.Combine(this.projectDir, "display_setting.dta");
            if (File.Exists(setting_file))
            {
                using (FileStream fs = new FileStream(setting_file, FileMode.Open))
                {
                    using (StreamReader r = new StreamReader(fs))
                    {
                        if ("DisPlayImage".Equals(r.ReadLine()))
                        {
                            this.Image.Source = this.colorBitmap;
                            this.display = true;
                        }
                        else
                        {
                            this.Image.Source = null;
                            this.display = false;
                        }


                    }
                }
            }
            return display;

        }


        private bool currentIRState = true;

        //turn the IR state in each interval
        void irTimer_TimerTask(object sender, EventArgs e)
        {
            currentIRState = !currentIRState;
            this.sensor1.ForceInfraredEmitterOff = currentIRState;
        }


    }

    public delegate void AutoTimerUp(object sender, EventArgs e);

    public class IRTimer
    {
        private DispatcherTimer timer;
        public event AutoTimerUp TimerTask;


        public IRTimer()
        {
            timer = new DispatcherTimer();
        }
        public void Start()
        {
            timer.Interval = TimeSpan.FromMilliseconds(1);
            timer.Tick += timer_Task;
            timer.Start();
            
            

            
        }

        public void Stop()
        {
            timer.Stop();
        }
        private void timer_Task(object sender, EventArgs e)
        {

            TimerTask(sender, EventArgs.Empty);
        }


    }
}
