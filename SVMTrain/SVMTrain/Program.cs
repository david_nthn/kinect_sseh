﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows.Data;
using System.Xml;

using Microsoft.Kinect;
using System.Data;
using Accord.MachineLearning.VectorMachines;
using Accord.MachineLearning.VectorMachines.Learning;
using Accord.Statistics.Kernels;

namespace SVMTrain
{
    
    public enum DimensionType { X = 0, Y = 1, Z = 2 } 

    class Program
    {
        // Default Dimension size for Positional Standard Deviation
        const int INPUT_DIMENSION = 60;

        static void Main(string[] args)
        {
            string dirpath = @"C:\kinectdata\";
            string machineFileName = string.Empty;

            if (args.Length == 2)
            {
                dirpath = dirpath + args[0] + @"\";
                machineFileName = args[1] + ".dat";
            }
            else
            {
                Console.WriteLine("Requires name of Session Folder and saved Machine");
                return;
            }

            Console.WriteLine("Press ENTER to start.");
            do {
                while (!Console.KeyAvailable) { }               
            } while (Console.ReadKey(true).Key != ConsoleKey.Enter);

            // Create DataTable from dirpath.
            // Columns are: 
            // FMS(string)|Description(string)|FilePath(string)|StartFrame(int)|StopFrame(int)|Rating(int)
            DataTable fmsTable = readFMScsv(dirpath);
            Console.WriteLine("FMS present in Session folder\n");
            // Write to console the unique FMS present
            viewUniqueFMS(fmsTable);
            Console.WriteLine("Enter FMS to train SVM on.");

            string fms2train = Console.ReadLine(); 
            fms2train = "'"+ fms2train +"'" ;
            
            DataRow[] fmsRows = fmsTable.Select("FMS =" + fms2train);
 
            if (fmsRows.Length > 0)
            {
                // Columns are:
                // FMS(string)|Description(string)|FilePath(string)|Features(Dictionary<JointType,double[]>)|StartFrame(int)|Rating(int)
                DataTable fmsFeatureTable = getFMSFeatureTable(fmsRows,fms2train);

                KernelSupportVectorMachine machine = GetKernelVectorMachine(fmsFeatureTable, INPUT_DIMENSION);

                string fileName = machineFileName;
                System.IO.Stream fileStream = new FileStream(fileName, FileMode.Create);
                machine.Save(fileStream);
                fileStream.Close();
                
            }
            else
            {
                Console.WriteLine("Invalid FMS.");
            }

            Console.WriteLine("Press ESC to stop.");
            do
            {
                while (!Console.KeyAvailable) { }
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }

        #region fms.csv Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dirpath"></param>
        /// <returns></returns>
        public static DataTable readFMScsv(string dirpath)
        {

            string fmscsvpath = dirpath + @"fms.csv";
            string rowline;
            DataTable fmstable = new DataTable("FMS Data");

            fmstable.Columns.Add("FMS", typeof(string));
            fmstable.Columns.Add("Description",typeof(string));
            fmstable.Columns.Add("FilePath", typeof(string));           
            fmstable.Columns.Add("StartFrame", typeof(int));
            fmstable.Columns.Add("StopFrame", typeof(int));
            fmstable.Columns.Add("Rating", typeof(int));

            fmstable.PrimaryKey = new DataColumn[] {fmstable.Columns["FilePath"], fmstable.Columns["StartFrame"]};

            StreamReader fReader = new StreamReader(fmscsvpath);
            string fpath= String.Empty; 
            string description = String.Empty;

            while ((rowline = fReader.ReadLine()) != null)
            {
                string[] strarray = rowline.Split(',');

                //First column is either "Description", " FMS", or name of fms.
                if (strarray[0].Equals("Description:"))
                {
                    fpath = strarray[3];
                    description = strarray[1];
                }
                else
                {
                    if (!strarray[0].Equals("FMS"))
                    {
                        fmstable.Rows.Add(strarray[0], description, fpath, Int32.Parse(strarray[1]), Int32.Parse(strarray[2]), Int32.Parse(strarray[3]));
                    }
                }
            }

            fReader.Close();  

            return fmstable;
        }
        #endregion

        #region Feature Extraction Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fmsRows"></param>
        /// <returns></returns>
        public static DataTable getFMSFeatureTable(DataRow[] fmsRows, string FMS)
        {            
            BinaryFrameRead bFrameRead = new BinaryFrameRead();
            DataTable fmsFeaturesTable = new DataTable("FMS Features");

            fmsFeaturesTable.Columns.Add("FMS", typeof(string));
            fmsFeaturesTable.Columns.Add("Description", typeof(string));
            fmsFeaturesTable.Columns.Add("FilePath", typeof(string));
            fmsFeaturesTable.Columns.Add("Features", typeof(Dictionary<JointType, double[]>));
            fmsFeaturesTable.Columns.Add("StartFrame", typeof(int));
            fmsFeaturesTable.Columns.Add("Rating", typeof(int));

            fmsFeaturesTable.PrimaryKey = new DataColumn[] { fmsFeaturesTable.Columns["FilePath"], fmsFeaturesTable.Columns["StartFrame"] };

            foreach (DataRow fmsRow in fmsRows)
            {
                string fpath = (string)fmsRow["FilePath"];
                int StartFrame = (int)fmsRow["StartFrame"];
                int StopFrame  = (int)fmsRow["StopFrame"];

                List<Frame> recording = new List<Frame>();
                BinaryReader bFile = new BinaryReader(File.Open(fpath, FileMode.Open));

                for (int i = StartFrame; i <= StopFrame; i++)
                {
                    Frame recordingFrame = new Frame();
                    recordingFrame.features = new Dictionary<JointType, double[]>();
                    recordingFrame.timestamp = (long)bFrameRead.GetTime(bFile, (long)i);

                    // File Frame object with position of each jointType
                    foreach (JointType jntTyp in Enum.GetValues(typeof(JointType)))
                    {
                        double[] point = bFrameRead.GetPoint(bFile, (long)i, jntTyp);
                        recordingFrame.features.Add(jntTyp, point);
                    }
                    recording.Add(recordingFrame);
                }

                Dictionary<JointType, double[]> features = extractFMSFeature(recording);
                fmsFeaturesTable.Rows.Add(FMS, (string)fmsRow["Description"], fpath, features, StartFrame, (int)fmsRow["Rating"]);
                bFile.Close();

            }

            return fmsFeaturesTable;
        }

        /// <summary>
        /// This method calculates the standard deviation of the X,Y,Z dimension 
        /// of the 20 joints (60 features).
        /// </summary>
        /// <param name="recording"></param>
        /// <returns></returns>
        public static Dictionary<JointType, double[]> extractFMSFeature(List<Frame> recording)
        {
            Dictionary<JointType, double[]> features = new Dictionary<JointType, double[]>();

            foreach (JointType jntType in Enum.GetValues(typeof(JointType)))
            {  
                // Triplet for Standard Deviation of each Dimension
                double[] jntFeatures = new double[3];

                // IEnumerabl of double[] consisting of X,Y,Z position of jntType
                IEnumerable<double[]> trajectory = recording.Select(x => x.features[jntType]);

                foreach (DimensionType dimType in Enum.GetValues(typeof(DimensionType)))
                {
                    IEnumerable<double> dimarray = trajectory.Select(x => x[(int)dimType]);
                    // Average
                    double mu = dimarray.Average();
                    // Sum of Squared Differences
                    double SSD = dimarray.Select(x => (x - mu) * (x - mu)).Sum();
                    // Standard Deviation
                    double sigma = Math.Sqrt(SSD / dimarray.Count());

                    jntFeatures[(int)dimType] = sigma;                      
                }
                features.Add(jntType, jntFeatures);
            }
            return features;
        }
        #endregion

        #region Support Vector Machine Methods

        public static KernelSupportVectorMachine GetKernelVectorMachine(DataTable features, int inputDimension)
        {
            DataRow[] featuresRows = features.Select(null, null, DataViewRowState.CurrentRows);
            // First need NxINPUT_DIMENSION double[][] inputs and Nx1 double[] outputs where N is the number of datapoints

            double[][] inputs = new double[featuresRows.Length][];
            double[] outputs =  new double[featuresRows.Length];

            int rown = 0;
            foreach (DataRow row in featuresRows)
            {
                outputs[rown] = (int)row["Rating"];
                inputs[rown]  = new double[inputDimension];
                Dictionary<JointType, double[]> featuresDictionary = (Dictionary<JointType, double[]>)row["Features"];

                foreach (JointType jntType in Enum.GetValues(typeof(JointType)))
                {
                    int col = 3 * (int)jntType;
                    double[] jntTriplet = featuresDictionary[jntType];
                    inputs[rown][col]   = jntTriplet[0];
                    inputs[rown][col+1] = jntTriplet[1];
                    inputs[rown][col+2] = jntTriplet[2];
                }
                rown++;
            }

            // Create Kernel Support Vector Machine with a Gaussian Kernelx
            KernelSupportVectorMachine machine = new KernelSupportVectorMachine(new Gaussian(), inputs: inputDimension);

            // Create the sequential minimal optimization teacher
            SequentialMinimalOptimizationRegression learn = new SequentialMinimalOptimizationRegression(machine, inputs, outputs);

            Console.WriteLine("\nRunning Machine");

            // Run the learning algorithm
            double error = learn.Run();

            //Write error to console
            Console.WriteLine("\nError: {0}", error);

            return machine;
        }

        #endregion

        #region Main Helper Methods
        /// <summary>
        ///
        /// </summary>
        /// <param name="table"></param>
        static void viewUniqueFMS(DataTable table)
        {
            DataRow[] currentRows = table.Select(null, null, DataViewRowState.CurrentRows);
            if (currentRows.Length < 1)
                Console.WriteLine("No Current Rows Found");
            else
            {             
                DataTable unique = table.DefaultView.ToTable(true, "FMS");

                foreach (DataRow row in unique.Rows)
                {
                    Console.Write(" |{0,15}|", row[0]);
                    Console.WriteLine("\t");
                }                               
            }
        }

        /// <summary>
        /// Debugger method. 
        /// </summary>
        /// <param name="table"></param>
        static void viewDataTable(DataTable table)
        {
            DataRow[] currentRows = table.Select(null, null, DataViewRowState.CurrentRows);

            if (currentRows.Length < 1)
                Console.WriteLine("No Current Rows Found");
            else
            {
                foreach (DataColumn column in table.Columns)
                {
                    Console.Write(" {0}", column.ColumnName);
                }

                Console.WriteLine("\t");

                foreach (DataRow row in currentRows)
                {
                    foreach (DataColumn column in table.Columns)
                    {
                        Console.Write(" |{0,10}|", row[column]);
                    }
                    Console.WriteLine("\t");
                }
            }

        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    public class BinaryFrameRead
    {
        const int RECSIZE = 988;

        public int GetFrameCount(BinaryReader bFile)
        {
            return (int)((long)bFile.BaseStream.Length / RECSIZE);
        }


        public double[] GetPoint(BinaryReader bFile, long frame, JointType joint)
        {
            double[] point = new double[3];
            bFile.BaseStream.Seek(frame * RECSIZE + (long)joint * 25, SeekOrigin.Begin);
            point[0] = bFile.ReadDouble();
            point[1] = bFile.ReadDouble();
            point[2] = bFile.ReadDouble();
            return point;
        }

        public double GetTime(BinaryReader bFile, long frame)
        {
            bFile.BaseStream.Seek(frame * RECSIZE + RECSIZE - 8, SeekOrigin.Begin);
            return bFile.ReadDouble();
        }       
    }

    /// <summary>
    /// 
    /// </summary>
    class Frame
    {
        public long timestamp { get; set; }
        public Dictionary<JointType, double[]> features {get; set; }
    }
}
