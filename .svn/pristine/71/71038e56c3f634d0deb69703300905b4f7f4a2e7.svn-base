﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
//using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Timers;


namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int epoch = 5; // 5 second epochs //
        int nume; // number of epochs in data - calculated at run-time //
        int recsize = 508;
        long maxlim = 10000L;
        string mainstring = "";
        string secondstring = "";
        double[, ,] dta = new double[2, 20, 3];
        char[,] tst = new char[2, 20];
        bool autoon = false;
        double[] obs = new double[] { 0.0, 0.0, 0.0 }; /* observation point */
        const double conv = 57.29577951308232; /* radians to degrees */
        const double pi = 3.141592653589793;
        double depth = 4.25;
        const double maxx = 400.0;
        const double maxy = 400.0;
        const int points = 20;
        double[] fcs = new double[] { 0.0, 0.0, 4.25 }; /* focus point - center of screen */
        double[,] mp = new double[points, 2];
        int[,] keyj = new int[,] { { 4, 5, 6 }, { 8, 9, 10 }, { 12, 13, 14 }, { 16, 17, 18 }, { 1, 0, 13 }, { 1, 0, 17 }, { 1, 2, 5 }, { 1, 2, 9 }, { 12, 4, 5 }, { 2, 0, 20 }, { 0, 2, 10 } };
        BinaryReader bFile;
        int answers = 0;
        string res = "";
        string rulestring = "";
        long[] ans = new long[100];
        string[] cr = new string[15];
        bool debug = false;
        string[] opd;
        string header;
        int[] mcnt;
        int beginning = 0;

        string projectDir;

        public MainWindow()
        {
            InitializeComponent();
            populateSessionComboBox();
           // UpdateList();
            //OpenFile();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            var i = Int32.Parse(this.textBox1.Text);
            if (i > 0) i--;
            textBox1.Text = i.ToString();
            this.DrawIt();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var i = Int32.Parse(this.textBox1.Text);
            if (++i >= maxlim) i--;
            textBox1.Text = i.ToString();
            this.DrawIt();

        }

        private void UpdateList()
        {
            string masterstring =  GetFullFile("recordings.txt");
            listBox1.Items.Clear();
            if (File.Exists(masterstring))
            {
                using (StreamReader b = new StreamReader(masterstring))
                {
                    string s;
                    int index = 0;
                     
                    
                     
                    while ((s = b.ReadLine()) != null)
                    {
                        if (!s.Contains("Calibrat") && s.Length>0 )
                        {
                            ListBoxItem li = new ListBoxItem();
                            li.Content = s;
                            li.Tag = ""+index;
                            listBox1.Items.Add(li);
                        }

                        index++;
                    }
                }
            }
        }

        private double Jat(double v1, double v2)
        {
            if (v2 == 0.0)
            {
                if (v1 == 0.0) return 0.0;
                if (v1 >= 0.0) return pi / 2.0;
                else return pi * 1.5;
            }
            double ang = Math.Atan(v1 / v2);
            if (v2 < 0.0) ang += pi;
            return ang;
        }

        private void DrawIt()
        {
            double l, tim;
            string nw = "";

            using (BinaryReader b = new BinaryReader(File.Open(mainstring, FileMode.Open)))
            {
                maxlim = b.BaseStream.Length / recsize;
                label1.Content = "/ " + maxlim.ToString();
                b.BaseStream.Seek((long)Int32.Parse(textBox1.Text) * (long)recsize, SeekOrigin.Begin);
                for (int j = 0; j < 20; j++)
                {
                    dta[0, j, 0] = b.ReadDouble();
                    dta[0, j, 1] = b.ReadDouble();
                    dta[0, j, 2] = b.ReadDouble();
                    tst[0, j] = b.ReadChar();
                }
                tim = b.ReadDouble();
            }

            depth = Math.Sqrt(Math.Pow(fcs[0] - obs[0], 2) + Math.Pow(fcs[1] - obs[1], 2) + Math.Pow(fcs[2] - obs[2], 2));

            double zx = Math.Pow(obs[0] - fcs[0], 2) + Math.Pow(obs[2] - fcs[2], 2);
            double th1 = Jat(fcs[1] - obs[1], Math.Sqrt(zx));
            double th2 = Jat(fcs[0] - obs[0], fcs[2] - obs[2]);
            double[] tmp = new double[3];
            for (int i = 0; i < points; i++)
            {   /* translate point by observer position */
                tmp[0] = dta[0, i, 0] - obs[0];
                tmp[1] = dta[0, i, 1] - obs[1];
                tmp[2] = dta[0, i, 2] - obs[2];
                /* rotate by negative of observer z-x orientation */
                l = tmp[0] * Math.Cos(th2) - tmp[2] * Math.Sin(th2);
                tmp[2] = tmp[0] * Math.Sin(th2) + tmp[2] * Math.Cos(th2);
                tmp[0] = l;
                /* rotate by negative of observer y-x orientation */
                l = tmp[1] * Math.Cos(th1) - tmp[2] * Math.Sin(th1);
                tmp[2] = tmp[1] * Math.Sin(th1) + tmp[2] * Math.Cos(th1);
                tmp[1] = l;
                if (tmp[2] > 0.0)
                {
                    mp[i, 0] = tmp[0] * depth / tmp[2];
                    mp[i, 1] = tmp[1] * depth / tmp[2];
                }
                else
                {
                    mp[i, 0] = 0;
                    mp[i, 1] = 0;
                }
            }

            drawSkel(0);
            using (BinaryReader b = new BinaryReader(File.Open(secondstring, FileMode.Open)))
            {
                maxlim = b.BaseStream.Length / recsize;
                label1.Content = "/ " + maxlim.ToString();
                b.BaseStream.Seek((long)Int32.Parse(textBox1.Text) * (long)recsize, SeekOrigin.Begin);
                for (int j = 0; j < 20; j++)
                {
                    dta[0, j, 0] = b.ReadDouble();
                    dta[0, j, 1] = b.ReadDouble();
                    dta[0, j, 2] = b.ReadDouble();
                    tst[0, j] = b.ReadChar();
                }
                l = b.ReadDouble();
            }
            for (int i = 0; i < points; i++)
            {   /* translate point by observer position */
                tmp[0] = dta[0, i, 0] - obs[0];
                tmp[1] = dta[0, i, 1] - obs[1];
                tmp[2] = dta[0, i, 2] - obs[2];
                /* rotate by negative of observer z-x orientation */
                l = tmp[0] * Math.Cos(th2) - tmp[2] * Math.Sin(th2);
                tmp[2] = tmp[0] * Math.Sin(th2) + tmp[2] * Math.Cos(th2);
                tmp[0] = l;
                /* rotate by negative of observer y-x orientation */
                l = tmp[1] * Math.Cos(th1) - tmp[2] * Math.Sin(th1);
                tmp[2] = tmp[1] * Math.Sin(th1) + tmp[2] * Math.Cos(th1);
                tmp[1] = l;
                if (tmp[2] > 0.0)
                {
                    mp[i, 0] = tmp[0] * depth / tmp[2];
                    mp[i, 1] = tmp[1] * depth / tmp[2];
                }
                else
                {
                    mp[i, 0] = 0;
                    mp[i, 1] = 0;
                }
            }

            drawSkel(1);
            for (int i = 0; i < 11; i++)
            {
                double[] ang1 = new double[] { dta[0, keyj[i, 0], 0], dta[0, keyj[i, 0], 1], dta[0, keyj[i, 0], 2] };
                double[] ang2 = new double[] { dta[0, keyj[i, 1], 0], dta[0, keyj[i, 1], 1], dta[0, keyj[i, 1], 2] };
                double[] ang3 = new double[] { 0.0000, 0.00000, 0.00000 };
                int kj = keyj[i, 2];
                if (kj < 20)
                {
                    ang3[0] = dta[0, kj, 0];
                    ang3[1] = dta[0, kj, 1];
                    ang3[2] = dta[0, kj, 2];
                }
                if (kj == 20)
                {
                    ang3[0] = dta[0, keyj[i, 0], 0];
                    ang3[1] = 10000;
                    ang3[2] = dta[0, keyj[i, 0], 2];
                }
                if (kj == 21)
                {
                    ang3[0] = dta[0, keyj[i, 0], 0];
                    ang3[1] = -100000;
                    ang3[2] = dta[0, keyj[i, 0], 2];
                }

                nw += "Angle of " + JName(keyj[i, 1]) + " (" + JName(keyj[i, 0]) + " to " + JName(keyj[i, 2]) + ") : " + JKAngle(ang2, ang1, ang3) + Environment.NewLine;
            }

            // DateTime dt = new DateTime((long)l);

            DateTime dt = new DateTime((long)tim);
            nw += "Time Frame: " + dt.ToShortDateString() + " : " + dt.ToLongTimeString() + Environment.NewLine;

            textBox3.Text = nw;
        }

        private string JName(int s)
        {
            switch (s)
            {
                case 0: return "hip centre";
                case 1: return "spine";
                case 2: return "shoulder centre";
                case 3: return "head";
                case 4: return "left shoulder";
                case 5: return "left elbow";
                case 6: return "left wrist";
                case 7: return "left hand";
                case 8: return "right shoulder";
                case 9: return "right elbow";
                case 10: return "right wrist";
                case 11: return "right hand";
                case 12: return "left hip";
                case 13: return "left knee";
                case 14: return "left ankle";
                case 15: return "left foot";
                case 16: return "right hip";
                case 17: return "right knee";
                case 18: return "right ankle";
                case 19: return "right foot";
                case 20: return "straight up";
                case 21: return "straight down";

            }
            return "ERROR!";
        }
        private double JKAngle(double[] j1, double[] j2, double[] j3)
        // p1x, double p1y, double p1z, double p2x, double p2y, double p2z, double p3x, double p3y, double p3z)
        {
            double[] v1 = new double[3];
            double[] v2 = new double[3];
            v1[0] = j2[0] - j1[0];
            v1[1] = j2[1] - j1[1];
            v1[2] = j2[2] - j1[2];
            v2[0] = j3[0] - j1[0];
            v2[1] = j3[1] - j1[1];
            v2[2] = j3[2] - j1[2];
            double dp = v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
            double av1 = v1[0] * v1[0] + v1[1] * v1[1] + v1[2] * v1[2];
            double av2 = v2[0] * v2[0] + v2[1] * v2[1] + v2[2] * v2[2];
            double cth = Math.Acos(dp / (Math.Sqrt(av1) * Math.Sqrt(av2)));
            cth *= conv;
            return cth;
        }

        private void drawSkel(int i)
        {
            Canvas c;
            if (i == 0) c = canvas1;
            else c = canvas2;

            c.Children.Clear();
            Rectangle rect = new Rectangle { Stroke = Brushes.Black, StrokeThickness = 1 };
            Canvas.SetLeft(rect, 0);
            Canvas.SetTop(rect, 0);
            rect.Width = 400;
            rect.Height = 400;
            c.Children.Add(rect);

            drawSeg(i, 4, 5);
            drawSeg(i, 5, 6);
            drawSeg(i, 8, 9);
            drawSeg(i, 9, 10);
            drawSeg(i, 4, 8);
            drawSeg(i, 0, 2);
            drawSeg(i, 12, 13);
            drawSeg(i, 13, 14);
            drawSeg(i, 16, 17);
            drawSeg(i, 17, 18);
            drawSeg(i, 0, 12);
            drawSeg(i, 0, 16);
            drawHand(i, 7);
            drawHand(i, 11);
            drawHand(i, 15);
            drawHand(i, 19);
            drawHand(i, 3);
        }

        private void drawSeg(int i, int a, int b)
        {
            Line l = new Line { StrokeThickness = 2 };
            char ch = tst[0, a];
            if (tst[0, b] < tst[0, a]) ch = tst[0, b];
            if (ch == '0')
                l.Stroke = Brushes.Red;
            if (ch == '1')
                l.Stroke = Brushes.Blue;
            if (ch == '2')
                l.Stroke = Brushes.Green;
            if (ch == '3')
                l.Stroke = Brushes.DodgerBlue;
            l.X1 = ScreenX(mp[a, 0]);
            l.X2 = ScreenX(mp[b, 0]);
            l.Y1 = ScreenY(mp[a, 1]);
            l.Y2 = ScreenY(mp[b, 1]);
            if (i == 0)
                canvas1.Children.Add(l);
            else
                canvas2.Children.Add(l);
        }

        private void drawHand(int i, int a)
        {
            Line l1 = new Line { StrokeThickness = 2 };
            Line l2 = new Line { StrokeThickness = 2 };

            if (tst[0, a] == '0')
                l1.Stroke = l2.Stroke = Brushes.Red;
            if (tst[0, a] == '1')
                l1.Stroke = l2.Stroke = Brushes.Blue;
            if (tst[0, a] == '2')
                l1.Stroke = l2.Stroke = Brushes.Green;
            if (tst[0, a] == '3')
                l1.Stroke = l2.Stroke = Brushes.DodgerBlue;

            l1.X1 = ScreenX(mp[a, 0]) - 2;
            l1.X2 = l1.X1 + 4;
            l1.Y1 = ScreenY(mp[a, 1]) - 2;
            l1.Y2 = l1.Y1 + 4;
            l2.X1 = l1.X1;
            l2.X2 = l1.X2;
            l2.Y1 = l1.Y2;
            l2.Y2 = l1.Y1;

            if (i == 0)
            {
                canvas1.Children.Add(l1);
                canvas1.Children.Add(l2);
            }
            else
            {
                canvas2.Children.Add(l1);
                canvas2.Children.Add(l2);

            }

        } 

        private int ScreenX(double x)
        {
            return 200 - (int)(x * 400.0 / depth);
        }

        private int ScreenY(double y)
        {
            return 200 - (int)(y * 400.0 / depth);
        }

        private void listBox1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            try
            {
                ListBoxItem li = (ListBoxItem)listBox1.SelectedItem;
                string selectedIndex=li.Tag+"";
                String nd = "000" + selectedIndex.PadLeft(5, '0') + ".dta";
                mainstring =   GetFullFile("a" + nd);
                secondstring = GetFullFile("c" + nd);
                if (File.Exists(mainstring) && File.Exists(secondstring))
                {
                    this.button3.IsEnabled = true;
                    this.button2.IsEnabled = true;
                    OpenFile();
                    textBox4.Text = "";

                }
                else
                {
                    this.canvas1.Children.Clear();
                    this.canvas2.Children.Clear();
                    this.button3.IsEnabled = false;
                    this.button2.IsEnabled = false;
                    textBox4.Text = "Data not found for the selection";
                }
                
                //               DumpFiles();
            }
            catch
            {
            }
        }

        private void DumpFiles()
        {
            double l;
            int fr = 0;

            if (!File.Exists(mainstring) || !File.Exists(secondstring)) return;
            StreamWriter t = new StreamWriter(File.Open(GetFullFile("out1.csv"), FileMode.Create));

            using (BinaryReader b = new BinaryReader(File.Open(mainstring, FileMode.Open)))
            {
                maxlim = b.BaseStream.Length / recsize;
                label1.Content = "/ " + maxlim.ToString();
                for (fr = 0; fr < maxlim; fr++)
                {
                    t.WriteLine("==================================================================");
                    t.WriteLine("Frame " + fr.ToString() + "...");
                    for (int j = 0; j < 20; j++)
                    {
                        dta[0, j, 0] = b.ReadDouble();
                        dta[0, j, 1] = b.ReadDouble();
                        dta[0, j, 2] = b.ReadDouble();
                        tst[0, j] = b.ReadChar();
                        t.WriteLine("[" + JShow(dta[0, j, 0]) + "," + JShow(dta[0, j, 1]) + "," + JShow(dta[0, j, 2]) + "][" + tst[0, j] + "]");
                    }
                    l = b.ReadDouble();
                    t.WriteLine("Time Stamp : " + l.ToString());
                }
            }
            t.Close();
            t = new StreamWriter(File.Open(GetFullFile("out2.csv"), FileMode.Create));

            using (BinaryReader b = new BinaryReader(File.Open(secondstring, FileMode.Open)))
            {
                maxlim = b.BaseStream.Length / recsize;
                label1.Content = "/ " + maxlim.ToString();
                for (fr = 0; fr < maxlim; fr++)
                {
                    t.WriteLine("==================================================================");
                    t.WriteLine("Frame " + fr.ToString() + "...");
                    for (int j = 0; j < 20; j++)
                    {
                        dta[0, j, 0] = b.ReadDouble();
                        dta[0, j, 1] = b.ReadDouble();
                        dta[0, j, 2] = b.ReadDouble();
                        tst[0, j] = b.ReadChar();
                        t.WriteLine("[" + JShow(dta[0, j, 0]) + "," + JShow(dta[0, j, 1]) + "," + JShow(dta[0, j, 2]) + "][" + tst[0, j] + "]");
                    }
                    l = b.ReadDouble();
                    t.WriteLine("Time Stamp : " + l.ToString());
                }
            }
            t.Close();
        }

        private string JShow(double d)
        {
            double d2 = Math.Round(d, 4);
            return d2.ToString();
        }

        private void OpenFile()
        {
            if (!File.Exists(mainstring) || !File.Exists(secondstring)) return;
            textBox1.Text = "0";
            using (BinaryReader b = new BinaryReader(File.Open(mainstring, FileMode.Open)))
            {
                b.BaseStream.Seek((long)25, SeekOrigin.Begin);
                for (int j = 0; j < 3; j++)
                {
                    fcs[j] = b.ReadDouble();
                    obs[j] = 0.0;
                }

            }
            DrawIt();
        }

        private void JScale(double depth2)
        {
            depth2 /= depth;
            obs[0] = fcs[0] + depth2 * (obs[0] - fcs[0]);
            obs[1] = fcs[1] + depth2 * (obs[1] - fcs[1]);
            obs[2] = fcs[2] + depth2 * (obs[2] - fcs[2]);

            DrawIt();

        }

        private void JMove(double ang1, double ang2)
        {
            double zx = Math.Pow(obs[0] - fcs[0], 2) + Math.Pow(obs[2] - fcs[2], 2);
            double theta1 = Jat(obs[1] - fcs[1], Math.Sqrt(zx));
            double theta2 = Jat(obs[2] - fcs[2], obs[0] - fcs[0]);

            theta1 += ang1 / conv;
            theta2 += ang2 / conv;
            obs[0] = depth * Math.Cos(theta1) * Math.Cos(theta2) + fcs[0];
            obs[1] = depth * Math.Sin(theta1) + fcs[1];
            obs[2] = depth * Math.Sin(theta2) * Math.Cos(theta1) + fcs[2];
            DrawIt();
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {
            /* Move Left */
            JMove(0.0, 5.0);

        }

        private void button5_Click(object sender, RoutedEventArgs e)
        {
            /* Move Right */
            JMove(0.0, -5.0);

        }

        private void button6_Click(object sender, RoutedEventArgs e)
        {
            /* Move Up */
            JMove(5.0, 0.0);

        }

        private void button7_Click(object sender, RoutedEventArgs e)
        {
            /* Move Down */
            JMove(-5.0, 0.0);

        }

        private void button8_Click(object sender, RoutedEventArgs e)
        {
            /* Zoom In */
            if (depth > 0.5)
                JScale(depth - 0.5);

        }

        private void button9_Click(object sender, RoutedEventArgs e)
        {
            /* Zoom Out */
            JScale(depth + 0.5);

        }

        private void button10_Click(object sender, RoutedEventArgs e)
        {   /* evaluate chosen recording */
            int rule = 0;
            res = "";
            bFile = new BinaryReader(File.Open(mainstring, FileMode.Open));
            maxlim = bFile.BaseStream.Length / recsize;

            header = "Date,Time";

            epoch = Int32.Parse(textBox5.Text);

            double d = GetTime(0);
            DateTime dt = new DateTime((long)d);
            int time = rawtime(dt.ToLongTimeString());
            time = (int)Math.Floor((decimal)time / epoch) * epoch;
            beginning = time;
            d = GetTime(maxlim - 1);
            DateTime dt2 = new DateTime((long)d);
            int time2 = rawtime(dt2.ToLongTimeString());
            time2 = (int)Math.Floor((decimal)time2 / epoch) * epoch;

            bFile.Close();

            nume = (time2 - time) / epoch;
            opd = new string[++nume];
            mcnt = new int[nume];
            for (int ji = 0; ji < nume; ji++)
            {
                opd[ji] = dt.ToShortDateString() + "," + stringtime(time);
                time += epoch;
            }

            //rules are same for all sessions
            using (StreamReader b = new StreamReader(@"C:\\kinectdata\rules\rules.txt"))
            {
                string s;
                while ((s = b.ReadLine()) != null)
                {
                    res += "Looking for " + s + "..." + Environment.NewLine;
                    for (int ji = 0; ji < nume; ji++) mcnt[ji] = 0;
                    LookFor(rule++);
                    header += "," + s;
                    for (int ji = 0; ji < nume; ji++) opd[ji] += "," + mcnt[ji].ToString();
                }
            }

            res += "---------------------------------------------------------" + Environment.NewLine;
            string res2 = header + Environment.NewLine;
            for (int ji = 0; ji < nume; ji++)
                res2 += opd[ji] + Environment.NewLine;
            textBox4.Text = res + res2;
           
            StreamWriter t = new StreamWriter(File.Open( GetFullFile("output.csv"), FileMode.Create));
            t.Write(res2);
            t.Close();
        }

        private void LookFor(int rule)
        {
            rulestring = "";

            //rules are same for all sessions
            using (StreamReader b = new StreamReader(@"C:\\kinectdata\rules\rule" + rule.ToString() + ".txt"))
            {
                string s;
                while ((s = b.ReadLine()) != null)
                {
                    if (rulestring.Length == 0) rulestring = s;
                    else
                    {
                        string[] prt = s.Split('~');
                        if (prt[0] == "0") rulestring += '&' + s;
                        else if (prt[0] == "1") rulestring += '|' + s;
                        else rulestring += '>' + s;
                    }
                }
            }

            bFile = new BinaryReader(File.Open(mainstring, FileMode.Open));
            maxlim = bFile.BaseStream.Length / recsize;
            for (long current = 0; current < maxlim; current++)
            {
                answers = 0;
                if (ParseRule(rulestring, current, maxlim) == true)
                {
                    res += "Found : from " + current.ToString() + " to " + ans[answers - 1].ToString() + Environment.NewLine;
                    DateTime found = new DateTime((long)GetTime(current));
                    int i = rawtime(found.ToLongTimeString());
                    i = (int)Math.Floor((decimal)(i - beginning) / epoch);
                    mcnt[i]++;
                    current = ans[answers - 1];

                }
            }

            bFile.Close();
        }

        private bool ParseRule(string r, long start, long finish)
        {

            int a = r.IndexOf('>');
            //            res += "[start:" + start.ToString() + "][fin:" + finish.ToString() + "][" + r + "]" + Environment.NewLine;
            if (a > 0)
            {   // process THEN operator
                if (ParseRule(r.Substring(0, a), start, finish))
                {
                    long l = ans[answers - 1];
                    for (long ll = l; (ll < l + 2) && (ll < finish); ll++)
                        if (ParseRule(r.Substring(a + 1), ll, finish)) return true;
                }
            }
            else
            {  // process AND operator
                a = r.IndexOf('&');
                if (a > 0)
                {
                    if (ParseRule(r.Substring(0, a), start, finish))
                    {
                        long asave = ans[answers - 1];
                        bool b = ParseRule(r.Substring(a + 1), start, (long)Math.Min(finish, ans[answers - 1] + 3));
                        if (b == true)
                            ans[answers - 1] = Math.Max(ans[answers - 1], asave);
                        return b;
                    }
                    else
                        return false;
                }
                else
                    return Possible(r, start, finish);
            }
            return false;
        }



        private double[] GetPoint(long frame, int joint)
        {
            double[] point = new double[3];
            bFile.BaseStream.Seek(frame * recsize + (long)joint * 25, SeekOrigin.Begin);
            point[0] = bFile.ReadDouble();
            point[1] = bFile.ReadDouble();
            point[2] = bFile.ReadDouble();
            return point;
        }

        private double[] GetExtraPoint(long frame, int joint, double[] t1, double[] t2)
        {
            double[] point = new double[3];
            if (joint < 20) return GetPoint(frame, joint);
            if (joint == 20)
            {
                point[0] = t1[0];
                point[1] = 10000;
                point[2] = t1[2];
                return point;
            }
            if (joint == 21)
            {
                point[0] = t1[0];
                point[1] = -100000;
                point[2] = -t1[2];
                return point;
            }
            if (joint == 22)
            {
                point[0] = t2[0];
                point[1] = t2[1];
                point[2] = t1[2];
                return point;
            }
            return null;
        }


        private double GetTime(long frame)
        {
            bFile.BaseStream.Seek(frame * recsize + 500, SeekOrigin.Begin);
            return bFile.ReadDouble();
        }

        private bool Possible(string r, long begin, long finish)
        {
            double[] tmp = new double[3];
            double[] tmp1 = new double[3];
            double[] tmp2 = new double[3];
            double[] tmp3 = new double[3];
            double stt = 0, fin = 0, chng = 0;
            cr = r.Split('~');
            int j = Int32.Parse(cr[2]), typ = Int32.Parse(cr[1]);
            long rec = begin;
            double start = GetTime(begin), tnow;
            tmp = GetPoint(rec, j);
            if (typ == 1)
            {
                tmp2 = GetPoint(rec, Int32.Parse(cr[3]));
                tmp3 = GetExtraPoint(rec, Int32.Parse(cr[4]), tmp, tmp2);
                stt = JKAngle(tmp, tmp2, tmp3);
            }
            else stt = 0;
            if (debug == true)
                res += "Posible [" + r + "][stt=" + stt.ToString() + "][begin=" + begin.ToString() + "][finish=" + finish.ToString() + "]" + Environment.NewLine;
            if (cr[7].Length > 0)
                if (stt < Double.Parse(cr[7])) return false;
            if (cr[8].Length > 0)
                if (stt > Double.Parse(cr[8])) return false;

            while (++rec < finish)
            {
                bool ok = true;
                tnow = GetTime(rec);
                if (cr[12].Length > 0)
                    if ((tnow - start) / 10000000.0 > Double.Parse(cr[12])) return false;
                tmp1 = GetPoint(rec, j);
                if (typ == 1)
                {
                    tmp2 = GetPoint(rec, Int32.Parse(cr[3]));
                    tmp3 = GetExtraPoint(rec, Int32.Parse(cr[4]), tmp1, tmp2);
                    fin = JKAngle(tmp1, tmp2, tmp3);
                }
                if (typ == 2)
                    fin = 100 * (tmp1[1] - tmp[1]);
                if (typ == 3)
                    fin = 100 * Math.Sqrt(Math.Pow(tmp1[0] - tmp[0], 2) + Math.Pow(tmp1[1] - tmp[1], 2) + Math.Pow(tmp1[2] - tmp[2], 2));
                chng = fin - stt;
                if (debug == true)
                    res += "    rec=" + rec.ToString() + " fin=" + fin.ToString() + " chng=" + chng.ToString() + Environment.NewLine;
                if (cr[9].Length > 0)
                    if (fin < Double.Parse(cr[9])) ok = false;
                if (cr[10].Length > 0)
                    if (fin > Double.Parse(cr[10])) ok = false;
                if (cr[5].Length > 0)
                    if (chng < Double.Parse(cr[5])) ok = false;
                if (cr[6].Length > 0)
                    if (chng > Double.Parse(cr[6])) ok = false;
                if (cr[11].Length > 0)
                    if ((tnow - start) / 10000000.0 < Double.Parse(cr[11])) ok = false;
                if (ok == true)
                {
                    ans[answers++] = rec;
                    if (debug == true)
                        res += "Found answer upto frame " + rec.ToString() + Environment.NewLine;
                    return true;
                }
            }
            return false;
        }

        private string stringtime(int i)
        {
            string t = "";
            if (i > 43200)
            {
                t = " PM";
                i -= 43200;
            }
            else
                t = " AM";
            int h = (int)Math.Floor((decimal)i / 3600);
            i -= h * 3600;
            if (h == 0) h = 12;
            int m = (int)Math.Floor((decimal)i / 60);
            i -= m * 60;
            t = h.ToString() + ":" + m.ToString().PadLeft(2, '0') + ':' + i.ToString().PadLeft(2, '0') + t;
            return t;

        }

        private int rawtime(string s)
        {
            string[] pt = s.Split(':');
            int ampm = 0;
            int h = Int32.Parse(pt[0]);
            if (h == 12) h = 0;
            if (pt[2].IndexOf("PM") > 0) ampm = 43200;
            pt[2] = pt[2].Substring(0, 2);
            return ampm + Int32.Parse(pt[2]) + 60 * Int32.Parse(pt[1]) + 3600 * h;
        }


        /***
         * Returns recorded sessions saved in directories under c:\kinectdata\
         * 
         */
        private string[] populateSessionComboBox()
        {
            string[] dirlist = Directory.GetDirectories(@"C:\kinectdata");
            List<string> aList = dirlist.ToList();
            aList.Remove("rules");
            string[] finallist = aList.ToArray();
            ComboBoxItem cbox_message = new ComboBoxItem();
            cbox_message.Content = "Select a session directory";
            cbox_message.Tag = "";
            
            this.sessions_combo.Items.Add(cbox_message);

            for (int i = 0; i < finallist.Length; i++)
            {
                ComboBoxItem cboxitem = new ComboBoxItem();

                 cboxitem.Content = finallist[i];
                 cboxitem.Tag = finallist[i];
                 this.sessions_combo.Items.Add(cboxitem);
                
            }

            return finallist;

        }

        private void sessions_combo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxItem ci=(ComboBoxItem)this.sessions_combo.SelectedValue;
            this.projectDir = (string)ci.Tag;
            UpdateList();
            //OpenFile();
        }

        private string GetFullFile(string aFile)
        {

            return System.IO.Path.Combine(this.projectDir, aFile);
            
        }
    }


    
}
